<?php

/**
 *
 */
class m_Carrera
{

    function mostrarTodos()
    {
        $cnn = $GLOBALS['m_Conexion']->Conectar();
        $resulTbl = mysqli_query($cnn, "CALL 	SP_carreras_mostrargeneral_CRUD()");
        return $resulTbl;
        mysqli_close($cnn);
    }

    function mostrarUno($id)
    {
        $cnn = $GLOBALS['m_Conexion']->Conectar();
        $resulTbl = mysqli_query($cnn, "CALL SP_carreras_mostrarid('" . $id . "')");
        return $resulTbl;
        mysqli_close($cnn);
    }

    function eliminarCarrera($id)
    {
        $cnn = $GLOBALS['m_Conexion']->Conectar();
        $resultado = mysqli_query($cnn, "CALL SP_carreras_eliminar('" . $id . "')");

        if ($resultado)
        {
            //return "exito al insertar" . $resultado;
            @$_SESSION['vsMsj'] = "<br>Eliminado con exito...";
        }
        else
        {
            //return "Error en al insertar<br>" . $resultado	;
            //$_SESSION['vsMsj'] = "<br>error<br>" . $mysqli->errno($resultado);
            @$_SESSION['vsMsj'] = "Falló la llamada: (" . $mysqli->errno . ") " . $mysqli->error;

        }
        return $_SESSION['vsMsj'];
        //return $resultado;
        mysqli_close($cnn);
    }

    function insertar($carrera, $cod)
    {
        $cnn = $GLOBALS['m_Conexion']->Conectar();

        $resultado = mysqli_query($cnn, "CALL SP_carreras_insertar('" . $carrera . "', '" . $cod . "')");
        //return $resultado;
        //$sql = "call SP_docentes_insertar('".$carne."','".$nombre."','".$apellido."','".$estado."','".$email."')";
        if ($resultado)
        {
            //return "exito al insertar" . $resultado;
            @$_SESSION['vsMsj'] = "<br>Insertado con exito...";
        }
        else
        {
            //return "Error en al insertar<br>" . $resultado	;
            //$_SESSION['vsMsj'] = "<br>error<br>" . $mysqli->errno($resultado);
            @$_SESSION['vsMsj'] = "Falló la llamada: (" . $mysqli->errno . ") " . $mysqli->error;

        }
        return $_SESSION['vsMsj'];
        mysqli_close($cnn);
    }

    function modificar($carrera, $id, $est, $cod)
    {
        $cnn = $GLOBALS['m_Conexion']->Conectar();

        $resultado = mysqli_query($cnn, "CALL SP_carreras_modificar('" . $carrera . "','" . $id . "','" . $est . "', '" . $cod . "')");

        if ($resultado)
        {
            //return "exito al insertar" . $resultado;
            @$_SESSION['vsMsj'] = "<br>Modificado con exito...";
        }
        else
        {
            //return "Error en al insertar<br>" . $resultado	;
            //$_SESSION['vsMsj'] = "<br>error<br>" . $mysqli->errno($resultado);
            @$_SESSION['vsMsj'] = "Falló la llamada: (" . $mysqli->errno . ") " . $mysqli->error;

        }
        return $_SESSION['vsMsj'];
        mysqli_close($cnn);
    }
}

?>
