<?php 

/**
 * 
 */
class m_Actividades
{
	function mostrarMaxTramite(){
		$cnn=$GLOBALS['m_Conexion']->Conectar(); 
		$resulTbl=mysqli_query($cnn, "CALL SP_tramite_mostrarMaximo()");
		return $resulTbl;
		mysqli_close($cnn);

	}

	function mostrarEvaluaciones(){
		$cnn=$GLOBALS['m_Conexion']->Conectar(); 
		$resulTbl=mysqli_query($cnn, "CALL 	SP_evaluaciones_mostrargeneral()");
		return $resulTbl;
		mysqli_close($cnn);
	}
	
	function mostrarGrupoCarrera(){
		$cnn=$GLOBALS['m_Conexion']->Conectar(); 
		$resulTbl=mysqli_query($cnn, "CALL 	SP_carrera_materia_agruparCarrera()");
		return $resulTbl;
		mysqli_close($cnn);
	}

	function mostrarMateriasAsoc(){
		$cnn=$GLOBALS['m_Conexion']->Conectar(); 
		$resulTbl=mysqli_query($cnn, "CALL SP_carrera_materia_soloMaterias()");
		return $resulTbl;
		mysqli_close($cnn);
	}


	function mostrarDocentes(){
		$cnn=$GLOBALS['m_Conexion']->Conectar(); 
		$resulTbl=mysqli_query($cnn, "CALL SP_docentes_consultargeneral()");
		return $resulTbl;
		mysqli_close($cnn);
	}


	

	function mostrarSecciones(){
		$cnn=$GLOBALS['m_Conexion']->Conectar(); 
		$resulTbl=mysqli_query($cnn, "CALL SP_secciones_mostrargeneral()");
		return $resulTbl;
		mysqli_close($cnn);
	}


	function mostrarCiclos(){
		$cnn=$GLOBALS['m_Conexion']->Conectar(); 
		$resulTbl=mysqli_query($cnn, "CALL 	SP_ciclos_mostrargeneral()");
		return $resulTbl;
		mysqli_close($cnn);
	}

	function mostrarAlumnos(){
		$cnn=$GLOBALS['m_Conexion']->Conectar(); 
		$resulTbl=mysqli_query($cnn, "CALL 	SP_alumnos_mostrargeneral()");
		return $resulTbl;
		mysqli_close($cnn);
	}

	function mostrarTramites(){
		$cnn=$GLOBALS['m_Conexion']->Conectar(); 
		$resulTbl=mysqli_query($cnn, "CALL 		SP_actividad_consultar()");
		return $resulTbl;
		mysqli_close($cnn);
	}

	function validoCarnet($carn){
		$cnn=$GLOBALS['m_Conexion']->Conectar(); 
		$resultado=mysqli_query($cnn, "CALL SP_vallida_carnet('".$carn."')");
		//var_dump($resultado);
		

		$registro=$resultado->fetch_array();
		if($registro['CONTEO'] > 0)
		{
			$r = 1;
		}
		else
		{
			$r = 0;
		}
		return $r;
		mysqli_close($cnn);
	}

	

	function mostrarDetTramites($id){
		$cnn=$GLOBALS['m_Conexion']->Conectar(); 
		$resulTbl=mysqli_query($cnn, "CALL 	SP_actividad_consultar_detalle('".$id."')");
		return $resulTbl;
		mysqli_close($cnn);
	}

	function mostrarUna($id){
		$cnn=$GLOBALS['m_Conexion']->Conectar(); 
		$resulTbl=mysqli_query($cnn, "CALL 	SP_actividad_consultaID('".$id."')");
		return $resulTbl;
		mysqli_close($cnn);
	}

	function mostrarUnaLinea($carnet,$id){
		$cnn=$GLOBALS['m_Conexion']->Conectar(); 
		$resulTbl=mysqli_query($cnn, "CALL 	SP_actividad_consulta_linea('".$carnet."','".$id."')");
		return $resulTbl;
		mysqli_close($cnn);
	}

	function mostrarCarMat(){
		$cnn=$GLOBALS['m_Conexion']->Conectar(); 
		$resulTbl=mysqli_query($cnn, "CALL 	SP_carrera_materia_mostrargeneral()");
		return $resulTbl;
		mysqli_close($cnn);
	}

	function validarAlumno($car,$tram){
		$cnn=$GLOBALS['m_Conexion']->Conectar(); 
		$resulTbl=mysqli_query($cnn, "CALL 	SP_actividad_validarAlumno('".$car."', '".$tram."')");

		if($resulTbl)
		{
			//return "exito al insertar" . $resultado;
			@$_SESSION['vsMsj'] = "<br>Alumno ya ingresado en la actividad. Seleccione otro alumno..." ;
		}
		else
		{
			//return "Error en al insertar<br>" . $resultado	;
			//$_SESSION['vsMsj'] = "<br>error<br>" . $mysqli->errno($resultado);
			@$_SESSION['vsMsj'] = "Falló la llamada: (" . $mysqli->errno . ") " . $mysqli->error;

		}
		return $resulTbl;
		mysqli_close($cnn);
	}
	



	function insertar($car,$mat,$sec,$eval,$doc, $conteo,$ciclo, $fec){
		$cnn=$GLOBALS['m_Conexion']->Conectar();



		
		$resultado=mysqli_query($cnn, "CALL SP_actividades_insertar('".$car."','".$mat."','".$sec."','".$eval."', '".$doc."','".$conteo."', '".$ciclo."', '".$fec."')");
		//return $resultado;
		//$sql = "call SP_docentes_insertar('".$carne."','".$nombre."','".$apellido."','".$estado."','".$email."')";
		if($resultado)
		{
			//return "exito al insertar" . $resultado;
			@$_SESSION['vsMsj'] = "<br>Insertado con exito..." ;
		}
		else
		{
			//return "Error en al insertar<br>" . $resultado	;
			//$_SESSION['vsMsj'] = "<br>error<br>" . $mysqli->errno($resultado);
			@$_SESSION['vsMsj'] = "Fallo el ingreso del Encabezado: (" . $mysqli->errno . ") " . $mysqli->error;

		}
		return $_SESSION['vsMsj'];
		mysqli_close($cnn);
	}

function insertarDet1($tram,$nota,$carnet,$obser){
		
			$cnn=$GLOBALS['m_Conexion']->Conectar();
			$resultado=mysqli_query($cnn, "CALL SP_actividad_insertardet('".$tram."','".$nota."','".$carnet."','".$obser."')");
			//return $resultado;
			//echo $sql = "call SP_actividad_insertardet('".$tram."','".$nota."','".$carnet."','".$obser."')";
			if($resultado)
			{
				//return "exito al insertar" . $resultado;
				@$_SESSION['vsMsj'] = "<br>Insertado con exito..." ;
			}
			else
			{
				//return "Error en al insertar<br>" . $resultado	;
				//$_SESSION['vsMsj'] = "<br>error<br>" . $mysqli->errno($resultado);
				@$_SESSION['vsMsj'] = "Fallo el ingreso del Detalle: (" . $mysqli->errno . ") " . $mysqli->error;

			}
			
			//return "exito al insertar" . $resultado;
		
			
		
		return $_SESSION['vsMsj'];
		mysqli_close($cnn);


			
		}

		


		



	function insertarDet($tram,$nota,$carnet,$obser){
		$cnn=$GLOBALS['m_Conexion']->Conectar();

		$resulTbl=mysqli_query($cnn, "CALL 	SP_actividad_validarAlumno('".$carnet."', '".$tram."')");
		 $num_rows = $resulTbl->fetch_array();
		if($num_rows == 0)
		{
			$cnn=$GLOBALS['m_Conexion']->Conectar();
			$resultado=mysqli_query($cnn, "CALL SP_actividad_insertardet('".$tram."','".$nota."','".$carnet."','".$obser."')");
			//return $resultado;
			//echo $sql = "call SP_actividad_insertardet('".$tram."','".$nota."','".$carnet."','".$obser."')";
			if($resultado)
			{
				//return "exito al insertar" . $resultado;
				@$_SESSION['vsMsj'] = "<br>Insertado con exito..." ;
			}
			else
			{
				//return "Error en al insertar<br>" . $resultado	;
				//$_SESSION['vsMsj'] = "<br>error<br>" . $mysqli->errno($resultado);
				@$_SESSION['vsMsj'] = "Falló la llamada Detalle: (" . $mysqli->errno . ") " . $mysqli->error;

			}
			
			//return "exito al insertar" . $resultado;
		
			
		}
		else
		{
			//return "Error en al insertar<br>" . $resultado	;
			//$_SESSION['vsMsj'] = "<br>error<br>" . $mysqli->errno($resultado);
			//@$_SESSION['vsMsj'] = "Falló la llamada: (" . $mysqli->errno . ") " . $mysqli->error;
			@$_SESSION['vsMsj'] = "<br>Alumno ya ingresado en la actividad. Seleccione otro alumno..." ;
		

			
		}

		return $_SESSION['vsMsj'];
		mysqli_close($cnn);



		
	}


	function modificar($car,$mat,$sec,$eva,$doc, $conteo,$ci,$fecha,$tram, $est)
	{
		$cnn=$GLOBALS['m_Conexion']->Conectar();
		
		$resultado=mysqli_query($cnn, "CALL SP_actividad_modificar('".$car."','".$mat."','".$sec."','".$eva."','".$doc."','".$conteo."','".$ci."','".$fecha."', '".$tram."', '".$est."')");
		
		if($resultado)
		{
			//return "exito al insertar" . $resultado;
			@$_SESSION['vsMsj'] = "<br>Modificado con exito..." ;
		}
		else
		{
			//return "Error en al insertar<br>" . $resultado	;
			//$_SESSION['vsMsj'] = "<br>error<br>" . $mysqli->errno($resultado);
			@$_SESSION['vsMsj'] = "Falló la llamada: (" . $mysqli->errno . ") " . $mysqli->error;

		}
		return $_SESSION['vsMsj'];
		mysqli_close($cnn);
	}


	function modificarDet($car,$nota,$observ,$tram)
	{
		$cnn=$GLOBALS['m_Conexion']->Conectar();
		
		$resultado=mysqli_query($cnn, "CALL SP_actividad_modificar_linea('".$car."','".$nota."','".$observ."','".$tram."')");
		
		if($resultado)
		{
			//return "exito al insertar" . $resultado;
			@$_SESSION['vsMsj'] = "<br>Modificado con exito..." ;
		}
		else
		{
			//return "Error en al insertar<br>" . $resultado	;
			//$_SESSION['vsMsj'] = "<br>error<br>" . $mysqli->errno($resultado);
			@$_SESSION['vsMsj'] = "Falló la llamada: (" . $mysqli->errno . ") " . $mysqli->error;

		}
		return $_SESSION['vsMsj'];
		mysqli_close($cnn);
	}



	function eliminarActividad($id){
		$cnn=$GLOBALS['m_Conexion']->Conectar(); 
		$resultado=mysqli_query($cnn, "CALL SP_actividad_eliminar('".$id."')");

		if($resultado)
		{
			//return "exito al insertar" . $resultado;
			@$_SESSION['vsMsj'] = "<br>Eliminado con exito..." ;
		}
		else
		{
			//return "Error en al insertar<br>" . $resultado	;
			//$_SESSION['vsMsj'] = "<br>error<br>" . $mysqli->errno($resultado);
			@$_SESSION['vsMsj'] = "Falló la llamada: (" . $mysqli->errno . ") " . $mysqli->error;

		}
		return $_SESSION['vsMsj'];
		//return $resultado;
		mysqli_close($cnn);
	}


function eliminarLinea($id, $car){
		$cnn=$GLOBALS['m_Conexion']->Conectar(); 
		$resultado=mysqli_query($cnn, "CALL SP_actividad_eliminar_linea('".$id."','".$car."')");

		if($resultado)
		{
			//return "exito al insertar" . $resultado;
			@$_SESSION['vsMsj'] = "<br>Eliminado con exito..." ;
		}
		else
		{
			//return "Error en al insertar<br>" . $resultado	;
			//$_SESSION['vsMsj'] = "<br>error<br>" . $mysqli->errno($resultado);
			@$_SESSION['vsMsj'] = "Falló la llamada: (" . $mysqli->errno . ") " . $mysqli->error;

		}
		return $_SESSION['vsMsj'];
		//return $resultado;
		mysqli_close($cnn);
	}

	
}
	


	

?>