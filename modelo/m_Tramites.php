<?php 

class m_Tramites
{
	function mostrarSecciones()
	{
		$cnn=$GLOBALS['m_Conexion']->Conectar(); 
		$resulTbl=mysqli_query($cnn, "CALL SP_secciones_mostrargeneral()");
		return $resulTbl;
		mysqli_close($cnn);	
	}

	function mostrarEscuelas()
	{
		$cnn=$GLOBALS['m_Conexion']->Conectar(); 
		$resulTbl=mysqli_query($cnn, "CALL 	SP_escuelas_mostrargeneral()");
		return $resulTbl;
		mysqli_close($cnn);	
	}

	function mostrarEvaluaciones()
	{
		$cnn=$GLOBALS['m_Conexion']->Conectar(); 
		$resulTbl=mysqli_query($cnn, "CALL 	SP_evaluaciones_mostrargeneral()");
		return $resulTbl;
		mysqli_close($cnn);	
	}

	function mostrarCiclos()
	{
		$cnn=$GLOBALS['m_Conexion']->Conectar(); 
		$resulTbl=mysqli_query($cnn, "CALL 	SP_ciclos_mostrargeneral()");
		return $resulTbl;
		mysqli_close($cnn);	
	}


	function mostrarDocentes()
	{
		$cnn=$GLOBALS['m_Conexion']->Conectar(); 
		$resulTbl=mysqli_query($cnn, "CALL 	SP_docentes_consultargeneral()");
		return $resulTbl;
		mysqli_close($cnn);	
	}

	function mostrarAlumnos()
	{
		$cnn=$GLOBALS['m_Conexion']->Conectar(); 
		$resulTbl=mysqli_query($cnn, "CALL 	SP_alumnos_mostrargeneral()");
		return $resulTbl;
		mysqli_close($cnn);	
	}
	
	function mostrarCarreras(){
		$cnn=$GLOBALS['m_Conexion']->Conectar(); 
		$resulTbl=mysqli_query($cnn, "CALL 	SP_vwcarreras_Mostrar()");
		return $resulTbl;
		mysqli_close($cnn);	
	}
}
?>