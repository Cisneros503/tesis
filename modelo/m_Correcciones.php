<?php

/**
 * 
 */
class m_Correcciones
{
	function mostrarInicioUno($carne){
		$cnn=$GLOBALS['m_Conexion']->Conectar(); 
		$resulTbl=mysqli_query($cnn, "CALL SP_correcciones_consulta_id('".$carne."')");
		return $resulTbl;
		mysqli_close($cnn);
	}

	function mostrarCarreras(){
		$cnn=$GLOBALS['m_Conexion']->Conectar(); 
		$resulTbl=mysqli_query($cnn, "CALL 	SP_vwcarreras_Mostrar()");
		return $resulTbl;
		mysqli_close($cnn);	
	}

	function mostrarInicioTodos(){
		$cnn=$GLOBALS['m_Conexion']->Conectar(); 
		$resulTbl=mysqli_query($cnn, "CALL SP_correcciones_consulta_Todos()");
		return $resulTbl;
		mysqli_close($cnn);
	}

	function mostrarUnTrCr($idtr){
		$cnn=$GLOBALS['m_Conexion']->Conectar(); 
		$resulTbl=mysqli_query($cnn, "CALL SP_correcciones_consulta_uno('".$idtr."')");
		return $resulTbl;
		mysqli_close($cnn);
	}

	function mostrarUnaMateria($idcarma){
		$cnn=$GLOBALS['m_Conexion']->Conectar(); 
		$resulTbl=mysqli_query($cnn, "CALL SP_correccion_verMateria('".$idcarma."')");
		return $resulTbl;
		mysqli_close($cnn);
	}

	function verMatIdCar($idcar){
		$cnn=$GLOBALS['m_Conexion']->Conectar(); 
		$resulTbl=mysqli_query($cnn, "CALL SP_verMatxIdca('".$idcar."')");
		return $resulTbl;
		mysqli_close($cnn);
	}

	function mostrarUnIdCar($idcarma){
		$cnn=$GLOBALS['m_Conexion']->Conectar(); 
		$resulTbl=mysqli_query($cnn, "CALL SP_correccion_verIdCar('".$idcarma."')");
		return $resulTbl;
		mysqli_close($cnn);
	}

	function verSeccion($idsec){
		$cnn=$GLOBALS['m_Conexion']->Conectar(); 
		$sec=mysqli_query($cnn, "CALL SP_seccion_ver_uno('".$idsec."')");
		$x=$sec->fetch_array();
		return utf8_decode($x['seccion']);
		mysqli_close($cnn);
	}

	function verEvaluacion($idev){
		$cnn=$GLOBALS['m_Conexion']->Conectar(); 
		$ev=mysqli_query($cnn, "CALL SP_evaluaciones_mostrarid('".$idev."')");
		$x=$ev->fetch_array();	
		return $x['descripcion'];
		mysqli_close($cnn);
	}

	function verDocente($cardoc){
		$cnn=$GLOBALS['m_Conexion']->Conectar(); 
		$doc=mysqli_query($cnn, "CALL SP_docentes_consultarcarnet('".$cardoc."')");
		$x=$doc->fetch_array();	
		return $x['docente_nombres'];
		mysqli_close($cnn);
	}

	function verCiclo($idciclo){
		$cnn=$GLOBALS['m_Conexion']->Conectar(); 
		$cic=mysqli_query($cnn, "CALL SP_ciclos_mostrarid('".$idciclo."')");
		$x=$cic->fetch_array();	
		return $x['nombre_ciclo'];
		mysqli_close($cnn);
	}
//AGREGADO PARA MOSTRAR ALUMNOS EN SELECT

	function mostrarAlumnos()
	{
		$cnn=$GLOBALS['m_Conexion']->Conectar(); 
		$resulTbl=mysqli_query($cnn, "CALL 	SP_alumnos_mostrargeneral()");
		return $resulTbl;
		mysqli_close($cnn);	
	}

//AGREGADO PARA MOSTRAR ALUMNOS EN SELECT


	function eliminarCorreccion($idTram){
		$cnn=$GLOBALS['m_Conexion']->Conectar(); 
		$resultado=mysqli_query($cnn, "CALL SP_correccion_eliminar('".$idTram."')");
		if($resultado)
		{
			//return "exito al insertar" . $resultado;
			@$_SESSION['vsMsj'] = "<br>El proceso se eliminó con exito..." ;
		}
		else
		{
			@$_SESSION['vsMsj'] = false;
		}
		return $_SESSION['vsMsj'];
		mysqli_close($cnn);
	}

	function autorizarCo($est, $idTram){
		$cnn=$GLOBALS['m_Conexion']->Conectar(); 
		$resultado=mysqli_query($cnn, "CALL SP_correc_estado_updt('".$est."','".$idTram."')");
		if($resultado)
		{
			//return "exito al insertar" . $resultado;
			@$_SESSION['vsMsj'] = "<br>El estado de la corrección se actualizó con exito..." ;
		}
		else
		{
			@$_SESSION['vsMsj'] = false;
		}
		return $_SESSION['vsMsj'];
		mysqli_close($cnn);
	}

	function mostrarMaterias(){
		$cnn=$GLOBALS['m_Conexion']->Conectar(); 
		$resulTbl=mysqli_query($cnn, "CALL SP_carrera_materia_mostrargeneral()");
		return $resulTbl;
		mysqli_close($cnn);
	}

	function mostrarSecciones(){
		$cnn=$GLOBALS['m_Conexion']->Conectar(); 
		$resulTbl=mysqli_query($cnn, "CALL SP_secciones_mostrargeneral()");
		return $resulTbl;
		mysqli_close($cnn);
	}

	function mostrarEvaluaciones(){
		$cnn=$GLOBALS['m_Conexion']->Conectar(); 
		$resulTbl=mysqli_query($cnn, "CALL SP_evaluaciones_mostrargeneral()");	
		return $resulTbl;
		mysqli_close($cnn);
	}

	function mostrarDocentes(){
		$cnn=$GLOBALS['m_Conexion']->Conectar(); 
		$resulTbl=mysqli_query($cnn, "CALL SP_docentes_consultargeneral()");	
		return $resulTbl;
		mysqli_close($cnn);
	}

	function mostrarCiclos(){
		$cnn=$GLOBALS['m_Conexion']->Conectar(); 
		$resulTbl=mysqli_query($cnn, "CALL SP_ciclos_mostrargeneral()");	
		return $resulTbl;
		mysqli_close($cnn);
	}

	function mostrarUltramite(){
		$cnn=$GLOBALS['m_Conexion']->Conectar(); 
		$resulTbl=mysqli_query($cnn, "CALL SP_ultimo_tr_correccion()");	
		return $resulTbl;
		mysqli_close($cnn);
	}

	function insertarTr($idCar, $idMa, $idSec,$idEval,$carnDocnte, $idCiclo, $perio, $fSol, $nAct, $nCorreg, $motivoCorrec, $estCorrec, $carne, $obcervac, $usrReci, $fReci)
	{
		$cnn=$GLOBALS['m_Conexion']->Conectar();		
		$resultado=mysqli_query($cnn, "CALL SP_Correccion_insertarok('".$idCar."','".$idMa."','".$idSec."','".$idEval."','".$carnDocnte."','".$idCiclo."','".$perio."','".$fSol."','".$nAct."','".$nCorreg."','".$motivoCorrec."','".$estCorrec."','".$carne."','".$obcervac."','".$usrReci."','".$fReci."')");	
		
		if($resultado)
		{
			//return "exito al insertar" . $resultado;
			@$_SESSION['vsMsj'] = "<br>Proceso de Corrección agregado con exito..." ;
		}
		else
		{
			//return "Error en al insertar<br>" . $resultado	;
			//$_SESSION['vsMsj'] = "<br>error<br>" . $mysqli->errno($resultado);
			@$_SESSION['vsMsj'] = false;
		}
		return $_SESSION['vsMsj'];
		mysqli_close($cnn);
	}

	function editarTr($idtr, $idCar, $idMa, $idSec,$idEval,$carnDocnte, $idCiclo, $perio, $fSol, $nAct, $nCorreg, $motivoCorrec, $estCorrec, $carne, $obcervac, $usrReci, $fReci)
	{
		$cnn=$GLOBALS['m_Conexion']->Conectar();		
		$resultado=mysqli_query($cnn, "CALL SP_Correccion_Modificar1('".$idtr."','".$idCar."','".$idMa."','".$idSec."','".$idEval."','".$carnDocnte."','".$idCiclo."','".$perio."','".$fSol."','".$nAct."','".$nCorreg."','".$motivoCorrec."','".$estCorrec."','".$carne."','".$obcervac."','".$usrReci."','".$fReci."')");	
		
		if($resultado)
		{
			//return "exito al insertar" . $resultado;
			@$_SESSION['vsMsj'] = "<br>Proceso de Corrección Modificado con exito..." ;
		}
		else
		{
			//return "Error en al insertar<br>" . $resultado	;
			//$_SESSION['vsMsj'] = "<br>error<br>" . $mysqli->errno($resultado);
			@$_SESSION['vsMsj'] = false;
		}
		return $_SESSION['vsMsj'];
		mysqli_close($cnn);
	}

	function insertarCr($idTram,$nActual,$nCorregida,$motivoCorrec, $estadoCorrec, $carneAl, $obcervac, $usrReci, $fRecibe)
	{
		$cnn=$GLOBALS['m_Conexion']->Conectar();		
		$resultado=mysqli_query($cnn, "CALL SP_correcciones_insert('".$idTram."','".$nActual."','".$nCorregida."','".$motivoCorrec."','".$estadoCorrec."','".$carneAl."','".$obcervac."','".$usrReci."','".$fRecibe."')");	
		
		if($resultado)
		{
			//return "exito al insertar" . $resultado;
			@$_SESSION['vsMsj'] = "<br>Exito... Puede ver la corrección ingresando el carné del alumno" ;
		}
		else
		{
			//return "Error en al insertar<br>" . $resultado	;
			//$_SESSION['vsMsj'] = "<br>error<br>" . $mysqli->errno($resultado);
			@$_SESSION['vsMsj'] = false;
		}
		return $_SESSION['vsMsj'];
		mysqli_close($cnn);
	}


	
	
}


?>