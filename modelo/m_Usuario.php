<?php

class Usr
{
	/// MODELOS DEL USUARIO
	
	function mostrarTodos(){
		$cnn=$GLOBALS['m_Conexion']->Conectar(); 
		$resulTbl=mysqli_query($cnn, "CALL SP_usuarios_mostrar()");
		return $resulTbl;
		mysqli_close($cnn);
	}

	function mostrarUno($id){
		$cnn=$GLOBALS['m_Conexion']->Conectar(); 
		$resulTbl=mysqli_query($cnn, "CALL SP_usuarios_mostrar_uno('".$id."')");
		return $resulTbl;
		mysqli_close($cnn);
	}

	function opcionesMenu(){
		$cnn=$GLOBALS['m_Conexion']->Conectar(); 
		$resulTbl=mysqli_query($cnn, "CALL SP_usuarios_opc_menu()");
		return $resulTbl;
		mysqli_close($cnn);
	}

	function ultimoUsuario()
	{
		$cnn=$GLOBALS['m_Conexion']->Conectar(); 
		$result=mysqli_query($cnn, "CALL SP_usuario_ultimo()");
		if($row=$result->fetch_array())
        	{
        		$resultado = $row['id'];
        	}
		return $resultado;
		mysqli_close($cnn);
	}

	function ultimoRol()
	{
		$cnn=$GLOBALS['m_Conexion']->Conectar(); 
		$result=mysqli_query($cnn, "CALL SP_roles_ultimo()");
		if($row=$result->fetch_array())
        	{
        		$resultado = $row['id'];
        	}
		return $resultado;
		mysqli_close($cnn);
	}

	function modifRol($id, $rol)
	{
		$cnn=$GLOBALS['m_Conexion']->Conectar(); 
		$resultado=mysqli_query($cnn, "CALL SP_usuario_Modif_Rol('".$id."','".$rol."')");
		if($resultado)
		{
			@$_SESSION['vsMsj'] = "<br>Realizado con exito..." ;
		}
		else
		{
			@$_SESSION['vsMsj'] = "Falló la operación: (" . $mysqli->errno . ") " . $mysqli->error;
		}
		return $resultado;
		mysqli_close($cnn);
	}

	function insertaOpcionMenu($id, $id_opcion)
	{
		$cnn=$GLOBALS['m_Conexion']->Conectar(); 
		$resultado=mysqli_query($cnn, "CALL SP_rol_menu_insertar('".$id_opcion."','". 1 ."','".$id."')");
		if($resultado)
		{
			@$_SESSION['vsMsj'] = "<br>Realizado con exito..." ;
		}
		else
		{
			@$_SESSION['vsMsj'] = "Falló la operación: (" . $mysqli->errno . ") " . $mysqli->error;
		}
		return $resultado;
		mysqli_close($cnn);
	}

	function insertRol($rol)
	{
		$cnn=$GLOBALS['m_Conexion']->Conectar(); 
		$resultado=mysqli_query($cnn, "CALL SP_roles_insertar('".$rol."')");
		if($resultado)
		{
			@$_SESSION['vsMsj'] = "<br>Rol insertado con exito..." ;
		}
		else
		{
			@$_SESSION['vsMsj'] = "Falló la operación: (" . $mysqli->errno . ") " . $mysqli->error;
		}
		return $resultado;
		mysqli_close($cnn);
	}

	function eliminar($id){
		$cnn=$GLOBALS['m_Conexion']->Conectar(); 
		$resultado=mysqli_query($cnn, "CALL SP_usuarios_eliminar('".$id."')");
		if($resultado)
		{
			@$_SESSION['vsMsj'] = "<br>Usuario eliminado con exito..." ;
		}
		else
		{
			@$_SESSION['vsMsj'] = "Falló la operación: (" . $mysqli->errno . ") " . $mysqli->error;
		}
		return $resultado;
		mysqli_close($cnn);
	}

	function insertar($nombre, $apellido, $usr, $pass, $email,$rol){
		$cnn=$GLOBALS['m_Conexion']->Conectar();
        
		$resultado=mysqli_query($cnn, "CALL SP_usuarios_insertar('".$nombre."','".$apellido."','".$email."','".$usr."','".$pass."','". 1 ."','". 1 ."','". 2 ."')");
		
		if($resultado)
		{
			
			@$_SESSION['vsMsj'] = "<br>insertado con exito..." ;
		}
		else
		{
			
			@$_SESSION['vsMsj'] = "Falló la llamada: (" . $mysqli->errno . ") " . $mysqli->error;

		}
		return $_SESSION['vsMsj'];
		mysqli_close($cnn);
	}

	function modificar($id, $nombre, $apellido, $usr, $pass, $email)
	{
		$cnn=$GLOBALS['m_Conexion']->Conectar();
		
		$resultado=mysqli_query($cnn, "CALL SP_usuarios_modificar('".$nombre."','".$apellido."','".$email."','".$pass."','".$usr."','".$id."')");
		
		if($resultado)
		{
			//return "exito al insertar" . $resultado;
			@$_SESSION['vsMsj'] = "<br>Modificado con exito..." ;
		}
		else
		{
			//return "Error en al insertar<br>" . $resultado	;
			//$_SESSION['vsMsj'] = "<br>error<br>" . $mysqli->errno($resultado);
			@$_SESSION['vsMsj'] = "Falló la llamada: (" . $mysqli->errno . ") " . $mysqli->error;

		}
		return $_SESSION['vsMsj'];
		mysqli_close($cnn);
	}

	//pendiente: sp para traer la info del usr
	public function PerfilUsr($cn)
	{
		$resultado=mysqli_query($cn, "CALL sp_perfilUsuario(" . $id . ")");
		return $resultado;
		if($resultado)
		{
			return "exito en la consulta";
		}
		else
		{
			return "Error en la consulta<br>".$txtSql;
		}
	}
	

}

?>