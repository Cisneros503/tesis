-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 14, 2018 at 05:14 AM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bd_utec`
--
CREATE DATABASE IF NOT EXISTS `bd_utec` DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish_ci;
USE `bd_utec`;

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_actividades_insertar` (IN `idcar` INT, IN `idmat` VARCHAR(100), IN `idsec` INT, IN `ideval` INT, IN `carDoc` VARCHAR(50), IN `conteo` INT, IN `idci` INT, IN `fecsol` DATETIME)  BEGIN
START TRANSACTION;


INSERT INTO tramites(`ID_carrera_materia`, `ID_seccion`, `ID_tipo_tramite`, `ID_eval`, `carnet_docente`, `autorizado`, `conteo`, `fechaRegistro`, `ID_ciclo`, `fechaEnvio`, `periodo`, `fechaSolicitud`)
SELECT ID_carrera_materia,idsec,1,ideval,carDoc,1, conteo, CURDATE() ,idci,CURDATE(),1,fecsol FROM carrera_materia cm left join carreras c on cm.ID_carrera = c.ID_carrera 
where cm.ID_carrera = idcar and cm.ID_materia =  idmat;



COMMIT;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_actividad_consultaID` (IN `idTram` INT)  NO SQL
select T.*, SEC.seccion, TIPTRAM.nombreTramite, eva.descripcion as evaluacion, doc.docente_nombres, ci.nombre_ciclo, car.nombre_carrera, car.id_carrera as ID_carrera, mat.nombre_materia, mat.ID_materia from tramites as t left outer join secciones as sec on t.ID_SECCION=SEC.ID_seccion LEFT OUTER JOIN tipotramite AS tiptram on t.ID_TIPO_TRAMITE=TIPTRAM.ID_tipo_tramite left outer join evaluaciones as eva on t.ID_EVAL=EVA.ID_eval LEFT OUTER JOIN docentes AS DOC ON T.CARNET_DOCENTE=DOC.carnet_docente LEFT OUTER JOIN CICLOS AS CI ON T.ID_CICLO=CI.ID_ciclo left outer join carrera_materia as carMat on t.id_carrera_materia=carMat.ID_carrera_materia left outer join carreras as car on carMat.ID_carrera=car.ID_carrera left outer join materias as mat on mat.ID_materia=carMat.ID_materia where tiptram.ID_tipo_tramite=1 and ID_tramite=idTram$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_actividad_consultar` ()  NO SQL
select T.*, SEC.seccion, TIPTRAM.nombreTramite, eva.descripcion as evaluacion, doc.docente_nombres, ci.nombre_ciclo, car.nombre_carrera, mat.nombre_materia  from tramites as t left outer join secciones as sec on t.ID_SECCION=SEC.ID_seccion LEFT OUTER JOIN tipotramite AS tiptram on t.ID_TIPO_TRAMITE=TIPTRAM.ID_tipo_tramite left outer join evaluaciones as eva on t.ID_EVAL=EVA.ID_eval LEFT OUTER JOIN docentes AS DOC ON T.CARNET_DOCENTE=DOC.carnet_docente LEFT OUTER JOIN CICLOS AS CI ON T.ID_CICLO=CI.ID_ciclo left outer join carrera_materia as carMat on t.id_carrera_materia=carMat.ID_carrera_materia left outer join carreras as car on carMat.ID_carrera=car.ID_carrera left outer join materias as mat on mat.ID_materia=carMat.ID_materia where tiptram.ID_tipo_tramite=1$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_actividad_consultar_detalle` (IN `idTram` INT)  NO SQL
select repact.*, tipact.nombreActividad, a.alumno from reporteactividades as repact left outer join tipoactividad as tipact on repact.ID_tipo_actividad=tipact.ID_tipo_actividad 
left outer join alumnos as a on a.carnet=repact.carnet
where repact.ID_tramite=idTram and repact.estado=1$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_actividad_consulta_linea` (IN `carnet` VARCHAR(100), IN `idTram` INT)  NO SQL
select repact.*, tipact.nombreActividad, a.alumno from reporteactividades as repact left outer join tipoactividad as tipact on repact.ID_tipo_actividad=tipact.ID_tipo_actividad 
left outer join alumnos as a on a.carnet=repact.carnet
where repact.ID_tramite=idTram and a.carnet=carnet and repact.estado=1$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_actividad_eliminar` (IN `tram` INT)  NO SQL
update tramites set estado=0 where id_tramite=tram$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_actividad_eliminar_linea` (IN `tram` INT, IN `car` VARCHAR(100))  NO SQL
update reporteactividades set estado=0 where ID_tramite=tram and carnet=car$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_actividad_insertardet` (IN `idTram` INT, IN `nota` FLOAT, IN `carnet` VARCHAR(50), IN `obser` TEXT)  NO SQL
INSERT INTO reporteactividades (`ID_tramite`,`nota`, `observaciones`, `carnet`, `id_tipo_actividad` )
VALUES(idTram,nota, obser, carnet,1)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_actividad_modificar` (IN `idCar` INT, IN `idMat` VARCHAR(100), IN `idSec` INT, IN `idEva` INT, IN `doc` VARCHAR(100), IN `conteo` INT, IN `idCi` INT, IN `fecha` INT, IN `idTram` INT, IN `est` INT)  NO SQL
update tramites set ID_carrera_materia=(SELECT ID_carrera_materia FROM carrera_materia cm left join carreras c on cm.ID_carrera = c.ID_carrera 
where cm.ID_carrera = idcar and cm.ID_materia =  idmat), ID_seccion=idSec,ID_eval=idEva, carnet_docente=doc, conteo=conteo, ID_ciclo=idCi, fechaSolicitud=fecha, estado=est where ID_tramite=idTram$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_actividad_modificar_linea` (IN `car` VARCHAR(100), IN `nota` FLOAT, IN `observ` TEXT, IN `idTram` INT)  NO SQL
update reporteactividades set  nota=nota, observaciones=observ where ID_tramite=idTram and carnet=car$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_actividad_reporteEval_ciclos` (IN `c` INT)  NO SQL
SELECT EVALUACIONES, count(*) as CONTEO FROM vw_rptactivciclo where  CICLO = c group by EVALUACIONES$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_actividad_reportesEval_fechas` (IN `fecha1` DATE, IN `fecha2` DATE)  NO SQL
SELECT a.id_eval, b.descripcion as evaluacion, count(a.id_tramite) as conteo FROM tramites as a left outer join evaluaciones as b on a.ID_eval=b.ID_eval where a.fechaSolicitud>=fecha1 and a.fechaSolicitud<=fecha2 and a.ID_tipo_tramite=1 group by id_eval$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_actividad_validarAlumno` (IN `car` VARCHAR(100), IN `tram` INT)  NO SQL
select * from reporteactividades where carnet=car and ID_tramite=tram and estado=1$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_alumnos_eliminar` (IN `car` TEXT)  NO SQL
UPDATE alumnos
SET estado = 0
WHERE carnet = car$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_alumnos_insertar` (IN `car` TEXT, IN `noms` TEXT, IN `cor` TEXT)  NO SQL
INSERT INTO alumnos(carnet,alumno,correo_alumno)
VALUES (car,ft_limpieza(noms),cor)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_alumnos_modificar` (IN `car` TEXT, IN `noms` TEXT, IN `est` BOOLEAN, IN `cor` TEXT)  NO SQL
UPDATE alumnos	
SET alumno = noms,  estado = est, correo_alumno = cor
WHERE carnet = car$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_alumnos_mostrargeneral` ()  NO SQL
SELECT carnet, alumno,estado, correo_alumno, CONCAT(REPLACE(carnet,'-',''),' - ',alumno) as carAlum
FROM alumnos
WHERE estado = 1
order by carnet desc$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_alumnos_mostrargeneral_CRUD` ()  NO SQL
SELECT carnet, alumno,estado, correo_alumno, CONCAT(REPLACE(carnet,'-',''),' - ',alumno) as carAlum
FROM alumnos
order by carnet desc$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_alumnos_mostrarid` (IN `car` TEXT)  NO SQL
SELECT * 
FROM alumnos 
where carnet = car$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_asignarReciencreadoRolUsuario` (IN `r` VARCHAR(50), IN `nom` VARCHAR(50), IN `ape` VARCHAR(50), IN `cor` VARCHAR(100), IN `usu` VARCHAR(50), IN `cla` VARCHAR(100), IN `niv` INT)  BEGIN
START TRANSACTION;
insert into roles(rol)
values(r);

INSERT INTO usuarios (nombre, apellido, correo, usuario, clave, nivel, id_rol) 
VALUES (nom, ape, cor, usu, cla, niv, LAST_INSERT_ID());

COMMIT;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_carreras_eliminar` (IN `id` INT)  NO SQL
UPDATE carreras
SET carreras.estado = 0
where ID_carrera = id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_carreras_insertar` (IN `nom` TEXT, IN `codCar` INT)  NO SQL
INSERT INTO carreras(nombre_carrera, Codigo_carrera)
VALUES (nom, codCar)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_carreras_modificar` (IN `nom` TEXT, IN `id` INT, IN `est` INT, IN `codCar` INT)  NO SQL
UPDATE carreras	SET nombre_carrera = nom, estado=est, Codigo_carrera=codCar where ID_carrera = id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_carreras_mostrargeneral` ()  NO SQL
SELECT * 
FROM carreras
WHERE estado = 1$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_carreras_mostrargeneral_CRUD` ()  NO SQL
SELECT * 
FROM carreras$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_carreras_mostrarid` (IN `id` INT)  NO SQL
SELECT * 
FROM carreras 
where ID_carrera = id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_carrera_materia_agruparCarrera` ()  NO SQL
SELECT a.ID_carrera, c.nombre_carrera
FROM carrera_materia as a left outer join carreras as c on a.ID_carrera=c.ID_carrera 
WHERE a.estado = 1 
group by a.ID_carrera$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_carrera_materia_eliminar` (IN `id` INT)  NO SQL
UPDATE carrera_materia
SET estado = 0
where ID_carrera_materia = id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_carrera_materia_id` (IN `id` INT)  NO SQL
SELECT a.ID_materia, A.ID_carrera, c.nombre_materia FROM carrera_materia as a left outer join materias as c on a.ID_materia=c.ID_materia  where a.ID_carrera_materia=id order by id_carrera$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_carrera_materia_insertar` (IN `idcar` INT, IN `idcate` INT, IN `idmate` TEXT)  NO SQL
INSERT INTO carrera_materia(ID_carrera,ID_catedra,ID_materia)
VALUES (idcar,idcate,idmate)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_carrera_materia_modificar` (IN `est` BOOLEAN, IN `idcate` INT, IN `idmat` TEXT, IN `idcar` INT, IN `id` INT)  NO SQL
UPDATE carrera_materia	
SET  estado = est, ID_catedra = idcate, ID_materia = idmat, ID_carrera=idcar
where ID_carrera_materia = id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_carrera_materia_mostrargeneral` ()  NO SQL
SELECT b.nombre_catedra, c.nombre_carrera, d.nombre_materia, a.*
FROM carrera_materia as a left outer join catedras as b on a.ID_catedra=b.ID_catedra left outer join carreras as c on a.ID_carrera=c.ID_carrera left outer join materias as d on a.ID_materia=d.ID_materia 
where a.estado = 1
order by a.id_carrera, a.id_catedra, a.id_materia$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_carrera_materia_mostrargeneral_CRUD` ()  NO SQL
SELECT b.nombre_catedra, c.nombre_carrera, d.nombre_materia, a.*
FROM carrera_materia as a left outer join catedras as b on a.ID_catedra=b.ID_catedra left outer join carreras as c on a.ID_carrera=c.ID_carrera left outer join materias as d on a.ID_materia=d.ID_materia order by a.id_carrera, a.id_catedra, a.id_materia$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_carrera_materia_mostrarid` (IN `id` INT)  NO SQL
SELECT * 
FROM carrera_materia 
where ID_carrera_materia = id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_carrera_materia_retornovariable` (IN `idc` INT, IN `idm` VARCHAR(50))  NO SQL
SELECT ID_carrera_materia FROM carrera_materia cm left join carreras c on cm.ID_carrera = c.ID_carrera 
where cm.ID_carrera = idc and cm.ID_materia =  idm$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_carrera_materia_soloMaterias` ()  NO SQL
SELECT a.ID_materia, A.ID_carrera, c.nombre_materia FROM carrera_materia as a left outer join materias as c on a.ID_materia=c.ID_materia order by id_carrera$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_catedras_eliminar` (IN `id` INT)  NO SQL
UPDATE catedras
SET estado = 0
where ID_catedra = id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_catedras_insertar` (IN `idesc` INT, IN `nom` TEXT)  NO SQL
INSERT INTO catedras(ID_escuela,nombre_catedra)
VALUES (idesc,nom)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_catedras_modificar` (IN `est` BOOLEAN, IN `idesc` INT, IN `nom` TEXT, IN `id` INT)  NO SQL
UPDATE catedras	
SET  estado = est, ID_escuela = idesc, nombre_catedra = nom
where ID_catedra = id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_catedras_mostrargeneral` ()  NO SQL
SELECT catedras.*, escuela.nombre_escuela 
FROM catedras left outer join escuela on catedras.ID_escuela=escuela.id_escuela$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_catedras_mostrarid` (IN `id` INT)  NO SQL
SELECT * 
FROM catedras 
where ID_catedra = id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_ciclos_eliminar` (IN `id` INT)  NO SQL
UPDATE ciclos
SET estado = 0
where ID_ciclo = id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_ciclos_insertar` (IN `nom` TEXT)  NO SQL
INSERT INTO ciclos(nombre_ciclo)
VALUES (nom)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_ciclos_modificar` (IN `est` BOOLEAN, IN `nom` TEXT, IN `id` INT)  NO SQL
UPDATE ciclos	
SET estado = est, nombre_ciclo = nom
where ID_ciclo = id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_ciclos_mostrargeneral` ()  NO SQL
SELECT * 
FROM ciclos
where estado = 1$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_ciclos_mostrargeneral_CRUD` ()  NO SQL
SELECT * 
FROM ciclos$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_ciclos_mostrarid` (IN `id` INT)  NO SQL
SELECT * 
FROM ciclos 
where ID_ciclo = id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_correcciones_consulta_Todos` ()  NO SQL
SELECT DISTINCT tr.ID_tramite, cr.ID_correccion, crm.ID_carrera, crm.ID_materia, crm.ID_catedra, tr.ID_carrera_materia, tr.ID_eval, tr.carnet_docente, tr.fechaRegistro, tr.fechaEnvio, tr.periodo, tr.fechaSolicitud, cr.notaActual, cr.notaCorregida, cr.motivoCorreccion, cr.estadoCorreccion, cr.carnet, cr.estado, cr.personaRecibe, cr.fechaRecibe  FROM tramites tr INNER JOIN correcciones cr on cr.ID_tramite = tr.ID_tramite INNER JOIN carrera_materia crm ON tr.ID_carrera_materia = crm.ID_carrera_materia
WHERE cr.estado=1 AND tr.estado=1
ORDER BY tr.ID_tramite DESC$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_correcciones_consulta_uno` (IN `idt` INT)  NO SQL
SELECT DISTINCT tr.ID_ciclo, tr.ID_tramite, tr.ID_seccion, cr.ID_correccion, crm.ID_carrera, crm.ID_materia, crm.ID_catedra, tr.ID_carrera_materia, tr.ID_eval, tr.carnet_docente, tr.fechaRegistro, tr.fechaEnvio, tr.periodo, tr.fechaSolicitud, cr.notaActual, cr.notaCorregida, cr.motivoCorreccion, cr.estadoCorreccion, cr.carnet, cr.estado, cr.personaRecibe, cr.fechaRecibe, cr.observaciones FROM tramites tr INNER JOIN correcciones cr on cr.ID_tramite = tr.ID_tramite INNER JOIN carrera_materia crm ON tr.ID_carrera_materia = crm.ID_carrera_materia
WHERE cr.estado=1 AND tr.estado=1 AND  cr.ID_tramite = idt
ORDER BY tr.ID_tramite DESC$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_correccion_eliminar` (IN `idt` INT)  NO SQL
update correcciones
set estado = 0
where ID_tramite = idt$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_Correccion_insertar` (IN `idc` INT, IN `idm` VARCHAR(100), IN `idsec` INT, IN `idev` INT, IN `cardoc` VARCHAR(50), IN `idci` INT, IN `per` VARCHAR(50), IN `fecsol` DATETIME, IN `car` VARCHAR(50), IN `idTram` INT, IN `notaActu` FLOAT, IN `notaCorreg` FLOAT, IN `motivoCorrec` VARCHAR(200), IN `estadoCorrec` VARCHAR(50), IN `carne` CHAR(12), IN `obcervac` VARCHAR(200), IN `usrRecibe` VARCHAR(100), IN `FechaRecibe` DATETIME)  BEGIN
START TRANSACTION;


INSERT INTO tramites(`ID_carrera_materia`, `ID_seccion`, `ID_tipo_tramite`, `ID_eval`, `carnet_docente`, `autorizado`, `conteo`, `fechaRegistro`, `ID_ciclo`, `fechaEnvio`, `periodo`, `fechaSolicitud`)
SELECT ID_carrera_materia,idsec,2,idev,cardoc,0,0,NOW(),idci,NULL,per,fecsol FROM carrera_materia cm left join carreras c on cm.ID_carrera = c.ID_carrera 
where cm.ID_carrera = idc and cm.ID_materia =  idm;


INSERT INTO correcciones (
	ID_tramite, notaActual, 
    notaCorregida, 
    motivoCorreccion, 
    estadoCorreccion, 
    carnet, 
    observaciones, 
    personaRecibe, 
    fechaRecibe
) VALUES (
	LAST_INSERT_ID(),
	notaActu,
    notaCorreg,
    motivoCorrec,
    estadoCorrec,
    carne,
    obcervac,
    usrRecibe,
    FechaRecibe
);


COMMIT;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_Correccion_insertarok` (IN `idc` INT, IN `idm` VARCHAR(100), IN `idsec` INT, IN `idev` INT, IN `cardoc` VARCHAR(50), IN `idci` INT, IN `per` VARCHAR(50), IN `fecsol` DATETIME, IN `notaActu` FLOAT, IN `notaCorreg` FLOAT, IN `motivoCorrec` VARCHAR(200), IN `estadoCorrec` VARCHAR(50), IN `carne` CHAR(12), IN `obcervac` VARCHAR(200), IN `usrRecibe` VARCHAR(100), IN `FechaRecibe` DATETIME)  BEGIN
START TRANSACTION;


INSERT INTO tramites(`ID_carrera_materia`, `ID_seccion`, `ID_tipo_tramite`, `ID_eval`, `carnet_docente`, `autorizado`, `conteo`, `fechaRegistro`, `ID_ciclo`, `fechaEnvio`, `periodo`, `fechaSolicitud`)
SELECT ID_carrera_materia,idsec,2,idev,cardoc,0,0,NOW(),idci,NOW(),per,fecsol FROM carrera_materia cm left join carreras c on cm.ID_carrera = c.ID_carrera 
where cm.ID_carrera = idc and cm.ID_materia =  idm;


INSERT INTO correcciones (
	ID_tramite, notaActual, 
    notaCorregida, 
    motivoCorreccion, 
    estadoCorreccion, 
    carnet, 
    observaciones, 
    personaRecibe, 
    fechaRecibe
) VALUES (
	LAST_INSERT_ID(),
	notaActu,
    notaCorreg,
    motivoCorrec,
    estadoCorrec,
    carne,
    obcervac,
    usrRecibe,
    FechaRecibe
);


COMMIT;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_Correccion_Modificar1` (IN `idt` INT, IN `idc` INT, IN `idm` VARCHAR(100), IN `idsec` INT, IN `idev` INT, IN `cardoc` VARCHAR(50), IN `idci` INT, IN `per` VARCHAR(50), IN `fecsol` DATETIME, IN `notaActu` FLOAT, IN `notaCorreg` FLOAT, IN `motivoCorrec` VARCHAR(200), IN `estadoCorrec` VARCHAR(50), IN `carne` CHAR(12), IN `obcervac` VARCHAR(200), IN `usrRecibe` VARCHAR(100), IN `FechaRecibe` DATETIME)  BEGIN
START TRANSACTION;


UPDATE tramites SET 
    `ID_carrera_materia`=(SELECT ID_carrera_materia FROM carrera_materia cm left join carreras c on cm.ID_carrera = c.ID_carrera 
where cm.ID_carrera = idc and cm.ID_materia =  idm), 
    `ID_seccion`=idsec, 
    `ID_tipo_tramite`=2, 
    `ID_eval`=idev, 
    `carnet_docente`= cardoc, 
    `autorizado`=0, 
    `conteo`=0, 
    `fechaRegistro`=NOW(), 
    `ID_ciclo`=idci, 
    `fechaEnvio`=NOW(), 
    `periodo`=per, 
    `fechaSolicitud`=fecsol
    WHERE tramites.ID_tramite = idt;


UPDATE correcciones SET  
    notaActual=notaActu, 
    notaCorregida=notaCorreg, 
    motivoCorreccion=motivoCorrec, 
    estadoCorreccion=estadoCorrec, 
    carnet=carne, 
    observaciones=obcervac, 
    personaRecibe=usrRecibe, 
    fechaRecibe=FechaRecibe

WHERE correcciones.ID_tramite = idt;


COMMIT;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_correccion_verIdCar` (IN `idcm` INT)  NO SQL
select crm.ID_carrera, crm.ID_catedra, crm.ID_materia, cr.nombre_carrera FROM carrera_materia crm INNER JOIN carreras cr ON cr.ID_carrera = crm.ID_carrera where cr.estado = 1 AND crm.ID_carrera_materia = idcm$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_correccion_verMateria` (IN `idcm` INT)  NO SQL
select ID_materia FROM carrera_materia 
where ID_carrera_materia = idcm$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_correc_estado_updt` (IN `est` VARCHAR(50), IN `idtr` INT)  NO SQL
UPDATE correcciones SET estadoCorreccion = est WHERE ID_tramite = idtr$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_diferidos_eliminar` (IN `id` INT)  NO SQL
update diferidos
set estado = 0
where ID_tramite = id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_diferidos_insertar` (IN `idc` INT, IN `idm` VARCHAR(100), IN `idsec` INT, IN `idev` INT, IN `cardoc` VARCHAR(50), IN `idci` INT, IN `per` VARCHAR(50), IN `fecsol` DATETIME, IN `car` VARCHAR(50))  BEGIN
START TRANSACTION;


INSERT INTO tramites(`ID_carrera_materia`, `ID_seccion`, `ID_tipo_tramite`, `ID_eval`, `carnet_docente`, `autorizado`, `conteo`, `fechaRegistro`, `ID_ciclo`, `fechaEnvio`, `periodo`, `fechaSolicitud`)
SELECT ID_carrera_materia,idsec,3,idev,cardoc,0,0,NOW(),idci,NULL,per,fecsol FROM carrera_materia cm left join carreras c on cm.ID_carrera = c.ID_carrera 
where cm.ID_carrera = idc and cm.ID_materia =  idm;


INSERT INTO diferidos (`ID_tramite`,`carnet`)
VALUES(LAST_INSERT_ID(),car);


COMMIT;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_diferidos_modificar` (IN `idt` INT, IN `idc` INT, IN `idm` VARCHAR(100), IN `idsec` INT, IN `idev` INT, IN `cardoc` VARCHAR(50), IN `idci` INT, IN `per` VARCHAR(50), IN `fecsol` DATETIME, IN `car` VARCHAR(50), IN `est` BOOLEAN)  BEGIN
START TRANSACTION;

UPDATE tramites
SET ID_carrera_materia = (select ID_carrera_materia FROM carrera_materia cm left join carreras c on cm.ID_carrera = c.ID_carrera 
where cm.ID_carrera = idc and cm.ID_materia =  idm LIMIT 1), ID_seccion = idsec, ID_eval = idev,
carnet_docente = cardoc, ID_ciclo = idci, periodo = per,fechaSolicitud = fecsol, estado = est
WHERE ID_tramite = idt;

UPDATE diferidos
SET carnet = car, estado = est
WHERE ID_tramite = idt;


COMMIT;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_diferidos_MostrarGeneral` ()  NO SQL
SELECT t.ID_tramite, c.nombre_carrera, a.alumno, m.nombre_materia, e.nombre_escuela, s.seccion, dc.docente_nombres,ev.descripcion,t.periodo,t.fechaSolicitud
,t.autorizado
FROM tramites t  
left join diferidos d on t.id_tramite = d.ID_tramite 
left join carrera_materia cm on t.ID_carrera_materia = cm.ID_carrera_materia
left join alumnos a on a.carnet = d.carnet
left join materias m on m.ID_materia = cm.ID_materia 
left join carreras c on c.ID_carrera = cm.ID_carrera
left join catedras ct on ct.ID_catedra = cm.ID_catedra
left join escuela e on e.ID_escuela = ct.ID_escuela
left join secciones s on s.ID_seccion = t.ID_seccion
left join docentes dc on dc.carnet_docente = t.carnet_docente
left join evaluaciones ev on ev.ID_eval = t.ID_eval
left join tipotramite tt on tt.ID_tipo_tramite = t.ID_tipo_tramite
WHERE tt.ID_tipo_tramite = 3$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_diferidos_MostrarUno` (IN `id` INT)  NO SQL
select * 
from vw_diferidostodos
where IDTRAMITE = id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_docentes_consultarcarnet` (IN `car` VARCHAR(12))  NO SQL
SELECT * 
FROM docentes
where carnet_docente = car$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_docentes_ConsultarCRUD` ()  NO SQL
SELECT * FROM docentes$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_docentes_consultargeneral` ()  NO SQL
SELECT carnet_docente,correo_docente, docente_nombres, estado,  CONCAT(REPLACE(carnet_docente,'-','') ,' - ', docente_nombres) as carNom
FROM docentes WHERE estado = 1
order by docente_nombres ASC$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_docentes_eliminar` (IN `car` VARCHAR(12))  NO SQL
UPDATE docentes
SET estado = 0
WHERE  carnet_docente = car$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_docentes_insertar` (IN `car` VARCHAR(12), IN `noms` VARCHAR(100), IN `cor` VARCHAR(500))  NO SQL
insert into docentes (carnet_docente,docente_nombres,correo_docente)
values (car,noms,cor)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_docentes_modificar` (IN `car` VARCHAR(12), IN `noms` VARCHAR(100), IN `est` BOOLEAN, IN `cor` VARCHAR(500))  NO SQL
UPDATE docentes	 
SET docente_nombres = noms, estado = est, correo_docente = cor
WHERE carnet_docente = car$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_escuelas_eliminar` (IN `id` INT)  NO SQL
UPDATE escuela
SET estado = 0
WHERE id_escuela = id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_escuelas_insertar` (IN `nom` TEXT, IN `id_fac` INT)  NO SQL
INSERT INTO escuela(nombre_escuela,ID_facultad)
VALUES (nom,id_fac)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_escuelas_modificar` (IN `id` INT, IN `nom` TEXT, IN `id_fac` INT, IN `est` BOOLEAN)  NO SQL
UPDATE escuela	
SET nombre_escuela = nom, estado = est, id_facultad = id_fac
WHERE id_escuela = id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_escuelas_mostrargeneral` ()  NO SQL
SELECT a.*, b.nombre_facultad
FROM escuela as a left outer join facultades as b on a.ID_facultad=b.id_facultad
where a.estado = 1$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_escuelas_mostrargeneral_CRUD` ()  NO SQL
SELECT a.*, b.nombre_facultad
FROM escuela as a left outer join facultades as b on a.ID_facultad=b.id_facultad$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_escuelas_mostrarid` (IN `id` INT)  NO SQL
SELECT * 
FROM escuela
where id_escuela = id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_evaluaciones_eliminar` (IN `id` INT)  NO SQL
UPDATE evaluaciones
SET estado = 0
where ID_eval = id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_evaluaciones_insertar` (IN `des` TEXT)  NO SQL
INSERT INTO evaluaciones(descripcion)
VALUES (des)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_evaluaciones_modificar` (IN `des` TEXT, IN `est` BOOLEAN, IN `id` INT)  NO SQL
UPDATE evaluaciones	
SET descripcion = des, estado = est
where ID_eval = id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_evaluaciones_mostrargeneral` ()  NO SQL
SELECT * 
FROM evaluaciones
where estado = 1$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_evaluaciones_mostrargeneral_CRUD` ()  NO SQL
SELECT * 
FROM evaluaciones$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_evaluaciones_mostrarid` (IN `id` INT)  NO SQL
SELECT * 
FROM evaluaciones 
where ID_eval = id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_facultades_eliminar` (IN `id` INT)  NO SQL
UPDATE facultades
SET estado = 0
WHERE id_facultad = id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_facultades_insertar` (IN `nom` VARCHAR(100))  NO SQL
INSERT INTO facultades(nombre_facultad)
VALUES (nom)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_facultades_modificar` (IN `nom` TEXT, IN `est` BOOLEAN, IN `id` INT)  NO SQL
UPDATE facultades	
SET nombre_facultad = nom, estado = est
WHERE id_facultad = id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_facultades_mostrargeneral` ()  NO SQL
SELECT * 
FROM facultades$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_facultades_mostrarid` (IN `id` INT)  NO SQL
SELECT * 
FROM facultades 
where id_facultad = id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_login2` (IN `usu` VARCHAR(100), IN `cla` VARCHAR(200))  NO SQL
SELECT ID_usuario, usuarios.nombre, usuarios.apellido, usuarios.id_rol, usuarios.correo, usuarios.estado
FROM usuarios
WHERE usu = usuario AND cla = CAST(AES_DECRYPT(clave,'tesis')as char(100)) AND estado = 1$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_materias_eliminar` (IN `id` VARCHAR(12))  NO SQL
UPDATE materias 
SET estado = 0
WHERE  ID_materia = id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_materias_insertar` (IN `id` VARCHAR(12), IN `nom` VARCHAR(100))  NO SQL
INSERT INTO materias (ID_materia,nombre_materia)
VALUES (id,nom)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_materias_modificar` (IN `id` VARCHAR(12), IN `nom` VARCHAR(100), IN `est` BOOLEAN)  NO SQL
UPDATE materias 
SET   nombre_materia = nom, estado =  est
WHERE  ID_materia = id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_materias_mostrargeneral` ()  NO SQL
SELECT *
FROM materias
where estado = 1$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_materias_mostrargeneral_CRUD` ()  NO SQL
SELECT *
FROM materias$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_materias_mostrarID` (IN `id` VARCHAR(12))  NO SQL
SELECT *
FROM materias
WHERE  ID_materia = id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_menu_eliminar` (IN `id` INT)  NO SQL
UPDATE menu
SET estado = 0
where id_menu = id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_menu_insertar` (IN `m` TEXT, IN `i` TEXT)  NO SQL
INSERT INTO menu(menu,icono)
VALUES (m,i)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_menu_modificar` (IN `m` TEXT, IN `i` TEXT, IN `id` INT)  NO SQL
UPDATE menu	
SET  menu = m, icono = i
where id_menu = id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_menu_mostrargeneral` ()  NO SQL
SELECT * 
FROM menu$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_menu_mostrarid` (IN `id` INT)  NO SQL
SELECT * 
FROM menu 
where id_menu = id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_opciones_mostrarSegunRol` (IN `idr` INT)  NO SQL
SELECT 
	 om.id_opcion,
    opcion,
    CASE WHEN id_rol IS NULL THEN 0 ELSE id_rol END AS id_rol, 
    CASE WHEN acceso IS NULL THEN 0 ELSE acceso END AS acceso,
    CASE WHEN url IS NULL THEN 0 ELSE url END AS url,
    CASE WHEN id_rol IS NULL OR (id_rol <> idr) THEN 0 ELSE 1 END AS activo 
    FROM opcionmenu om 
    LEFT JOIN rol_menu rm ON rm.id_opcion = om.id_opcion$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_opcionmenu_eliminar` (IN `id` INT)  NO SQL
UPDATE opcionmenu
SET estado = 0
where id_opcion = id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_opcionmenu_insertar` (IN `opc` TEXT, IN `u_r_l` LONGTEXT, IN `idm` INT)  NO SQL
INSERT INTO opcionmenu(opcion,url,id_menu)
VALUES (opc,u_r_l,idm)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_opcionmenu_modificar` (IN `opc` TEXT, IN `u_r_l` TEXT, IN `idm` INT, IN `est` BOOLEAN, IN `id` INT)  NO SQL
UPDATE opcionmenu	
SET  opcion = opc, url = u_r_l, id_menu = idm, estado = est 
where id_opcion = id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_opcionmenu_mostrargeneral` ()  NO SQL
SELECT * 
FROM opcionmenu$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_opcionmenu_mostrarid` (IN `id` INT)  NO SQL
SELECT * 
FROM opcionmenu 
where id_opcion = id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_roles_eliminar` (IN `id` INT)  NO SQL
UPDATE roles
SET estado = 0
where id_rol = id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_roles_insertar` (IN `r` TEXT)  NO SQL
INSERT INTO roles(rol)
VALUES (r)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_roles_modificar` (IN `r` TEXT, IN `est` BOOLEAN, IN `id` INT)  NO SQL
UPDATE roles	
SET  rol = r, estado = est 
where id_rol = id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_roles_mostrargeneral` ()  NO SQL
SELECT * 
FROM roles$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_roles_mostrarid` (IN `id` INT)  NO SQL
SELECT * 
FROM roles 
where id_rol = id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_roles_ultimo` ()  NO SQL
SELECT MAX(`id_rol`) AS id FROM roles$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_rol_menu_insertar` (IN `idop` INT, IN `acc` BOOLEAN, IN `idr` INT)  NO SQL
INSERT INTO rol_menu(id_opcion,acceso,id_rol)
VALUES (idop,acc,idr)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_rol_menu_mostrarsegunRol` (IN `idr` INT)  NO SQL
SELECT * 
FROM rol_menu 
where id_rol = idr and acceso=1$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_secciones_eliminar` (IN `id` INT)  NO SQL
UPDATE secciones
SET estado = 0
where ID_seccion = id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_secciones_insertar` (IN `sec` TEXT)  NO SQL
INSERT INTO secciones(seccion)
VALUES (sec)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_secciones_modificar` (IN `est` BOOLEAN, IN `sec` TEXT, IN `id` INT)  NO SQL
UPDATE secciones	
SET  estado = est, seccion = sec
where ID_seccion = id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_secciones_mostrargeneral` ()  NO SQL
SELECT * 
FROM secciones
where estado = 1$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_secciones_mostrargeneral_CRUD` ()  NO SQL
SELECT * 
FROM secciones$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_secciones_mostrarid` (IN `id` INT)  NO SQL
SELECT * 
FROM secciones 
where ID_seccion = id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_seccion_ver_uno` (IN `idsec` INT)  NO SQL
SELECT ID_seccion, seccion, estado FROM secciones WHERE ID_seccion = idsec$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_tipoactividad_eliminar` (IN `id` INT)  NO SQL
UPDATE tipoactividad
SET estado = 0
where ID_tipo_actividad = id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_tipoactividad_insertar` (IN `nom` TEXT)  NO SQL
INSERT INTO tipoactividad(nombreActividad)
VALUES (nom)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_tipoactividad_modificar` (IN `est` BOOLEAN, IN `nom` TEXT, IN `id` INT)  NO SQL
UPDATE tipoactividad	
SET  estado = est, nombreActividad = nom
where ID_tipo_actividad = id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_tipoactividad_mostrargeneral` ()  NO SQL
SELECT * 
FROM tipoactividad$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_tipoactividad_mostrarid` (IN `id` INT)  NO SQL
SELECT * 
FROM tipoactividad 
where ID_tipo_actividad = id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_tipotramite_eliminar` (IN `id` INT)  NO SQL
UPDATE tipotramite
SET estado = 0
where ID_tipo_actividad = id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_tipotramite_insertar` (IN `nom` TEXT)  NO SQL
INSERT INTO tipotramite(nombreTramite)
VALUES (nom)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_tipotramite_modificar` (IN `est` BOOLEAN, IN `nom` TEXT, IN `id` INT)  NO SQL
UPDATE tipotramite	
SET  estado = est, nombreActividad = nom
where ID_tipo_actividad = id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_tipotramite_mostrargeneral` ()  NO SQL
SELECT * 
FROM tipotramite$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_tipotramite_mostrarid` (IN `id` INT)  NO SQL
SELECT * 
FROM tipotramite 
where ID_tipo_tramite = id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_tramites_correccion_insert` (IN `idCarMa` INT, IN `idSec` INT, IN `idEval` INT, IN `CarnDoce` VARCHAR(12), IN `fRegist` DATETIME, IN `idCiclo` INT, IN `fEnvio` DATETIME, IN `perio` INT(11), IN `fSolici` DATETIME)  NO SQL
INSERT INTO tramites
( 
 ID_carrera_materia, 
 ID_seccion, 
 estado, 
 ID_tipo_tramite, 
 ID_eval, 
 carnet_docente, 
 autorizado, 
 conteo, 
 fechaRegistro, 
 ID_ciclo, 
 fechaEnvio, 
 periodo, 
 fechaSolicitud)
VALUES 
(
 idCarMa,
 idSec,
 1,
 2,
 idEval,
 CarnDoce,
 1,
 1,
 fRegist,
 idCiclo,
 fEnvio,
 perio,
 fSolici)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_tramite_mostrarMaximo` ()  NO SQL
SELECT case when (max(ID_tramite)<1 OR max(ID_tramite) IS NULL )then 1 else max(ID_tramite)+1 end as max FROM `tramites`$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_urlSec_ver` (IN `idrol` INT)  NO SQL
SELECT CASE WHEN om.url IS NULL THEN'http://localhost/Cisneros/Tesis/index.php?c=user' ELSE om.url END url 

FROM rol_menu rm
inner join opcionmenu om on om.id_opcion = rm.id_opcion
left join menu m on m.id_menu = om.id_menu
where  id_rol = idrol
ORDER BY m.menu ASC, rm.id_opcion asc$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_usuarios_eliminar` (IN `id` INT)  NO SQL
UPDATE usuarios
SET estado = 0
WHERE ID_usuario = id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_usuarios_insertar` (IN `nom` VARCHAR(50), IN `ape` VARCHAR(50), IN `cor` VARCHAR(500), IN `usuario` VARCHAR(20), IN `cla` VARCHAR(100), IN `est` BOOLEAN, IN `nivel` INT, IN `rol` INT)  NO SQL
INSERT INTO usuarios(nombre, apellido,correo,usuario,clave,estado,nivel,id_rol)
VALUES(nom,ape,cor,usuario,AES_ENCRYPT(cla,'tesis'),est,nivel,rol)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_usuarios_modificar` (IN `nom` VARCHAR(50), IN `ape` VARCHAR(50), IN `cor` VARCHAR(100), IN `clav` VARCHAR(100), IN `usu` VARCHAR(100), IN `id` INT)  NO SQL
UPDATE usuarios
SET nombre = nom, apellido = ape, correo = cor, usuario=usu, clave = AES_ENCRYPT(clav,'tesis'), fecha_modificacion = NOW()
WHERE ID_usuario = id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_usuarios_momenu` (IN `idrol` INT)  NO SQL
SELECT m.menu, m.icono, om.opcion, CASE WHEN om.url IS NULL THEN'http://localhost/tesis/index.php?c=user' ELSE om.url END url, rm.acceso, id_rol 
FROM rol_menu rm
inner join opcionmenu om on om.id_opcion = rm.id_opcion
left join menu m on m.id_menu = om.id_menu
where rm.acceso = 1 and id_rol = idrol and om.estado=1
ORDER BY m.menu ASC, rm.id_opcion asc$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_usuarios_mostrar` ()  NO SQL
select ID_usuario, nombre, apellido, correo,usuario, clave, fecha_creacion, estado, nivel  
FROM usuarios where estado=1$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_usuarios_mostrar_uno` (IN `id` INT)  NO SQL
SELECT u.ID_usuario, u.nombre, u.apellido, u.correo, u.usuario,  aes_decrypt(u.clave, 'tesis') as clave, u.estado, u.fecha_creacion, u.nivel, u.fecha_modificacion, u.id_rol  FROM usuarios u WHERE `ID_usuario` = id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_usuarios_opc_menu` ()  NO SQL
select id_opcion, opcion, estado from opcionmenu WHERE estado=1$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_usuario_Modif_Rol` (IN `id` INT, IN `rol` INT)  NO SQL
UPDATE usuarios
SET id_rol = rol, fecha_modificacion = NOW()
WHERE ID_usuario = id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_usuario_ultimo` ()  NO SQL
SELECT MAX(`ID_usuario`) AS id FROM usuarios$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_validar_login` (IN `usu` VARCHAR(100), IN `cla` VARCHAR(200))  NO SQL
SELECT COUNT(*)
FROM usuarios
WHERE usu = usuario AND cla = CAST(AES_DECRYPT(clave,'tesis')as char(100))$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_vallida_carnet` (IN `carn` VARCHAR(100))  NO SQL
SELECT COUNT(alumno) AS CONTEO FROM alumnos WHERE carnet=carn$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_vwcarreras_Mostrar` ()  NO SQL
SELECT  * 
FROM vw_mostrarCarreras$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `alumnos`
--

CREATE TABLE IF NOT EXISTS `alumnos` (
  `carnet` char(12) NOT NULL,
  `alumno` varchar(50) DEFAULT NULL,
  `estado` tinyint(1) DEFAULT '1',
  `correo_alumno` varchar(500) NOT NULL,
  PRIMARY KEY (`carnet`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELATIONSHIPS FOR TABLE `alumnos`:
--

--
-- Dumping data for table `alumnos`
--

INSERT INTO `alumnos` (`carnet`, `alumno`, `estado`, `correo_alumno`) VALUES
('1233212313', 'ÃƒÂ¡grÃƒÂ©gÃƒÂ¡rÃƒÂºÃƒÂ³ÃƒÂ±', 1, '132132'),
('1700142006', 'KARELYN VERENICE HERNANDEZ FLORES', 1, '1700142006@mail.utec.edu.sv'),
('1701632004', 'JENNIFER ROSEMARY DIAZ PARADA', 1, '1701632004@mail.utec.edu.sv'),
('1701642006', 'RUTH VANESSA AZUCAR VASQUEZ', 1, '1701642006@mail.utec.edu.sv'),
('1702602006', 'ROXANA LISSETH CORNEJO LOPEZ', 1, '1702602006@mail.utec.edu.sv'),
('1703742000', 'CLAUDIA GUADALUPE ECHEVERRIA RAMOS', 1, '1703742000@mail.utec.edu.sv'),
('1705412006', 'JACKELYN GUADALUPE HENRIQUEZ MENA', 1, '1705412006@mail.utec.edu.sv'),
('1708052006', 'NATHANAEL HERNANDEZ LARIOS', 1, '1708052006@mail.utec.edu.sv'),
('1713402006', 'KARLA LORENA GARCIA AGUILLON', 1, '1713402006@mail.utec.edu.sv'),
('1715532006', 'MARLENE ELIZABETH GARCIA MUNOZ', 1, '1715532006@mail.utec.edu.sv'),
('1716152004', 'LINDA LEONOR HERNANDEZ ORELLANA', 1, '1716152004@mail.utec.edu.sv'),
('1724562006', 'JOSUE JONATHAN LANDAVERDE LOPEZ', 1, '1724562006@mail.utec.edu.sv'),
('1728982005', 'HUGO STANLEY AGUILAR SORIANO', 1, '1728982005@mail.utec.edu.sv'),
('1729112006', 'GILBERTO ALEXANDER ALVARADO MARINERO', 1, '1729112006@mail.utec.edu.sv'),
('1730342002', 'ZULEYMA BEATRIZ DERAS CARDOZA', 1, '1730342002@mail.utec.edu.sv'),
('1731852000', 'VERONICA ELIZABETH CASTELLANOS ANGEL', 1, '1731852000@mail.utec.edu.sv'),
('1736982004', 'JUNIA SUSANA HERNANDEZ MEJIA', 1, '1736982004@mail.utec.edu.sv'),
('1739242006', 'OSCAR JESE AQUINO CABEZAS', 1, '1739242006@mail.utec.edu.sv'),
('2123132132', 'ÃƒÂ¡ÃƒÂ©ÃƒÂ­ÃƒÂ³ÃƒÂºÃƒÂ±', 1, 'asdasdad'),
('2500022007', 'SANDRA YANETH BERNAL GOMEZ', 1, '2500022007@mail.utec.edu.sv'),
('2500152007', 'ELIAS NEFTALI CONSUEGRA GARCIA', 1, '2500152007@mail.utec.edu.sv'),
('2500692007', 'KAREN LETICIA CORDOVA LOPEZ', 1, '2500692007@mail.utec.edu.sv'),
('2503522007', 'MARYCRUZ HERNANDEZ CENTENO', 1, '2503522007@mail.utec.edu.sv'),
('2504202007', 'DARWIN ISAI JERONIMO GUZMAN', 1, '2504202007@mail.utec.edu.sv'),
('2504402007', 'MAYRA ABIGAIL AGUILAR VELASQUEZ', 1, '2504402007@mail.utec.edu.sv'),
('2505002007', 'RUTH OLINDA GUEVARA QUINTANILLA', 1, '2505002007@mail.utec.edu.sv'),
('2505492007', 'DALILA MARISOL ALFARO MENDEZ', 1, '2505492007@mail.utec.edu.sv'),
('2506562007', 'GLADIS ESMERALDA CORNEJO VASQUEZ', 1, '2506562007@mail.utec.edu.sv'),
('2507602006', 'EDWIN ALFREDO HERNANDEZ CORTEZ', 1, '2507602006@mail.utec.edu.sv'),
('2507652007', 'WENDY BEATRIZ HERNANDEZ RIVERA', 1, '2507652007@mail.utec.edu.sv'),
('2509922007', 'CARLOS MAURICIO AGUIRRE CARRILLO', 1, '2509922007@mail.utec.edu.sv'),
('2510012007', 'CARLOS EMILIO AMAYA DIAZ', 1, '2510012007@mail.utec.edu.sv'),
('2510192007', 'ELSY ESPERANZA ACOSTA DIAZ', 1, '2510192007@mail.utec.edu.sv'),
('2510782007', 'GLORIA VANESSA LÒPEZ GALDÀMEZ', 1, '2510782007@mail.utec.edu.sv'),
('2512052007', 'JOSE ROBERTO LOPEZ CAMPOS', 1, '2512052007@mail.utec.edu.sv'),
('2513772007', 'FRANKLIN JAVIER CRUZ ALDANA', 1, '2513772007@mail.utec.edu.sv'),
('2514112007', 'BETSY NATHALIA ESCOBAR MENENDEZ', 1, '2514112007@mail.utec.edu.sv'),
('2514602007', 'YONATHAN DAVID CEDILLOS LOPEZ', 1, '2514602007@mail.utec.edu.sv'),
('2514872007', 'VICTOR MANUEL EDGARDO GALAN ALEGRIA', 1, '2514872007@mail.utec.edu.sv'),
('2515762007', 'CHRISTIAN JONATHAN LOPEZ ALFARO', 1, '2515762007@mail.utec.edu.sv'),
('2516282007', 'EDGAR RENE GARCIA SURIA', 1, '2516282007@mail.utec.edu.sv'),
('2518412007', 'CESIA JOHANNA CAROLINA ESCOBAR GONZALEZ', 1, '2518412007@mail.utec.edu.sv'),
('2518982007', 'YOANY EUNICE ESCOBAR SERRANO', 1, '2518982007@mail.utec.edu.sv'),
('2519972007', 'JULIO ADALBERTO ESCOBAR GUZMÀN', 1, '2519972007@mail.utec.edu.sv'),
('2522482007', 'ELMERSON WILFREDO AVALOS VASQUEZ', 1, '2522482007@mail.utec.edu.sv'),
('2523502007', 'EDUARDO ERNESTO GUADRON PALMA', 1, '2523502007@mail.utec.edu.sv'),
('2524372007', 'KAREN ELIZABETH HERNANDEZ MARROQUIN', 1, '2524372007@mail.utec.edu.sv'),
('2524962007', 'KARLA BEATRIZ ALVARADO RODAS', 1, '2524962007@mail.utec.edu.sv'),
('2525332007', 'ALBA LISSETH ASCENCIO SERPAS', 1, '2525332007@mail.utec.edu.sv'),
('2525542017', 'Gabriel Omar Mendez Diaz', 1, '2525542017@mail.utec.edu.sv'),
('2525592007', 'CLAUDIA BEATRIZ CABEZAS ALVARADO', 1, '2525592007@mail.utec.edu.sv'),
('2526622007', 'JESSICA LISSETTE IRAHETA PUERTAS', 1, '2526622007@mail.utec.edu.sv'),
('2527942007', 'HERBERT EDGARDO BARRERA GOMEZ', 1, '2527942007@mail.utec.edu.sv'),
('2528422007', 'RUTH ELIZA CHEVEZ LOPEZ', 1, '2528422007@mail.utec.edu.sv'),
('2529142007', 'DANIA BEATRIZ  ALEMAN GUZMAN', 1, '2529142007@mail.utec.edu.sv'),
('2529202007', 'CLAUDIA KARINA AQUINO ORELLANA', 1, '2529202007@mail.utec.edu.sv'),
('2531002007', 'KENIA VERONICA GUIROLA MELARA', 1, '2531002007@mail.utec.edu.sv'),
('2531322007', 'JEOVANY FRANCISCO DUARTE PACHECO', 1, '2531322007@mail.utec.edu.sv'),
('2531332007', 'ANA GRISELDA GUARDADO VILLAFRANCO', 1, '2531332007@mail.utec.edu.sv'),
('2533962007', 'GLORIA STEFFANY GUARDADO VILLAFRANCO', 1, '2533962007@mail.utec.edu.sv'),
('2534822007', 'JOSE ARMANDO HERNANDEZ LUNA', 1, '2534822007@mail.utec.edu.sv'),
('2536352007', 'VERENISE ANTONIETA LUNA PEREZ', 1, '2536352007@mail.utec.edu.sv'),
('2540532007', 'IRENE NATALY CASTRO CALDERON', 1, '2540532007@mail.utec.edu.sv'),
('2541402007', 'JOSE EFRAIN HUEZO MERLOS', 1, '2541402007@mail.utec.edu.sv'),
('2541812006', 'RENE DAVID CHINCHILLA TURCIOS', 1, '2541812006@mail.utec.edu.sv'),
('2542612007', 'FRANCISCO ERNESTO GARCIA AMAYA', 1, '2542612007@mail.utec.edu.sv'),
('2542862007', 'MARCELA ALEJANDRA CORPENO CUELLAR', 1, '2542862007@mail.utec.edu.sv'),
('2546292007', 'MARIO ERNESTO GUARDADO TEJADA', 1, '2546292007@mail.utec.edu.sv'),
('2546782007', 'ORLANDO DE JESUS GIRON MARROQUIN', 1, '2546782007@mail.utec.edu.sv'),
('2550242006', 'INDIRA ABIGAIL GALLEGOS NATHAREN', 1, '2550242006@mail.utec.edu.sv'),
('2554482016', 'Luis Eduardo Herrera Dimas', 1, '2525542017@mail.utec.edu.sv'),
('2712342018', 'JUAN PEREZ', 1, 'NO TIENE'),
('2740372016', 'GERMAN VLADIMIR CISNEROS LEMUS', 1, '2740372016@mail.utec.edu.sv '),
('2746522016', 'HECTOR JONATHAN GARCIA ORTIZ', 1, '2746522016@mail.utec.edu.sv '),
('2759792016', 'JÃƒÂ‰SSICA LISSETTE INTERIANO SEGURA ', 1, '2759792016@mail.utec.edu.sv'),
('5465132132', 'xfvxvxcvx', 1, 'asdadasd');

-- --------------------------------------------------------

--
-- Table structure for table `carreras`
--

CREATE TABLE IF NOT EXISTS `carreras` (
  `ID_carrera` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombre_carrera` varchar(500) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL,
  `estado` tinyint(1) DEFAULT '1',
  `Codigo_carrera` int(2) NOT NULL,
  PRIMARY KEY (`ID_carrera`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

--
-- RELATIONSHIPS FOR TABLE `carreras`:
--

--
-- Dumping data for table `carreras`
--

INSERT INTO `carreras` (`ID_carrera`, `nombre_carrera`, `estado`, `Codigo_carrera`) VALUES
(1, 'TÃƒÂ©cnico en ingenieria de Software', 1, 27),
(2, 'Lic. en Admin de Empresas con ÃƒÂ©nfasis  en ComputaciÃƒÂ³n', 1, 15),
(3, 'IngenierÃƒÂ­a en Sistemas', 1, 25),
(4, 'Licenciatura en InformÃƒÂ¡tica', 1, 0),
(5, 'TÃƒÂ©cnico en hardware', 1, 0),
(6, 'IngenierÃƒÂ­a civil', 1, 0),
(7, 'IngenierÃƒÂ­a Industrial', 1, 0),
(8, 'TÃƒÂ©cnico en Redes', 1, 0),
(9, 'asdadad', 1, 27),
(13, 'tÃƒÂ©steo', 1, 4);

--
-- Triggers `carreras`
--
DELIMITER $$
CREATE TRIGGER `TRG_carrera_eliminar` AFTER UPDATE ON `carreras` FOR EACH ROW update carrera_materia  set  estado = new.estado  where ID_carrera = old.id_Carrera
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `carrera_materia`
--

CREATE TABLE IF NOT EXISTS `carrera_materia` (
  `ID_carrera_materia` bigint(20) NOT NULL AUTO_INCREMENT,
  `ID_carrera` bigint(20) DEFAULT NULL,
  `ID_catedra` bigint(20) DEFAULT NULL,
  `ID_materia` char(10) DEFAULT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ID_carrera_materia`),
  KEY `ID_carrera` (`ID_carrera`) USING BTREE,
  KEY `ID_materia` (`ID_materia`) USING BTREE,
  KEY `ID_catedra` (`ID_catedra`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=latin1;

--
-- RELATIONSHIPS FOR TABLE `carrera_materia`:
--   `ID_carrera`
--       `carreras` -> `ID_carrera`
--   `ID_materia`
--       `materias` -> `ID_materia`
--   `ID_catedra`
--       `catedras` -> `ID_catedra`
--

--
-- Dumping data for table `carrera_materia`
--

INSERT INTO `carrera_materia` (`ID_carrera_materia`, `ID_carrera`, `ID_catedra`, `ID_materia`, `estado`) VALUES
(1, 1, 1, 'ALG1-E', 1),
(14, 3, 2, 'DSIW1', 1),
(16, 1, 1, 'DPWEB', 1),
(17, 3, 1, 'DSIW1', 1),
(18, 4, 1, 'DSIW2', 1),
(19, 1, 2, 'PROGRA-1', 1),
(20, 3, 2, 'PROGRA-1', 1),
(21, 4, 2, 'PROGRA-1', 1),
(22, 1, 2, 'PROGRA-2', 1),
(23, 3, 2, 'PROGRA-2', 1),
(24, 4, 2, 'PROGRA-2', 1),
(25, 1, 2, 'PROGRA-3', 1),
(26, 3, 2, 'PROGRA-3', 1),
(27, 4, 2, 'PROGRA-3', 1),
(28, 5, 1, 'REDES-I', 1),
(29, 8, 1, 'REDES-II', 1),
(30, 5, 1, 'REDES-I', 1),
(31, 5, 1, 'REDES-II', 1),
(32, 1, 2, 'ALG-II', 1),
(33, 3, 2, 'ALG-II', 1),
(34, 4, 2, 'ALG-II', 1),
(35, 3, 3, 'SO-SERV', 1),
(36, 3, 3, 'SEG-RED', 1);

-- --------------------------------------------------------

--
-- Table structure for table `catedras`
--

CREATE TABLE IF NOT EXISTS `catedras` (
  `ID_catedra` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombre_catedra` varchar(100) NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  `ID_escuela` bigint(20) NOT NULL,
  PRIMARY KEY (`ID_catedra`),
  KEY `ID_escuela` (`ID_escuela`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- RELATIONSHIPS FOR TABLE `catedras`:
--   `ID_escuela`
--       `escuela` -> `ID_escuela`
--

--
-- Dumping data for table `catedras`
--

INSERT INTO `catedras` (`ID_catedra`, `nombre_catedra`, `estado`, `ID_escuela`) VALUES
(1, 'CÃƒÂ¡tedra en redes', 1, 1),
(2, 'CÃƒÂ¡tedra en programaciÃƒÂ³n', 1, 1),
(3, 'CÃƒÂ¡tedra en infraestructura', 1, 2),
(4, 'CÃƒÂ¡tedra de Base de datos', 1, 1),
(5, 'CÃƒÂ¡tedra de desarrollo web', 1, 1),
(6, 'CÃƒÂ¡tedra de FÃƒÂ­sica', 1, 1),
(7, 'CARRERA NUEVA', 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `ciclos`
--

CREATE TABLE IF NOT EXISTS `ciclos` (
  `ID_ciclo` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombre_ciclo` varchar(50) NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ID_ciclo`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- RELATIONSHIPS FOR TABLE `ciclos`:
--

--
-- Dumping data for table `ciclos`
--

INSERT INTO `ciclos` (`ID_ciclo`, `nombre_ciclo`, `estado`) VALUES
(1, '01-2018', 0),
(2, '02-2018', 1),
(3, '01-2019', 1),
(4, '02-2019', 1),
(5, '01-2020', 0),
(6, '02-2020', 0),
(7, '01-2021', 0),
(8, '02-2021', 0),
(9, '01-2022', 0),
(10, '02-2022', 0),
(11, '01-2022', 0);

-- --------------------------------------------------------

--
-- Table structure for table `correcciones`
--

CREATE TABLE IF NOT EXISTS `correcciones` (
  `ID_correccion` bigint(20) NOT NULL AUTO_INCREMENT,
  `ID_tramite` bigint(20) NOT NULL,
  `notaActual` float NOT NULL,
  `notaCorregida` float NOT NULL,
  `motivoCorreccion` varchar(200) NOT NULL,
  `estadoCorreccion` varchar(50) NOT NULL,
  `carnet` char(12) NOT NULL,
  `observaciones` text NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  `personaRecibe` varchar(100) NOT NULL,
  `fechaRecibe` datetime NOT NULL,
  PRIMARY KEY (`ID_correccion`),
  KEY `ID_tramite` (`ID_tramite`),
  KEY `carnet` (`carnet`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;

--
-- RELATIONSHIPS FOR TABLE `correcciones`:
--   `ID_tramite`
--       `tramites` -> `ID_tramite`
--   `carnet`
--       `alumnos` -> `carnet`
--

--
-- Dumping data for table `correcciones`
--

INSERT INTO `correcciones` (`ID_correccion`, `ID_tramite`, `notaActual`, `notaCorregida`, `motivoCorreccion`, `estadoCorreccion`, `carnet`, `observaciones`, `estado`, `personaRecibe`, `fechaRecibe`) VALUES
(12, 8, 8, 10, 'FALTO AGREGAR ACTIVIDAD', 'En proceso', '2759792016', 'N/A', 0, 'GermanCisneros          ', '2018-11-07 00:00:00'),
(13, 9, 9, 10, 'ERROR EN DECIMALES', 'Rechazado', '2500022007', 'N/A', 1, '          German Cisneros          ', '2018-11-08 00:00:00'),
(15, 11, 8, 10, 'ERRONEA SUMATORIA', 'Aprobado', '1731852000', 'N/A', 1, 'GermanCisneros          ', '2018-11-09 00:00:00'),
(17, 13, 7, 9, 'CORRECCION', 'En proceso', '1731852000', 'N/A', 1, '          German Cisneros          ', '2018-10-31 00:00:00'),
(18, 21, 10, 9, 'NO HABIA PRESENTADO TRABAJO', 'En proceso', '1731852000', 'NO HABÃA PRESENTADO TRABAJO', 0, '          German Cisneros          ', '2018-11-08 00:00:00'),
(19, 22, 10, 10, 'NUEVA CORRECCION DE NOTAS', 'Rechazado', '1731852000', 'NO HABÃA PRESENTADO TRABAJO', 1, '          German Cisneros          ', '2018-11-02 00:00:00'),
(20, 23, 8, 10, 'NO SE ADICIONO TAREA', 'Aprobado', '1701632004', 'NO SE ADICIONO', 1, '          German Cisneros          ', '2018-11-21 00:00:00'),
(24, 27, 7, 8.5, 'NO SE ADICIONO', 'Aprobado', '1731852000', 'NO SE ADICIONO', 1, 'GermanCisneros          ', '2018-11-08 00:00:00'),
(25, 35, 10, 9, 'MOTIVO', 'En proceso', '1731852000', 'N/A', 1, '          German Cisneros          ', '2018-11-01 00:00:00'),
(26, 39, 8, 10, 'N/A', 'Rechazado', '1731852000', 'N/A', 1, '          Jessica Interiano          ', '2018-11-08 00:00:00'),
(27, 43, 4, 8, '--', 'En proceso', '2759792016', '-', 1, '          Jessica Interiano          ', '2018-11-08 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `diferidos`
--

CREATE TABLE IF NOT EXISTS `diferidos` (
  `ID_diferido` bigint(20) NOT NULL AUTO_INCREMENT,
  `ID_tramite` bigint(20) NOT NULL,
  `carnet` char(12) NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ID_diferido`),
  KEY `ID_tramite` (`ID_tramite`),
  KEY `carnet` (`carnet`),
  KEY `carnet_2` (`carnet`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=latin1;

--
-- RELATIONSHIPS FOR TABLE `diferidos`:
--   `ID_tramite`
--       `tramites` -> `ID_tramite`
--   `carnet`
--       `alumnos` -> `carnet`
--

--
-- Dumping data for table `diferidos`
--

INSERT INTO `diferidos` (`ID_diferido`, `ID_tramite`, `carnet`, `estado`) VALUES
(19, 1, '1700142006', 0),
(20, 2, '2531002007', 0),
(21, 3, '2546292007', 0),
(22, 6, '2529142007', 0),
(23, 7, '2531322007', 0),
(24, 28, '2529202007', 0),
(25, 32, '2542612007', 0),
(26, 36, '2533962007', 0),
(27, 37, '2531002007', 0),
(28, 42, '2542612007', 0),
(33, 51, '2746522016', 1),
(34, 52, '2759792016', 1),
(35, 53, '2759792016', 1),
(37, 55, '2759792016', 1),
(38, 56, '2759792016', 1);

-- --------------------------------------------------------

--
-- Table structure for table `docentes`
--

CREATE TABLE IF NOT EXISTS `docentes` (
  `carnet_docente` char(12) NOT NULL,
  `docente_nombres` varchar(100) DEFAULT NULL,
  `estado` tinyint(1) DEFAULT '1',
  `correo_docente` varchar(500) NOT NULL,
  PRIMARY KEY (`carnet_docente`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELATIONSHIPS FOR TABLE `docentes`:
--

--
-- Dumping data for table `docentes`
--

INSERT INTO `docentes` (`carnet_docente`, `docente_nombres`, `estado`, `correo_docente`) VALUES
('0000000906', 'JosÃƒÂ© Guillermo Rivera Pleitez', 1, 'guillermo.rivera@utec.edu.sv'),
('00000301', 'MarÃƒÂ­a Eva Carranza', 1, '-'),
('0121031032', 'tÃƒÂ©st', 1, 'TEST'),
('0123132132', 'teÃƒÂ©steo', 1, 'asdadada'),
('10012018', 'ING EDWIN ALBERTO CALLEJAS PRUEBA', 0, '10012018@mail.utec.edu.sv'),
('10022018', 'ING WILLIAM ALEXANDER COREAS RODRIGUEZ', 0, '10022018@mail.utec.edu.sv'),
('10032018', 'ING JUAN CARLOS CAMPOS RIVERA', 0, '10032018@mail.utec.edu.sv'),
('10042018', 'ING EDWIN OSVALDO MELGAR FUENTES', 0, '10042018@mail.utec.edu.sv'),
('10052018', 'ING JOSE ORLANDO GIRON BARRERA', 0, '10052018@mail.utec.edu.sv'),
('10062018', 'ING NOE ELIAS RODRIGUEZ MERLOS', 0, '10062018@mail.utec.edu.sv'),
('10072018', 'ING MARVIN FREDY VILLALOBOS MARTINEZ', 0, '10072018@mail.utec.edu.sv'),
('10082018', 'ING TOMAS EDUARDO URBINA', 0, '10082018@mail.utec.edu.sv'),
('10092018', 'ING MARLON GIOVANNI MARTINEZ PEREZ', 0, '10092018@mail.utec.edu.sv'),
('10102018', 'LIC JORGE ALBERTO ACEVEDO DIAZ', 0, '10102018@mail.utec.edu.sv'),
('10112018', 'ING JORGE EDWIN MACHADO LEIVA', 0, '10112018@mail.utec.edu.sv'),
('10122018', 'ING JOSE OSWALDO BARRERA MONTES', 0, '10122018@mail.utec.edu.sv'),
('10132018', 'LIC SALVADOR SANDOVAL', 0, '10132018@mail.utec.edu.sv'),
('1212312333', 'asdadas', 1, 'asdadas'),
('123', 'VVVVVVVVVVVV', 1, 'VVVVVVVVVVVV'),
('1234', 'NUEVO DOCENTE', 1, 'NUEVO DOCENTE'),
('1472583699', 'rrrrrrrr', 1, 'rrrrrrrr'),
('3211313213', 'ÃƒÂ¡ÃƒÂ©ÃƒÂ­ÃƒÂ³ÃƒÂº', 1, 'asdad');

-- --------------------------------------------------------

--
-- Table structure for table `escuela`
--

CREATE TABLE IF NOT EXISTS `escuela` (
  `ID_escuela` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombre_escuela` varchar(500) DEFAULT NULL,
  `estado` tinyint(1) DEFAULT '1',
  `ID_facultad` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID_escuela`),
  KEY `ID_facultad` (`ID_facultad`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- RELATIONSHIPS FOR TABLE `escuela`:
--   `ID_facultad`
--       `facultades` -> `id_facultad`
--

--
-- Dumping data for table `escuela`
--

INSERT INTO `escuela` (`ID_escuela`, `nombre_escuela`, `estado`, `ID_facultad`) VALUES
(1, 'EscÃƒÂºÃƒÂ©la de InformÃƒÂ¡tica ', 1, 1),
(2, 'Escuela de Ciencias Aplicadas', 1, 1),
(3, 'ÃƒÂ¡ÃƒÂ©ÃƒÂ³ÃƒÂ±', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `evaluaciones`
--

CREATE TABLE IF NOT EXISTS `evaluaciones` (
  `ID_eval` bigint(20) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(50) NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ID_eval`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- RELATIONSHIPS FOR TABLE `evaluaciones`:
--

--
-- Dumping data for table `evaluaciones`
--

INSERT INTO `evaluaciones` (`ID_eval`, `descripcion`, `estado`) VALUES
(1, 'Primera EvaluaciÃƒÂ³n', 1),
(2, 'Segunda EvaluaciÃƒÂ³n', 1),
(3, 'Tercera EvaluaciÃƒÂ³n', 1),
(4, 'Cuarta EvaluaciÃƒÂ³n', 1),
(5, 'Quinta EvaluaciÃƒÂ³n', 1);

-- --------------------------------------------------------

--
-- Table structure for table `facultades`
--

CREATE TABLE IF NOT EXISTS `facultades` (
  `id_facultad` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombre_facultad` varchar(500) DEFAULT NULL,
  `estado` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id_facultad`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- RELATIONSHIPS FOR TABLE `facultades`:
--

--
-- Dumping data for table `facultades`
--

INSERT INTO `facultades` (`id_facultad`, `nombre_facultad`, `estado`) VALUES
(1, 'Facultad de informÃƒÂ¡tica y ciencias aplicadas', 1),
(2, 'Facultad de  Ciencas Empresariales', 1),
(3, 'FÃƒÂ¡cultÃƒÂ¡', 1);

-- --------------------------------------------------------

--
-- Table structure for table `materias`
--

CREATE TABLE IF NOT EXISTS `materias` (
  `ID_materia` char(15) NOT NULL,
  `nombre_materia` varchar(200) DEFAULT NULL,
  `estado` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`ID_materia`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELATIONSHIPS FOR TABLE `materias`:
--

--
-- Dumping data for table `materias`
--

INSERT INTO `materias` (`ID_materia`, `nombre_materia`, `estado`) VALUES
('ALG-II', 'ÃƒÂlgoritmos II', 1),
('ALG1-E', 'Algoritmos 1', 1),
('BD-I', 'Base de datos I', 1),
('BD-II', 'Base de datos II', 1),
('DPWEB', 'Desarrollo de la Plataforma Web', 1),
('DSIW1', 'Desarrollo de Sistemas InformÃƒÂ¡ticos Web 1', 1),
('DSIW2', 'Desarrollo de Sistemas InformÃƒÂ¡ticos Web 2', 1),
('ING-I', 'InglÃƒÂ©s', 1),
('PROGRA-1', 'ProgramaciÃƒÂ³n', 1),
('PROGRA-2', 'ProgramaciÃƒÂ³n II', 1),
('PROGRA-3', 'ProgramaciÃƒÂ³n III', 1),
('REDES-I', 'Redes I', 1),
('REDES-II', 'Redes II', 1),
('REDES-IIII', 'ÃƒÂ¡lgÃƒÂ³rÃƒÂ­c', 1),
('SEG-RED', 'Seguridad en redes InformÃƒÂ¡ticas', 1),
('SO-SERV', 'Sistemas operativos', 1);

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE IF NOT EXISTS `menu` (
  `id_menu` bigint(20) NOT NULL AUTO_INCREMENT,
  `menu` varchar(45) DEFAULT NULL,
  `icono` varchar(350) DEFAULT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_menu`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- RELATIONSHIPS FOR TABLE `menu`:
--

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id_menu`, `menu`, `icono`, `estado`) VALUES
(1, 'Mantenimientos ', 'fa fa-cog', 1),
(2, 'Procesos ', 'fa fa-repeat', 1),
(3, 'Reportes ', 'fa fa-file-pdf-o', 1),
(4, 'Extras ', 'fa  fa-desktop', 1),
(5, 'Listados', 'fa fa-list-ol\r\n', 1);

-- --------------------------------------------------------

--
-- Table structure for table `opcionmenu`
--

CREATE TABLE IF NOT EXISTS `opcionmenu` (
  `id_opcion` bigint(20) NOT NULL AUTO_INCREMENT,
  `opcion` varchar(24) DEFAULT NULL,
  `url` varchar(300) DEFAULT NULL,
  `id_menu` bigint(11) NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_opcion`),
  KEY `id_menu` (`id_menu`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=latin1;

--
-- RELATIONSHIPS FOR TABLE `opcionmenu`:
--   `id_menu`
--       `menu` -> `id_menu`
--

--
-- Dumping data for table `opcionmenu`
--

INSERT INTO `opcionmenu` (`id_opcion`, `opcion`, `url`, `id_menu`, `estado`) VALUES
(1, 'CRUD Docentes', 'http://127.0.0.1/SistemaJGJ/index.php?c=docente&ev=listar', 1, 1),
(2, 'CRUD Carreras', 'http://127.0.0.1/SistemaJGJ/index.php?c=carrera&ev=listar', 1, 1),
(3, 'CRUD Facultades', 'http://127.0.0.1/SistemaJGJ/index.php?c=facultad&ev=listar', 1, 1),
(4, 'CRUD Materias ', 'http://127.0.0.1/SistemaJGJ/index.php?c=materia&ev=listar', 1, 1),
(5, 'CRUD Catedras', 'http://127.0.0.1/SistemaJGJ/index.php?c=catedra&ev=listar', 1, 1),
(6, 'CRUD Asignacion Materias', 'http://127.0.0.1/SistemaJGJ/index.php?c=asigna&ev=listar', 1, 1),
(7, 'CRUD Alumnos', 'http://127.0.0.1/SistemaJGJ/index.php?c=alumno&ev=listar', 1, 1),
(8, 'CRUD Ciclos', 'http://127.0.0.1/SistemaJGJ/index.php?c=ciclo&ev=listar', 1, 1),
(9, 'CRUD Escuelas ', 'http://127.0.0.1/SistemaJGJ/index.php?c=escuela&ev=listar', 1, 1),
(11, 'CRUD Evaluaciones', 'http://127.0.0.1/SistemaJGJ/index.php?c=evaluacion&ev=listar', 1, 1),
(12, 'CRUD Secciones', 'http://127.0.0.1/SistemaJGJ/index.php?c=seccion&ev=listar', 1, 1),
(13, 'CRUD Usuarios', 'http://127.0.0.1/SistemaJGJ/index.php?c=user&ev=listar', 1, 1),
(15, 'Rpt-Diferidos', 'http://127.0.0.1/SistemaJGJ/index.php?c=reporteDif', 3, 1),
(16, 'Tramite de Diferidos ', 'http://127.0.0.1/SistemaJGJ/index.php?c=diferido', 2, 1),
(17, 'Correccion de notas', 'http://127.0.0.1/SistemaJGJ/index.php?c=procesos&ev=inicioCorrecciones', 2, 1),
(18, 'Actividades', 'http://127.0.0.1/SistemaJGJ/index.php?c=actividad2', 2, 1),
(19, 'Listar Docentes', 'http://127.0.0.1/SistemaJGJ/index.php?c=docente', 5, 1),
(20, 'Procesos', 'http://127.0.0.1/SistemaJGJ/index.php?c=procesos', 2, 1),
(21, 'Listar Ciclos ', 'http://127.0.0.1/SistemaJGJ/index.php?c=ciclo', 5, 1),
(22, 'Listar Alumnos', 'http://127.0.0.1/SistemaJGJ/index.php?c=alumno', 5, 1),
(23, 'Listar Asignacion Mat', 'http://127.0.0.1/SistemaJGJ/index.php?c=asigna', 5, 1),
(24, 'Listar Carreras\r\n', 'http://127.0.0.1/SistemaJGJ/index.php?c=carrera', 5, 1),
(25, 'Listar Catedras', 'http://127.0.0.1/SistemaJGJ/index.php?c=catedra', 5, 1),
(26, 'Listar Escuelas', 'http://127.0.0.1/SistemaJGJ/index.php?c=escuela', 5, 1),
(27, 'Listar Evaluaciones', 'http://127.0.0.1/SistemaJGJ/index.php?c=evaluacion', 5, 1),
(28, 'Listar Facultades', 'http://127.0.0.1/SistemaJGJ/index.php?c=facultad', 5, 1),
(29, 'Listar Materias', 'http://127.0.0.1/SistemaJGJ/index.php?c=materia', 5, 1),
(30, 'Listar Secciones', 'http://127.0.0.1/SistemaJGJ/index.php?c=seccion', 5, 1),
(33, 'Listar Usuarios', 'http://127.0.0.1/SistemaJGJ/index.php?c=user', 5, 1),
(35, 'Busqueda', 'http://127.0.0.1/SistemaJGJ/vista/AdminLTE/response.php', 4, 0),
(36, 'Rpt-Actividad', 'http://127.0.0.1/SistemaJGJ/index.php?c=reportesAct', 3, 1),
(37, 'Rpt-Correcciones', 'http://127.0.0.1/SistemaJGJ/index.php?c=reportesCor', 3, 1),
(38, 'Rpt-Diferidos materias', 'http://127.0.0.1/SistemaJGJ/index.php?c=reporteDif2', 3, 1),
(39, 'Rpt-Actividades materias', 'http://127.0.0.1/SistemaJGJ/index.php?c=reportesActMat', 3, 1),
(40, 'Rpt-Correccion materias', 'http://127.0.0.1/SistemaJGJ/index.php?c=reportesCorMat', 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `reporteactividades`
--

CREATE TABLE IF NOT EXISTS `reporteactividades` (
  `ID_reporte_actividad` bigint(20) NOT NULL AUTO_INCREMENT,
  `ID_tramite` bigint(20) NOT NULL,
  `ID_tipo_actividad` bigint(20) NOT NULL,
  `nota` float NOT NULL,
  `observaciones` text NOT NULL,
  `carnet` char(12) NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ID_reporte_actividad`),
  KEY `ID_tramite` (`ID_tramite`),
  KEY `ID_tipo_actividad` (`ID_tipo_actividad`),
  KEY `carnet` (`carnet`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=latin1;

--
-- RELATIONSHIPS FOR TABLE `reporteactividades`:
--   `ID_tramite`
--       `tramites` -> `ID_tramite`
--   `ID_tipo_actividad`
--       `tipoactividad` -> `ID_tipo_actividad`
--   `carnet`
--       `alumnos` -> `carnet`
--

--
-- Dumping data for table `reporteactividades`
--

INSERT INTO `reporteactividades` (`ID_reporte_actividad`, `ID_tramite`, `ID_tipo_actividad`, `nota`, `observaciones`, `carnet`, `estado`) VALUES
(4, 14, 1, 8, 'ACTIVIDAD EXTRA', '2541402007', 0),
(5, 14, 1, 7, 'ACTIVIDAD EXTRA', '2546782007', 0),
(6, 14, 1, 8, 'OTRA ACTIVIDAD', '2746522016', 0),
(7, 14, 1, 10, 'OTRA ACTIVIDAD', '2531332007', 0),
(8, 15, 1, 10, 'ACTIVIDAD PARA COMPENSAR PARCIAL EN ESTA EVALUACION', '1701632004', 1),
(9, 15, 1, 8, 'APLICADA', '1736982004', 1),
(10, 15, 1, 9, 'APLICA PARA COMPENSAR OTRA ACTIVIDAD', '1703742000', 1),
(11, 16, 1, 5, 'COMPLETA', '2500022007', 1),
(12, 16, 1, 8, 'INGRESO DE ACTIVIDAD', '1703742000', 1),
(13, 16, 1, 10, 'OTRA', '1731852000', 1),
(14, 16, 1, 8, 'COMPLETA', '2503522007', 1),
(15, 15, 1, 10, 'ESTUDIANTE FALTANTE', '2546782007', 1),
(16, 17, 1, 10, 'NUEVA ACTIVIDAD', '2759792016', 1),
(17, 17, 1, 10, 'ALUMNO ADICIONAL', '2746522016', 1),
(18, 17, 1, 10, 'ALUMNO ADICIONAL', '2740372016', 1),
(19, 15, 1, 10, 'OTRAAAA', '2550242006', 1),
(20, 14, 1, 10, 'EXCELENTE', '2740372016', 1),
(21, 18, 1, 8, 'NUEVA ACTIVIDAD', '1701632004', 1),
(22, 18, 1, 10, 'NUEVA ALUMNA', '1716152004', 1),
(23, 18, 1, 8, 'ALUMNO ADICIONAL', '2503522007', 1),
(24, 18, 1, 10, 'INCLUIDO', '1731852000', 0),
(25, 19, 1, 10, 'OTRA ACTIVIDAD', '2500692007', 1),
(26, 19, 1, 8, 'NUEVA ACTIVIDAD', '1728982005', 1),
(27, 19, 1, 9, 'ALUMNO ADICIONAL', '1736982004', 1),
(28, 19, 1, 8, 'BUENA NOTA', '1728982005', 1),
(29, 20, 1, 10, 'CONFIRMADA', '1736982004', 1),
(30, 20, 1, 8, 'SIGUIENTE ACTIVIDAD', '2500022007', 1),
(31, 20, 1, 10, 'EXCELENTE ALUMNO', '2504402007', 1),
(32, 19, 1, 8, 'ALUMNO AGREGADO POSTERIORMENTE', '2541812006', 1),
(33, 29, 1, 10, 'BUENA NOTA', '2740372016', 1),
(34, 29, 1, 10, 'EXCELENTE NOTA', '2759792016', 1),
(35, 29, 1, 10, 'EXCELENTES ALUMNOS', '2746522016', 1),
(36, 18, 1, 10, 'ALUMNO NUEVO SELECCIONADO', '2550242006', 1),
(37, 30, 1, 10, '', '2500022007', 1),
(38, 30, 1, 10, '', '2500692007', 1),
(39, 30, 1, 10, '', '2504202007', 1),
(40, 31, 1, 10, '', '1736982004', 1),
(41, 31, 1, 9, '', '2500692007', 1),
(42, 31, 1, 10, '', '2504402007', 1),
(43, 38, 1, 10, '', '2759792016', 1),
(44, 38, 1, 10, '', '2740372016', 1),
(45, 38, 1, 10, '', '2746522016', 1),
(46, 44, 1, 10, '', '2759792016', 1),
(47, 44, 1, 10, '', '2746522016', 1),
(48, 44, 1, 10, '', '2740372016', 1),
(49, 46, 1, 10, 'otraaaa', '2759792016', 1),
(50, 46, 1, 10, 'NEW', '2712342018', 1),
(51, 57, 1, 7, 'asdasda', '2746522016', 1),
(52, 57, 1, 7, 'asdasda', '2746522016', 1),
(53, 59, 1, 4, 'asfasdasfgg', '2746522016', 1);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `id_rol` bigint(20) NOT NULL AUTO_INCREMENT,
  `rol` varchar(45) DEFAULT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_rol`),
  UNIQUE KEY `rol` (`rol`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- RELATIONSHIPS FOR TABLE `roles`:
--

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id_rol`, `rol`, `estado`) VALUES
(1, 'Administrador', 1),
(2, 'Asistente', 1),
(3, 'Reportes', 1),
(4, '4', 1);

-- --------------------------------------------------------

--
-- Table structure for table `rol_menu`
--

CREATE TABLE IF NOT EXISTS `rol_menu` (
  `id_rol_menu` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_opcion` bigint(20) NOT NULL,
  `acceso` tinyint(1) DEFAULT '0',
  `id_rol` bigint(20) NOT NULL,
  PRIMARY KEY (`id_rol_menu`),
  KEY `id_opcion` (`id_opcion`),
  KEY `id_rol` (`id_rol`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=latin1;

--
-- RELATIONSHIPS FOR TABLE `rol_menu`:
--   `id_rol`
--       `roles` -> `id_rol`
--   `id_opcion`
--       `opcionmenu` -> `id_opcion`
--

--
-- Dumping data for table `rol_menu`
--

INSERT INTO `rol_menu` (`id_rol_menu`, `id_opcion`, `acceso`, `id_rol`) VALUES
(1, 1, 1, 1),
(2, 2, 1, 1),
(3, 3, 1, 1),
(4, 4, 1, 1),
(5, 5, 1, 1),
(6, 6, 1, 1),
(7, 7, 1, 1),
(8, 8, 1, 1),
(9, 9, 1, 1),
(10, 11, 1, 1),
(11, 12, 1, 1),
(12, 13, 1, 1),
(13, 15, 1, 1),
(14, 17, 1, 1),
(15, 18, 1, 1),
(16, 19, 1, 1),
(17, 20, 1, 1),
(18, 21, 1, 1),
(19, 22, 1, 1),
(20, 23, 1, 1),
(21, 24, 1, 1),
(22, 25, 1, 1),
(23, 26, 1, 1),
(24, 27, 1, 1),
(25, 28, 1, 1),
(26, 29, 1, 1),
(27, 30, 1, 1),
(28, 33, 1, 1),
(29, 16, 1, 1),
(30, 35, 0, 1),
(31, 36, 1, 1),
(32, 37, 1, 1),
(33, 38, 1, 1),
(34, 39, 1, 1),
(35, 40, 1, 1),
(36, 16, 1, 2),
(37, 17, 1, 2),
(38, 18, 1, 2),
(39, 19, 1, 2),
(40, 20, 1, 2),
(41, 21, 1, 2),
(42, 22, 1, 2),
(43, 23, 1, 2),
(44, 24, 1, 2),
(45, 25, 1, 2),
(46, 26, 1, 2),
(47, 15, 1, 3),
(48, 36, 1, 3),
(49, 37, 1, 3),
(50, 38, 1, 3),
(51, 39, 1, 3),
(52, 40, 1, 3),
(53, 2, 1, 4),
(54, 15, 1, 4),
(55, 24, 1, 4),
(58, 7, 1, 4),
(60, 7, 1, 2),
(61, 27, 1, 2),
(62, 28, 1, 2),
(63, 29, 1, 2),
(64, 30, 1, 2),
(65, 33, 0, 2);

-- --------------------------------------------------------

--
-- Table structure for table `secciones`
--

CREATE TABLE IF NOT EXISTS `secciones` (
  `ID_seccion` bigint(20) NOT NULL AUTO_INCREMENT,
  `seccion` varchar(50) NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ID_seccion`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- RELATIONSHIPS FOR TABLE `secciones`:
--

--
-- Dumping data for table `secciones`
--

INSERT INTO `secciones` (`ID_seccion`, `seccion`, `estado`) VALUES
(1, 'SecciÃƒÂ³n 01', 1),
(2, 'SecciÃƒÂ³n 02', 1),
(3, 'SecciÃƒÂ³n 03', 1),
(4, 'SecciÃƒÂ³n 04', 1),
(5, 'SecciÃƒÂ³n 05', 1),
(6, 'SecciÃƒÂ³n 06', 1),
(7, 'SecciÃƒÂ³n 07', 1),
(8, 'SecciÃƒÂ³n 08', 1),
(9, 'SecciÃƒÂ³n 09', 1),
(10, 'SecciÃƒÂ³n 10', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tipoactividad`
--

CREATE TABLE IF NOT EXISTS `tipoactividad` (
  `ID_tipo_actividad` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombreActividad` varchar(50) NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ID_tipo_actividad`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- RELATIONSHIPS FOR TABLE `tipoactividad`:
--

--
-- Dumping data for table `tipoactividad`
--

INSERT INTO `tipoactividad` (`ID_tipo_actividad`, `nombreActividad`, `estado`) VALUES
(1, 'Seminario', 1),
(2, 'Actividad Ex-aula', 1),
(3, 'Actividad Individual', 1),
(4, 'ExposiciÃ³n', 1),
(5, 'Actividad Grupal', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tipotramite`
--

CREATE TABLE IF NOT EXISTS `tipotramite` (
  `ID_tipo_tramite` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombreTramite` varchar(50) NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ID_tipo_tramite`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- RELATIONSHIPS FOR TABLE `tipotramite`:
--

--
-- Dumping data for table `tipotramite`
--

INSERT INTO `tipotramite` (`ID_tipo_tramite`, `nombreTramite`, `estado`) VALUES
(1, 'Actividades', 1),
(2, 'Correcciones de Notas', 1),
(3, 'Examenes Diferidos', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tramites`
--

CREATE TABLE IF NOT EXISTS `tramites` (
  `ID_tramite` bigint(20) NOT NULL AUTO_INCREMENT,
  `ID_carrera_materia` bigint(20) NOT NULL,
  `ID_seccion` bigint(20) NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  `ID_tipo_tramite` bigint(20) NOT NULL,
  `ID_eval` bigint(20) NOT NULL,
  `carnet_docente` varchar(12) NOT NULL,
  `autorizado` tinyint(1) NOT NULL,
  `conteo` int(20) NOT NULL,
  `fechaRegistro` datetime NOT NULL,
  `ID_ciclo` bigint(20) NOT NULL,
  `fechaEnvio` datetime NOT NULL,
  `periodo` int(11) NOT NULL,
  `fechaSolicitud` datetime NOT NULL,
  PRIMARY KEY (`ID_tramite`),
  KEY `ID_seccion` (`ID_seccion`),
  KEY `ID_carrera_materia` (`ID_carrera_materia`),
  KEY `ID_tipo_tramite` (`ID_tipo_tramite`),
  KEY `ID_eval` (`ID_eval`),
  KEY `carnet_docente` (`carnet_docente`),
  KEY `ID_ciclo` (`ID_ciclo`)
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=latin1;

--
-- RELATIONSHIPS FOR TABLE `tramites`:
--   `ID_seccion`
--       `secciones` -> `ID_seccion`
--   `ID_carrera_materia`
--       `carrera_materia` -> `ID_carrera_materia`
--   `ID_tipo_tramite`
--       `tipotramite` -> `ID_tipo_tramite`
--   `ID_eval`
--       `evaluaciones` -> `ID_eval`
--   `carnet_docente`
--       `docentes` -> `carnet_docente`
--   `ID_ciclo`
--       `ciclos` -> `ID_ciclo`
--

--
-- Dumping data for table `tramites`
--

INSERT INTO `tramites` (`ID_tramite`, `ID_carrera_materia`, `ID_seccion`, `estado`, `ID_tipo_tramite`, `ID_eval`, `carnet_docente`, `autorizado`, `conteo`, `fechaRegistro`, `ID_ciclo`, `fechaEnvio`, `periodo`, `fechaSolicitud`) VALUES
(1, 1, 1, 1, 3, 1, '10132018', 0, 0, '2018-11-25 13:38:11', 1, '0000-00-00 00:00:00', 1, '2018-11-25 00:00:00'),
(2, 32, 10, 1, 3, 5, '10102018', 0, 0, '2018-11-25 13:39:06', 2, '0000-00-00 00:00:00', 2, '2018-11-25 00:00:00'),
(3, 14, 4, 1, 3, 3, '10042018', 0, 0, '2018-11-25 13:39:48', 8, '0000-00-00 00:00:00', 1, '2018-11-20 00:00:00'),
(4, 17, 4, 1, 3, 3, '10042018', 0, 0, '2018-11-25 13:39:48', 6, '0000-00-00 00:00:00', 1, '2018-11-20 00:00:00'),
(6, 24, 3, 1, 3, 2, '10072018', 0, 0, '2018-11-25 13:43:14', 5, '0000-00-00 00:00:00', 2, '2018-11-25 00:00:00'),
(7, 20, 3, 1, 3, 4, '10072018', 0, 0, '2018-11-25 13:47:01', 4, '0000-00-00 00:00:00', 2, '2018-11-25 00:00:00'),
(8, 24, 1, 1, 2, 1, '10042018', 0, 0, '2018-11-25 13:51:39', 2, '2018-11-25 13:51:39', 0, '2018-11-16 00:00:00'),
(9, 31, 2, 1, 2, 2, '10012018', 0, 0, '2018-11-25 13:50:12', 2, '2018-11-25 13:50:12', 1, '2018-11-03 00:00:00'),
(11, 20, 3, 1, 2, 3, '10052018', 0, 0, '2018-11-25 15:25:03', 4, '2018-11-25 15:25:03', 0, '2018-11-10 00:00:00'),
(13, 21, 3, 1, 2, 2, '10052018', 0, 0, '2018-11-25 14:39:01', 3, '2018-11-25 14:39:01', 1, '2018-11-16 00:00:00'),
(14, 20, 3, 0, 1, 3, '10072018', 1, 4, '2018-11-25 00:00:00', 3, '2018-11-25 00:00:00', 1, '2018-11-25 00:00:00'),
(15, 21, 3, 1, 1, 5, '10032018', 1, 3, '2018-11-25 00:00:00', 4, '2018-11-25 00:00:00', 1, '2018-11-25 00:00:00'),
(16, 23, 7, 1, 1, 4, '10062018', 1, 4, '2018-11-25 00:00:00', 4, '2018-11-25 00:00:00', 1, '0000-00-00 00:00:00'),
(17, 31, 2, 1, 1, 4, '10062018', 1, 3, '2018-11-25 00:00:00', 5, '2018-11-25 00:00:00', 1, '0000-00-00 00:00:00'),
(18, 29, 4, 1, 1, 3, '1212312333', 1, 4, '2018-11-25 00:00:00', 2, '2018-11-25 00:00:00', 1, '0000-00-00 00:00:00'),
(19, 31, 2, 1, 1, 4, '10032018', 1, 4, '2018-11-25 00:00:00', 2, '2018-11-25 00:00:00', 1, '0000-00-00 00:00:00'),
(20, 31, 2, 1, 1, 4, '10092018', 1, 3, '2018-11-25 00:00:00', 3, '2018-11-25 00:00:00', 1, '2018-11-25 00:00:00'),
(21, 21, 2, 1, 2, 2, '10032018', 0, 0, '2018-11-25 15:24:36', 3, '2018-11-25 15:24:36', 1, '2018-11-10 00:00:00'),
(22, 23, 1, 1, 2, 2, '10042018', 0, 0, '2018-11-25 15:27:07', 3, '2018-11-25 15:27:07', 1, '2018-11-09 00:00:00'),
(23, 27, 5, 1, 2, 3, '10112018', 0, 0, '2018-11-25 15:27:58', 3, '2018-11-25 15:27:58', 1, '2018-11-08 00:00:00'),
(27, 19, 2, 1, 2, 2, '10132018', 0, 0, '2018-11-25 17:08:27', 5, '2018-11-25 17:08:27', 1, '2018-11-09 00:00:00'),
(28, 21, 3, 1, 3, 2, '10072018', 0, 0, '2018-11-25 15:31:30', 3, '0000-00-00 00:00:00', 1, '2018-11-25 00:00:00'),
(29, 31, 3, 1, 1, 2, '10062018', 1, 3, '2018-11-25 00:00:00', 4, '2018-11-25 00:00:00', 1, '2018-11-25 00:00:00'),
(30, 20, 2, 1, 1, 3, '10082018', 1, 3, '2018-11-25 00:00:00', 5, '2018-11-25 00:00:00', 1, '0000-00-00 00:00:00'),
(31, 21, 1, 1, 1, 1, '10072018', 1, 3, '2018-11-25 00:00:00', 2, '2018-11-25 00:00:00', 1, '2018-11-25 00:00:00'),
(32, 14, 1, 1, 3, 1, '10072018', 0, 0, '2018-11-25 17:07:31', 5, '0000-00-00 00:00:00', 1, '2017-10-24 00:00:00'),
(33, 17, 1, 1, 3, 1, '10022018', 0, 0, '2018-11-25 17:07:31', 5, '0000-00-00 00:00:00', 1, '2017-10-24 00:00:00'),
(35, 21, 2, 1, 2, 3, '10112018', 0, 0, '2018-11-25 17:08:58', 2, '2018-11-25 17:08:58', 1, '2018-11-09 00:00:00'),
(36, 19, 4, 1, 3, 2, '10072018', 0, 0, '2018-11-25 22:09:15', 3, '0000-00-00 00:00:00', 2, '2018-11-25 00:00:00'),
(37, 27, 5, 1, 3, 2, '10062018', 0, 0, '2018-11-25 22:58:46', 3, '0000-00-00 00:00:00', 2, '2018-11-25 00:00:00'),
(38, 21, 2, 1, 1, 3, '10062018', 1, 3, '2018-11-25 00:00:00', 2, '2018-11-25 00:00:00', 1, '2018-11-26 00:00:00'),
(39, 14, 2, 1, 2, 2, '10012018', 0, 0, '2018-11-25 23:14:11', 2, '2018-11-25 23:14:11', 0, '2018-11-02 00:00:00'),
(40, 17, 2, 1, 2, 2, '10012018', 0, 0, '2018-11-25 23:14:11', 2, '2018-11-25 23:14:11', 0, '2018-11-02 00:00:00'),
(42, 22, 3, 1, 3, 2, '10052018', 0, 0, '2018-11-26 21:22:58', 4, '0000-00-00 00:00:00', 2, '2018-11-26 00:00:00'),
(43, 16, 1, 1, 2, 5, '10052018', 0, 0, '2018-11-26 21:25:50', 8, '2018-11-26 21:25:50', 0, '2018-11-15 00:00:00'),
(44, 14, 3, 1, 1, 3, '10112018', 1, 3, '2018-11-26 00:00:00', 3, '2018-11-26 00:00:00', 1, '2018-11-27 00:00:00'),
(45, 17, 3, 1, 1, 3, '10112018', 1, 3, '2018-11-26 00:00:00', 3, '2018-11-26 00:00:00', 1, '2018-11-27 00:00:00'),
(46, 20, 3, 1, 1, 2, '10112018', 1, 2, '2018-11-29 00:00:00', 2, '2018-11-29 00:00:00', 1, '2018-11-30 00:00:00'),
(51, 16, 4, 1, 3, 2, '00000301', 0, 0, '2018-12-11 21:21:38', 2, '0000-00-00 00:00:00', 2, '2018-12-11 00:00:00'),
(52, 1, 6, 1, 3, 3, '0000000906', 0, 0, '2018-12-11 21:22:10', 2, '0000-00-00 00:00:00', 2, '2018-12-11 00:00:00'),
(53, 19, 3, 1, 3, 2, '3211313213', 0, 0, '2018-12-11 23:20:58', 2, '0000-00-00 00:00:00', 2, '2018-12-11 00:00:00'),
(55, 16, 2, 1, 3, 1, '1212312333', 0, 0, '2018-12-13 20:33:18', 2, '0000-00-00 00:00:00', 1, '2018-12-13 00:00:00'),
(56, 19, 4, 1, 3, 1, '3211313213', 0, 0, '2018-12-13 21:04:44', 2, '0000-00-00 00:00:00', 1, '2018-12-13 00:00:00'),
(57, 20, 1, 1, 1, 2, '1212312333', 1, 1, '2018-12-13 00:00:00', 2, '2018-12-13 00:00:00', 1, '2018-12-14 00:00:00'),
(58, 20, 1, 1, 1, 2, '1212312333', 1, 1, '2018-12-13 00:00:00', 2, '2018-12-13 00:00:00', 1, '2018-12-14 00:00:00'),
(59, 24, 3, 1, 1, 3, '3211313213', 1, 1, '2018-12-13 00:00:00', 2, '2018-12-13 00:00:00', 1, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
  `ID_usuario` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) DEFAULT NULL,
  `apellido` varchar(50) DEFAULT NULL,
  `correo` varchar(500) DEFAULT NULL,
  `usuario` varchar(20) NOT NULL,
  `clave` varchar(100) DEFAULT NULL,
  `estado` tinyint(1) DEFAULT '1',
  `fecha_creacion` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `nivel` int(11) NOT NULL DEFAULT '1',
  `fecha_modificacion` datetime DEFAULT CURRENT_TIMESTAMP,
  `id_rol` bigint(20) NOT NULL,
  PRIMARY KEY (`ID_usuario`),
  UNIQUE KEY `usuario` (`usuario`),
  KEY `id_rol` (`id_rol`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- RELATIONSHIPS FOR TABLE `usuarios`:
--   `id_rol`
--       `roles` -> `id_rol`
--

--
-- Dumping data for table `usuarios`
--

INSERT INTO `usuarios` (`ID_usuario`, `nombre`, `apellido`, `correo`, `usuario`, `clave`, `estado`, `fecha_creacion`, `nivel`, `fecha_modificacion`, `id_rol`) VALUES
(5, 'Blanca', 'Martinez', 'blanca@mail.utec.edu.sv', 'asistente', ' ùÆäªú­Âî	[ƒ‘', 1, '2018-11-21 22:01:32', 1, '2018-12-13 21:03:16', 2),
(6, 'Admin', 'Del Sistema', 'Modificame@mail.utec.edu.sv', 'Administrador', ' ùÆäªú­Âî	[ƒ‘', 1, '2018-11-24 04:15:48', 1, '2018-11-26 00:27:20', 1),
(7, 'reportes', 'Del Sistema', 'reportes@mail.com', 'reportes', ' ùÆäªú­Âî	[ƒ‘', 1, '2018-11-24 04:36:47', 1, '2018-11-26 00:27:25', 3),
(9, 'Juan', 'Perez', 'user@gmail.com', 'user', ' ùÆäªú­Âî	[ƒ‘', 1, '2018-11-26 00:23:17', 1, '2018-11-26 00:23:17', 1),
(11, 'Blanca', 'Beltran', 'bebam.2011@gmail.com', 'blanquy', 'úN1\rz´ýx;ò&Ë&o', 1, '2018-12-05 12:10:25', 1, '2018-12-05 12:10:30', 1),
(12, 'Oswaldo', 'Barrera', 'jose.barrera@utec.edu.sv', 'oz', ' ùÆäªú­Âî	[ƒ‘', 1, '2018-12-05 12:11:38', 1, '2018-12-05 12:11:39', 1);

-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_actividadestodos`
-- (See below for the actual view)
--
CREATE TABLE IF NOT EXISTS `vw_actividadestodos` (
`IDTRAMITE` bigint(20)
,`EVALUACION` varchar(50)
,`CARRERA` varchar(500)
,`MATERIA` varchar(200)
,`SECCION` varchar(50)
,`CICLO` varchar(50)
,`DOCENTE` varchar(100)
,`CASOS` int(20)
,`ESTADO` varchar(8)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_diferidostodos`
-- (See below for the actual view)
--
CREATE TABLE IF NOT EXISTS `vw_diferidostodos` (
`IDTRAMITE` bigint(20)
,`CARRERA` varchar(500)
,`ALUMNO` varchar(50)
,`MATERIA` varchar(200)
,`ESCUELA` varchar(500)
,`SECCION` varchar(50)
,`DOCENTE` varchar(100)
,`EVALUACION` varchar(50)
,`PERIODO` varchar(22)
,`FECHAREG` datetime
,`FECHASOL` datetime
,`CICLO` varchar(50)
,`ESTADO` varchar(8)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_mostrarcarreras`
-- (See below for the actual view)
--
CREATE TABLE IF NOT EXISTS `vw_mostrarcarreras` (
`ID_carrera` bigint(20)
,`nombre_carrera` varchar(500)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_rpcorrecion`
-- (See below for the actual view)
--
CREATE TABLE IF NOT EXISTS `vw_rpcorrecion` (
`EVALUACIONES` varchar(50)
,`CICLO` bigint(20)
,`FECHASOLICITUD` date
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_rpdiferidos`
-- (See below for the actual view)
--
CREATE TABLE IF NOT EXISTS `vw_rpdiferidos` (
`EVALUACIONES` varchar(50)
,`CICLO` bigint(20)
,`FECHASOLICITUD` date
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_rptactivciclo`
-- (See below for the actual view)
--
CREATE TABLE IF NOT EXISTS `vw_rptactivciclo` (
`EVALUACIONES` varchar(50)
,`CICLO` bigint(20)
,`FECHASOLICITUD` date
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_rptactmat`
-- (See below for the actual view)
--
CREATE TABLE IF NOT EXISTS `vw_rptactmat` (
`EVALUACIONES` varchar(50)
,`CICLO` bigint(20)
,`FECHASOLICITUD` date
,`MATERIA` text
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_rptcormat`
-- (See below for the actual view)
--
CREATE TABLE IF NOT EXISTS `vw_rptcormat` (
`EVALUACIONES` varchar(50)
,`CICLO` bigint(20)
,`FECHASOLICITUD` date
,`MATERIA` text
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_rptdifmat`
-- (See below for the actual view)
--
CREATE TABLE IF NOT EXISTS `vw_rptdifmat` (
`EVALUACIONES` varchar(50)
,`CICLO` bigint(20)
,`FECHASOLICITUD` date
,`MATERIA` text
);

-- --------------------------------------------------------

--
-- Structure for view `vw_actividadestodos`
--
DROP TABLE IF EXISTS `vw_actividadestodos`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_actividadestodos`  AS  select `t`.`ID_tramite` AS `IDTRAMITE`,`eva`.`descripcion` AS `EVALUACION`,`car`.`nombre_carrera` AS `CARRERA`,`mat`.`nombre_materia` AS `MATERIA`,`sec`.`seccion` AS `SECCION`,`ci`.`nombre_ciclo` AS `CICLO`,`doc`.`docente_nombres` AS `DOCENTE`,`t`.`conteo` AS `CASOS`,(case when (`t`.`estado` = 1) then 'Activo' else 'Inactivo' end) AS `ESTADO` from ((((((((`tramites` `t` left join `secciones` `sec` on((`t`.`ID_seccion` = `sec`.`ID_seccion`))) left join `tipotramite` `tiptram` on((`t`.`ID_tipo_tramite` = `tiptram`.`ID_tipo_tramite`))) left join `evaluaciones` `eva` on((`t`.`ID_eval` = `eva`.`ID_eval`))) left join `docentes` `doc` on((`t`.`carnet_docente` = `doc`.`carnet_docente`))) left join `ciclos` `ci` on((`t`.`ID_ciclo` = `ci`.`ID_ciclo`))) left join `carrera_materia` `carmat` on((`t`.`ID_carrera_materia` = `carmat`.`ID_carrera_materia`))) left join `carreras` `car` on((`carmat`.`ID_carrera` = `car`.`ID_carrera`))) left join `materias` `mat` on((`mat`.`ID_materia` = `carmat`.`ID_materia`))) where (`tiptram`.`ID_tipo_tramite` = 1) ;

-- --------------------------------------------------------

--
-- Structure for view `vw_diferidostodos`
--
DROP TABLE IF EXISTS `vw_diferidostodos`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_diferidostodos`  AS  select `t`.`ID_tramite` AS `IDTRAMITE`,`c`.`nombre_carrera` AS `CARRERA`,`a`.`alumno` AS `ALUMNO`,`m`.`nombre_materia` AS `MATERIA`,`e`.`nombre_escuela` AS `ESCUELA`,`s`.`seccion` AS `SECCION`,`dc`.`docente_nombres` AS `DOCENTE`,`ev`.`descripcion` AS `EVALUACION`,(case when (`t`.`periodo` = 1) then 'Periodo Extraordinario' when (`t`.`periodo` = 2) then 'Periodo Ordinario' else 'NINGUN PERIODO' end) AS `PERIODO`,`t`.`fechaRegistro` AS `FECHAREG`,`t`.`fechaSolicitud` AS `FECHASOL`,`ci`.`nombre_ciclo` AS `CICLO`,(case when (`d`.`estado` = 1) then 'Activo' else 'inactivo' end) AS `ESTADO` from ((((((((((((`tramites` `t` join `diferidos` `d` on((`t`.`ID_tramite` = `d`.`ID_tramite`))) left join `carrera_materia` `cm` on((`t`.`ID_carrera_materia` = `cm`.`ID_carrera_materia`))) left join `alumnos` `a` on((`a`.`carnet` = `d`.`carnet`))) left join `materias` `m` on((`m`.`ID_materia` = `cm`.`ID_materia`))) left join `carreras` `c` on((`c`.`ID_carrera` = `cm`.`ID_carrera`))) left join `catedras` `ct` on((`ct`.`ID_catedra` = `cm`.`ID_catedra`))) left join `escuela` `e` on((`e`.`ID_escuela` = `ct`.`ID_escuela`))) left join `secciones` `s` on((`s`.`ID_seccion` = `t`.`ID_seccion`))) left join `docentes` `dc` on((`dc`.`carnet_docente` = `t`.`carnet_docente`))) left join `evaluaciones` `ev` on((`ev`.`ID_eval` = `t`.`ID_eval`))) left join `tipotramite` `tt` on((`tt`.`ID_tipo_tramite` = `t`.`ID_tipo_tramite`))) left join `ciclos` `ci` on((`ci`.`ID_ciclo` = `t`.`ID_ciclo`))) where (`tt`.`ID_tipo_tramite` = 3) ;

-- --------------------------------------------------------

--
-- Structure for view `vw_mostrarcarreras`
--
DROP TABLE IF EXISTS `vw_mostrarcarreras`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_mostrarcarreras`  AS  select `cm`.`ID_carrera` AS `ID_carrera`,`c`.`nombre_carrera` AS `nombre_carrera` from (`carrera_materia` `cm` left join `carreras` `c` on((`cm`.`ID_carrera` = `c`.`ID_carrera`))) where (`cm`.`estado` = 1) group by `cm`.`ID_carrera` order by `c`.`nombre_carrera` ;

-- --------------------------------------------------------

--
-- Structure for view `vw_rpcorrecion`
--
DROP TABLE IF EXISTS `vw_rpcorrecion`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_rpcorrecion`  AS  select `e`.`descripcion` AS `EVALUACIONES`,`t`.`ID_ciclo` AS `CICLO`,cast(`t`.`fechaSolicitud` as date) AS `FECHASOLICITUD` from ((`correcciones` `d` join `tramites` `t` on((`d`.`ID_tramite` = `t`.`ID_tramite`))) join `evaluaciones` `e` on((`e`.`ID_eval` = `t`.`ID_eval`))) where ((`t`.`ID_tipo_tramite` = 2) and (`d`.`estado` = 1) and ((`t`.`fechaSolicitud` <> '') or (`t`.`fechaSolicitud` <> '0000-00-00'))) ;

-- --------------------------------------------------------

--
-- Structure for view `vw_rpdiferidos`
--
DROP TABLE IF EXISTS `vw_rpdiferidos`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_rpdiferidos`  AS  select `e`.`descripcion` AS `EVALUACIONES`,`t`.`ID_ciclo` AS `CICLO`,cast(`t`.`fechaSolicitud` as date) AS `FECHASOLICITUD` from ((`diferidos` `d` join `tramites` `t` on((`d`.`ID_tramite` = `t`.`ID_tramite`))) join `evaluaciones` `e` on((`e`.`ID_eval` = `t`.`ID_eval`))) where ((`t`.`ID_tipo_tramite` = 3) and (`d`.`estado` = 1) and ((`t`.`fechaSolicitud` <> '') or (`t`.`fechaSolicitud` <> '0000-00-00'))) ;

-- --------------------------------------------------------

--
-- Structure for view `vw_rptactivciclo`
--
DROP TABLE IF EXISTS `vw_rptactivciclo`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_rptactivciclo`  AS  select `e`.`descripcion` AS `EVALUACIONES`,`t`.`ID_ciclo` AS `CICLO`,cast(`t`.`fechaSolicitud` as date) AS `FECHASOLICITUD` from (`tramites` `t` join `evaluaciones` `e` on((`e`.`ID_eval` = `t`.`ID_eval`))) where ((`t`.`ID_tipo_tramite` = 1) and (`t`.`estado` = 1) and ((`t`.`fechaSolicitud` <> '') or (`t`.`fechaSolicitud` <> '0000-00-00'))) ;

-- --------------------------------------------------------

--
-- Structure for view `vw_rptactmat`
--
DROP TABLE IF EXISTS `vw_rptactmat`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_rptactmat`  AS  select `e`.`descripcion` AS `EVALUACIONES`,`t`.`ID_ciclo` AS `CICLO`,cast(`t`.`fechaSolicitud` as date) AS `FECHASOLICITUD`,concat(convert(`m`.`nombre_materia` using utf8),' - ',`c`.`nombre_carrera`) AS `MATERIA` from ((((`tramites` `t` join `evaluaciones` `e` on((`e`.`ID_eval` = `t`.`ID_eval`))) join `carrera_materia` `cm` on((`cm`.`ID_carrera_materia` = `t`.`ID_carrera_materia`))) join `carreras` `c` on((`c`.`ID_carrera` = `cm`.`ID_carrera`))) join `materias` `m` on((`m`.`ID_materia` = `cm`.`ID_materia`))) where ((`t`.`ID_tipo_tramite` = 1) and (`t`.`estado` = 1) and ((`t`.`fechaSolicitud` <> '') or (`t`.`fechaSolicitud` <> '0000-00-00'))) ;

-- --------------------------------------------------------

--
-- Structure for view `vw_rptcormat`
--
DROP TABLE IF EXISTS `vw_rptcormat`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_rptcormat`  AS  select `e`.`descripcion` AS `EVALUACIONES`,`t`.`ID_ciclo` AS `CICLO`,cast(`t`.`fechaSolicitud` as date) AS `FECHASOLICITUD`,concat(convert(`m`.`nombre_materia` using utf8),' - ',`c`.`nombre_carrera`) AS `MATERIA` from ((((`tramites` `t` join `evaluaciones` `e` on((`e`.`ID_eval` = `t`.`ID_eval`))) join `carrera_materia` `cm` on((`cm`.`ID_carrera_materia` = `t`.`ID_carrera_materia`))) join `carreras` `c` on((`c`.`ID_carrera` = `cm`.`ID_carrera`))) join `materias` `m` on((`m`.`ID_materia` = `cm`.`ID_materia`))) where ((`t`.`ID_tipo_tramite` = 2) and (`t`.`estado` = 1) and ((`t`.`fechaSolicitud` <> '') or (`t`.`fechaSolicitud` <> '0000-00-00'))) ;

-- --------------------------------------------------------

--
-- Structure for view `vw_rptdifmat`
--
DROP TABLE IF EXISTS `vw_rptdifmat`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_rptdifmat`  AS  select `e`.`descripcion` AS `EVALUACIONES`,`t`.`ID_ciclo` AS `CICLO`,cast(`t`.`fechaSolicitud` as date) AS `FECHASOLICITUD`,concat(convert(`m`.`nombre_materia` using utf8),' - ',`c`.`nombre_carrera`) AS `MATERIA` from ((((`tramites` `t` join `evaluaciones` `e` on((`e`.`ID_eval` = `t`.`ID_eval`))) join `carrera_materia` `cm` on((`cm`.`ID_carrera_materia` = `t`.`ID_carrera_materia`))) join `carreras` `c` on((`c`.`ID_carrera` = `cm`.`ID_carrera`))) join `materias` `m` on((`m`.`ID_materia` = `cm`.`ID_materia`))) where ((`t`.`ID_tipo_tramite` = 3) and (`t`.`estado` = 1) and ((`t`.`fechaSolicitud` <> '') or (`t`.`fechaSolicitud` <> '0000-00-00'))) ;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `carrera_materia`
--
ALTER TABLE `carrera_materia`
  ADD CONSTRAINT `carrera_materia_ibfk_1` FOREIGN KEY (`ID_carrera`) REFERENCES `carreras` (`ID_carrera`),
  ADD CONSTRAINT `carrera_materia_ibfk_2` FOREIGN KEY (`ID_materia`) REFERENCES `materias` (`ID_materia`),
  ADD CONSTRAINT `carrera_materia_ibfk_3` FOREIGN KEY (`ID_catedra`) REFERENCES `catedras` (`ID_catedra`);

--
-- Constraints for table `catedras`
--
ALTER TABLE `catedras`
  ADD CONSTRAINT `catedras_ibfk_1` FOREIGN KEY (`ID_escuela`) REFERENCES `escuela` (`ID_escuela`);

--
-- Constraints for table `correcciones`
--
ALTER TABLE `correcciones`
  ADD CONSTRAINT `correcciones_ibfk_1` FOREIGN KEY (`ID_tramite`) REFERENCES `tramites` (`ID_tramite`),
  ADD CONSTRAINT `correcciones_ibfk_2` FOREIGN KEY (`carnet`) REFERENCES `alumnos` (`carnet`);

--
-- Constraints for table `diferidos`
--
ALTER TABLE `diferidos`
  ADD CONSTRAINT `diferidos_ibfk_1` FOREIGN KEY (`ID_tramite`) REFERENCES `tramites` (`ID_tramite`),
  ADD CONSTRAINT `diferidos_ibfk_2` FOREIGN KEY (`carnet`) REFERENCES `alumnos` (`carnet`);

--
-- Constraints for table `escuela`
--
ALTER TABLE `escuela`
  ADD CONSTRAINT `escuela_ibfk_1` FOREIGN KEY (`ID_facultad`) REFERENCES `facultades` (`id_facultad`) ON DELETE SET NULL ON UPDATE SET NULL;

--
-- Constraints for table `opcionmenu`
--
ALTER TABLE `opcionmenu`
  ADD CONSTRAINT `opcionmenu_ibfk_1` FOREIGN KEY (`id_menu`) REFERENCES `menu` (`id_menu`);

--
-- Constraints for table `reporteactividades`
--
ALTER TABLE `reporteactividades`
  ADD CONSTRAINT `reporteactividades_ibfk_1` FOREIGN KEY (`ID_tramite`) REFERENCES `tramites` (`ID_tramite`),
  ADD CONSTRAINT `reporteactividades_ibfk_2` FOREIGN KEY (`ID_tipo_actividad`) REFERENCES `tipoactividad` (`ID_tipo_actividad`),
  ADD CONSTRAINT `reporteactividades_ibfk_3` FOREIGN KEY (`carnet`) REFERENCES `alumnos` (`carnet`);

--
-- Constraints for table `rol_menu`
--
ALTER TABLE `rol_menu`
  ADD CONSTRAINT `rol_menu_ibfk_2` FOREIGN KEY (`id_rol`) REFERENCES `roles` (`id_rol`),
  ADD CONSTRAINT `rol_menu_ibfk_3` FOREIGN KEY (`id_opcion`) REFERENCES `opcionmenu` (`id_opcion`);

--
-- Constraints for table `tramites`
--
ALTER TABLE `tramites`
  ADD CONSTRAINT `tramites_ibfk_3` FOREIGN KEY (`ID_seccion`) REFERENCES `secciones` (`ID_seccion`),
  ADD CONSTRAINT `tramites_ibfk_5` FOREIGN KEY (`ID_carrera_materia`) REFERENCES `carrera_materia` (`ID_carrera_materia`),
  ADD CONSTRAINT `tramites_ibfk_6` FOREIGN KEY (`ID_tipo_tramite`) REFERENCES `tipotramite` (`ID_tipo_tramite`),
  ADD CONSTRAINT `tramites_ibfk_7` FOREIGN KEY (`ID_eval`) REFERENCES `evaluaciones` (`ID_eval`),
  ADD CONSTRAINT `tramites_ibfk_8` FOREIGN KEY (`carnet_docente`) REFERENCES `docentes` (`carnet_docente`),
  ADD CONSTRAINT `tramites_ibfk_9` FOREIGN KEY (`ID_ciclo`) REFERENCES `ciclos` (`ID_ciclo`);

--
-- Constraints for table `usuarios`
--
ALTER TABLE `usuarios`
  ADD CONSTRAINT `usuarios_ibfk_1` FOREIGN KEY (`id_rol`) REFERENCES `roles` (`id_rol`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
