-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 15-12-2018 a las 06:53:46
-- Versión del servidor: 10.1.35-MariaDB
-- Versión de PHP: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `bd_utec`
--
CREATE DATABASE IF NOT EXISTS `bd_utec` DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish_ci;
USE `bd_utec`;

DELIMITER $$
--
-- Procedimientos
--
DROP PROCEDURE IF EXISTS `SP_actividades_insertar`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_actividades_insertar` (IN `idcar` INT, IN `idmat` VARCHAR(100), IN `idsec` INT, IN `ideval` INT, IN `carDoc` VARCHAR(50), IN `conteo` INT, IN `idci` INT, IN `fecsol` DATETIME)  BEGIN
START TRANSACTION;


INSERT INTO tramites(`ID_carrera_materia`, `ID_seccion`, `ID_tipo_tramite`, `ID_eval`, `carnet_docente`, `autorizado`, `conteo`, `fechaRegistro`, `ID_ciclo`, `fechaEnvio`, `periodo`, `fechaSolicitud`)
SELECT ID_carrera_materia,idsec,1,ideval,carDoc,1, conteo, CURDATE() ,idci,CURDATE(),1,fecsol FROM carrera_materia cm left join carreras c on cm.ID_carrera = c.ID_carrera 
where cm.ID_carrera = idcar and cm.ID_materia =  idmat;



COMMIT;
END$$

DROP PROCEDURE IF EXISTS `SP_actividad_consultaID`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_actividad_consultaID` (IN `idTram` INT)  NO SQL
select T.*, SEC.seccion, TIPTRAM.nombreTramite, eva.descripcion as evaluacion, doc.docente_nombres, ci.nombre_ciclo, car.nombre_carrera, car.id_carrera as ID_carrera, mat.nombre_materia, mat.ID_materia from tramites as t left outer join secciones as sec on t.ID_SECCION=SEC.ID_seccion LEFT OUTER JOIN tipotramite AS tiptram on t.ID_TIPO_TRAMITE=TIPTRAM.ID_tipo_tramite left outer join evaluaciones as eva on t.ID_EVAL=EVA.ID_eval LEFT OUTER JOIN docentes AS DOC ON T.CARNET_DOCENTE=DOC.carnet_docente LEFT OUTER JOIN CICLOS AS CI ON T.ID_CICLO=CI.ID_ciclo left outer join carrera_materia as carMat on t.id_carrera_materia=carMat.ID_carrera_materia left outer join carreras as car on carMat.ID_carrera=car.ID_carrera left outer join materias as mat on mat.ID_materia=carMat.ID_materia where tiptram.ID_tipo_tramite=1 and ID_tramite=idTram$$

DROP PROCEDURE IF EXISTS `SP_actividad_consultar`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_actividad_consultar` ()  NO SQL
select T.*, SEC.seccion, TIPTRAM.nombreTramite, eva.descripcion as evaluacion, doc.docente_nombres, ci.nombre_ciclo, car.nombre_carrera, mat.nombre_materia  from tramites as t left outer join secciones as sec on t.ID_SECCION=SEC.ID_seccion LEFT OUTER JOIN tipotramite AS tiptram on t.ID_TIPO_TRAMITE=TIPTRAM.ID_tipo_tramite left outer join evaluaciones as eva on t.ID_EVAL=EVA.ID_eval LEFT OUTER JOIN docentes AS DOC ON T.CARNET_DOCENTE=DOC.carnet_docente LEFT OUTER JOIN CICLOS AS CI ON T.ID_CICLO=CI.ID_ciclo left outer join carrera_materia as carMat on t.id_carrera_materia=carMat.ID_carrera_materia left outer join carreras as car on carMat.ID_carrera=car.ID_carrera left outer join materias as mat on mat.ID_materia=carMat.ID_materia where tiptram.ID_tipo_tramite=1$$

DROP PROCEDURE IF EXISTS `SP_actividad_consultar_detalle`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_actividad_consultar_detalle` (IN `idTram` INT)  NO SQL
select repact.*, tipact.nombreActividad, a.alumno from reporteactividades as repact left outer join tipoactividad as tipact on repact.ID_tipo_actividad=tipact.ID_tipo_actividad 
left outer join alumnos as a on a.carnet=repact.carnet
where repact.ID_tramite=idTram and repact.estado=1$$

DROP PROCEDURE IF EXISTS `SP_actividad_consulta_linea`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_actividad_consulta_linea` (IN `carnet` VARCHAR(100), IN `idTram` INT)  NO SQL
select repact.*, tipact.nombreActividad, a.alumno from reporteactividades as repact left outer join tipoactividad as tipact on repact.ID_tipo_actividad=tipact.ID_tipo_actividad 
left outer join alumnos as a on a.carnet=repact.carnet
where repact.ID_tramite=idTram and a.carnet=carnet and repact.estado=1$$

DROP PROCEDURE IF EXISTS `SP_actividad_eliminar`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_actividad_eliminar` (IN `tram` INT)  NO SQL
update tramites set estado=0 where id_tramite=tram$$

DROP PROCEDURE IF EXISTS `SP_actividad_eliminar_linea`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_actividad_eliminar_linea` (IN `tram` INT, IN `car` VARCHAR(100))  NO SQL
update reporteactividades set estado=0 where ID_tramite=tram and carnet=car$$

DROP PROCEDURE IF EXISTS `SP_actividad_insertardet`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_actividad_insertardet` (IN `idTram` INT, IN `nota` FLOAT, IN `carnet` VARCHAR(50), IN `obser` TEXT)  NO SQL
INSERT INTO reporteactividades (`ID_tramite`,`nota`, `observaciones`, `carnet`, `id_tipo_actividad` )
VALUES(idTram,nota, obser, carnet,1)$$

DROP PROCEDURE IF EXISTS `SP_actividad_modificar`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_actividad_modificar` (IN `idCar` INT, IN `idMat` VARCHAR(100), IN `idSec` INT, IN `idEva` INT, IN `doc` VARCHAR(100), IN `conteo` INT, IN `idCi` INT, IN `fecha` INT, IN `idTram` INT, IN `est` INT)  NO SQL
update tramites set ID_carrera_materia=(SELECT ID_carrera_materia FROM carrera_materia cm left join carreras c on cm.ID_carrera = c.ID_carrera 
where cm.ID_carrera = idcar and cm.ID_materia =  idmat), ID_seccion=idSec,ID_eval=idEva, carnet_docente=doc, conteo=conteo, ID_ciclo=idCi, fechaSolicitud=fecha, estado=est where ID_tramite=idTram$$

DROP PROCEDURE IF EXISTS `SP_actividad_modificar_linea`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_actividad_modificar_linea` (IN `car` VARCHAR(100), IN `nota` FLOAT, IN `observ` TEXT, IN `idTram` INT)  NO SQL
update reporteactividades set  nota=nota, observaciones=observ where ID_tramite=idTram and carnet=car$$

DROP PROCEDURE IF EXISTS `SP_actividad_reporteEval_ciclos`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_actividad_reporteEval_ciclos` (IN `c` INT)  NO SQL
SELECT EVALUACIONES, count(*) as CONTEO FROM vw_rptactivciclo where  CICLO = c group by EVALUACIONES$$

DROP PROCEDURE IF EXISTS `SP_actividad_reportesEval_fechas`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_actividad_reportesEval_fechas` (IN `fecha1` DATE, IN `fecha2` DATE)  NO SQL
SELECT a.id_eval, b.descripcion as evaluacion, count(a.id_tramite) as conteo FROM tramites as a left outer join evaluaciones as b on a.ID_eval=b.ID_eval where a.fechaSolicitud>=fecha1 and a.fechaSolicitud<=fecha2 and a.ID_tipo_tramite=1 group by id_eval$$

DROP PROCEDURE IF EXISTS `SP_actividad_validarAlumno`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_actividad_validarAlumno` (IN `car` VARCHAR(100), IN `tram` INT)  NO SQL
select * from reporteactividades where carnet=car and ID_tramite=tram and estado=1$$

DROP PROCEDURE IF EXISTS `SP_alumnos_eliminar`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_alumnos_eliminar` (IN `car` TEXT)  NO SQL
UPDATE alumnos
SET estado = 0
WHERE carnet = car$$

DROP PROCEDURE IF EXISTS `SP_alumnos_insertar`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_alumnos_insertar` (IN `car` TEXT, IN `noms` TEXT, IN `cor` TEXT)  NO SQL
INSERT INTO alumnos(carnet,alumno,correo_alumno)
VALUES (car,ft_limpieza(noms),cor)$$

DROP PROCEDURE IF EXISTS `SP_alumnos_modificar`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_alumnos_modificar` (IN `car` TEXT, IN `noms` TEXT, IN `est` BOOLEAN, IN `cor` TEXT)  NO SQL
UPDATE alumnos	
SET alumno = noms,  estado = est, correo_alumno = cor
WHERE carnet = car$$

DROP PROCEDURE IF EXISTS `SP_alumnos_mostrargeneral`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_alumnos_mostrargeneral` ()  NO SQL
SELECT carnet, alumno,estado, correo_alumno, CONCAT(REPLACE(carnet,'-',''),' - ',alumno) as carAlum
FROM alumnos
WHERE estado = 1
order by carnet desc$$

DROP PROCEDURE IF EXISTS `SP_alumnos_mostrargeneral_CRUD`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_alumnos_mostrargeneral_CRUD` ()  NO SQL
SELECT carnet, alumno,estado, correo_alumno, CONCAT(REPLACE(carnet,'-',''),' - ',alumno) as carAlum
FROM alumnos
order by carnet desc$$

DROP PROCEDURE IF EXISTS `SP_alumnos_mostrarid`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_alumnos_mostrarid` (IN `car` TEXT)  NO SQL
SELECT * 
FROM alumnos 
where carnet = car$$

DROP PROCEDURE IF EXISTS `SP_asignarReciencreadoRolUsuario`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_asignarReciencreadoRolUsuario` (IN `r` VARCHAR(50), IN `nom` VARCHAR(50), IN `ape` VARCHAR(50), IN `cor` VARCHAR(100), IN `usu` VARCHAR(50), IN `cla` VARCHAR(100), IN `niv` INT)  BEGIN
START TRANSACTION;
insert into roles(rol)
values(r);

INSERT INTO usuarios (nombre, apellido, correo, usuario, clave, nivel, id_rol) 
VALUES (nom, ape, cor, usu, cla, niv, LAST_INSERT_ID());

COMMIT;
END$$

DROP PROCEDURE IF EXISTS `SP_carreras_eliminar`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_carreras_eliminar` (IN `id` INT)  NO SQL
UPDATE carreras
SET carreras.estado = 0
where ID_carrera = id$$

DROP PROCEDURE IF EXISTS `SP_carreras_insertar`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_carreras_insertar` (IN `nom` TEXT, IN `codCar` INT)  NO SQL
INSERT INTO carreras(nombre_carrera, Codigo_carrera)
VALUES (nom, codCar)$$

DROP PROCEDURE IF EXISTS `SP_carreras_modificar`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_carreras_modificar` (IN `nom` TEXT, IN `id` INT, IN `est` INT, IN `codCar` INT)  NO SQL
UPDATE carreras	SET nombre_carrera = nom, estado=est, Codigo_carrera=codCar where ID_carrera = id$$

DROP PROCEDURE IF EXISTS `SP_carreras_mostrargeneral`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_carreras_mostrargeneral` ()  NO SQL
SELECT * 
FROM carreras
WHERE estado = 1$$

DROP PROCEDURE IF EXISTS `SP_carreras_mostrargeneral_CRUD`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_carreras_mostrargeneral_CRUD` ()  NO SQL
SELECT * 
FROM carreras$$

DROP PROCEDURE IF EXISTS `SP_carreras_mostrarid`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_carreras_mostrarid` (IN `id` INT)  NO SQL
SELECT * 
FROM carreras 
where ID_carrera = id$$

DROP PROCEDURE IF EXISTS `SP_carrera_materia_agruparCarrera`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_carrera_materia_agruparCarrera` ()  NO SQL
SELECT a.ID_carrera, c.nombre_carrera
FROM carrera_materia as a left outer join carreras as c on a.ID_carrera=c.ID_carrera 
WHERE a.estado = 1 
group by a.ID_carrera$$

DROP PROCEDURE IF EXISTS `SP_carrera_materia_eliminar`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_carrera_materia_eliminar` (IN `id` INT)  NO SQL
UPDATE carrera_materia
SET estado = 0
where ID_carrera_materia = id$$

DROP PROCEDURE IF EXISTS `SP_carrera_materia_id`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_carrera_materia_id` (IN `id` INT)  NO SQL
SELECT a.ID_materia, A.ID_carrera, c.nombre_materia FROM carrera_materia as a left outer join materias as c on a.ID_materia=c.ID_materia  where a.ID_carrera_materia=id order by id_carrera$$

DROP PROCEDURE IF EXISTS `SP_carrera_materia_insertar`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_carrera_materia_insertar` (IN `idcar` INT, IN `idcate` INT, IN `idmate` TEXT)  NO SQL
INSERT INTO carrera_materia(ID_carrera,ID_catedra,ID_materia)
VALUES (idcar,idcate,idmate)$$

DROP PROCEDURE IF EXISTS `SP_carrera_materia_modificar`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_carrera_materia_modificar` (IN `est` BOOLEAN, IN `idcate` INT, IN `idmat` TEXT, IN `idcar` INT, IN `id` INT)  NO SQL
UPDATE carrera_materia	
SET  estado = est, ID_catedra = idcate, ID_materia = idmat, ID_carrera=idcar
where ID_carrera_materia = id$$

DROP PROCEDURE IF EXISTS `SP_carrera_materia_mostrargeneral`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_carrera_materia_mostrargeneral` ()  NO SQL
SELECT b.nombre_catedra, c.nombre_carrera, d.nombre_materia, a.*
FROM carrera_materia as a left outer join catedras as b on a.ID_catedra=b.ID_catedra left outer join carreras as c on a.ID_carrera=c.ID_carrera left outer join materias as d on a.ID_materia=d.ID_materia 
where a.estado = 1
order by a.id_carrera, a.id_catedra, a.id_materia$$

DROP PROCEDURE IF EXISTS `SP_carrera_materia_mostrargeneral_CRUD`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_carrera_materia_mostrargeneral_CRUD` ()  NO SQL
SELECT b.nombre_catedra, c.nombre_carrera, d.nombre_materia, a.*
FROM carrera_materia as a left outer join catedras as b on a.ID_catedra=b.ID_catedra left outer join carreras as c on a.ID_carrera=c.ID_carrera left outer join materias as d on a.ID_materia=d.ID_materia order by a.id_carrera, a.id_catedra, a.id_materia$$

DROP PROCEDURE IF EXISTS `SP_carrera_materia_mostrarid`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_carrera_materia_mostrarid` (IN `id` INT)  NO SQL
SELECT * 
FROM carrera_materia 
where ID_carrera_materia = id$$

DROP PROCEDURE IF EXISTS `SP_carrera_materia_retornovariable`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_carrera_materia_retornovariable` (IN `idc` INT, IN `idm` VARCHAR(50))  NO SQL
SELECT ID_carrera_materia FROM carrera_materia cm left join carreras c on cm.ID_carrera = c.ID_carrera 
where cm.ID_carrera = idc and cm.ID_materia =  idm$$

DROP PROCEDURE IF EXISTS `SP_carrera_materia_soloMaterias`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_carrera_materia_soloMaterias` ()  NO SQL
SELECT a.ID_materia, A.ID_carrera, c.nombre_materia FROM carrera_materia as a left outer join materias as c on a.ID_materia=c.ID_materia order by id_carrera$$

DROP PROCEDURE IF EXISTS `SP_catedras_eliminar`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_catedras_eliminar` (IN `id` INT)  NO SQL
UPDATE catedras
SET estado = 0
where ID_catedra = id$$

DROP PROCEDURE IF EXISTS `SP_catedras_insertar`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_catedras_insertar` (IN `idesc` INT, IN `nom` TEXT)  NO SQL
INSERT INTO catedras(ID_escuela,nombre_catedra)
VALUES (idesc,nom)$$

DROP PROCEDURE IF EXISTS `SP_catedras_modificar`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_catedras_modificar` (IN `est` BOOLEAN, IN `idesc` INT, IN `nom` TEXT, IN `id` INT)  NO SQL
UPDATE catedras	
SET  estado = est, ID_escuela = idesc, nombre_catedra = nom
where ID_catedra = id$$

DROP PROCEDURE IF EXISTS `SP_catedras_mostrargeneral`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_catedras_mostrargeneral` ()  NO SQL
SELECT catedras.*, escuela.nombre_escuela 
FROM catedras left outer join escuela on catedras.ID_escuela=escuela.id_escuela$$

DROP PROCEDURE IF EXISTS `SP_catedras_mostrarid`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_catedras_mostrarid` (IN `id` INT)  NO SQL
SELECT * 
FROM catedras 
where ID_catedra = id$$

DROP PROCEDURE IF EXISTS `SP_ciclos_eliminar`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_ciclos_eliminar` (IN `id` INT)  NO SQL
UPDATE ciclos
SET estado = 0
where ID_ciclo = id$$

DROP PROCEDURE IF EXISTS `SP_ciclos_insertar`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_ciclos_insertar` (IN `nom` TEXT)  NO SQL
INSERT INTO ciclos(nombre_ciclo)
VALUES (nom)$$

DROP PROCEDURE IF EXISTS `SP_ciclos_modificar`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_ciclos_modificar` (IN `est` BOOLEAN, IN `nom` TEXT, IN `id` INT)  NO SQL
UPDATE ciclos	
SET estado = est, nombre_ciclo = nom
where ID_ciclo = id$$

DROP PROCEDURE IF EXISTS `SP_ciclos_mostrargeneral`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_ciclos_mostrargeneral` ()  NO SQL
SELECT * 
FROM ciclos
where estado = 1$$

DROP PROCEDURE IF EXISTS `SP_ciclos_mostrargeneral_CRUD`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_ciclos_mostrargeneral_CRUD` ()  NO SQL
SELECT * 
FROM ciclos$$

DROP PROCEDURE IF EXISTS `SP_ciclos_mostrarid`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_ciclos_mostrarid` (IN `id` INT)  NO SQL
SELECT * 
FROM ciclos 
where ID_ciclo = id$$

DROP PROCEDURE IF EXISTS `SP_correcciones_consulta_Todos`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_correcciones_consulta_Todos` ()  NO SQL
SELECT DISTINCT tr.ID_tramite, cr.ID_correccion, crm.ID_carrera, crm.ID_materia, crm.ID_catedra, tr.ID_carrera_materia, tr.ID_eval, tr.carnet_docente, tr.fechaRegistro, tr.fechaEnvio, tr.periodo, tr.fechaSolicitud, cr.notaActual, cr.notaCorregida, cr.motivoCorreccion, cr.estadoCorreccion, cr.carnet, cr.estado, cr.personaRecibe, cr.fechaRecibe  FROM tramites tr INNER JOIN correcciones cr on cr.ID_tramite = tr.ID_tramite INNER JOIN carrera_materia crm ON tr.ID_carrera_materia = crm.ID_carrera_materia
WHERE cr.estado=1 AND tr.estado=1
ORDER BY tr.ID_tramite DESC$$

DROP PROCEDURE IF EXISTS `SP_correcciones_consulta_uno`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_correcciones_consulta_uno` (IN `idt` INT)  NO SQL
SELECT DISTINCT tr.ID_ciclo, tr.ID_tramite, tr.ID_seccion, cr.ID_correccion, crm.ID_carrera, crm.ID_materia, crm.ID_catedra, tr.ID_carrera_materia, tr.ID_eval, tr.carnet_docente, tr.fechaRegistro, tr.fechaEnvio, tr.periodo, tr.fechaSolicitud, cr.notaActual, cr.notaCorregida, cr.motivoCorreccion, cr.estadoCorreccion, cr.carnet, cr.estado, cr.personaRecibe, cr.fechaRecibe, cr.observaciones FROM tramites tr INNER JOIN correcciones cr on cr.ID_tramite = tr.ID_tramite INNER JOIN carrera_materia crm ON tr.ID_carrera_materia = crm.ID_carrera_materia
WHERE cr.estado=1 AND tr.estado=1 AND  cr.ID_tramite = idt
ORDER BY tr.ID_tramite DESC$$

DROP PROCEDURE IF EXISTS `SP_correccion_eliminar`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_correccion_eliminar` (IN `idt` INT)  NO SQL
update correcciones
set estado = 0
where ID_tramite = idt$$

DROP PROCEDURE IF EXISTS `SP_Correccion_insertar`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_Correccion_insertar` (IN `idc` INT, IN `idm` VARCHAR(100), IN `idsec` INT, IN `idev` INT, IN `cardoc` VARCHAR(50), IN `idci` INT, IN `per` VARCHAR(50), IN `fecsol` DATETIME, IN `car` VARCHAR(50), IN `idTram` INT, IN `notaActu` FLOAT, IN `notaCorreg` FLOAT, IN `motivoCorrec` VARCHAR(200), IN `estadoCorrec` VARCHAR(50), IN `carne` CHAR(12), IN `obcervac` VARCHAR(200), IN `usrRecibe` VARCHAR(100), IN `FechaRecibe` DATETIME)  BEGIN
START TRANSACTION;


INSERT INTO tramites(`ID_carrera_materia`, `ID_seccion`, `ID_tipo_tramite`, `ID_eval`, `carnet_docente`, `autorizado`, `conteo`, `fechaRegistro`, `ID_ciclo`, `fechaEnvio`, `periodo`, `fechaSolicitud`)
SELECT ID_carrera_materia,idsec,2,idev,cardoc,0,0,NOW(),idci,NULL,per,fecsol FROM carrera_materia cm left join carreras c on cm.ID_carrera = c.ID_carrera 
where cm.ID_carrera = idc and cm.ID_materia =  idm;


INSERT INTO correcciones (
	ID_tramite, notaActual, 
    notaCorregida, 
    motivoCorreccion, 
    estadoCorreccion, 
    carnet, 
    observaciones, 
    personaRecibe, 
    fechaRecibe
) VALUES (
	LAST_INSERT_ID(),
	notaActu,
    notaCorreg,
    motivoCorrec,
    estadoCorrec,
    carne,
    obcervac,
    usrRecibe,
    FechaRecibe
);


COMMIT;
END$$

DROP PROCEDURE IF EXISTS `SP_Correccion_insertarok`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_Correccion_insertarok` (IN `idc` INT, IN `idm` VARCHAR(100), IN `idsec` INT, IN `idev` INT, IN `cardoc` VARCHAR(50), IN `idci` INT, IN `per` VARCHAR(50), IN `fecsol` DATETIME, IN `notaActu` FLOAT, IN `notaCorreg` FLOAT, IN `motivoCorrec` VARCHAR(200), IN `estadoCorrec` VARCHAR(50), IN `carne` CHAR(12), IN `obcervac` VARCHAR(200), IN `usrRecibe` VARCHAR(100), IN `FechaRecibe` DATETIME)  BEGIN
START TRANSACTION;


INSERT INTO tramites(`ID_carrera_materia`, `ID_seccion`, `ID_tipo_tramite`, `ID_eval`, `carnet_docente`, `autorizado`, `conteo`, `fechaRegistro`, `ID_ciclo`, `fechaEnvio`, `periodo`, `fechaSolicitud`)
SELECT ID_carrera_materia,idsec,2,idev,cardoc,0,0,NOW(),idci,NOW(),per,fecsol FROM carrera_materia cm left join carreras c on cm.ID_carrera = c.ID_carrera 
where cm.ID_carrera = idc and cm.ID_materia =  idm;


INSERT INTO correcciones (
	ID_tramite, notaActual, 
    notaCorregida, 
    motivoCorreccion, 
    estadoCorreccion, 
    carnet, 
    observaciones, 
    personaRecibe, 
    fechaRecibe
) VALUES (
	LAST_INSERT_ID(),
	notaActu,
    notaCorreg,
    motivoCorrec,
    estadoCorrec,
    carne,
    obcervac,
    usrRecibe,
    FechaRecibe
);


COMMIT;
END$$

DROP PROCEDURE IF EXISTS `SP_Correccion_Modificar1`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_Correccion_Modificar1` (IN `idt` INT, IN `idc` INT, IN `idm` VARCHAR(100), IN `idsec` INT, IN `idev` INT, IN `cardoc` VARCHAR(50), IN `idci` INT, IN `per` VARCHAR(50), IN `fecsol` DATETIME, IN `notaActu` FLOAT, IN `notaCorreg` FLOAT, IN `motivoCorrec` VARCHAR(200), IN `estadoCorrec` VARCHAR(50), IN `carne` CHAR(12), IN `obcervac` VARCHAR(200), IN `usrRecibe` VARCHAR(100), IN `FechaRecibe` DATETIME)  BEGIN
START TRANSACTION;


UPDATE tramites SET 
    `ID_carrera_materia`=(SELECT ID_carrera_materia FROM carrera_materia cm left join carreras c on cm.ID_carrera = c.ID_carrera 
where cm.ID_carrera = idc and cm.ID_materia =  idm), 
    `ID_seccion`=idsec, 
    `ID_tipo_tramite`=2, 
    `ID_eval`=idev, 
    `carnet_docente`= cardoc, 
    `autorizado`=0, 
    `conteo`=0, 
    `fechaRegistro`=NOW(), 
    `ID_ciclo`=idci, 
    `fechaEnvio`=NOW(), 
    `periodo`=per, 
    `fechaSolicitud`=fecsol
    WHERE tramites.ID_tramite = idt;


UPDATE correcciones SET  
    notaActual=notaActu, 
    notaCorregida=notaCorreg, 
    motivoCorreccion=motivoCorrec, 
    estadoCorreccion=estadoCorrec, 
    carnet=carne, 
    observaciones=obcervac, 
    personaRecibe=usrRecibe, 
    fechaRecibe=FechaRecibe

WHERE correcciones.ID_tramite = idt;


COMMIT;
END$$

DROP PROCEDURE IF EXISTS `SP_correccion_verIdCar`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_correccion_verIdCar` (IN `idcm` INT)  NO SQL
select crm.ID_carrera, crm.ID_catedra, crm.ID_materia, cr.nombre_carrera FROM carrera_materia crm INNER JOIN carreras cr ON cr.ID_carrera = crm.ID_carrera where cr.estado = 1 AND crm.ID_carrera_materia = idcm$$

DROP PROCEDURE IF EXISTS `SP_correccion_verMateria`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_correccion_verMateria` (IN `idcm` INT)  NO SQL
select ID_materia FROM carrera_materia 
where ID_carrera_materia = idcm$$

DROP PROCEDURE IF EXISTS `SP_correc_estado_updt`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_correc_estado_updt` (IN `est` VARCHAR(50), IN `idtr` INT)  NO SQL
UPDATE correcciones SET estadoCorreccion = est WHERE ID_tramite = idtr$$

DROP PROCEDURE IF EXISTS `SP_diferidos_consultarID`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_diferidos_consultarID` (IN `idTram` INT)  NO SQL
select T.*, SEC.seccion, TIPTRAM.nombreTramite, eva.descripcion as evaluacion, doc.docente_nombres, ci.nombre_ciclo, car.nombre_carrera, car.id_carrera as ID_carrera, mat.nombre_materia, mat.ID_materia from tramites as t left outer join secciones as sec on t.ID_SECCION=SEC.ID_seccion LEFT OUTER JOIN tipotramite AS tiptram on t.ID_TIPO_TRAMITE=TIPTRAM.ID_tipo_tramite left outer join evaluaciones as eva on t.ID_EVAL=EVA.ID_eval LEFT OUTER JOIN docentes AS DOC ON T.CARNET_DOCENTE=DOC.carnet_docente LEFT OUTER JOIN CICLOS AS CI ON T.ID_CICLO=CI.ID_ciclo left outer join carrera_materia as carMat on t.id_carrera_materia=carMat.ID_carrera_materia left outer join carreras as car on carMat.ID_carrera=car.ID_carrera left outer join materias as mat on mat.ID_materia=carMat.ID_materia where tiptram.ID_tipo_tramite=3 and ID_tramite=idTram$$

DROP PROCEDURE IF EXISTS `SP_diferidos_eliminar`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_diferidos_eliminar` (IN `id` INT)  NO SQL
update diferidos
set estado = 0
where ID_tramite = id$$

DROP PROCEDURE IF EXISTS `SP_diferidos_insertar`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_diferidos_insertar` (IN `idc` INT, IN `idm` VARCHAR(100), IN `idsec` INT, IN `idev` INT, IN `cardoc` VARCHAR(50), IN `idci` INT, IN `per` VARCHAR(50), IN `fecsol` DATETIME, IN `car` VARCHAR(50))  BEGIN
START TRANSACTION;


INSERT INTO tramites(`ID_carrera_materia`, `ID_seccion`, `ID_tipo_tramite`, `ID_eval`, `carnet_docente`, `autorizado`, `conteo`, `fechaRegistro`, `ID_ciclo`, `fechaEnvio`, `periodo`, `fechaSolicitud`)
SELECT ID_carrera_materia,idsec,3,idev,cardoc,0,0,NOW(),idci,NULL,per,fecsol FROM carrera_materia cm left join carreras c on cm.ID_carrera = c.ID_carrera 
where cm.ID_carrera = idc and cm.ID_materia =  idm;


INSERT INTO diferidos (`ID_tramite`,`carnet`)
VALUES(LAST_INSERT_ID(),car);


COMMIT;
END$$

DROP PROCEDURE IF EXISTS `SP_diferidos_modificar`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_diferidos_modificar` (IN `idt` INT, IN `idc` INT, IN `idm` VARCHAR(100), IN `idsec` INT, IN `idev` INT, IN `cardoc` VARCHAR(50), IN `idci` INT, IN `per` VARCHAR(50), IN `fecsol` DATETIME, IN `car` VARCHAR(50), IN `est` BOOLEAN)  BEGIN
START TRANSACTION;

UPDATE tramites
SET ID_carrera_materia = (select ID_carrera_materia FROM carrera_materia cm left join carreras c on cm.ID_carrera = c.ID_carrera 
where cm.ID_carrera = idc and cm.ID_materia =  idm LIMIT 1), ID_seccion = idsec, ID_eval = idev,
carnet_docente = cardoc, ID_ciclo = idci, periodo = per,fechaSolicitud = fecsol, estado = est
WHERE ID_tramite = idt;

UPDATE diferidos
SET carnet = car, estado = est
WHERE ID_tramite = idt;


COMMIT;
END$$

DROP PROCEDURE IF EXISTS `SP_diferidos_MostrarGeneral`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_diferidos_MostrarGeneral` ()  NO SQL
SELECT t.ID_tramite, c.nombre_carrera, a.alumno, m.nombre_materia, e.nombre_escuela, s.seccion, dc.docente_nombres,ev.descripcion,t.periodo,t.fechaSolicitud
,t.autorizado
FROM tramites t  
left join diferidos d on t.id_tramite = d.ID_tramite 
left join carrera_materia cm on t.ID_carrera_materia = cm.ID_carrera_materia
left join alumnos a on a.carnet = d.carnet
left join materias m on m.ID_materia = cm.ID_materia 
left join carreras c on c.ID_carrera = cm.ID_carrera
left join catedras ct on ct.ID_catedra = cm.ID_catedra
left join escuela e on e.ID_escuela = ct.ID_escuela
left join secciones s on s.ID_seccion = t.ID_seccion
left join docentes dc on dc.carnet_docente = t.carnet_docente
left join evaluaciones ev on ev.ID_eval = t.ID_eval
left join tipotramite tt on tt.ID_tipo_tramite = t.ID_tipo_tramite
WHERE tt.ID_tipo_tramite = 3$$

DROP PROCEDURE IF EXISTS `SP_diferidos_MostrarUno`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_diferidos_MostrarUno` (IN `id` INT)  NO SQL
select * 
from vw_diferidostodos
where IDTRAMITE = id$$

DROP PROCEDURE IF EXISTS `SP_docentes_consultarcarnet`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_docentes_consultarcarnet` (IN `car` VARCHAR(12))  NO SQL
SELECT * 
FROM docentes
where carnet_docente = car$$

DROP PROCEDURE IF EXISTS `SP_docentes_ConsultarCRUD`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_docentes_ConsultarCRUD` ()  NO SQL
SELECT * FROM docentes$$

DROP PROCEDURE IF EXISTS `SP_docentes_consultargeneral`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_docentes_consultargeneral` ()  NO SQL
SELECT carnet_docente,correo_docente, docente_nombres, estado,  CONCAT(REPLACE(carnet_docente,'-','') ,' - ', docente_nombres) as carNom
FROM docentes WHERE estado = 1
order by docente_nombres ASC$$

DROP PROCEDURE IF EXISTS `SP_docentes_eliminar`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_docentes_eliminar` (IN `car` VARCHAR(12))  NO SQL
UPDATE docentes
SET estado = 0
WHERE  carnet_docente = car$$

DROP PROCEDURE IF EXISTS `SP_docentes_insertar`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_docentes_insertar` (IN `car` VARCHAR(12), IN `noms` VARCHAR(100), IN `cor` VARCHAR(500))  NO SQL
insert into docentes (carnet_docente,docente_nombres,correo_docente)
values (car,noms,cor)$$

DROP PROCEDURE IF EXISTS `SP_docentes_modificar`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_docentes_modificar` (IN `car` VARCHAR(12), IN `noms` VARCHAR(100), IN `est` BOOLEAN, IN `cor` VARCHAR(500))  NO SQL
UPDATE docentes	 
SET docente_nombres = noms, estado = est, correo_docente = cor
WHERE carnet_docente = car$$

DROP PROCEDURE IF EXISTS `SP_escuelas_eliminar`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_escuelas_eliminar` (IN `id` INT)  NO SQL
UPDATE escuela
SET estado = 0
WHERE id_escuela = id$$

DROP PROCEDURE IF EXISTS `SP_escuelas_insertar`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_escuelas_insertar` (IN `nom` TEXT, IN `id_fac` INT)  NO SQL
INSERT INTO escuela(nombre_escuela,ID_facultad)
VALUES (nom,id_fac)$$

DROP PROCEDURE IF EXISTS `SP_escuelas_modificar`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_escuelas_modificar` (IN `id` INT, IN `nom` TEXT, IN `id_fac` INT, IN `est` BOOLEAN)  NO SQL
UPDATE escuela	
SET nombre_escuela = nom, estado = est, id_facultad = id_fac
WHERE id_escuela = id$$

DROP PROCEDURE IF EXISTS `SP_escuelas_mostrargeneral`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_escuelas_mostrargeneral` ()  NO SQL
SELECT a.*, b.nombre_facultad
FROM escuela as a left outer join facultades as b on a.ID_facultad=b.id_facultad
where a.estado = 1$$

DROP PROCEDURE IF EXISTS `SP_escuelas_mostrargeneral_CRUD`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_escuelas_mostrargeneral_CRUD` ()  NO SQL
SELECT a.*, b.nombre_facultad
FROM escuela as a left outer join facultades as b on a.ID_facultad=b.id_facultad$$

DROP PROCEDURE IF EXISTS `SP_escuelas_mostrarid`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_escuelas_mostrarid` (IN `id` INT)  NO SQL
SELECT * 
FROM escuela
where id_escuela = id$$

DROP PROCEDURE IF EXISTS `SP_evaluaciones_eliminar`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_evaluaciones_eliminar` (IN `id` INT)  NO SQL
UPDATE evaluaciones
SET estado = 0
where ID_eval = id$$

DROP PROCEDURE IF EXISTS `SP_evaluaciones_insertar`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_evaluaciones_insertar` (IN `des` TEXT)  NO SQL
INSERT INTO evaluaciones(descripcion)
VALUES (des)$$

DROP PROCEDURE IF EXISTS `SP_evaluaciones_modificar`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_evaluaciones_modificar` (IN `des` TEXT, IN `est` BOOLEAN, IN `id` INT)  NO SQL
UPDATE evaluaciones	
SET descripcion = des, estado = est
where ID_eval = id$$

DROP PROCEDURE IF EXISTS `SP_evaluaciones_mostrargeneral`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_evaluaciones_mostrargeneral` ()  NO SQL
SELECT * 
FROM evaluaciones
where estado = 1$$

DROP PROCEDURE IF EXISTS `SP_evaluaciones_mostrargeneral_CRUD`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_evaluaciones_mostrargeneral_CRUD` ()  NO SQL
SELECT * 
FROM evaluaciones$$

DROP PROCEDURE IF EXISTS `SP_evaluaciones_mostrarid`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_evaluaciones_mostrarid` (IN `id` INT)  NO SQL
SELECT * 
FROM evaluaciones 
where ID_eval = id$$

DROP PROCEDURE IF EXISTS `SP_facultades_eliminar`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_facultades_eliminar` (IN `id` INT)  NO SQL
UPDATE facultades
SET estado = 0
WHERE id_facultad = id$$

DROP PROCEDURE IF EXISTS `SP_facultades_insertar`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_facultades_insertar` (IN `nom` VARCHAR(100))  NO SQL
INSERT INTO facultades(nombre_facultad)
VALUES (nom)$$

DROP PROCEDURE IF EXISTS `SP_facultades_modificar`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_facultades_modificar` (IN `nom` TEXT, IN `est` BOOLEAN, IN `id` INT)  NO SQL
UPDATE facultades	
SET nombre_facultad = nom, estado = est
WHERE id_facultad = id$$

DROP PROCEDURE IF EXISTS `SP_facultades_mostrargeneral`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_facultades_mostrargeneral` ()  NO SQL
SELECT * 
FROM facultades$$

DROP PROCEDURE IF EXISTS `SP_facultades_mostrarid`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_facultades_mostrarid` (IN `id` INT)  NO SQL
SELECT * 
FROM facultades 
where id_facultad = id$$

DROP PROCEDURE IF EXISTS `SP_login2`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_login2` (IN `usu` VARCHAR(100), IN `cla` VARCHAR(200))  NO SQL
SELECT ID_usuario, usuarios.nombre, usuarios.apellido, usuarios.id_rol, usuarios.correo, usuarios.estado
FROM usuarios
WHERE usu = usuario AND cla = CAST(AES_DECRYPT(clave,'tesis')as char(100)) AND estado = 1$$

DROP PROCEDURE IF EXISTS `SP_materias_eliminar`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_materias_eliminar` (IN `id` VARCHAR(12))  NO SQL
UPDATE materias 
SET estado = 0
WHERE  ID_materia = id$$

DROP PROCEDURE IF EXISTS `SP_materias_insertar`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_materias_insertar` (IN `id` VARCHAR(12), IN `nom` VARCHAR(100))  NO SQL
INSERT INTO materias (ID_materia,nombre_materia)
VALUES (id,nom)$$

DROP PROCEDURE IF EXISTS `SP_materias_modificar`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_materias_modificar` (IN `id` VARCHAR(12), IN `nom` VARCHAR(100), IN `est` BOOLEAN)  NO SQL
UPDATE materias 
SET   nombre_materia = nom, estado =  est
WHERE  ID_materia = id$$

DROP PROCEDURE IF EXISTS `SP_materias_mostrargeneral`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_materias_mostrargeneral` ()  NO SQL
SELECT *
FROM materias
where estado = 1$$

DROP PROCEDURE IF EXISTS `SP_materias_mostrargeneral_CRUD`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_materias_mostrargeneral_CRUD` ()  NO SQL
SELECT *
FROM materias$$

DROP PROCEDURE IF EXISTS `SP_materias_mostrarID`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_materias_mostrarID` (IN `id` VARCHAR(12))  NO SQL
SELECT *
FROM materias
WHERE  ID_materia = id$$

DROP PROCEDURE IF EXISTS `SP_menu_eliminar`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_menu_eliminar` (IN `id` INT)  NO SQL
UPDATE menu
SET estado = 0
where id_menu = id$$

DROP PROCEDURE IF EXISTS `SP_menu_insertar`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_menu_insertar` (IN `m` TEXT, IN `i` TEXT)  NO SQL
INSERT INTO menu(menu,icono)
VALUES (m,i)$$

DROP PROCEDURE IF EXISTS `SP_menu_modificar`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_menu_modificar` (IN `m` TEXT, IN `i` TEXT, IN `id` INT)  NO SQL
UPDATE menu	
SET  menu = m, icono = i
where id_menu = id$$

DROP PROCEDURE IF EXISTS `SP_menu_mostrargeneral`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_menu_mostrargeneral` ()  NO SQL
SELECT * 
FROM menu$$

DROP PROCEDURE IF EXISTS `SP_menu_mostrarid`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_menu_mostrarid` (IN `id` INT)  NO SQL
SELECT * 
FROM menu 
where id_menu = id$$

DROP PROCEDURE IF EXISTS `SP_opciones_mostrarSegunRol`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_opciones_mostrarSegunRol` (IN `idr` INT)  NO SQL
SELECT 
	 om.id_opcion,
    opcion,
    CASE WHEN id_rol IS NULL THEN 0 ELSE id_rol END AS id_rol, 
    CASE WHEN acceso IS NULL THEN 0 ELSE acceso END AS acceso,
    CASE WHEN url IS NULL THEN 0 ELSE url END AS url,
    CASE WHEN id_rol IS NULL OR (id_rol <> idr) THEN 0 ELSE 1 END AS activo 
    FROM opcionmenu om 
    LEFT JOIN rol_menu rm ON rm.id_opcion = om.id_opcion$$

DROP PROCEDURE IF EXISTS `SP_opcionmenu_eliminar`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_opcionmenu_eliminar` (IN `id` INT)  NO SQL
UPDATE opcionmenu
SET estado = 0
where id_opcion = id$$

DROP PROCEDURE IF EXISTS `SP_opcionmenu_insertar`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_opcionmenu_insertar` (IN `opc` TEXT, IN `u_r_l` LONGTEXT, IN `idm` INT)  NO SQL
INSERT INTO opcionmenu(opcion,url,id_menu)
VALUES (opc,u_r_l,idm)$$

DROP PROCEDURE IF EXISTS `SP_opcionmenu_modificar`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_opcionmenu_modificar` (IN `opc` TEXT, IN `u_r_l` TEXT, IN `idm` INT, IN `est` BOOLEAN, IN `id` INT)  NO SQL
UPDATE opcionmenu	
SET  opcion = opc, url = u_r_l, id_menu = idm, estado = est 
where id_opcion = id$$

DROP PROCEDURE IF EXISTS `SP_opcionmenu_mostrargeneral`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_opcionmenu_mostrargeneral` ()  NO SQL
SELECT * 
FROM opcionmenu$$

DROP PROCEDURE IF EXISTS `SP_opcionmenu_mostrarid`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_opcionmenu_mostrarid` (IN `id` INT)  NO SQL
SELECT * 
FROM opcionmenu 
where id_opcion = id$$

DROP PROCEDURE IF EXISTS `SP_roles_eliminar`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_roles_eliminar` (IN `id` INT)  NO SQL
UPDATE roles
SET estado = 0
where id_rol = id$$

DROP PROCEDURE IF EXISTS `SP_roles_insertar`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_roles_insertar` (IN `r` TEXT)  NO SQL
INSERT INTO roles(rol)
VALUES (r)$$

DROP PROCEDURE IF EXISTS `SP_roles_modificar`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_roles_modificar` (IN `r` TEXT, IN `est` BOOLEAN, IN `id` INT)  NO SQL
UPDATE roles	
SET  rol = r, estado = est 
where id_rol = id$$

DROP PROCEDURE IF EXISTS `SP_roles_mostrargeneral`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_roles_mostrargeneral` ()  NO SQL
SELECT * 
FROM roles$$

DROP PROCEDURE IF EXISTS `SP_roles_mostrarid`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_roles_mostrarid` (IN `id` INT)  NO SQL
SELECT * 
FROM roles 
where id_rol = id$$

DROP PROCEDURE IF EXISTS `SP_roles_ultimo`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_roles_ultimo` ()  NO SQL
SELECT MAX(`id_rol`) AS id FROM roles$$

DROP PROCEDURE IF EXISTS `SP_rol_menu_insertar`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_rol_menu_insertar` (IN `idop` INT, IN `acc` BOOLEAN, IN `idr` INT)  NO SQL
INSERT INTO rol_menu(id_opcion,acceso,id_rol)
VALUES (idop,acc,idr)$$

DROP PROCEDURE IF EXISTS `SP_rol_menu_mostrarsegunRol`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_rol_menu_mostrarsegunRol` (IN `idr` INT)  NO SQL
SELECT * 
FROM rol_menu 
where id_rol = idr and acceso=1$$

DROP PROCEDURE IF EXISTS `SP_secciones_eliminar`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_secciones_eliminar` (IN `id` INT)  NO SQL
UPDATE secciones
SET estado = 0
where ID_seccion = id$$

DROP PROCEDURE IF EXISTS `SP_secciones_insertar`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_secciones_insertar` (IN `sec` TEXT)  NO SQL
INSERT INTO secciones(seccion)
VALUES (sec)$$

DROP PROCEDURE IF EXISTS `SP_secciones_modificar`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_secciones_modificar` (IN `est` BOOLEAN, IN `sec` TEXT, IN `id` INT)  NO SQL
UPDATE secciones	
SET  estado = est, seccion = sec
where ID_seccion = id$$

DROP PROCEDURE IF EXISTS `SP_secciones_mostrargeneral`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_secciones_mostrargeneral` ()  NO SQL
SELECT * 
FROM secciones
where estado = 1$$

DROP PROCEDURE IF EXISTS `SP_secciones_mostrargeneral_CRUD`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_secciones_mostrargeneral_CRUD` ()  NO SQL
SELECT * 
FROM secciones$$

DROP PROCEDURE IF EXISTS `SP_secciones_mostrarid`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_secciones_mostrarid` (IN `id` INT)  NO SQL
SELECT * 
FROM secciones 
where ID_seccion = id$$

DROP PROCEDURE IF EXISTS `SP_seccion_ver_uno`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_seccion_ver_uno` (IN `idsec` INT)  NO SQL
SELECT ID_seccion, seccion, estado FROM secciones WHERE ID_seccion = idsec$$

DROP PROCEDURE IF EXISTS `SP_tipoactividad_eliminar`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_tipoactividad_eliminar` (IN `id` INT)  NO SQL
UPDATE tipoactividad
SET estado = 0
where ID_tipo_actividad = id$$

DROP PROCEDURE IF EXISTS `SP_tipoactividad_insertar`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_tipoactividad_insertar` (IN `nom` TEXT)  NO SQL
INSERT INTO tipoactividad(nombreActividad)
VALUES (nom)$$

DROP PROCEDURE IF EXISTS `SP_tipoactividad_modificar`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_tipoactividad_modificar` (IN `est` BOOLEAN, IN `nom` TEXT, IN `id` INT)  NO SQL
UPDATE tipoactividad	
SET  estado = est, nombreActividad = nom
where ID_tipo_actividad = id$$

DROP PROCEDURE IF EXISTS `SP_tipoactividad_mostrargeneral`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_tipoactividad_mostrargeneral` ()  NO SQL
SELECT * 
FROM tipoactividad$$

DROP PROCEDURE IF EXISTS `SP_tipoactividad_mostrarid`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_tipoactividad_mostrarid` (IN `id` INT)  NO SQL
SELECT * 
FROM tipoactividad 
where ID_tipo_actividad = id$$

DROP PROCEDURE IF EXISTS `SP_tipotramite_eliminar`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_tipotramite_eliminar` (IN `id` INT)  NO SQL
UPDATE tipotramite
SET estado = 0
where ID_tipo_actividad = id$$

DROP PROCEDURE IF EXISTS `SP_tipotramite_insertar`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_tipotramite_insertar` (IN `nom` TEXT)  NO SQL
INSERT INTO tipotramite(nombreTramite)
VALUES (nom)$$

DROP PROCEDURE IF EXISTS `SP_tipotramite_modificar`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_tipotramite_modificar` (IN `est` BOOLEAN, IN `nom` TEXT, IN `id` INT)  NO SQL
UPDATE tipotramite	
SET  estado = est, nombreActividad = nom
where ID_tipo_actividad = id$$

DROP PROCEDURE IF EXISTS `SP_tipotramite_mostrargeneral`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_tipotramite_mostrargeneral` ()  NO SQL
SELECT * 
FROM tipotramite$$

DROP PROCEDURE IF EXISTS `SP_tipotramite_mostrarid`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_tipotramite_mostrarid` (IN `id` INT)  NO SQL
SELECT * 
FROM tipotramite 
where ID_tipo_tramite = id$$

DROP PROCEDURE IF EXISTS `SP_tramites_correccion_insert`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_tramites_correccion_insert` (IN `idCarMa` INT, IN `idSec` INT, IN `idEval` INT, IN `CarnDoce` VARCHAR(12), IN `fRegist` DATETIME, IN `idCiclo` INT, IN `fEnvio` DATETIME, IN `perio` INT(11), IN `fSolici` DATETIME)  NO SQL
INSERT INTO tramites
( 
 ID_carrera_materia, 
 ID_seccion, 
 estado, 
 ID_tipo_tramite, 
 ID_eval, 
 carnet_docente, 
 autorizado, 
 conteo, 
 fechaRegistro, 
 ID_ciclo, 
 fechaEnvio, 
 periodo, 
 fechaSolicitud)
VALUES 
(
 idCarMa,
 idSec,
 1,
 2,
 idEval,
 CarnDoce,
 1,
 1,
 fRegist,
 idCiclo,
 fEnvio,
 perio,
 fSolici)$$

DROP PROCEDURE IF EXISTS `SP_tramite_mostrarMaximo`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_tramite_mostrarMaximo` ()  NO SQL
SELECT case when (max(ID_tramite)<1 OR max(ID_tramite) IS NULL )then 1 else max(ID_tramite)+1 end as max FROM `tramites`$$

DROP PROCEDURE IF EXISTS `SP_urlSec_ver`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_urlSec_ver` (IN `idrol` INT)  NO SQL
SELECT CASE WHEN om.url IS NULL THEN'http://localhost/Cisneros/Tesis/index.php?c=user' ELSE om.url END url 

FROM rol_menu rm
inner join opcionmenu om on om.id_opcion = rm.id_opcion
left join menu m on m.id_menu = om.id_menu
where  id_rol = idrol
ORDER BY m.menu ASC, rm.id_opcion asc$$

DROP PROCEDURE IF EXISTS `SP_usuarios_eliminar`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_usuarios_eliminar` (IN `id` INT)  NO SQL
UPDATE usuarios
SET estado = 0
WHERE ID_usuario = id$$

DROP PROCEDURE IF EXISTS `SP_usuarios_insertar`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_usuarios_insertar` (IN `nom` VARCHAR(50), IN `ape` VARCHAR(50), IN `cor` VARCHAR(500), IN `usuario` VARCHAR(20), IN `cla` VARCHAR(100), IN `est` BOOLEAN, IN `nivel` INT, IN `rol` INT)  NO SQL
INSERT INTO usuarios(nombre, apellido,correo,usuario,clave,estado,nivel,id_rol)
VALUES(nom,ape,cor,usuario,AES_ENCRYPT(cla,'tesis'),est,nivel,rol)$$

DROP PROCEDURE IF EXISTS `SP_usuarios_modificar`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_usuarios_modificar` (IN `nom` VARCHAR(50), IN `ape` VARCHAR(50), IN `cor` VARCHAR(100), IN `clav` VARCHAR(100), IN `usu` VARCHAR(100), IN `id` INT)  NO SQL
UPDATE usuarios
SET nombre = nom, apellido = ape, correo = cor, usuario=usu, clave = AES_ENCRYPT(clav,'tesis'), fecha_modificacion = NOW()
WHERE ID_usuario = id$$

DROP PROCEDURE IF EXISTS `SP_usuarios_momenu`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_usuarios_momenu` (IN `idrol` INT)  NO SQL
SELECT m.menu, m.icono, om.opcion, CASE WHEN om.url IS NULL THEN'http://localhost/tesis/index.php?c=user' ELSE om.url END url, rm.acceso, id_rol 
FROM rol_menu rm
inner join opcionmenu om on om.id_opcion = rm.id_opcion
left join menu m on m.id_menu = om.id_menu
where rm.acceso = 1 and id_rol = idrol and om.estado=1
ORDER BY m.menu ASC, rm.id_opcion asc$$

DROP PROCEDURE IF EXISTS `SP_usuarios_mostrar`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_usuarios_mostrar` ()  NO SQL
select ID_usuario, nombre, apellido, correo,usuario, clave, fecha_creacion, estado, nivel  
FROM usuarios where estado=1$$

DROP PROCEDURE IF EXISTS `SP_usuarios_mostrar_uno`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_usuarios_mostrar_uno` (IN `id` INT)  NO SQL
SELECT u.ID_usuario, u.nombre, u.apellido, u.correo, u.usuario,  aes_decrypt(u.clave, 'tesis') as clave, u.estado, u.fecha_creacion, u.nivel, u.fecha_modificacion, u.id_rol  FROM usuarios u WHERE `ID_usuario` = id$$

DROP PROCEDURE IF EXISTS `SP_usuarios_opc_menu`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_usuarios_opc_menu` ()  NO SQL
select id_opcion, opcion, estado from opcionmenu WHERE estado=1$$

DROP PROCEDURE IF EXISTS `SP_usuario_Modif_Rol`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_usuario_Modif_Rol` (IN `id` INT, IN `rol` INT)  NO SQL
UPDATE usuarios
SET id_rol = rol, fecha_modificacion = NOW()
WHERE ID_usuario = id$$

DROP PROCEDURE IF EXISTS `SP_usuario_ultimo`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_usuario_ultimo` ()  NO SQL
SELECT MAX(`ID_usuario`) AS id FROM usuarios$$

DROP PROCEDURE IF EXISTS `SP_validar_login`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_validar_login` (IN `usu` VARCHAR(100), IN `cla` VARCHAR(200))  NO SQL
SELECT COUNT(*)
FROM usuarios
WHERE usu = usuario AND cla = CAST(AES_DECRYPT(clave,'tesis')as char(100))$$

DROP PROCEDURE IF EXISTS `SP_vallida_carnet`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_vallida_carnet` (IN `carn` VARCHAR(100))  NO SQL
SELECT COUNT(alumno) AS CONTEO FROM alumnos WHERE carnet=carn$$

DROP PROCEDURE IF EXISTS `sp_verDocen`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_verDocen` (IN `p` VARCHAR(50))  NO SQL
SELECT carnet_docente FROM docentes  WHERE docente_nombres = p$$

DROP PROCEDURE IF EXISTS `SP_vwcarreras_Mostrar`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_vwcarreras_Mostrar` ()  NO SQL
SELECT  * 
FROM vw_mostrarCarreras$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alumnos`
--

DROP TABLE IF EXISTS `alumnos`;
CREATE TABLE `alumnos` (
  `carnet` char(12) NOT NULL,
  `alumno` varchar(50) DEFAULT NULL,
  `estado` tinyint(1) DEFAULT '1',
  `correo_alumno` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELACIONES PARA LA TABLA `alumnos`:
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `carreras`
--

DROP TABLE IF EXISTS `carreras`;
CREATE TABLE `carreras` (
  `ID_carrera` bigint(20) NOT NULL,
  `nombre_carrera` varchar(500) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL,
  `estado` tinyint(1) DEFAULT '1',
  `Codigo_carrera` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELACIONES PARA LA TABLA `carreras`:
--

--
-- Disparadores `carreras`
--
DROP TRIGGER IF EXISTS `TRG_carrera_eliminar`;
DELIMITER $$
CREATE TRIGGER `TRG_carrera_eliminar` AFTER UPDATE ON `carreras` FOR EACH ROW update carrera_materia  set  estado = new.estado  where ID_carrera = old.id_Carrera
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `carrera_materia`
--

DROP TABLE IF EXISTS `carrera_materia`;
CREATE TABLE `carrera_materia` (
  `ID_carrera_materia` bigint(20) NOT NULL,
  `ID_carrera` bigint(20) DEFAULT NULL,
  `ID_catedra` bigint(20) DEFAULT NULL,
  `ID_materia` char(10) DEFAULT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELACIONES PARA LA TABLA `carrera_materia`:
--   `ID_carrera`
--       `carreras` -> `ID_carrera`
--   `ID_materia`
--       `materias` -> `ID_materia`
--   `ID_catedra`
--       `catedras` -> `ID_catedra`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `catedras`
--

DROP TABLE IF EXISTS `catedras`;
CREATE TABLE `catedras` (
  `ID_catedra` bigint(20) NOT NULL,
  `nombre_catedra` varchar(100) NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  `ID_escuela` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELACIONES PARA LA TABLA `catedras`:
--   `ID_escuela`
--       `escuela` -> `ID_escuela`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ciclos`
--

DROP TABLE IF EXISTS `ciclos`;
CREATE TABLE `ciclos` (
  `ID_ciclo` bigint(20) NOT NULL,
  `nombre_ciclo` varchar(50) NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELACIONES PARA LA TABLA `ciclos`:
--

--
-- Volcado de datos para la tabla `ciclos`
--

INSERT INTO `ciclos` (`ID_ciclo`, `nombre_ciclo`, `estado`) VALUES
(1, '01-2018', 0),
(2, '02-2018', 1),
(3, '01-2019', 1),
(4, '02-2019', 1),
(5, '01-2020', 0),
(6, '02-2020', 0),
(7, '01-2021', 0),
(8, '02-2021', 0),
(9, '01-2022', 0),
(10, '02-2022', 0),
(11, '01-2022', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `correcciones`
--

DROP TABLE IF EXISTS `correcciones`;
CREATE TABLE `correcciones` (
  `ID_correccion` bigint(20) NOT NULL,
  `ID_tramite` bigint(20) NOT NULL,
  `notaActual` float NOT NULL,
  `notaCorregida` float NOT NULL,
  `motivoCorreccion` varchar(200) NOT NULL,
  `estadoCorreccion` varchar(50) NOT NULL,
  `carnet` char(12) NOT NULL,
  `observaciones` text NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  `personaRecibe` varchar(100) NOT NULL,
  `fechaRecibe` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELACIONES PARA LA TABLA `correcciones`:
--   `ID_tramite`
--       `tramites` -> `ID_tramite`
--   `carnet`
--       `alumnos` -> `carnet`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `diferidos`
--

DROP TABLE IF EXISTS `diferidos`;
CREATE TABLE `diferidos` (
  `ID_diferido` bigint(20) NOT NULL,
  `ID_tramite` bigint(20) NOT NULL,
  `carnet` char(12) NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELACIONES PARA LA TABLA `diferidos`:
--   `ID_tramite`
--       `tramites` -> `ID_tramite`
--   `carnet`
--       `alumnos` -> `carnet`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `docentes`
--

DROP TABLE IF EXISTS `docentes`;
CREATE TABLE `docentes` (
  `carnet_docente` char(12) NOT NULL,
  `docente_nombres` varchar(100) DEFAULT NULL,
  `estado` tinyint(1) DEFAULT '1',
  `correo_docente` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELACIONES PARA LA TABLA `docentes`:
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `escuela`
--

DROP TABLE IF EXISTS `escuela`;
CREATE TABLE `escuela` (
  `ID_escuela` bigint(20) NOT NULL,
  `nombre_escuela` varchar(500) DEFAULT NULL,
  `estado` tinyint(1) DEFAULT '1',
  `ID_facultad` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELACIONES PARA LA TABLA `escuela`:
--   `ID_facultad`
--       `facultades` -> `id_facultad`
--

--
-- Volcado de datos para la tabla `escuela`
--

INSERT INTO `escuela` (`ID_escuela`, `nombre_escuela`, `estado`, `ID_facultad`) VALUES
(1, 'EscÃƒÂºÃƒÂ©la de InformÃƒÂ¡tica ', 1, 1),
(2, 'Escuela de Ciencias Aplicadas', 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `evaluaciones`
--

DROP TABLE IF EXISTS `evaluaciones`;
CREATE TABLE `evaluaciones` (
  `ID_eval` bigint(20) NOT NULL,
  `descripcion` varchar(50) NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELACIONES PARA LA TABLA `evaluaciones`:
--

--
-- Volcado de datos para la tabla `evaluaciones`
--

INSERT INTO `evaluaciones` (`ID_eval`, `descripcion`, `estado`) VALUES
(1, 'Primera EvaluaciÃƒÂ³n', 1),
(2, 'Segunda EvaluaciÃƒÂ³n', 1),
(3, 'Tercera EvaluaciÃƒÂ³n', 1),
(4, 'Cuarta EvaluaciÃƒÂ³n', 1),
(5, 'Quinta EvaluaciÃƒÂ³n', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `facultades`
--

DROP TABLE IF EXISTS `facultades`;
CREATE TABLE `facultades` (
  `id_facultad` bigint(20) NOT NULL,
  `nombre_facultad` varchar(500) DEFAULT NULL,
  `estado` tinyint(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELACIONES PARA LA TABLA `facultades`:
--

--
-- Volcado de datos para la tabla `facultades`
--

INSERT INTO `facultades` (`id_facultad`, `nombre_facultad`, `estado`) VALUES
(1, 'Facultad de informÃƒÂ¡tica y ciencias aplicadas', 1),
(2, 'Facultad de  Ciencas Empresariales', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `materias`
--

DROP TABLE IF EXISTS `materias`;
CREATE TABLE `materias` (
  `ID_materia` char(15) NOT NULL,
  `nombre_materia` varchar(200) DEFAULT NULL,
  `estado` tinyint(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELACIONES PARA LA TABLA `materias`:
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menu`
--

DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu` (
  `id_menu` bigint(20) NOT NULL,
  `menu` varchar(45) DEFAULT NULL,
  `icono` varchar(350) DEFAULT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELACIONES PARA LA TABLA `menu`:
--

--
-- Volcado de datos para la tabla `menu`
--

INSERT INTO `menu` (`id_menu`, `menu`, `icono`, `estado`) VALUES
(1, 'Mantenimientos ', 'fa fa-cog', 1),
(2, 'Procesos ', 'fa fa-repeat', 1),
(3, 'Reportes ', 'fa fa-file-pdf-o', 1),
(4, 'Extras ', 'fa  fa-desktop', 1),
(5, 'Listados', 'fa fa-list-ol\r\n', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `opcionmenu`
--

DROP TABLE IF EXISTS `opcionmenu`;
CREATE TABLE `opcionmenu` (
  `id_opcion` bigint(20) NOT NULL,
  `opcion` varchar(24) DEFAULT NULL,
  `url` varchar(300) DEFAULT NULL,
  `id_menu` bigint(11) NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELACIONES PARA LA TABLA `opcionmenu`:
--   `id_menu`
--       `menu` -> `id_menu`
--

--
-- Volcado de datos para la tabla `opcionmenu`
--

INSERT INTO `opcionmenu` (`id_opcion`, `opcion`, `url`, `id_menu`, `estado`) VALUES
(1, 'CRUD Docentes', 'http://127.0.0.1/SistemaJGJ/index.php?c=docente&ev=listar', 1, 1),
(2, 'CRUD Carreras', 'http://127.0.0.1/SistemaJGJ/index.php?c=carrera&ev=listar', 1, 1),
(3, 'CRUD Facultades', 'http://127.0.0.1/SistemaJGJ/index.php?c=facultad&ev=listar', 1, 1),
(4, 'CRUD Materias ', 'http://127.0.0.1/SistemaJGJ/index.php?c=materia&ev=listar', 1, 1),
(5, 'CRUD Catedras', 'http://127.0.0.1/SistemaJGJ/index.php?c=catedra&ev=listar', 1, 1),
(6, 'CRUD Asignacion Materias', 'http://127.0.0.1/SistemaJGJ/index.php?c=asigna&ev=listar', 1, 1),
(7, 'CRUD Alumnos', 'http://127.0.0.1/SistemaJGJ/index.php?c=alumno&ev=listar', 1, 1),
(8, 'CRUD Ciclos', 'http://127.0.0.1/SistemaJGJ/index.php?c=ciclo&ev=listar', 1, 1),
(9, 'CRUD Escuelas ', 'http://127.0.0.1/SistemaJGJ/index.php?c=escuela&ev=listar', 1, 1),
(11, 'CRUD Evaluaciones', 'http://127.0.0.1/SistemaJGJ/index.php?c=evaluacion&ev=listar', 1, 1),
(12, 'CRUD Secciones', 'http://127.0.0.1/SistemaJGJ/index.php?c=seccion&ev=listar', 1, 1),
(13, 'CRUD Usuarios', 'http://127.0.0.1/SistemaJGJ/index.php?c=user&ev=listar', 1, 1),
(15, 'Rpt-Diferidos', 'http://127.0.0.1/SistemaJGJ/index.php?c=reporteDif', 3, 1),
(16, 'Tramite de Diferidos ', 'http://127.0.0.1/SistemaJGJ/index.php?c=diferido', 2, 1),
(17, 'Correccion de notas', 'http://127.0.0.1/SistemaJGJ/index.php?c=procesos&ev=inicioCorrecciones', 2, 1),
(18, 'Actividades', 'http://127.0.0.1/SistemaJGJ/index.php?c=actividad2', 2, 1),
(19, 'Listar Docentes', 'http://127.0.0.1/SistemaJGJ/index.php?c=docente', 5, 1),
(20, 'Procesos', 'http://127.0.0.1/SistemaJGJ/index.php?c=procesos', 2, 1),
(21, 'Listar Ciclos ', 'http://127.0.0.1/SistemaJGJ/index.php?c=ciclo', 5, 1),
(22, 'Listar Alumnos', 'http://127.0.0.1/SistemaJGJ/index.php?c=alumno', 5, 1),
(23, 'Listar Asignacion Mat', 'http://127.0.0.1/SistemaJGJ/index.php?c=asigna', 5, 1),
(24, 'Listar Carreras\r\n', 'http://127.0.0.1/SistemaJGJ/index.php?c=carrera', 5, 1),
(25, 'Listar Catedras', 'http://127.0.0.1/SistemaJGJ/index.php?c=catedra', 5, 1),
(26, 'Listar Escuelas', 'http://127.0.0.1/SistemaJGJ/index.php?c=escuela', 5, 1),
(27, 'Listar Evaluaciones', 'http://127.0.0.1/SistemaJGJ/index.php?c=evaluacion', 5, 1),
(28, 'Listar Facultades', 'http://127.0.0.1/SistemaJGJ/index.php?c=facultad', 5, 1),
(29, 'Listar Materias', 'http://127.0.0.1/SistemaJGJ/index.php?c=materia', 5, 1),
(30, 'Listar Secciones', 'http://127.0.0.1/SistemaJGJ/index.php?c=seccion', 5, 1),
(33, 'Listar Usuarios', 'http://127.0.0.1/SistemaJGJ/index.php?c=user', 5, 1),
(35, 'Busqueda', 'http://127.0.0.1/SistemaJGJ/vista/AdminLTE/response.php', 4, 0),
(36, 'Rpt-Actividad', 'http://127.0.0.1/SistemaJGJ/index.php?c=reportesAct', 3, 1),
(37, 'Rpt-Correcciones', 'http://127.0.0.1/SistemaJGJ/index.php?c=reportesCor', 3, 1),
(38, 'Rpt-Diferidos materias', 'http://127.0.0.1/SistemaJGJ/index.php?c=reporteDif2', 3, 1),
(39, 'Rpt-Actividades materias', 'http://127.0.0.1/SistemaJGJ/index.php?c=reportesActMat', 3, 1),
(40, 'Rpt-Correccion materias', 'http://127.0.0.1/SistemaJGJ/index.php?c=reportesCorMat', 3, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reporteactividades`
--

DROP TABLE IF EXISTS `reporteactividades`;
CREATE TABLE `reporteactividades` (
  `ID_reporte_actividad` bigint(20) NOT NULL,
  `ID_tramite` bigint(20) NOT NULL,
  `ID_tipo_actividad` bigint(20) NOT NULL,
  `nota` float NOT NULL,
  `observaciones` text NOT NULL,
  `carnet` char(12) NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELACIONES PARA LA TABLA `reporteactividades`:
--   `ID_tramite`
--       `tramites` -> `ID_tramite`
--   `ID_tipo_actividad`
--       `tipoactividad` -> `ID_tipo_actividad`
--   `carnet`
--       `alumnos` -> `carnet`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `id_rol` bigint(20) NOT NULL,
  `rol` varchar(45) DEFAULT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELACIONES PARA LA TABLA `roles`:
--

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`id_rol`, `rol`, `estado`) VALUES
(1, 'Administrador', 1),
(2, 'Asistente', 1),
(3, 'Reportes', 1),
(4, '4', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rol_menu`
--

DROP TABLE IF EXISTS `rol_menu`;
CREATE TABLE `rol_menu` (
  `id_rol_menu` bigint(20) NOT NULL,
  `id_opcion` bigint(20) NOT NULL,
  `acceso` tinyint(1) DEFAULT '0',
  `id_rol` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELACIONES PARA LA TABLA `rol_menu`:
--   `id_rol`
--       `roles` -> `id_rol`
--   `id_opcion`
--       `opcionmenu` -> `id_opcion`
--

--
-- Volcado de datos para la tabla `rol_menu`
--

INSERT INTO `rol_menu` (`id_rol_menu`, `id_opcion`, `acceso`, `id_rol`) VALUES
(1, 1, 1, 1),
(2, 2, 1, 1),
(3, 3, 1, 1),
(4, 4, 1, 1),
(5, 5, 1, 1),
(6, 6, 1, 1),
(7, 7, 1, 1),
(8, 8, 1, 1),
(9, 9, 1, 1),
(10, 11, 1, 1),
(11, 12, 1, 1),
(12, 13, 1, 1),
(13, 15, 1, 1),
(14, 17, 1, 1),
(15, 18, 1, 1),
(16, 19, 1, 1),
(17, 20, 1, 1),
(18, 21, 1, 1),
(19, 22, 1, 1),
(20, 23, 1, 1),
(21, 24, 1, 1),
(22, 25, 1, 1),
(23, 26, 1, 1),
(24, 27, 1, 1),
(25, 28, 1, 1),
(26, 29, 1, 1),
(27, 30, 1, 1),
(28, 33, 1, 1),
(29, 16, 1, 1),
(30, 35, 0, 1),
(31, 36, 1, 1),
(32, 37, 1, 1),
(33, 38, 1, 1),
(34, 39, 1, 1),
(35, 40, 1, 1),
(36, 16, 1, 2),
(37, 17, 1, 2),
(38, 18, 1, 2),
(39, 19, 1, 2),
(40, 20, 1, 2),
(41, 21, 1, 2),
(42, 22, 1, 2),
(43, 23, 1, 2),
(44, 24, 1, 2),
(45, 25, 1, 2),
(46, 26, 1, 2),
(47, 15, 1, 3),
(48, 36, 1, 3),
(49, 37, 1, 3),
(50, 38, 1, 3),
(51, 39, 1, 3),
(52, 40, 1, 3),
(53, 2, 1, 4),
(54, 15, 1, 4),
(55, 24, 1, 4),
(58, 7, 1, 4),
(60, 7, 1, 2),
(61, 27, 1, 2),
(62, 28, 1, 2),
(63, 29, 1, 2),
(64, 30, 1, 2),
(65, 33, 0, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `secciones`
--

DROP TABLE IF EXISTS `secciones`;
CREATE TABLE `secciones` (
  `ID_seccion` bigint(20) NOT NULL,
  `seccion` varchar(50) NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELACIONES PARA LA TABLA `secciones`:
--

--
-- Volcado de datos para la tabla `secciones`
--

INSERT INTO `secciones` (`ID_seccion`, `seccion`, `estado`) VALUES
(1, 'SecciÃƒÂ³n 01', 1),
(2, 'SecciÃƒÂ³n 02', 1),
(3, 'SecciÃƒÂ³n 03', 1),
(4, 'SecciÃƒÂ³n 04', 1),
(5, 'SecciÃƒÂ³n 05', 1),
(6, 'SecciÃƒÂ³n 06', 1),
(7, 'SecciÃƒÂ³n 07', 1),
(8, 'SecciÃƒÂ³n 08', 1),
(9, 'SecciÃƒÂ³n 09', 1),
(10, 'SecciÃƒÂ³n 10', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipoactividad`
--

DROP TABLE IF EXISTS `tipoactividad`;
CREATE TABLE `tipoactividad` (
  `ID_tipo_actividad` bigint(20) NOT NULL,
  `nombreActividad` varchar(50) NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELACIONES PARA LA TABLA `tipoactividad`:
--

--
-- Volcado de datos para la tabla `tipoactividad`
--

INSERT INTO `tipoactividad` (`ID_tipo_actividad`, `nombreActividad`, `estado`) VALUES
(1, 'Seminario', 1),
(2, 'Actividad Ex-aula', 1),
(3, 'Actividad Individual', 1),
(4, 'ExposiciÃ³n', 1),
(5, 'Actividad Grupal', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipotramite`
--

DROP TABLE IF EXISTS `tipotramite`;
CREATE TABLE `tipotramite` (
  `ID_tipo_tramite` bigint(20) NOT NULL,
  `nombreTramite` varchar(50) NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELACIONES PARA LA TABLA `tipotramite`:
--

--
-- Volcado de datos para la tabla `tipotramite`
--

INSERT INTO `tipotramite` (`ID_tipo_tramite`, `nombreTramite`, `estado`) VALUES
(1, 'Actividades', 1),
(2, 'Correcciones de Notas', 1),
(3, 'Examenes Diferidos', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tramites`
--

DROP TABLE IF EXISTS `tramites`;
CREATE TABLE `tramites` (
  `ID_tramite` bigint(20) NOT NULL,
  `ID_carrera_materia` bigint(20) NOT NULL,
  `ID_seccion` bigint(20) NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  `ID_tipo_tramite` bigint(20) NOT NULL,
  `ID_eval` bigint(20) NOT NULL,
  `carnet_docente` varchar(12) NOT NULL,
  `autorizado` tinyint(1) NOT NULL,
  `conteo` int(20) NOT NULL,
  `fechaRegistro` datetime NOT NULL,
  `ID_ciclo` bigint(20) NOT NULL,
  `fechaEnvio` datetime NOT NULL,
  `periodo` int(11) NOT NULL,
  `fechaSolicitud` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELACIONES PARA LA TABLA `tramites`:
--   `ID_seccion`
--       `secciones` -> `ID_seccion`
--   `ID_carrera_materia`
--       `carrera_materia` -> `ID_carrera_materia`
--   `ID_tipo_tramite`
--       `tipotramite` -> `ID_tipo_tramite`
--   `ID_eval`
--       `evaluaciones` -> `ID_eval`
--   `carnet_docente`
--       `docentes` -> `carnet_docente`
--   `ID_ciclo`
--       `ciclos` -> `ID_ciclo`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE `usuarios` (
  `ID_usuario` bigint(20) NOT NULL,
  `nombre` varchar(50) DEFAULT NULL,
  `apellido` varchar(50) DEFAULT NULL,
  `correo` varchar(500) DEFAULT NULL,
  `usuario` varchar(20) NOT NULL,
  `clave` varchar(100) DEFAULT NULL,
  `estado` tinyint(1) DEFAULT '1',
  `fecha_creacion` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `nivel` int(11) NOT NULL DEFAULT '1',
  `fecha_modificacion` datetime DEFAULT CURRENT_TIMESTAMP,
  `id_rol` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELACIONES PARA LA TABLA `usuarios`:
--   `id_rol`
--       `roles` -> `id_rol`
--

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`ID_usuario`, `nombre`, `apellido`, `correo`, `usuario`, `clave`, `estado`, `fecha_creacion`, `nivel`, `fecha_modificacion`, `id_rol`) VALUES
(5, 'Blanca', 'Martinez', 'blanca@mail.utec.edu.sv', 'asistente', ' ùÆäªú­Âî	[ƒ‘', 1, '2018-11-21 22:01:32', 1, '2018-12-13 21:03:16', 2),
(6, 'Admin', 'Del Sistema', 'Modificame@mail.utec.edu.sv', 'Administrador', ' ùÆäªú­Âî	[ƒ‘', 1, '2018-11-24 04:15:48', 1, '2018-11-26 00:27:20', 1),
(7, 'reportes', 'Del Sistema', 'reportes@mail.com', 'reportes', ' ùÆäªú­Âî	[ƒ‘', 1, '2018-11-24 04:36:47', 1, '2018-11-26 00:27:25', 3),
(9, 'Juan', 'Perez', 'user@gmail.com', 'user', ' ùÆäªú­Âî	[ƒ‘', 1, '2018-11-26 00:23:17', 1, '2018-11-26 00:23:17', 1),
(11, 'Blanca', 'Beltran', 'bebam.2011@gmail.com', 'blanquy', 'úN1\rz´ýx;ò&Ë&o', 1, '2018-12-05 12:10:25', 1, '2018-12-05 12:10:30', 1),
(12, 'Oswaldo', 'Barrera', 'jose.barrera@utec.edu.sv', 'oz', ' ùÆäªú­Âî	[ƒ‘', 1, '2018-12-05 12:11:38', 1, '2018-12-05 12:11:39', 1);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `vw_actividadestodos`
-- (Véase abajo para la vista actual)
--
DROP VIEW IF EXISTS `vw_actividadestodos`;
CREATE TABLE `vw_actividadestodos` (
`IDTRAMITE` bigint(20)
,`EVALUACION` varchar(50)
,`CARRERA` varchar(500)
,`MATERIA` varchar(200)
,`SECCION` varchar(50)
,`CICLO` varchar(50)
,`DOCENTE` varchar(100)
,`CASOS` int(20)
,`ESTADO` varchar(8)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `vw_diferidostodos`
-- (Véase abajo para la vista actual)
--
DROP VIEW IF EXISTS `vw_diferidostodos`;
CREATE TABLE `vw_diferidostodos` (
`IDTRAMITE` bigint(20)
,`CARRERA` varchar(500)
,`ALUMNO` varchar(50)
,`MATERIA` varchar(200)
,`ESCUELA` varchar(500)
,`SECCION` varchar(50)
,`DOCENTE` varchar(100)
,`EVALUACION` varchar(50)
,`PERIODO` varchar(22)
,`FECHAREG` datetime
,`FECHASOL` datetime
,`CICLO` varchar(50)
,`ESTADO` varchar(8)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `vw_mostrarcarreras`
-- (Véase abajo para la vista actual)
--
DROP VIEW IF EXISTS `vw_mostrarcarreras`;
CREATE TABLE `vw_mostrarcarreras` (
`ID_carrera` bigint(20)
,`nombre_carrera` varchar(500)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `vw_rpcorrecion`
-- (Véase abajo para la vista actual)
--
DROP VIEW IF EXISTS `vw_rpcorrecion`;
CREATE TABLE `vw_rpcorrecion` (
`EVALUACIONES` varchar(50)
,`CICLO` bigint(20)
,`FECHASOLICITUD` date
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `vw_rpdiferidos`
-- (Véase abajo para la vista actual)
--
DROP VIEW IF EXISTS `vw_rpdiferidos`;
CREATE TABLE `vw_rpdiferidos` (
`EVALUACIONES` varchar(50)
,`CICLO` bigint(20)
,`FECHASOLICITUD` date
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `vw_rptactivciclo`
-- (Véase abajo para la vista actual)
--
DROP VIEW IF EXISTS `vw_rptactivciclo`;
CREATE TABLE `vw_rptactivciclo` (
`EVALUACIONES` varchar(50)
,`CICLO` bigint(20)
,`FECHASOLICITUD` date
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `vw_rptactmat`
-- (Véase abajo para la vista actual)
--
DROP VIEW IF EXISTS `vw_rptactmat`;
CREATE TABLE `vw_rptactmat` (
`EVALUACIONES` varchar(50)
,`CICLO` bigint(20)
,`FECHASOLICITUD` date
,`MATERIA` text
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `vw_rptcormat`
-- (Véase abajo para la vista actual)
--
DROP VIEW IF EXISTS `vw_rptcormat`;
CREATE TABLE `vw_rptcormat` (
`EVALUACIONES` varchar(50)
,`CICLO` bigint(20)
,`FECHASOLICITUD` date
,`MATERIA` text
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `vw_rptdifmat`
-- (Véase abajo para la vista actual)
--
DROP VIEW IF EXISTS `vw_rptdifmat`;
CREATE TABLE `vw_rptdifmat` (
`EVALUACIONES` varchar(50)
,`CICLO` bigint(20)
,`FECHASOLICITUD` date
,`MATERIA` text
);

-- --------------------------------------------------------

--
-- Estructura para la vista `vw_actividadestodos`
--
DROP TABLE IF EXISTS `vw_actividadestodos`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_actividadestodos`  AS  select `t`.`ID_tramite` AS `IDTRAMITE`,`eva`.`descripcion` AS `EVALUACION`,`car`.`nombre_carrera` AS `CARRERA`,`mat`.`nombre_materia` AS `MATERIA`,`sec`.`seccion` AS `SECCION`,`ci`.`nombre_ciclo` AS `CICLO`,`doc`.`docente_nombres` AS `DOCENTE`,`t`.`conteo` AS `CASOS`,(case when (`t`.`estado` = 1) then 'Activo' else 'Inactivo' end) AS `ESTADO` from ((((((((`tramites` `t` left join `secciones` `sec` on((`t`.`ID_seccion` = `sec`.`ID_seccion`))) left join `tipotramite` `tiptram` on((`t`.`ID_tipo_tramite` = `tiptram`.`ID_tipo_tramite`))) left join `evaluaciones` `eva` on((`t`.`ID_eval` = `eva`.`ID_eval`))) left join `docentes` `doc` on((`t`.`carnet_docente` = `doc`.`carnet_docente`))) left join `ciclos` `ci` on((`t`.`ID_ciclo` = `ci`.`ID_ciclo`))) left join `carrera_materia` `carmat` on((`t`.`ID_carrera_materia` = `carmat`.`ID_carrera_materia`))) left join `carreras` `car` on((`carmat`.`ID_carrera` = `car`.`ID_carrera`))) left join `materias` `mat` on((`mat`.`ID_materia` = `carmat`.`ID_materia`))) where (`tiptram`.`ID_tipo_tramite` = 1) ;

-- --------------------------------------------------------

--
-- Estructura para la vista `vw_diferidostodos`
--
DROP TABLE IF EXISTS `vw_diferidostodos`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_diferidostodos`  AS  select `t`.`ID_tramite` AS `IDTRAMITE`,`c`.`nombre_carrera` AS `CARRERA`,`a`.`alumno` AS `ALUMNO`,`m`.`nombre_materia` AS `MATERIA`,`e`.`nombre_escuela` AS `ESCUELA`,`s`.`seccion` AS `SECCION`,`dc`.`docente_nombres` AS `DOCENTE`,`ev`.`descripcion` AS `EVALUACION`,(case when (`t`.`periodo` = 1) then 'Periodo Extraordinario' when (`t`.`periodo` = 2) then 'Periodo Ordinario' else 'NINGUN PERIODO' end) AS `PERIODO`,`t`.`fechaRegistro` AS `FECHAREG`,`t`.`fechaSolicitud` AS `FECHASOL`,`ci`.`nombre_ciclo` AS `CICLO`,(case when (`d`.`estado` = 1) then 'Activo' else 'inactivo' end) AS `ESTADO` from ((((((((((((`tramites` `t` join `diferidos` `d` on((`t`.`ID_tramite` = `d`.`ID_tramite`))) left join `carrera_materia` `cm` on((`t`.`ID_carrera_materia` = `cm`.`ID_carrera_materia`))) left join `alumnos` `a` on((`a`.`carnet` = `d`.`carnet`))) left join `materias` `m` on((`m`.`ID_materia` = `cm`.`ID_materia`))) left join `carreras` `c` on((`c`.`ID_carrera` = `cm`.`ID_carrera`))) left join `catedras` `ct` on((`ct`.`ID_catedra` = `cm`.`ID_catedra`))) left join `escuela` `e` on((`e`.`ID_escuela` = `ct`.`ID_escuela`))) left join `secciones` `s` on((`s`.`ID_seccion` = `t`.`ID_seccion`))) left join `docentes` `dc` on((`dc`.`carnet_docente` = `t`.`carnet_docente`))) left join `evaluaciones` `ev` on((`ev`.`ID_eval` = `t`.`ID_eval`))) left join `tipotramite` `tt` on((`tt`.`ID_tipo_tramite` = `t`.`ID_tipo_tramite`))) left join `ciclos` `ci` on((`ci`.`ID_ciclo` = `t`.`ID_ciclo`))) where (`tt`.`ID_tipo_tramite` = 3) ;

-- --------------------------------------------------------

--
-- Estructura para la vista `vw_mostrarcarreras`
--
DROP TABLE IF EXISTS `vw_mostrarcarreras`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_mostrarcarreras`  AS  select `cm`.`ID_carrera` AS `ID_carrera`,`c`.`nombre_carrera` AS `nombre_carrera` from (`carrera_materia` `cm` left join `carreras` `c` on((`cm`.`ID_carrera` = `c`.`ID_carrera`))) where (`cm`.`estado` = 1) group by `cm`.`ID_carrera` order by `c`.`nombre_carrera` ;

-- --------------------------------------------------------

--
-- Estructura para la vista `vw_rpcorrecion`
--
DROP TABLE IF EXISTS `vw_rpcorrecion`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_rpcorrecion`  AS  select `e`.`descripcion` AS `EVALUACIONES`,`t`.`ID_ciclo` AS `CICLO`,cast(`t`.`fechaSolicitud` as date) AS `FECHASOLICITUD` from ((`correcciones` `d` join `tramites` `t` on((`d`.`ID_tramite` = `t`.`ID_tramite`))) join `evaluaciones` `e` on((`e`.`ID_eval` = `t`.`ID_eval`))) where ((`t`.`ID_tipo_tramite` = 2) and (`d`.`estado` = 1) and ((`t`.`fechaSolicitud` <> '') or (`t`.`fechaSolicitud` <> '0000-00-00'))) ;

-- --------------------------------------------------------

--
-- Estructura para la vista `vw_rpdiferidos`
--
DROP TABLE IF EXISTS `vw_rpdiferidos`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_rpdiferidos`  AS  select `e`.`descripcion` AS `EVALUACIONES`,`t`.`ID_ciclo` AS `CICLO`,cast(`t`.`fechaSolicitud` as date) AS `FECHASOLICITUD` from ((`diferidos` `d` join `tramites` `t` on((`d`.`ID_tramite` = `t`.`ID_tramite`))) join `evaluaciones` `e` on((`e`.`ID_eval` = `t`.`ID_eval`))) where ((`t`.`ID_tipo_tramite` = 3) and (`d`.`estado` = 1) and ((`t`.`fechaSolicitud` <> '') or (`t`.`fechaSolicitud` <> '0000-00-00'))) ;

-- --------------------------------------------------------

--
-- Estructura para la vista `vw_rptactivciclo`
--
DROP TABLE IF EXISTS `vw_rptactivciclo`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_rptactivciclo`  AS  select `e`.`descripcion` AS `EVALUACIONES`,`t`.`ID_ciclo` AS `CICLO`,cast(`t`.`fechaSolicitud` as date) AS `FECHASOLICITUD` from (`tramites` `t` join `evaluaciones` `e` on((`e`.`ID_eval` = `t`.`ID_eval`))) where ((`t`.`ID_tipo_tramite` = 1) and (`t`.`estado` = 1) and ((`t`.`fechaSolicitud` <> '') or (`t`.`fechaSolicitud` <> '0000-00-00'))) ;

-- --------------------------------------------------------

--
-- Estructura para la vista `vw_rptactmat`
--
DROP TABLE IF EXISTS `vw_rptactmat`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_rptactmat`  AS  select `e`.`descripcion` AS `EVALUACIONES`,`t`.`ID_ciclo` AS `CICLO`,cast(`t`.`fechaSolicitud` as date) AS `FECHASOLICITUD`,concat(convert(`m`.`nombre_materia` using utf8),' - ',`c`.`nombre_carrera`) AS `MATERIA` from ((((`tramites` `t` join `evaluaciones` `e` on((`e`.`ID_eval` = `t`.`ID_eval`))) join `carrera_materia` `cm` on((`cm`.`ID_carrera_materia` = `t`.`ID_carrera_materia`))) join `carreras` `c` on((`c`.`ID_carrera` = `cm`.`ID_carrera`))) join `materias` `m` on((`m`.`ID_materia` = `cm`.`ID_materia`))) where ((`t`.`ID_tipo_tramite` = 1) and (`t`.`estado` = 1) and ((`t`.`fechaSolicitud` <> '') or (`t`.`fechaSolicitud` <> '0000-00-00'))) ;

-- --------------------------------------------------------

--
-- Estructura para la vista `vw_rptcormat`
--
DROP TABLE IF EXISTS `vw_rptcormat`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_rptcormat`  AS  select `e`.`descripcion` AS `EVALUACIONES`,`t`.`ID_ciclo` AS `CICLO`,cast(`t`.`fechaSolicitud` as date) AS `FECHASOLICITUD`,concat(convert(`m`.`nombre_materia` using utf8),' - ',`c`.`nombre_carrera`) AS `MATERIA` from ((((`tramites` `t` join `evaluaciones` `e` on((`e`.`ID_eval` = `t`.`ID_eval`))) join `carrera_materia` `cm` on((`cm`.`ID_carrera_materia` = `t`.`ID_carrera_materia`))) join `carreras` `c` on((`c`.`ID_carrera` = `cm`.`ID_carrera`))) join `materias` `m` on((`m`.`ID_materia` = `cm`.`ID_materia`))) where ((`t`.`ID_tipo_tramite` = 2) and (`t`.`estado` = 1) and ((`t`.`fechaSolicitud` <> '') or (`t`.`fechaSolicitud` <> '0000-00-00'))) ;

-- --------------------------------------------------------

--
-- Estructura para la vista `vw_rptdifmat`
--
DROP TABLE IF EXISTS `vw_rptdifmat`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_rptdifmat`  AS  select `e`.`descripcion` AS `EVALUACIONES`,`t`.`ID_ciclo` AS `CICLO`,cast(`t`.`fechaSolicitud` as date) AS `FECHASOLICITUD`,concat(convert(`m`.`nombre_materia` using utf8),' - ',`c`.`nombre_carrera`) AS `MATERIA` from ((((`tramites` `t` join `evaluaciones` `e` on((`e`.`ID_eval` = `t`.`ID_eval`))) join `carrera_materia` `cm` on((`cm`.`ID_carrera_materia` = `t`.`ID_carrera_materia`))) join `carreras` `c` on((`c`.`ID_carrera` = `cm`.`ID_carrera`))) join `materias` `m` on((`m`.`ID_materia` = `cm`.`ID_materia`))) where ((`t`.`ID_tipo_tramite` = 3) and (`t`.`estado` = 1) and ((`t`.`fechaSolicitud` <> '') or (`t`.`fechaSolicitud` <> '0000-00-00'))) ;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `alumnos`
--
ALTER TABLE `alumnos`
  ADD PRIMARY KEY (`carnet`);

--
-- Indices de la tabla `carreras`
--
ALTER TABLE `carreras`
  ADD PRIMARY KEY (`ID_carrera`);

--
-- Indices de la tabla `carrera_materia`
--
ALTER TABLE `carrera_materia`
  ADD PRIMARY KEY (`ID_carrera_materia`),
  ADD KEY `ID_carrera` (`ID_carrera`) USING BTREE,
  ADD KEY `ID_materia` (`ID_materia`) USING BTREE,
  ADD KEY `ID_catedra` (`ID_catedra`) USING BTREE;

--
-- Indices de la tabla `catedras`
--
ALTER TABLE `catedras`
  ADD PRIMARY KEY (`ID_catedra`),
  ADD KEY `ID_escuela` (`ID_escuela`);

--
-- Indices de la tabla `ciclos`
--
ALTER TABLE `ciclos`
  ADD PRIMARY KEY (`ID_ciclo`);

--
-- Indices de la tabla `correcciones`
--
ALTER TABLE `correcciones`
  ADD PRIMARY KEY (`ID_correccion`),
  ADD KEY `ID_tramite` (`ID_tramite`),
  ADD KEY `carnet` (`carnet`);

--
-- Indices de la tabla `diferidos`
--
ALTER TABLE `diferidos`
  ADD PRIMARY KEY (`ID_diferido`),
  ADD KEY `ID_tramite` (`ID_tramite`),
  ADD KEY `carnet` (`carnet`),
  ADD KEY `carnet_2` (`carnet`);

--
-- Indices de la tabla `docentes`
--
ALTER TABLE `docentes`
  ADD PRIMARY KEY (`carnet_docente`);

--
-- Indices de la tabla `escuela`
--
ALTER TABLE `escuela`
  ADD PRIMARY KEY (`ID_escuela`),
  ADD KEY `ID_facultad` (`ID_facultad`) USING BTREE;

--
-- Indices de la tabla `evaluaciones`
--
ALTER TABLE `evaluaciones`
  ADD PRIMARY KEY (`ID_eval`);

--
-- Indices de la tabla `facultades`
--
ALTER TABLE `facultades`
  ADD PRIMARY KEY (`id_facultad`);

--
-- Indices de la tabla `materias`
--
ALTER TABLE `materias`
  ADD PRIMARY KEY (`ID_materia`);

--
-- Indices de la tabla `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id_menu`);

--
-- Indices de la tabla `opcionmenu`
--
ALTER TABLE `opcionmenu`
  ADD PRIMARY KEY (`id_opcion`),
  ADD KEY `id_menu` (`id_menu`);

--
-- Indices de la tabla `reporteactividades`
--
ALTER TABLE `reporteactividades`
  ADD PRIMARY KEY (`ID_reporte_actividad`),
  ADD KEY `ID_tramite` (`ID_tramite`),
  ADD KEY `ID_tipo_actividad` (`ID_tipo_actividad`),
  ADD KEY `carnet` (`carnet`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id_rol`),
  ADD UNIQUE KEY `rol` (`rol`);

--
-- Indices de la tabla `rol_menu`
--
ALTER TABLE `rol_menu`
  ADD PRIMARY KEY (`id_rol_menu`),
  ADD KEY `id_opcion` (`id_opcion`),
  ADD KEY `id_rol` (`id_rol`);

--
-- Indices de la tabla `secciones`
--
ALTER TABLE `secciones`
  ADD PRIMARY KEY (`ID_seccion`);

--
-- Indices de la tabla `tipoactividad`
--
ALTER TABLE `tipoactividad`
  ADD PRIMARY KEY (`ID_tipo_actividad`);

--
-- Indices de la tabla `tipotramite`
--
ALTER TABLE `tipotramite`
  ADD PRIMARY KEY (`ID_tipo_tramite`);

--
-- Indices de la tabla `tramites`
--
ALTER TABLE `tramites`
  ADD PRIMARY KEY (`ID_tramite`),
  ADD KEY `ID_seccion` (`ID_seccion`),
  ADD KEY `ID_carrera_materia` (`ID_carrera_materia`),
  ADD KEY `ID_tipo_tramite` (`ID_tipo_tramite`),
  ADD KEY `ID_eval` (`ID_eval`),
  ADD KEY `carnet_docente` (`carnet_docente`),
  ADD KEY `ID_ciclo` (`ID_ciclo`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`ID_usuario`),
  ADD UNIQUE KEY `usuario` (`usuario`),
  ADD KEY `id_rol` (`id_rol`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `carreras`
--
ALTER TABLE `carreras`
  MODIFY `ID_carrera` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `carrera_materia`
--
ALTER TABLE `carrera_materia`
  MODIFY `ID_carrera_materia` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT de la tabla `catedras`
--
ALTER TABLE `catedras`
  MODIFY `ID_catedra` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `ciclos`
--
ALTER TABLE `ciclos`
  MODIFY `ID_ciclo` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `correcciones`
--
ALTER TABLE `correcciones`
  MODIFY `ID_correccion` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT de la tabla `diferidos`
--
ALTER TABLE `diferidos`
  MODIFY `ID_diferido` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT de la tabla `escuela`
--
ALTER TABLE `escuela`
  MODIFY `ID_escuela` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `evaluaciones`
--
ALTER TABLE `evaluaciones`
  MODIFY `ID_eval` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `facultades`
--
ALTER TABLE `facultades`
  MODIFY `id_facultad` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `menu`
--
ALTER TABLE `menu`
  MODIFY `id_menu` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `opcionmenu`
--
ALTER TABLE `opcionmenu`
  MODIFY `id_opcion` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT de la tabla `reporteactividades`
--
ALTER TABLE `reporteactividades`
  MODIFY `ID_reporte_actividad` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `id_rol` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `rol_menu`
--
ALTER TABLE `rol_menu`
  MODIFY `id_rol_menu` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;

--
-- AUTO_INCREMENT de la tabla `secciones`
--
ALTER TABLE `secciones`
  MODIFY `ID_seccion` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `tipoactividad`
--
ALTER TABLE `tipoactividad`
  MODIFY `ID_tipo_actividad` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `tipotramite`
--
ALTER TABLE `tipotramite`
  MODIFY `ID_tipo_tramite` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `tramites`
--
ALTER TABLE `tramites`
  MODIFY `ID_tramite` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `ID_usuario` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `carrera_materia`
--
ALTER TABLE `carrera_materia`
  ADD CONSTRAINT `carrera_materia_ibfk_1` FOREIGN KEY (`ID_carrera`) REFERENCES `carreras` (`ID_carrera`),
  ADD CONSTRAINT `carrera_materia_ibfk_2` FOREIGN KEY (`ID_materia`) REFERENCES `materias` (`ID_materia`),
  ADD CONSTRAINT `carrera_materia_ibfk_3` FOREIGN KEY (`ID_catedra`) REFERENCES `catedras` (`ID_catedra`);

--
-- Filtros para la tabla `catedras`
--
ALTER TABLE `catedras`
  ADD CONSTRAINT `catedras_ibfk_1` FOREIGN KEY (`ID_escuela`) REFERENCES `escuela` (`ID_escuela`);

--
-- Filtros para la tabla `correcciones`
--
ALTER TABLE `correcciones`
  ADD CONSTRAINT `correcciones_ibfk_1` FOREIGN KEY (`ID_tramite`) REFERENCES `tramites` (`ID_tramite`),
  ADD CONSTRAINT `correcciones_ibfk_2` FOREIGN KEY (`carnet`) REFERENCES `alumnos` (`carnet`);

--
-- Filtros para la tabla `diferidos`
--
ALTER TABLE `diferidos`
  ADD CONSTRAINT `diferidos_ibfk_1` FOREIGN KEY (`ID_tramite`) REFERENCES `tramites` (`ID_tramite`),
  ADD CONSTRAINT `diferidos_ibfk_2` FOREIGN KEY (`carnet`) REFERENCES `alumnos` (`carnet`);

--
-- Filtros para la tabla `escuela`
--
ALTER TABLE `escuela`
  ADD CONSTRAINT `escuela_ibfk_1` FOREIGN KEY (`ID_facultad`) REFERENCES `facultades` (`id_facultad`) ON DELETE SET NULL ON UPDATE SET NULL;

--
-- Filtros para la tabla `opcionmenu`
--
ALTER TABLE `opcionmenu`
  ADD CONSTRAINT `opcionmenu_ibfk_1` FOREIGN KEY (`id_menu`) REFERENCES `menu` (`id_menu`);

--
-- Filtros para la tabla `reporteactividades`
--
ALTER TABLE `reporteactividades`
  ADD CONSTRAINT `reporteactividades_ibfk_1` FOREIGN KEY (`ID_tramite`) REFERENCES `tramites` (`ID_tramite`),
  ADD CONSTRAINT `reporteactividades_ibfk_2` FOREIGN KEY (`ID_tipo_actividad`) REFERENCES `tipoactividad` (`ID_tipo_actividad`),
  ADD CONSTRAINT `reporteactividades_ibfk_3` FOREIGN KEY (`carnet`) REFERENCES `alumnos` (`carnet`);

--
-- Filtros para la tabla `rol_menu`
--
ALTER TABLE `rol_menu`
  ADD CONSTRAINT `rol_menu_ibfk_2` FOREIGN KEY (`id_rol`) REFERENCES `roles` (`id_rol`),
  ADD CONSTRAINT `rol_menu_ibfk_3` FOREIGN KEY (`id_opcion`) REFERENCES `opcionmenu` (`id_opcion`);

--
-- Filtros para la tabla `tramites`
--
ALTER TABLE `tramites`
  ADD CONSTRAINT `tramites_ibfk_3` FOREIGN KEY (`ID_seccion`) REFERENCES `secciones` (`ID_seccion`),
  ADD CONSTRAINT `tramites_ibfk_5` FOREIGN KEY (`ID_carrera_materia`) REFERENCES `carrera_materia` (`ID_carrera_materia`),
  ADD CONSTRAINT `tramites_ibfk_6` FOREIGN KEY (`ID_tipo_tramite`) REFERENCES `tipotramite` (`ID_tipo_tramite`),
  ADD CONSTRAINT `tramites_ibfk_7` FOREIGN KEY (`ID_eval`) REFERENCES `evaluaciones` (`ID_eval`),
  ADD CONSTRAINT `tramites_ibfk_8` FOREIGN KEY (`carnet_docente`) REFERENCES `docentes` (`carnet_docente`),
  ADD CONSTRAINT `tramites_ibfk_9` FOREIGN KEY (`ID_ciclo`) REFERENCES `ciclos` (`ID_ciclo`);

--
-- Filtros para la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD CONSTRAINT `usuarios_ibfk_1` FOREIGN KEY (`id_rol`) REFERENCES `roles` (`id_rol`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
