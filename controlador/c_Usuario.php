<?php 
/**
 PROYECTO DE GRADUACIÓN UTEC.
 */
class Usuario
{
	function swUsuario(){

		switch (@$_REQUEST['ev']) 
		{   //EVENTOS
			case 'listar':
				$_SESSION['vsValidacion'] = 1 ;
				$this->mostrar();
				break;
			case 'prueba':
				$this->prueba();
				break;

			case 'formInsertar':
				$this->formInsert();
				//$this->flujoInsert();
				break;
			case 'insertar':
				$this->insertar();
				break;
			case 'finsert':
				$this->flujoInsert();
				break;
			case 'finsert2':
				$this->flujoInsert2();
				break;
			case 'finsertAuto1':
				$this->flujoInsertAuto(1);
				break;
			case 'finsertAuto2':
				$this->flujoInsertAuto(2);
				break;
			case 'finsertAuto3':
				$this->flujoInsertAuto(3);
				break;
			case 'insertarMenu':
				$this->insertarMenu();
				break;
			case 'formEditar':
				$this->mostrarUnUsuario();
				break;
			case 'editar':
				$this->editar();
				break;
			case 'eliminar':
				$this->eliminarUsuario();
				break;

			default:
				$_SESSION['vsValidacion'] = 0 ;
				$this->mostrar();
				//echo "Evento no encontrado";					
				break;
		}
	}

	function prueba(){
		//echo "chech: ";
		//echo (@$_POST['1']); //en este punto es string
		//echo "<br>nombre menú: ";

		for ($i=1; $i < 99 ; $i++) { 
			if (@$_POST[$i] !== null) {
				echo "opcion:" . $i ." = ";
				echo "chech: ";
				var_dump($_POST[$i]);
				echo "<br>";
				//crearé un array con todas las opc y luego lo ire insertando 1a1en otra funcion cuando registre al usr.
			}
		}
	}

	function insertarMenu()
	{
		if (@$_SESSION['idUsr'] !== null) {
			$id = $_SESSION['idUsr'];
			$_SESSION['idUsr'] = null;
		}else{
			$id = $_POST['id'];
		}
		@$rol = 1 + ($GLOBALS['m_Usuario']->ultimoRol());
		$rolnuevo=$GLOBALS['m_Usuario']->insertRol($rol);
		
			for ($i=1; $i < 99 ; $i++) { 
				if (@$_POST[$i] !== null) {
					$GLOBALS['m_Usuario']->insertaOpcionMenu($rol, $i);
				}
				
			}
			$this->asignarRol($id, @$rol);
			$this->mostrar();
		
		
		
		
	}

 	function flujoInsertAuto($rol)
	{
		$this->asignarRol($_SESSION['idUsr'], $rol);
		$this->mostrar();
	}

	function mostrar()
	{
		//Headder
		require ($GLOBALS['r'].'header.php'); 
		//Datos que llevara la vista
		$this->alert();
		$verUsuarios=$GLOBALS['m_Usuario']->mostrarTodos();
		//Llamada a la vista
		require_once ($GLOBALS['r']."v_ListaUsuarios.php");
		//footer
		require ($GLOBALS['r'].'footer.php');
	}

	function asignarRol($id, $rol)
	{
		$GLOBALS['m_Usuario']->modifRol($id, $rol);
	}

	function editar()
	{
		//#######VALIDAR CAMPOS DEL LADO DEL SERVER  <<<<<------------####
		if ($_POST['txtNombre']AND$_POST['txtApellido']AND$_POST['txtUser']AND$_POST['txtPass']AND$_POST['txtEmail']) 
		{
			$edit = $GLOBALS['m_Usuario']->modificar($_POST['id'],$_POST['txtNombre'],$_POST['txtApellido'],$_POST['txtUser'],$_POST['txtPass'],$_POST['txtEmail']);

			if (@$edit) {
				$_SESSION['vsMsj'] = $edit;
			}
		}else{
			$_SESSION['vsMsj'] = "Por favor, complete todos los campos...
			 " .  "<br>intente nuevamente.";
		}
		
		
		require ($GLOBALS['r'].'header.php');
		$this->alert();
		
		$verUsuarios=$GLOBALS['m_Usuario']->mostrarTodos();
		//Llamada a la vista
		require_once ($GLOBALS['r']."v_ListaUsuarios.php");
		require ($GLOBALS['r'].'footer.php');
		
	}

	function mostrarUnUsuario()
	{
		//Headder
		require ($GLOBALS['r'].'header.php'); 
		//Datos que llevara la vista
		$verUsuario=$GLOBALS['m_Usuario']->mostrarUno($_POST['id']);
		//Llamada a la vista
		require_once ($GLOBALS['r']."pages/examples/v_modificarUsuario.php");
		//footer
		require ($GLOBALS['r'].'footer.php');
	}

	function eliminarUsuario()
	{
		$GLOBALS['m_Usuario']->eliminar($_POST['id']);
		//$this->alert ("success");
		$this->mostrar();
	}

	function formInsert()
	{
		//Headder
		require ($GLOBALS['r'].'header.php'); 
		$this->alert();
		unset($_SESSION['vsMsj']);
		//Llamada a la vista
		require_once ($GLOBALS['r']."pages/examples/v_insertarUsuario.php");
		//footer
		require ($GLOBALS['r'].'footer.php');
	}

	function flujoInsert()
	{
		//Headder
		require ($GLOBALS['r'].'header.php'); 
		//Llamada a la vista
		$this->alert();
		@$_SESSION['idUsr'] = $GLOBALS['m_Usuario']->ultimoUsuario();
		require_once ($GLOBALS['r']."pages/examples/v_selecMenu.php");
		//footer
		require ($GLOBALS['r'].'footer.php');
	}

	function flujoInsert2()
	{
		//Headder
		require ($GLOBALS['r'].'header.php'); 
		
		$verOpcionesMenu=$GLOBALS['m_Usuario']->opcionesMenu();
		//Llamada a la vista
		require_once ($GLOBALS['r']."v_CheckMenu.php");
		//footer
		require ($GLOBALS['r'].'footer.php');
	}

	function insertar()
	{
		 //#######VALIDAR CAMPOS DEL LADO DEL SERVER  <<<<<------------####
		if ($_POST['txtNombre']AND$_POST['txtApellido']AND$_POST['txtUsuario']AND$_POST['txtPass']AND$_POST['txtEmail']) 
		{
			$insert = $GLOBALS['m_Usuario']->insertar($_POST['txtNombre'],$_POST['txtApellido'],$_POST['txtUsuario'],$_POST['txtPass'],$_POST['txtEmail'],1);

			if (@$insert) {
				$_SESSION['vsMsj'] = $insert;
				$this->flujoInsert();
			}
		}else{
			$_SESSION['vsMsj'] = "Por favor, complete todos los campos";
			$this->mostrar();
		}
		
	}

	//CONVIERTE ELEMENTOS DE UN ARRAY EN STRING
	function menuURL(){
		$_SESSION['vsMenuURL'] = array();
		$x = 0;

		foreach ($_SESSION['vsMenu2'] as $url)
		{
			$URLSTR = implode($url);
			$_SESSION['vsMenuURL'][$x] = $URLSTR;
			//print($_SESSION['vsMenuURL'][$x])."<br>"; //<<---
			$x=$x+1;
		}
	}

	//VALIDA PERMISOS PARA ACCEDER A LAS URLs SI NO PERTENECE AL USR LO REDIRIGE A 404
	function validarUrl(){
		$this->menu2();//llena los datos para verificar las url

		if ($this->urlSec() == 0) 
		{
			$GLOBALS['c'] = 'error';
		}
		//else{
			//echo"URL CORRECTA!!!";
			//echo '<script language="javascript">alert("URL CORRECTA");</script>'; 
		//}
	}	

	function urlSec(){
		//le paso los las urls al arreglo de sesion
		$_SESSION['vsMenuURL'] = array();
		$x = 0; //como el dato viene en array convierto cada elemento a string
		$_SESSION['vsSearch'] = false;
		$_SESSION['vsPagIni'] = false;
		foreach ($_SESSION['vsMenu2'] as $url)
		{
			$URLSTR = implode($url);
			$_SESSION['vsMenuURL'][$x] = $URLSTR;
			//con el siguiente echo imprimo las URLs que tiene acceso el usuario logeado
		//print($_SESSION['vsMenuURL'][$x])."<br>";
			//var_dump($_SESSION['vsMenuURL'][$x]);
			//echo "<br>";

			//VALIDACIÓN PARA MOSTRAR EL BUSCADOR DEL MENÚ vsSearch VALIDARÁ
			$varUrl = "http://127.0.0.1/SistemaJGJ/index.php?c=alumno";
			if ($_SESSION['vsMenuURL'][$x] === $varUrl) {
				$_SESSION['vsSearch'] = true;
	//			$_SESSION['vsPagIni'] = true;
				//echo "<br>".$_SESSION['vsSearch'];
			}
			

			/*
			$vAct = "http://127.0.0.1/SistemaJGJ/index.php?c=actividad2";
			$vCor = "http://127.0.0.1/SistemaJGJ/index.php?c=procesos";
			$vDif = "http://127.0.0.1/SistemaJGJ/index.php?c=diferido";
			if ($_SESSION['vsMenuURL'][$x] == $vCor OR
				$_SESSION['vsMenuURL'][$x] == $vAct OR
				$_SESSION['vsMenuURL'][$x] == $vDif) {
				$_SESSION['vsPagIni'] = true;
			}
			*/
			$x=$x+1;
		}
		
		//comparo la URL solicitada con las que tiene acceso el usr si no la tiene retorna 0 
		$resultado = 0;//esta var determina si existe la url o no con 0-1	
		for ($i=0; $i < count($_SESSION['vsMenuURL']) ; $i++) 
		{ 
			if ("http://". $_SERVER["HTTP_HOST"] . $_SERVER['REQUEST_URI'] == $_SESSION['vsMenuURL'][$i]) 
			{
				$resultado = 1;
				break;
			}
			}
		return $resultado;
	}

	//ACA SE ARMA EL ARRAY DEL MENU DINAMICO DEL FRONT-END
	function menu(){
		$cnn=$GLOBALS['m_Conexion']->Conectar(); //INSTANCIA DE LA CONEXION ACTIVA	
		$_SESSION['vsMenu']= $GLOBALS['m_Conexion']->MenuUsr($cnn, $_SESSION['vsPerfil']);
	}

	//ACA SE ARMA EL ARRAY DE LAS OPCIONES QUE PODRÁ VER EL USR
	function menu2(){
		$cnn=$GLOBALS['m_Conexion']->Conectar(); //INSTANCIA DE LA CONEXION ACTIVA	
		$_SESSION['vsMenu2']= $GLOBALS['m_Conexion']->MenuUrl($cnn, $_SESSION['vsPerfil']);
	}

	function alert()
	{
		if (isset($_SESSION['vsMsj'])) {

			//echo mysqli_errno($_SESSION['vsMsj']);
			echo ' <br>
			<div class="container-fluid">
			  <div class="alert alert-success alert-dismissible">
			    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			    <strong>Aviso!</strong> '. $_SESSION['vsMsj'] .'.
			  </div>
			</div>' ;
			unset($_SESSION['vsMsj']);
		}
		
	}

	function login(){		
		require("modelo/login.php"); 
	}

	function error(){
		require ($GLOBALS['r'].'header.php'); 
		if (@$_SESSION['vPagIni']) {
			echo "Paginicio";
		}else{
			require_once 'vista/AdminLTE/pages/examples/404.php';
		}
		require ($GLOBALS['r'].'footer.php');
	}

	function perfil() {
		//$r = ruta de la vista
		//Esto solo es de ejemplo, Necesito llamar a una vista para mostrar esa data
		switch ($_SESSION['vsPerfil']) {
					//SI ES ADMIN
					case 1:
						require ($GLOBALS['r'].'header.php'); 
						//require ($r.'perfil.php'); //acá llamará a la vista del perfil

						echo "<div><h1>Administrador del sistema</h1></div> <br>";

						//echo "<pre>" . $_SESSION['vsMenu'] . "<pre>";
						/*
						echo "<br>" . var_dump($_SESSION['vsMenuURL'])  . "<br>";

						echo "<br>" . var_dump($_SESSION['vsMenu2'])  . "<br>";
						
						$var = "http://".$_SERVER["HTTP_HOST"] . $_SERVER['REQUEST_URI']; // folder + parametros
						echo $var;
						
						$r=$_SESSION['vsMenuURL'][3];
						echo "<br>". $var . "<---->"  . $r  . "<br>";

						echo "<br>" . var_dump($_SESSION['vsMenuURL'])  . "<br>";
						$arr = array("foo", "bar", "hello", "world");
						echo "<br>" . var_dump($arr)  . "<br>";
						echo "IMPRESION DE ARREGLO MANUAL: " . $arr['2'] . "<br>";
						*/
						//require_once 'vista/AdminLTE/pages/examples/404.php';
						
						echo "<br>id:  " . $_SESSION['vsIdUsuario'];
						echo "<br>nombre: " . $_SESSION['vsNombre'];
						echo "<br>apellido: " . $_SESSION['vsApellido'];
						echo "<br>perfil: " . $_SESSION['vsPerfil'];
						echo "<br>correo: " . $_SESSION['vsEmail'];
						echo "<br>estado: " . $_SESSION['vsEstado'];
						
						require ($GLOBALS['r'].'footer.php');
						break;
					//SI ES USUARIO (solo para mostrar el contenido)
					case 2:
						require ($GLOBALS['r'].'header.php'); 
						require ($GLOBALS['r'].'pages/examples/ejemplo.php'); //ACÁ IRÁ EL CONTENIDO				
						require ($GLOBALS['r'].'footer.php');				
						break;
					default:
						require ($GLOBALS['r'].'header.php'); 
						//require ($GLOBALS['r'].'pages/examples/ejemplo.php'); //ACÁ IRÁ EL CONTENIDO
						require ($GLOBALS['r'].'footer.php');
						break;
				}
	}

}



 ?>