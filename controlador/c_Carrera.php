<?php

/**
 Trabajo de graduacion UTEC
 */






class c_Carrera
{
	
	public $msj = null;

	function swCarrera(){
		/* //POR SI DA PROBLEMA RECIBIR CON $_REQUEST['ev']
		if (isset(@$_POST['ev'])) {
			$ev = $_POST['ev'];
		}else{
			$ev = @$_REQUEST['ev'];
		}
		*/
		switch (@$_REQUEST['ev']) 
		{   //EVENTOS
			case 'listar':
			$_SESSION['vsValidacion'] = 1 ;
				$this->mostrar();
				break;

			case 'formInsertar':
				$this->formInsert();
				break;
			case 'insertar':
				$this->insertar();
				break;
			case 'formEditar':
				$this->mostrarUnaCarrera();
				break;
			case 'editar':
				$this->editar();
				break;
			case 'eliminar':
				$this->eliminarUnaCarrera();
				break;

			default:
			$_SESSION['vsValidacion'] = 0 ;
				$this->mostrar();
				//echo "Evento no encontrado";					
				break;
		}
	}



	function formEditar()
	{
		echo "ev: " . $_POST['ev'];
		echo "<br>";
		echo "id: " . $_POST['id'];
		echo "<br>";
		//var_dump($_POST['carne']);
		$this->mostrar();
	}

	function mostrar()
	{
		//Headder
		require ($GLOBALS['r'].'header.php'); 
		//Datos que llevara la vista
		$verCarreras=$GLOBALS['m_Carrera']->mostrarTodos();
		//Llamada a la vista
		require_once ($GLOBALS['r']."v_ListaCarreras.php");
		//footer
		require ($GLOBALS['r'].'footer.php');
	}

	function mostrarUnaCarrera()
	{
		//Headder
		require ($GLOBALS['r'].'header.php'); 
		//Datos que llevara la vista
		$verCarrera=$GLOBALS['m_Carrera']->mostrarUno($_POST['id']);
		//Llamada a la vista
		require_once ($GLOBALS['r']."pages/examples/v_modificarCarrera.php");
		//footer
		require ($GLOBALS['r'].'footer.php');
	}

	function eliminarUnaCarrera(){

	
		$verCarrera=$GLOBALS['m_Carrera']->eliminarCarrera($_POST['id']);

		$this->mostrar();
	}

	function formInsert()
	{
		//Headder
		require ($GLOBALS['r'].'header.php'); 
		if (isset($_SESSION['vsMsj'])) {

			//echo mysqli_errno($_SESSION['vsMsj']);
			echo ' <br>
			<div class="container-fluid">
			  <div class="alert alert-success alert-dismissible">
			    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			    <strong>Aviso!</strong> '. $_SESSION['vsMsj'] .'.
			  </div>
			</div>' ;
		}
		unset($_SESSION['vsMsj']);
		//Llamada a la vista
		require_once ($GLOBALS['r']."pages/examples/v_insertarCarrera.php");
		//footer
		require ($GLOBALS['r'].'footer.php');
	}

	function insertar()
	{
		 //#######VALIDAR CAMPOS DEL LADO DEL SERVER  <<<<<------------####
		if (isset($_POST['txtCarrera'])) 
		{
			$insert = $GLOBALS['m_Carrera']->insertar(utf8_encode($_POST['txtCarrera']), $_POST['txtCodCarrera']);

			if (@$insert) {
				$_SESSION['vsMsj'] = $insert;
			}
		}else{
			$_SESSION['vsMsj'] = "Por favor, complete todos los campos";
		}
		$this->formInsert();
	}

	function editar()
	{
		/* // ESTE CODIGO COMENTARIADO ES PARA VALIDAR QUE LLEGAN LOS DATOS
		echo "ev: " . $_POST['ev'];
		echo "<br>";
		echo "carne: " . $_POST['txtCarne'];
		echo "<br>";
		echo "carne: " . $_POST['txtNombre'];
		echo "<br>";
		echo "carne: " . $_POST['txtApellido'];
		echo "<br>";
		echo "carne: " . $_POST['txtEmail'];
		echo "<br>";
		*/
		 //#######VALIDAR CAMPOS DEL LADO DEL SERVER  <<<<<------------####
		if ($_POST['txtIdCarrera']AND$_POST['txtCarrera']) 
		{
			$edit = $GLOBALS['m_Carrera']->modificar(utf8_encode($_POST['txtCarrera']),$_POST['txtIdCarrera'], $_POST['listEstado'], $_POST['txtCodCarrera']);

			if (@$edit) {
				$_SESSION['vsMsj'] = $edit;
			}
		}else{
			$_SESSION['vsMsj'] = "Por favor, complete todos los campos...
			 " .  "<br>intente nuevamente.";
		}
		//$this->mostrarUnDocente();
		require ($GLOBALS['r'].'header.php');
		if (isset($_SESSION['vsMsj'])) {

			//echo mysqli_errno($_SESSION['vsMsj']);
			echo ' <br>
			<div class="container-fluid">
			  <div class="alert alert-success alert-dismissible">
			    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			    <strong>Aviso!</strong> '. $_SESSION['vsMsj'] .'.
			  </div>
			</div>' ;
		}
		$verCarreras=$GLOBALS['m_Carrera']->mostrarTodos();
		//Llamada a la vista
		require_once ($GLOBALS['r']."v_ListaCarreras.php");
		require ($GLOBALS['r'].'footer.php');
	}

}
?>