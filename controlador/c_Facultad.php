<?php

/**
 Trabajo de graduacion UTEC
 */






class c_Facultad
{
	
	public $msj = null;

	function swFacultad(){
		/* //POR SI DA PROBLEMA RECIBIR CON $_REQUEST['ev']
		if (isset(@$_POST['ev'])) {
			$ev = $_POST['ev'];
		}else{
			$ev = @$_REQUEST['ev'];
		}
		*/
		switch (@$_REQUEST['ev']) 
		{   //EVENTOS
			case 'listar':
			$_SESSION['vsValidacion'] = 1 ;
				$this->mostrar();
				break;

			case 'formInsertar':
				$this->formInsert();
				break;
			case 'insertar':
				$this->insertar();
				break;
			case 'formEditar':
				$this->mostrarUnaFacultad();
				break;
			case 'editar':
				$this->editar();
				break;
			case 'eliminar':
				$this->eliminarFacultad();
				break;

			default:
			$_SESSION['vsValidacion'] = 0 ;
				$this->mostrar();
				//echo "Evento no encontrado";					
				break;
		}
	}



	function formEditar()
	{
		echo "ev: " . $_POST['ev'];
		echo "<br>";
		echo "carne: " . $_POST['id'];
		echo "<br>";
		//var_dump($_POST['carne']);
		$this->mostrar();
	}

	function mostrar()
	{
		//Headder
		require ($GLOBALS['r'].'header.php'); 
		//Datos que llevara la vista
		$verFacultades=$GLOBALS['m_Facultad']->mostrarTodos();
		//Llamada a la vista
		require_once ($GLOBALS['r']."v_ListaFacultades.php");
		//footer
		require ($GLOBALS['r'].'footer.php');
	}

	function mostrarUnaFacultad()
	{
		//Headder
		require ($GLOBALS['r'].'header.php'); 
		//Datos que llevara la vista
		$verFacultad=$GLOBALS['m_Facultad']->mostrarUno($_POST['id']);
		//Llamada a la vista
		require_once ($GLOBALS['r']."pages/examples/v_modificarFacultad.php");
		//footer
		require ($GLOBALS['r'].'footer.php');
	}

	function eliminarFacultad(){

	
		$verCarrera=$GLOBALS['m_Facultad']->eliminarFacultad($_POST['id']);

		$this->mostrar();
	}

	function formInsert()
	{
		//Headder
		require ($GLOBALS['r'].'header.php'); 
		if (isset($_SESSION['vsMsj'])) {

			//echo mysqli_errno($_SESSION['vsMsj']);
			echo ' <br>
			<div class="container-fluid">
			  <div class="alert alert-success alert-dismissible">
			    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			    <strong>Aviso!</strong> '. $_SESSION['vsMsj'] .'.
			  </div>
			</div>' ;
		}
		unset($_SESSION['vsMsj']);
		//Llamada a la vista
		require_once ($GLOBALS['r']."pages/examples/v_insertarFacultad.php");
		//footer
		require ($GLOBALS['r'].'footer.php');
	}

	function insertar()
	{
		 //#######VALIDAR CAMPOS DEL LADO DEL SERVER  <<<<<------------####
		if (isset($_POST['txtFacultad'])) 
		{
			$insert = $GLOBALS['m_Facultad']->insertar(utf8_encode($_POST['txtFacultad']));

			if (@$insert) {
				$_SESSION['vsMsj'] = $insert;
			}
		}else{
			$_SESSION['vsMsj'] = "Por favor, complete todos los campos";
		}
		$this->formInsert();
	}

	function editar()
	{
		/* // ESTE CODIGO COMENTARIADO ES PARA VALIDAR QUE LLEGAN LOS DATOS
		echo "ev: " . $_POST['ev'];
		echo "<br>";
		echo "carne: " . $_POST['txtCarne'];
		echo "<br>";
		echo "carne: " . $_POST['txtNombre'];
		echo "<br>";
		echo "carne: " . $_POST['txtApellido'];
		echo "<br>";
		echo "carne: " . $_POST['txtEmail'];
		echo "<br>";
		*/
		 //#######VALIDAR CAMPOS DEL LADO DEL SERVER  <<<<<------------####
		if ($_POST['txtFacultad']AND$_POST['listEstado']AND$_POST['txtIdFacultad']) 
		{
			$edit = $GLOBALS['m_Facultad']->modificar(utf8_encode($_POST['txtFacultad']),$_POST['listEstado'], $_POST['txtIdFacultad']);

			if (@$edit) {
				$_SESSION['vsMsj'] = $edit;
			}
		}else{
			$_SESSION['vsMsj'] = "Por favor, complete todos los campos...
			 " .  "<br>intente nuevamente.";
		}
		//$this->mostrarUnDocente();
		require ($GLOBALS['r'].'header.php');
		if (isset($_SESSION['vsMsj'])) {

			//echo mysqli_errno($_SESSION['vsMsj']);
			echo ' <br>
			<div class="container-fluid">
			  <div class="alert alert-success alert-dismissible">
			    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			    <strong>Aviso!</strong> '. $_SESSION['vsMsj'] .'.
			  </div>
			</div>' ;
		}
		$verFacultades=$GLOBALS['m_Facultad']->mostrarTodos();
		//Llamada a la vista
		require_once ($GLOBALS['r']."v_ListaFacultades.php");
		require ($GLOBALS['r'].'footer.php');
	}

}
?>