<?php

/**
 Trabajo de graduacion UTEC
 */


class c_Docente
{
	
	public $msj = null;

	function swDocente(){
		/* //POR SI DA PROBLEMA RECIBIR CON $_REQUEST['ev']
		if (isset(@$_POST['ev'])) {
			$ev = $_POST['ev'];
		}else{
			$ev = @$_REQUEST['ev'];
		}
		*/
		switch (@$_REQUEST['ev']) 
		{   //EVENTOS
			case 'listar':
				$_SESSION['vsValidacion'] = 1 ;
				$this->mostrar();
				break;

			case 'formInsertar':
				$this->formInsert();
				break;
			case 'insertar':
				$this->insertar();
				break;
			case 'formEditar':
				$this->mostrarUnDocente();
				break;
			case 'editar':
				$this->editar();
				break;
			case 'eliminar':
				$this->eliminar();
				break;

			default:
				$_SESSION['vsValidacion'] = 0 ;
				$this->mostrar();
				//echo "Evento no encontrado";					
				break;
		}
	}

	function formEditar()
	{
		echo "ev: " . $_POST['ev'];
		echo "<br>";
		echo "id: " . $_POST['id'];
		echo "<br>";
		//var_dump($_POST['carne']);
		$this->mostrar();
	}


	function mostrar()
	{
		//Headder
		require ($GLOBALS['r'].'header.php'); 
		//Datos que llevara la vista
		$verDocentes=$GLOBALS['m_Docente']->mostrarTodosCRUD();
		//Llamada a la vista
		require_once ($GLOBALS['r']."v_Listadocentes.php");
		//footer
		require ($GLOBALS['r'].'footer.php');
	}

	function mostrarUnDocente()
	{
		//Headder
		require ($GLOBALS['r'].'header.php'); 
		//Datos que llevara la vista
		$verDocentes=$GLOBALS['m_Docente']->mostrarUno($_POST['id']);
		//Llamada a la vista
		require_once ($GLOBALS['r']."pages/examples/v_modificarDocente.php");
		//footer
		require ($GLOBALS['r'].'footer.php');
	}

	function eliminar(){

	
		$verDocentes=$GLOBALS['m_Docente']->eliminar($_POST['id']);

		$this->mostrar();
	}

	function formInsert()
	{
		//Headder
		require ($GLOBALS['r'].'header.php'); 
		if (isset($_SESSION['vsMsj'])) {

			//echo mysqli_errno($_SESSION['vsMsj']);
			echo ' <br>
			<div class="container-fluid">
			  <div class="alert alert-success alert-dismissible">
			    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			    <strong>Aviso!</strong> '. $_SESSION['vsMsj'] .'.
			  </div>
			</div>' ;
		}
		unset($_SESSION['vsMsj']);
		//Llamada a la vista
		require_once ($GLOBALS['r']."pages/examples/v_insertarDocente.php");
		//footer
		require ($GLOBALS['r'].'footer.php');
	}

	function insertar()
	{
		 //#######VALIDAR CAMPOS DEL LADO DEL SERVER  <<<<<------------####
		if (isset($_POST['txtCarne'])AND$_POST['txtNombre']AND$_POST['txtEmail']) 
		{
			$insert = $GLOBALS['m_Docente']->insertar($_POST['txtCarne'],utf8_encode($_POST['txtNombre']),$_POST['txtEmail']);

			if (@$insert) {
				$_SESSION['vsMsj'] = $insert;
			}
		}else{
			$_SESSION['vsMsj'] = "Por favor, complete todos los campos";
		}
		//echo mysqli_errno($insert);
		$this->formInsert();
		//Llamada a la vista
		//require_once ($GLOBALS['r']."pages/examples/v_insertarDocente.php");
		//footer
		//require ($GLOBALS['r'].'footer.php');

	}


	function editar()
	{
		/* // ESTE CODIGO COMENTARIADO ES PARA VALIDAR QUE LLEGAN LOS DATOS
		echo "ev: " . $_POST['ev'];
		echo "<br>";
		echo "carne: " . $_POST['txtCarne'];
		echo "<br>";
		echo "carne: " . $_POST['txtNombre'];
		echo "<br>";
		echo "carne: " . $_POST['txtApellido'];
		echo "<br>";
		echo "carne: " . $_POST['txtEmail'];
		echo "<br>";
		*/
		 //#######VALIDAR CAMPOS DEL LADO DEL SERVER  <<<<<------------####
		if ($_POST['txtCarne']) 
		{
			$edit = $GLOBALS['m_Docente']->modificar($_POST['txtCarne'],utf8_encode($_POST['txtNombre']),$_POST['listEstado'],$_POST['txtEmail']);

			if (@$edit) {
				$_SESSION['vsMsj'] = $edit;
			}
		}else{
			$_SESSION['vsMsj'] = "Por favor, complete todos los campos...
			 " .  "<br>intente nuevamente.";
		}
		//$this->mostrarUnDocente();
		require ($GLOBALS['r'].'header.php');
		if (isset($_SESSION['vsMsj'])) {

			//echo mysqli_errno($_SESSION['vsMsj']);
			echo ' <br>
			<div class="container-fluid">
			  <div class="alert alert-success alert-dismissible">
			    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			    <strong>Aviso!</strong> '. $_SESSION['vsMsj'] .'.
			  </div>
			</div>' ;
			unset($_SESSION['vsMsj']);
		}
		$verDocentes=$GLOBALS['m_Docente']->mostrarTodosCRUD();
		//Llamada a la vista
		require_once ($GLOBALS['r']."v_Listadocentes.php");
		require ($GLOBALS['r'].'footer.php');
	}

}
?>