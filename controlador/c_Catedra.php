<?php

/**
 Trabajo de graduacion UTEC
 */






class c_Catedra
{
	
	public $msj = null;

	function swCatedra(){
		/* //POR SI DA PROBLEMA RECIBIR CON $_REQUEST['ev']
		if (isset(@$_POST['ev'])) {
			$ev = $_POST['ev'];
		}else{
			$ev = @$_REQUEST['ev'];
		}
		*/
		switch (@$_REQUEST['ev']) 
		{   //EVENTOS
			case 'listar':
			$_SESSION['vsValidacion'] = 1 ;
				$this->mostrar();
				break;

			case 'formInsertar':
				$this->formInsert();
				break;
			case 'insertar':
				$this->insertar();
				break;
			case 'formEditar':
				$this->mostrarUnaCatedra();
				break;
			case 'editar':
				$this->editar();
				break;
			case 'eliminar':
				$this->eliminarUnaCatedra();
				break;

			default:
			$_SESSION['vsValidacion'] = 0 ;
				$this->mostrar();
				//echo "Evento no encontrado";					
				break;
		}
	}



	function formEditar()
	{


		echo "ev: " . $_POST['ev'];
		echo "<br>";
		echo "id: " . $_POST['id'];
		echo "<br>";
		//var_dump($_POST['carne']);

		$this->mostrar();
	}

	function mostrar()
	{
		//Headder
		require ($GLOBALS['r'].'header.php'); 
		//Datos que llevara la vista
		$verCatedras=$GLOBALS['m_Catedra']->mostrarTodos();
		//Llamada a la vista
		require_once ($GLOBALS['r']."v_ListaCatedras.php");
		//footer
		require ($GLOBALS['r'].'footer.php');
	}

	function mostrarUnaCatedra()
	{
		//Headder
		require ($GLOBALS['r'].'header.php'); 
		//Datos que llevara la vista
		$verCatedra=$GLOBALS['m_Catedra']->mostrarUno($_POST['id']);
		$verEscuelas=$GLOBALS['m_Catedra']->mostrarEscuelas();
		//Llamada a la vista
		require_once ($GLOBALS['r']."pages/examples/v_modificarCatedra.php");
		//footer
		require ($GLOBALS['r'].'footer.php');
	}
	function eliminarUnaCatedra(){

	
		$verCarrera=$GLOBALS['m_Catedra']->eliminarCatedra($_POST['id']);

		$this->mostrar();
	}
	function formInsert()
	{
		//Headder
		require ($GLOBALS['r'].'header.php'); 
		if (isset($_SESSION['vsMsj'])) {

			//echo mysqli_errno($_SESSION['vsMsj']);
			echo ' <br>
			<div class="container-fluid">
			  <div class="alert alert-success alert-dismissible">
			    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			    <strong>Aviso!</strong> '. $_SESSION['vsMsj'] .'.
			  </div>
			</div>' ;
		}
		unset($_SESSION['vsMsj']);
		//Llamada a la vista
		$verEscuelas=$GLOBALS['m_Catedra']->mostrarEscuelas();
		require_once ($GLOBALS['r']."pages/examples/v_insertarCatedra.php");
		//footer
		require ($GLOBALS['r'].'footer.php');
	}

	function insertar()
	{



		 //#######VALIDAR CAMPOS DEL LADO DEL SERVER  <<<<<------------####
		if (isset($_POST['txtCatedra'])) 
		{
			$insert = $GLOBALS['m_Catedra']->insertar($_POST['listEscuela'], utf8_encode($_POST['txtCatedra']));


			if (@$insert) {
				$_SESSION['vsMsj'] = $insert;
			}
		}else{
			$_SESSION['vsMsj'] = "Por favor, complete todos los campos";
		}
		$this->formInsert();
	}

	function editar()
	{
		/* // ESTE CODIGO COMENTARIADO ES PARA VALIDAR QUE LLEGAN LOS DATOS
		echo "ev: " . $_POST['ev'];
		echo "<br>";
		echo "carne: " . $_POST['txtCarne'];
		echo "<br>";
		echo "carne: " . $_POST['txtNombre'];
		echo "<br>";
		echo "carne: " . $_POST['txtApellido'];
		echo "<br>";
		echo "carne: " . $_POST['txtEmail'];
		echo "<br>";
		*/
		 //#######VALIDAR CAMPOS DEL LADO DEL SERVER  <<<<<------------####
		if ($_POST['txtIdCatedra']AND$_POST['txtCatedra']AND$_POST['listEstado']AND$_POST['listEscuela']) 
		{
			$edit = $GLOBALS['m_Catedra']->modificar($_POST['listEstado'],$_POST['listEscuela'],utf8_encode($_POST['txtCatedra']),$_POST['txtIdCatedra']);

			if (@$edit) {
				$_SESSION['vsMsj'] = $edit;
			}
		}else{
			$_SESSION['vsMsj'] = "Por favor, complete todos los campos...
			 " .  "<br>intente nuevamente.";
		}
		//$this->mostrarUnDocente();
		require ($GLOBALS['r'].'header.php');
		if (isset($_SESSION['vsMsj'])) {

			//echo mysqli_errno($_SESSION['vsMsj']);
			echo ' <br>
			<div class="container-fluid">
			  <div class="alert alert-success alert-dismissible">
			    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			    <strong>Aviso!</strong> '. $_SESSION['vsMsj'] .'.
			  </div>
			</div>' ;
		}
		$verCatedras=$GLOBALS['m_Catedra']->mostrarTodos();
		//Llamada a la vista
		require_once ($GLOBALS['r']."v_ListaCatedras.php");
		require ($GLOBALS['r'].'footer.php');
	}

}
?>