<?php

/**
 Trabajo de graduacion UTEC
 */

class c_Escuela
{
	
	public $msj = null;

	function swEscuela(){
		/* //POR SI DA PROBLEMA RECIBIR CON $_REQUEST['ev']
		if (isset(@$_POST['ev'])) {
			$ev = $_POST['ev'];
		}else{
			$ev = @$_REQUEST['ev'];
		}
		*/
		switch (@$_REQUEST['ev']) 
		{   //EVENTOS
			case 'listar':
			$_SESSION['vsValidacion'] = 1 ;
				$this->mostrar();
				break;

			case 'formInsertar':
				$this->formInsert();
				break;
			case 'insertar':
				$this->insertar();
				break;
			case 'formEditar':
				$this->mostrarUnaEscuela();
				break;
			case 'editar':
				$this->editar();
				break;
			case 'eliminar':
				$this->eliminarUnaEscuela();
				break;

			default:
				$this->mostrar();
				$_SESSION['vsValidacion'] = 0 ;
				//echo "Evento no encontrado";					
				break;
		}
	}



	function formEditar()
	{
		echo "ev: " . $_POST['ev'];
		echo "<br>";
		echo "id: " . $_POST['id'];
		echo "<br>";
		//var_dump($_POST['carne']);
		$this->mostrar();
	}

	function mostrar()
	{
		//Headder
		require ($GLOBALS['r'].'header.php'); 
		//Datos que llevara la vista
		$verEscuelas=$GLOBALS['m_Escuela']->mostrarTodos();
		//Llamada a la vista
		require_once ($GLOBALS['r']."v_ListaEscuelas.php");
		//footer
		require ($GLOBALS['r'].'footer.php');
	}

	function mostrarUnaEscuela()
	{
		//Headder
		require ($GLOBALS['r'].'header.php'); 
		//Datos que llevara la vista
		$verEscuela=$GLOBALS['m_Escuela']->mostrarUno($_POST['id']);
		$verFacultades=$GLOBALS['m_Escuela']->mostrarFacultades();
		//Llamada a la vista
		require_once ($GLOBALS['r']."pages/examples/v_modificarEscuela.php");
		//footer
		require ($GLOBALS['r'].'footer.php');
	}

	function eliminarUnaEscuela(){

	
		$verCarrera=$GLOBALS['m_Escuela']->eliminarEscuela($_POST['id']);

		$this->mostrar();
	}

	function formInsert()
	{
		//Headder
		require ($GLOBALS['r'].'header.php'); 
		if (isset($_SESSION['vsMsj'])) {

			//echo mysqli_errno($_SESSION['vsMsj']);
			echo ' <br>
			<div class="container-fluid">
			  <div class="alert alert-success alert-dismissible">
			    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			    <strong>Aviso!</strong> '. $_SESSION['vsMsj'] .'.
			  </div>
			</div>' ;
		}
		unset($_SESSION['vsMsj']);
		//Llamada a la vista

		$verFacultades=$GLOBALS['m_Escuela']->mostrarFacultades();
		require_once ($GLOBALS['r']."pages/examples/v_insertarEscuela.php");
	


		//footer
		require ($GLOBALS['r'].'footer.php');
	}

	function insertar()
	{
		 //#######VALIDAR CAMPOS DEL LADO DEL SERVER  <<<<<------------####
		if (isset($_POST['txtEscuela'])AND$_POST['listFacultad']) 
		{
			$insert = $GLOBALS['m_Escuela']->insertar(utf8_encode($_POST['txtEscuela']),$_POST['listFacultad']);

			if (@$insert) {
				$_SESSION['vsMsj'] = $insert;
			}
		}else{
			$_SESSION['vsMsj'] = "Por favor, complete todos los campos";
		}
		$this->formInsert();
	}

	function editar()
	{
		/*ESTE CODIGO COMENTARIADO ES PARA VALIDAR QUE LLEGAN LOS DATOS
		echo "ev: " . $_POST['ev'];
		echo "<br>";
		echo "carne: " . $_POST['txtIdEscuela'];
		echo "<br>";
		echo "carne: " . $_POST['txtEscuela'];
		echo "<br>";
		echo "carne: " . $_POST['listFacultad'];
		echo "<br>";
		echo "carne: " . $_POST['listEstado'];
		echo "<br>";*/
		 //#######VALIDAR CAMPOS DEL LADO DEL SERVER  <<<<<------------####
		if ($_POST['txtIdEscuela']AND$_POST['txtEscuela']AND$_POST['listFacultad']AND$_POST['listEstado']) 
		{
			$edit = $GLOBALS['m_Escuela']->modificar($_POST['txtIdEscuela'],utf8_encode($_POST['txtEscuela']),$_POST['listFacultad'],$_POST['listEstado']);

			if (@$edit) {
				$_SESSION['vsMsj'] = $edit;
			}
		}else{
			$_SESSION['vsMsj'] = "Por favor, complete todos los campos...
			 " .  "<br>intente nuevamente.";
		}
		//$this->mostrarUnEscuela();
		require ($GLOBALS['r'].'header.php');
		if (isset($_SESSION['vsMsj'])) {

			//echo mysqli_errno($_SESSION['vsMsj']);
			echo ' <br>
			<div class="container-fluid">
			  <div class="alert alert-success alert-dismissible">
			    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			    <strong>Aviso!</strong> '. $_SESSION['vsMsj'] .'.
			  </div>
			</div>' ;
		}
		$verEscuelas=$GLOBALS['m_Escuela']->mostrarTodos();
		//Llamada a la vista
		require_once ($GLOBALS['r']."v_ListaEscuelas.php");
		require ($GLOBALS['r'].'footer.php');
	}

}
?>