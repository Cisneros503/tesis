<?php

/**
 Trabajo de graduacion UTEC
 */






class c_Asigna
{
	
	public $msj = null;

	function swAsigna(){
		/* //POR SI DA PROBLEMA RECIBIR CON $_REQUEST['ev']
		if (isset(@$_POST['ev'])) {
			$ev = $_POST['ev'];
		}else{
			$ev = @$_REQUEST['ev'];
		}
		*/
		switch (@$_REQUEST['ev']) 
		{   //EVENTOS
			case 'listar':
			$_SESSION['vsValidacion'] = 1 ;
				$this->mostrar();
				break;

			case 'formInsertar':
				$this->formInsert();
				break;
			case 'insertar':
				$this->insertar();
				break;
			case 'formEditar':
				$this->mostrarUnaAsignacion();
				break;
			case 'editar':
				$this->editar();
				break;
			case 'eliminar':
				$this->eliminarUnaAsignacion();
				break;

			default:
			$_SESSION['vsValidacion'] = 0 ;
				$this->mostrar();
				//echo "Evento no encontrado";					
				break;
		}
	}



	function formEditar()
	{
		echo "ev: " . $_POST['ev'];
		echo "<br>";
		echo "id: " . $_POST['id'];
		echo "<br>";
		//var_dump($_POST['carne']);
		$this->mostrar();
	}

	function mostrar()
	{
		//Headder
		require ($GLOBALS['r'].'header.php'); 
		//Datos que llevara la vista
		$verAsignaciones=$GLOBALS['m_Asigna']->mostrarTodos();
		//Llamada a la vista
		require_once ($GLOBALS['r']."v_ListaAsignaciones.php");
		//footer
		require ($GLOBALS['r'].'footer.php');
	}

	function mostrarUnaAsignacion()
	{
		//Headder
		require ($GLOBALS['r'].'header.php'); 
		//Datos que llevara la vista
		$verAsignacion=$GLOBALS['m_Asigna']->mostrarUno($_POST['id']);
		$verCarreras=$GLOBALS['m_Asigna']->mostrarCarreras();
		$verCatedras=$GLOBALS['m_Asigna']->mostrarCatedras();
		$verMaterias=$GLOBALS['m_Asigna']->mostrarMaterias();
		//Llamada a la vista
		require_once ($GLOBALS['r']."pages/examples/v_modificarAsigna.php");
		//footer
		require ($GLOBALS['r'].'footer.php');
	}


	function eliminarUnaAsignacion(){

	
		$verAsignacion=$GLOBALS['m_Asigna']->eliminarAsignacion($_POST['id']);

		$this->mostrar();

	}

	function formInsert()
	{
		//Headder
		require ($GLOBALS['r'].'header.php'); 
		if (isset($_SESSION['vsMsj'])) {

			//echo mysqli_errno($_SESSION['vsMsj']);
			echo ' <br>
			<div class="container-fluid">
			  <div class="alert alert-success alert-dismissible">
			    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			    <strong>Aviso!</strong> '. $_SESSION['vsMsj'] .'.
			  </div>
			</div>' ;
		}
		unset($_SESSION['vsMsj']);

		$verCarreras=$GLOBALS['m_Asigna']->mostrarCarreras();
		$verCatedras=$GLOBALS['m_Asigna']->mostrarCatedras();
		$verMaterias=$GLOBALS['m_Asigna']->mostrarMaterias();
		//Llamada a la vista
		require_once ($GLOBALS['r']."pages/examples/v_insertarAsigna.php");
		//footer
		require ($GLOBALS['r'].'footer.php');
	}

	function insertar()
	{
		 //#######VALIDAR CAMPOS DEL LADO DEL SERVER  <<<<<------------####
		if (isset($_POST['listCarrera'])AND$_POST['listCatedra']AND$_POST['listMateria']) 
		{
			$insert = $GLOBALS['m_Asigna']->insertar($_POST['listCarrera'],$_POST['listCatedra'],$_POST['listMateria']);

			if (@$insert) {
				$_SESSION['vsMsj'] = $insert;
			}
		}else{
			$_SESSION['vsMsj'] = "Por favor, complete todos los campos";
		}
		$this->formInsert();
	}

	function editar()
	{
		/* // ESTE CODIGO COMENTARIADO ES PARA VALIDAR QUE LLEGAN LOS DATOS
		echo "ev: " . $_POST['ev'];
		echo "<br>";
		echo "carne: " . $_POST['listEstado'];
		echo "<br>";
		echo "carne: " . $_POST['listCatedra'];
		echo "<br>";
		echo "carne: " . $_POST['listMateria'];
		echo "<br>";
		echo "carne: " . $_POST['listCarrera'];
		echo "<br>";
		*/
		 //#######VALIDAR CAMPOS DEL LADO DEL SERVER  <<<<<------------####
		if ($_POST['listEstado']AND$_POST['listCatedra']AND$_POST['listMateria']AND$_POST['listCarrera']AND$_POST['txtId']) 
		{
			$edit = $GLOBALS['m_Asigna']->modificar($_POST['listEstado'],$_POST['listCatedra'],$_POST['listMateria'],$_POST['listCarrera'],$_POST['txtId']);

			if (@$edit) {
				$_SESSION['vsMsj'] = $edit;
			}
		}else{
			$_SESSION['vsMsj'] = "Por favor, complete todos los campos...
			 " .  "<br>intente nuevamente.";
		}
		//$this->mostrarUnAsigna();
		require ($GLOBALS['r'].'header.php');
		if (isset($_SESSION['vsMsj'])) {

			//echo mysqli_errno($_SESSION['vsMsj']);
			echo ' <br>
			<div class="container-fluid">
			  <div class="alert alert-success alert-dismissible">
			    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			    <strong>Aviso!</strong> '. $_SESSION['vsMsj'] .'.
			  </div>
			</div>' ;
		}
		$verAsignaciones=$GLOBALS['m_Asigna']->mostrarTodos();
		//Llamada a la vista
		require_once ($GLOBALS['r']."v_ListaAsignaciones.php");
		require ($GLOBALS['r'].'footer.php');
	}

}
?>