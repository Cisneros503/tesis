<?php 

/**
 * SISTEMA REALIZADO POR EL GRUPO #3. 
 * PROYECTO DE GRADUACIÓN TECNICOS EN INGENIERIA DE SOFTWARE
 * CICLO 02-2018.
 */
require_once 'modelo/m_Alumnos.php';
require_once 'controlador/c_Alumnos.php';
class c_Procesos
{
	function swProcesos(){
		/* //POR SI DA PROBLEMA RECIBIR CON $_REQUEST['ev']
		if (isset(@$_POST['ev'])) {
			$ev = $_POST['ev'];
		}else{
			$ev = @$_REQUEST['ev'];
		}
		*/
		switch (@$_REQUEST['ev']) 
		{   //EVENTOS
			case 'inicio':
				$this->inicio();
				//$this->formEditar();
				break;
			case 'inicioCorrecciones':
				$this->inicioCorrecciones();
				//$this->formEditar();
				break;
			case 'frmInserTramite':
				$this->frmInserTramite();
				break;
			case 'insertarTr':
				$this->insertarTr();
				//$this->probar();
				break;
			case 'eliminarCr':
				$this->deleteCorrec();
				break;
			case 'editCo':
				$this->editCorreccion();
				//$this->editCorPrv();
				break;
			case 'detCo':
				$this->detCorreccion();
				break;
			case 'frmInsertCorreccion':
				$this->frmInsertCorreccion();
				break;
			case 'aprobarCo':
				$this->autorizarCo(1,@$_POST['idTramite']);
				break;
			case 'denegarCo':
				$this->autorizarCo(2,@$_POST['idTramite']);
				break;
			case 'modificarTr':
				$this->editarTr();
				//$this->probar();
				break;
			case 'modifCorreccion':
				
				break;
			case 'listarCorrecciones':
				
				break;
			case 'verCorreccionId':
				
				break;


			//case 'diferido':
				//$this->formularioDiferido();
				//$this->formEditar();
				//break;	

			default:
				$this->inicio();			
				break;
		}
	}

	function probar()
	{
		/*
		echo $tr['ID_tramite']."<br>";
		echo $tr['ID_carrera_materia']."<br>";
		echo $tr['ID_materia']."<br>";
		echo $tr['ID_carrera_materia']."<br>";
		*/
		echo "<br> idTramite = " .$_POST['idTramite'];
		echo "<br> cbx_carrera = " . $_POST['cbx_carrera'];
		echo "<br> cbx_materia = " . $_POST['cbx_materia'];
		echo "<br> seccion = " .$_POST['listSecciones'];
		echo "<br> evaluacion = " .$_POST['listEvaluaciones'];
		echo "<br> carne = " .$_POST['listCarnDocen'];
		echo "<br> ciclo = " .$_POST['listIdCiclos'];
		echo "<br>fRec = " .$_POST['fRec'];
		echo "<br>perio = " .$_POST['listIdPeriodos'];
		echo "<br> fsolici = " .$_POST['fSolicitud'];


		echo "<br>txtNactual  = " . $_POST['txtNactual'];
		echo "<br>txtNcorregida  = " . $_POST['txtNcorregida'];
		echo "<br>txtMotCorrec  = " . $_POST['txtMotCorrec'];
		echo "<br>txtEstCorrec  = " . $_POST['txtEstCorrec'];
		echo "<br>txtCarne  = " . $_POST['txtCarne'];
		echo "<br>txtObcerv  = " . $_POST['txtObcerv'];
		echo "<br>fRec  = " . $_POST['fRec'];


		echo "<br>ev: " . $_POST['ev'];
		echo "<br>";
		//echo "id: " . $_POST['id'];
		echo "<br>";
		//var_dump($_POST['carne']);
		//$this->mostrar();
	}

	function editCorreccion()
	{
		//Headder
		require ($GLOBALS['r'].'header.php'); 

		//echo "post tr: " . $_POST['idTramite'] . "<br>";
		$verUnTr=$GLOBALS['m_Correcciones']->mostrarUnTrCr($_POST['idTramite']);
		$tr=$verUnTr->fetch_array();
		$verCarr=$GLOBALS['m_Correcciones']->mostrarUnIdCar($tr['ID_carrera_materia']);
		$carreraTbl=$verCarr->fetch_array();
		$verCarreras=$GLOBALS['m_Correcciones']->mostrarCarreras();
		//Preparo la data que se va a mostrar en la vista
		/*
		if (@$_POST['cbx_carrera']!=null) {
			$verMaterias=$GLOBALS['m_Correcciones']->verMatIdCar(@$_POST['cbx_carrera']);
		}*/

		$verMaterias=$GLOBALS['m_Correcciones']->mostrarMaterias();
		$verSecciones=$GLOBALS['m_Correcciones']->mostrarSecciones();
		$verEvaluaciones=$GLOBALS['m_Correcciones']->mostrarEvaluaciones();
		$verDocentes=$GLOBALS['m_Correcciones']->mostrarDocentes();
		$verCiclos=$GLOBALS['m_Correcciones']->mostrarCiclos();
		//Llamada a la vista
		require_once ($GLOBALS['r']."pages/examples/v_modificarCorreccion2.php");
		//footer
		require ($GLOBALS['r'].'footer.php');
	}

	function detCorreccion()
	{
		//Headder
		require ($GLOBALS['r'].'header.php'); 

		//echo "post tr: " . $_POST['idTramite'] . "<br>";
		$verUnTr=$GLOBALS['m_Correcciones']->mostrarUnTrCr($_POST['idTramite']);
		$tr=$verUnTr->fetch_array();
		$verCarr=$GLOBALS['m_Correcciones']->mostrarUnIdCar($tr['ID_carrera_materia']);
		$carreraTbl=$verCarr->fetch_array();
		$verCarreras=$GLOBALS['m_Correcciones']->mostrarCarreras();
		//Preparo la data que se va a mostrar en la vista
		/*
		if (@$_POST['cbx_carrera']!=null) {
			$verMaterias=$GLOBALS['m_Correcciones']->verMatIdCar(@$_POST['cbx_carrera']);
		}*/

		$verMaterias=$GLOBALS['m_Correcciones']->mostrarMaterias();
		$verSecciones=$GLOBALS['m_Correcciones']->mostrarSecciones();
		$verEvaluaciones=$GLOBALS['m_Correcciones']->mostrarEvaluaciones();
		$verDocentes=$GLOBALS['m_Correcciones']->mostrarDocentes();
		$verCiclos=$GLOBALS['m_Correcciones']->mostrarCiclos();
		//Llamada a la vista
		require_once ($GLOBALS['r']."pages/examples/v_detalleCorreccion.php");
		//footer
		require ($GLOBALS['r'].'footer.php');
	}

	function editCorPrv()
	{
		//Headder
		require ($GLOBALS['r'].'header.php'); 

		//Datos que llevara la vista
		$verActividad=$GLOBALS['m_Actividades']->mostrarUna($_POST['id']);

		$registro=$verActividad->fetch_array();
		//Llamada a la vista

		$verMaximo=$GLOBALS['m_Actividades']->mostrarMaxTramite();
		$verEvaluaciones=$GLOBALS['m_Actividades']->mostrarEvaluaciones();
		$verCarMaterias=$GLOBALS['m_Actividades']->mostrarGrupoCarrera();
		$verMaterias=$GLOBALS['m_Actividades']->mostrarMateriasAsoc();
		$verDocentes=$GLOBALS['m_Actividades']->mostrarDocentes();
		$verSecciones=$GLOBALS['m_Actividades']->mostrarSecciones();
		$verCiclos=$GLOBALS['m_Actividades']->mostrarCiclos();
		
		
		//require_once ($GLOBALS['r']."pages/examples/v_modificarCorrec.php");
		require_once ($GLOBALS['r']."pages/examples/v_modificarCorreccion.php");
		//footer
		require ($GLOBALS['r'].'footer.php');
	}

	function frmInsertCorreccion()
	{
		//Headder
		require ($GLOBALS['r'].'header.php'); 	
		//Llamada a la vista
		require_once ($GLOBALS['r']."pages/examples/v_insertarCorrec.php");
		//footer
		require ($GLOBALS['r'].'footer.php');
	}

	function insertarTr()
	{
		$t = time();
		$inserTr=$GLOBALS['m_Correcciones']->insertarTr(
			$_POST['cbx_carrera'],
			$_POST['cbx_materia'],
			$_POST['listSecciones'],
			$_POST['listEvaluaciones'],
			$_POST['listCarnDocen'],
			$_POST['listIdCiclos'],
			$_POST['listIdPeriodos'],
			$_POST['fSolicitud'],
			$_POST['txtNactual'],
			$_POST['txtNcorregida'],
			$_POST['txtMotCorrec'],
			$_POST['txtEstCorrec'],
			$_POST['txtCarne'],
			$_POST['txtObcerv'],
			$_POST['txtUsrRecibe'],
			$_POST['fRec']
		);

		if (@$inserTr) 
		{		
			$_SESSION['vsMsj'] = $inserTr;
			unset($_POST['txtCarne']);
			$this->inicioCorrecciones();			
		}else{
			$_SESSION['vsMsj'] = "Falló al insertar el proceso de corrección intentelo de nuevo";
			$this->frmInserTramite();
		}	
	}

	function editarTr()
	{
		$inserTr=$GLOBALS['m_Correcciones']->editarTr(
			$_POST['idTramite'],
			$_POST['cbx_carrera'],
			$_POST['cbx_materia'],
			$_POST['listSecciones'],
			$_POST['listEvaluaciones'],
			$_POST['listCarnDocen'],
			$_POST['listIdCiclos'],
			$_POST['listIdPeriodos'],
			$_POST['fSolicitud'],
			$_POST['txtNactual'],
			$_POST['txtNcorregida'],
			$_POST['txtMotCorrec'],
			$_POST['txtEstCorrec'],
			$_POST['txtCarne'],
			$_POST['txtObcerv'],
			$_POST['txtUsrRecibe'],
			$_POST['fRec']
		);
		$_POST['listCarnDocen'] = 0;		

		if (@$inserTr) 
		{		
			$_SESSION['vsMsj'] = $inserTr;
			unset($_POST['txtCarne']);
			$this->inicioCorrecciones();			
		}else{
			$_SESSION['vsMsj'] = "Falló al Modificar el proceso de corrección intentelo de nuevo";
			$this->editCorreccion();
		}	
	}

	function inicioCorrecciones()
	{
		//Headder
		require ($GLOBALS['r'].'header.php');
		
		if (@$_POST['id'] !== null) {
			$verCorrecciones=$GLOBALS['m_Correcciones']->mostrarInicioUno($_POST['id']);
		}else{
			$verCorrecciones=$GLOBALS['m_Correcciones']->mostrarInicioTodos();
		}
		
		//Llamada a la vista
		require_once ($GLOBALS['r']."v_OpcCorrecciones.php");
		//footer
		require ($GLOBALS['r'].'footerTbl.php');
	}

	function frmInserTramite()
	{
		//VALIDO EL CARNET
		if (@$_POST['txtCarne']!=null) {
			$ve=$GLOBALS['m_Procesos']->validoCarnet(@$_POST['txtCarne']);
			if ($ve==0) {
				$_SESSION['vsMsj'] = "El carnet  <strong>". @$_POST['txtCarne'] ."</strong>   es Invalido... <br>ingrese el carnet nuevamente";
			}
		}else{
			$_SESSION['vsMsj'] = "<br>Ingrese un numero de carnet";
		}
		//Headder
		require ($GLOBALS['r'].'header.php'); 	
		//Preparo la data que se va a mostrar en la vista
		$verMaterias=$GLOBALS['m_Correcciones']->mostrarMaterias();
		$verSecciones=$GLOBALS['m_Correcciones']->mostrarSecciones();
		$verEvaluaciones=$GLOBALS['m_Correcciones']->mostrarEvaluaciones();
		$verDocentes=$GLOBALS['m_Correcciones']->mostrarDocentes();
		$verCiclos=$GLOBALS['m_Correcciones']->mostrarCiclos();
		//AGREGADO PARA MOSTRAR ALUMNOS EN SELECT
		$verDifAlu=$GLOBALS['m_Correcciones']->mostrarAlumnos();
		//Llamada a la vista
		require_once ($GLOBALS['r']."pages/examples/v_insertarTramite.php");
		//require_once ($GLOBALS['r']."pages/examples/registrar.php");
		//footer
		require ($GLOBALS['r'].'footer.php');
	}

	function formEditar()
	{
		echo "ev: " . $_POST['ev'];
		echo "<br>";
		echo "id: " . $_POST['id'];
		echo "<br>";

		$id = (int) $_POST['id'] ; 
		var_dump($id);
		//var_dump($_POST['carne']);
		//$this->mostrar();
	}

	
    function inicio()
	{
		$id = (int) @$_POST['id'] ;
		$this->procesarUnAlumno(@$_POST['id']);
	}

	function deleteCorrec()
	{
		$deleteCorre=$GLOBALS['m_Correcciones']->eliminarCorreccion(@$_POST['idTramite']);
		@$_POST['id'] = null;
		if (@$deleteCorre) 
		{		
			$_SESSION['vsMsj'] = $deleteCorre;
			$this->inicioCorrecciones();			
		}else{
			$_SESSION['vsMsj'] = "Falló al Eliminar el proceso de corrección intentelo de nuevo";
			$this->inicioCorrecciones();
		}	
	}

	function autorizarCo($e,$idTram)
	{
		switch ($e) {
			case '1':
				$estado = "Aprobado";
				break;
			case '2':
				$estado = "Rechazado";
				break;
			case '3':
				$estado = "En proceso";
				break;	
		}
		$okCo=$GLOBALS['m_Correcciones']->autorizarCo($estado, $idTram);
		@$_POST['id'] = null;
		if (@$okCo) 
		{		
			$_SESSION['vsMsj'] = $okCo;
			$this->inicioCorrecciones();			
		}else{
			$_SESSION['vsMsj'] = "Falló al Actualizar el estado de corrección intentelo de nuevo";
			$this->inicioCorrecciones();
		}	
	}

	function procesarUnAlumno($id)
	{
		//Headder
		require ($GLOBALS['r'].'header.php'); 
		//Datos que llevara la vista
		
		$in = str_replace("-","",@$_POST['id']);
		$verAlumno=$GLOBALS['m_Procesos']->mostrarUno($in);
		//si la fila viene vacia $verAlumno=null para no mostrar la tabla sola en la vista	
		//VALIDO EL CARNET
		if (@$in!=null) {
			$ve=$GLOBALS['m_Procesos']->validoCarnet(@$in);
			if ($ve==0) {
				$_SESSION['vsMsj'] = "Numero de carnet: <strong>". @$_POST['id'] ."</strong> no encontrado...";
			}
		}else{
			$_SESSION['vsMsj'] = "<br>Ingrese un numero de carnet de estudiante en el buscador a su <br>
			<<-----  izquierda";
		}
		
		if($verAlumno->num_rows<1)
		{
			$verAlumno = null;
		}
		//Llamada a la vista
		require_once ($GLOBALS['r']."v_InicioProceso.php");
		//footer
		require ($GLOBALS['r'].'footer.php');
	}


}





 ?>



