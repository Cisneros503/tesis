<?php

/**
 Trabajo de graduacion UTEC
 */

class c_Diferidos
{


	public $msj = null;

	function swDiferido(){
		switch (@$_REQUEST['ev']) 
		{   //EVENTOS
			case 'listar':
				
				$this->mostrar();
				break;

			case 'formInsertar':
				$this->formInsert();
				break;
			case 'insertar':
				$this->insertar();
				break;
			case 'formEditar':
				$this->mostrarUno();
				break;
			case 'editar':
				$this->editar();
				break;
			
			case 'eliminar':
				$this->eliminarDiferido();
				break;

			default:
			
				$this->mostrar();
				//echo "Evento no encontrado";					
				break;	
		}

	}


	function formEditar()
	{
		echo "ev: " . $_POST['ev'];
		echo "<br>";
		echo "id: " . $_POST['id'];
		echo "<br>";
		//var_dump($_POST['carne']);
		
		$this->mostrar();
	}

	function mostrar()
	{
		//Headder
		require ($GLOBALS['r'].'header.php'); 
		//Datos que llevara la vista
		$verDiferidos=$GLOBALS['m_Diferidos']->mostrarTodos();
		//Llamada a la vista
		require_once ($GLOBALS['r']."v_ListaTest.php");
		//footer
		require ($GLOBALS['r'].'footer.php');
	}

	function formInsert()
	{
		//Headder
		require ($GLOBALS['r'].'header.php'); 
		if (isset($_SESSION['vsMsj'])) {

			//echo mysqli_errno($_SESSION['vsMsj']);
			echo ' <br>
			<div class="container-fluid">
			  <div class="alert alert-success alert-dismissible">
			    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			    <strong>Aviso!</strong> '. $_SESSION['vsMsj'] .'.
			  </div>
			</div>' ;
		}
		unset($_SESSION['vsMsj']);
		//Llamada a la vista
		//$verDiferidos=$GLOBALS['m_Diferidos']->mostrarTodos();
		$verDifSecci=$GLOBALS['m_Diferidos']->mostrarSecciones();
		$verDifEsc=$GLOBALS['m_Diferidos']->mostrarEscuelas();
		$verDifEva=$GLOBALS['m_Diferidos']->mostrarEvaluaciones();
		$verDifCic=$GLOBALS['m_Diferidos']->mostrarCiclos();
		$verDifDoc=$GLOBALS['m_Diferidos']->mostrarDocentes();
		$verDifAlu=$GLOBALS['m_Diferidos']->mostrarAlumnos();
		require_once ($GLOBALS['r']."pages/examples/v_insertarDiferidos.php");
		//footer
		require ($GLOBALS['r'].'footer.php');
	}

	function mostrarUno()
	{
		//Headder
		require ($GLOBALS['r'].'header.php'); 
		//Datos que llevara la vista
		$verDiferidos=$GLOBALS['m_Diferidos']->mostrarUno($_POST['id']);
		$verDifCarre=$GLOBALS['m_Diferidos']->mostrarCarreras($_POST['id']);
		$verDifSecci=$GLOBALS['m_Diferidos']->mostrarSecciones(); 
		$verDifEsc=$GLOBALS['m_Diferidos']->mostrarEscuelas();
		$verDifEva=$GLOBALS['m_Diferidos']->mostrarEvaluaciones();
		$verDifCic=$GLOBALS['m_Diferidos']->mostrarCiclos();
		$verDifDoc=$GLOBALS['m_Diferidos']->mostrarDocentes();
		//$verDifDoc=$GLOBALS['m_Tramites2']->mostrarDocentes();
		$verDifAlu=$GLOBALS['m_Diferidos']->mostrarAlumnos();

		$verDifDocIn=$GLOBALS['m_Diferidos']->mostrarDocentesInac();
		$verDifAluIn=$GLOBALS['m_Diferidos']->mostrarAlumnosInac();
		$verDifCicIn=$GLOBALS['m_Diferidos']->mostrarCiclosInac();

		//Llamada a la vista
		require_once ($GLOBALS['r']."pages/examples/v_modificarDiferido.php");
		//footer
		require ($GLOBALS['r'].'footer.php');
	}

	function insertar()
	{
		 
		 //#######VALIDAR CAMPOS DEL LADO DEL SERVER  <<<<<------------####
		if (isset($_POST['cbx_carrera'])AND$_POST['cbx_materia']AND$_POST['listSeccion']AND$_POST['listEvaluacion']AND$_POST['listTipo']AND$_POST['fechTramite']AND$_POST['listCiclo']AND$_POST['listDocente']AND$_POST['listCarnet']) 


		{
			$insert = $GLOBALS['m_Diferidos']->insertar($_POST['cbx_carrera'],$_POST['cbx_materia'],$_POST['listSeccion'],$_POST['listEvaluacion'],$_POST['listDocente'],$_POST['listCiclo'],$_POST['listTipo'],$_POST['fechTramite'],$_POST['listCarnet']);

			if (@$insert) {
				$_SESSION['vsMsj'] = $insert;
			}
		}else{
			$_SESSION['vsMsj'] = "Por favor, complete todos los campos";
		}
		//echo mysqli_errno($insert);
		$this->formInsert();
		//Llamada a la vista
		//require_once ($GLOBALS['r']."pages/examples/v_insertarDocente.php");
		//footer
		//require ($GLOBALS['r'].'footer.php');

	}
	
	function editar()
	{
		/* // ESTE CODIGO COMENTARIADO ES PARA VALIDAR QUE LLEGAN LOS DATOS
		echo "ev: " . $_POST['ev'];
		echo "<br>";
		echo "carne: " . $_POST['txtCarne'];
		echo "<br>";
		echo "carne: " . $_POST['txtNombre'];
		echo "<br>";
		echo "carne: " . $_POST['txtApellido'];
		echo "<br>";
		echo "carne: " . $_POST['txtEmail'];
		echo "<br>";
		*/
		 //#######VALIDAR CAMPOS DEL LADO DEL SERVER  <<<<<------------####
		if ($_POST['txtId']) 
		{
			$edit = $GLOBALS['m_Diferidos']->modificarDiferido($_POST['txtId'],$_POST['cbx_carrera'],$_POST['cbx_materia'],$_POST['listSeccion'],$_POST['listEvaluacion'],$_POST['listDocente'],$_POST['listCiclo'],$_POST['listTipo'],$_POST['fechTramite'],$_POST['listAlumno'],$_POST['listEstado']);

			if (@$edit) {
				$_SESSION['vsMsj'] = $edit;
			}
		}else{
			$_SESSION['vsMsj'] = "Por favor, complete todos los campos...
			 " .  "<br>intente nuevamente.";
		}
		//$this->mostrarUnDocente();
		require ($GLOBALS['r'].'header.php');
		
		$verEvaluaciones=$GLOBALS['m_Diferidos']->mostrarTodos();
		//Llamada a la vista
		require_once ($GLOBALS['r']."v_ListaTest.php");
		require ($GLOBALS['r'].'footer.php');
	}

		
		function eliminarDiferido(){

		
		$verDiferidos=$GLOBALS['m_Diferidos']->eliminarDiferido($_POST['id']);

		$this->mostrar();
	}

}


?>