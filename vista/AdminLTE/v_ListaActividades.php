

<br>
<!-- TABLE: LATEST ORDERS -->
		<div class="container">
          <div class="box box-info">
            <div class="box-header with-border">
              <center><b><h1 class="">Actividades Registradas</h1></b></center>
               <form action="index.php?c=actividad2" method="post"> 
                 <center> <button name="ev" type="submit" value="insertar"><span class="glyphicon glyphicon-plus"></br>Ingresar</br>Actividades</button> </center>
                </form>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              
              </div>
            </div>



            
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive table-striped">

                

                <table id="example1" class="table no-margin table-bordered table-striped" >
                  <thead>
                  <tr>
                    <th style="text-align: center">Tramite</th>
                    <th style="text-align: center">Evaluacion</th>             
                    <th style="text-align: center">Carrera</th>
                    <th style="text-align: center">Materia</th>                 
                    <th style="text-align: center" >Seccion</th>
                    <th style="text-align: center">Ciclo</th>
                     <th style="text-align: center">Docente</th>
                     <th style="text-align: center">Casos Tramitados</th>
                        <th>Estado</th>
                      <th colspan="3" style="text-align: center">Acciones</th>


                  </tr>
                  </thead>
                  <tbody>
                  <?php
                  	$activos=0;
                  	$inactivos=0;
                    while($rows=$verTramites->fetch_array())
        				    {
                    echo "<tr>";

                   
                          
                      echo ' <form action="index.php?c=actividad2" method="post"> ';
        				      
        				      echo "  <td>".$rows['ID_tramite']."</td>";

                        echo ' <input type="hidden" name="id" value="' . $rows['ID_tramite'] . ' "> ';

        				      echo "  <td>".$rows['evaluacion']."</td>";
        				      echo "  <td>".$rows['nombre_carrera']."</td>";


        				     /* if ($rows['estado'] == 1) {
        				        echo "<td><span class='label label-success'>Activo</span></td>";
        				        $activos=$activos+1;
        				      }else{
        				        echo "<td><span class='label label-danger'>Inactivo</span></td>";
        				        $inactivos=$inactivos+1;
        				      }*/

        				      //echo "  <td>".."</td>";
        				      echo "  <td>".$rows['nombre_materia']."</td>";
                      echo "  <td>".$rows['seccion']."</td>";
                      echo "  <td>".$rows['nombre_ciclo']."</td>";
                      echo "  <td>".utf8_encode($rows['docente_nombres'])."</td>";
                      echo "  <td>".$rows['conteo']."</td>";


                      if ($rows['estado'] == 1) {
                        echo "<td><span class='label label-success'>Activo</span></td>";
                        $activos=$activos+1;
                      }else{
                        echo "<td><span class='label label-danger'>Inactivo</span></td>";
                        $inactivos=$inactivos+1;
                      }

        				      
                       echo "  <td>".' <button name="ev" type="submit" value="formEditar"><span class="glyphicon glyphicon-edit"></br>Editar</button> '."</td>";

                      echo "  <td>".' <button name="ev" type="submit" value="verDetalle">
                      <span class="glyphicon glyphicon-th-list"></br>Detalle</button>'."</td>";

                      echo "  <td>".' <button name="ev" type="submit" value="eliminar">
                      <span class="glyphicon glyphicon-off"></br>Deshabilitar</button> '."</td>";
                      echo "</form>";

        				      echo "</tr>"; 

        				    }
                  ?>                 
                  </tbody>
                </table>
              </div>
              <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
          
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->

<div class="">
	<div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-green"><i class="ion ion-ios-people-outline"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">Activos</span>
          <span class="info-box-number">
          <?php echo $activos;?> 
          <small> Actividades</small>
          </span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>

    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-red"><i class="ion ion-ios-people-outline"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">Inactivos</span>
          <span class="info-box-number">
          <?php echo $inactivos;?> 
          <small> Actividades</small>
          </span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
</div>
</div>