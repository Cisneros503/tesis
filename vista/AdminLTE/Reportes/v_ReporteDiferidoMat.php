<script<script src="jquery.js"></script>
<div class="container">
   <div class="container-fluid" width="80%">
      <div class="row" align="center">
         <!-- ## ACÁ PORDREMOS UBICAR EL CONTENIDO CON CSS y BOOTSTRAP ## -->
         <br>
         <!-- /.login-logo -->
         <div class="login-box-body "  align="center">
            <h1 class="login-box-msg">Conteo de Diferidos por  Materias</h1>
            <form id="form" name="form" action="index.php?c=reporteDif2" method="post" class="container-fluid">
               <div><strong>ELEGIR FILTRO</strong></div>
               <div class="radio">
                  <input type="radio" name="reporte" onclick="document.form.fechaIni.disabled = true; document.form.ver2.disabled = true; document.form.fechaFin.disabled = true; document.form.ver1.disabled = false; listCiclo.disabled = !this.checked;"/>Conteo de Diferidos por Ciclo
               </div>
               <div class="radio">
                  <input type="radio" name="reporte" onclick="document.form.fechaIni.disabled = false; document.form.ver2.disabled = false; document.form.fechaFin.disabled = false; document.form.ver1.disabled = true; listCiclo.disabled = this.checked;" />Conteo de Diferidos por Fechas
               </div>
  
               <div class="form-group has-feedback">
                  <label class="col-lg-2 control-label">Ciclo</label>
                  <select name="listCiclo" id="listCiclo" class="form-control" disabled>
                     <option value="0">Selección de Ciclo:</option>
                     <?php       
                        while ($row=$verCiclos->fetch_array()) {
                                
                          echo '<option value="'.$row['ID_ciclo'].'">'.$row['nombre_ciclo'].'</option>';
                                  
                        }
                        
                        ?> 
                  </select>
               </div>
               <div class="form-group has-feedback">
                  <label class="col-lg-2 control-label">Fecha Inicio</label>
                  <input type="date" class="form-control" placeholder="Fecha Inicio" name="fechaIni" id="fechaIni" disabled>
                  </select>
               </div>
               <div class="form-group has-feedback">
                  <label class="col-lg-2 control-label">Fecha Fin</label>
                  <input type="date" class="form-control" placeholder="Fecha Fin" name="fechaFin" id="fechaFin" disabled>
                  </select>
               </div>
               <!-- /.col -->
               <div class="col-xs-4">
                  <button type="submit" id="ver1" name="ev" value="verReporte1" class="btn btn-primary btn-block btn-flat" disabled>Reporte por Ciclo</button>
               </div>
               <div class="col-xs-4">
                  <button type="submit" id="ver2" name="ev" value="verReporte1" class="btn btn-primary btn-block btn-flat" disabled>Reporte por Fechas</button>
               </div>
               <!-- /.col -->
         </div>
         </form>
         <!-- /.social-auth-links -->
      </div>
      <!-- /.login-box-body -->
   </div>
   <!-- /.login-box -->
</div>