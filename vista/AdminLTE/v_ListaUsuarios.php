

<br>
<!-- TABLE: LATEST ORDERS -->
		<div class="container">
          <div class="box box-info">
            <div class="box-header with-border">
              <center><b><h3 class=>Usuarios Registrados</h3></b></center>

              <?php   
                  if ($_SESSION['vsValidacion'] == 1){
                    echo '
                      <form action="index.php?c=user" method="post"> 
                       <center> <button name="ev" type="submit" value="formInsertar">Ingresar Nuevo Usuario</button> </center>
                      </form>
                          ';
                  }
              ?>

                
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget=""><i class="fa fa-times"></i></button>
              </div>
            </div>


            
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive table-striped">
                
                <div class="form-group">
                 <input type="text" class="form-control pull-right" style="width:20%" id="search" placeholder="Digite lo que desea buscar...">
                </div>
                <table  class="table no-margin" align="center" id="mytable">
                  <thead>
                  <tr>             
                    <th>Nombres</th>
                    <th>Apellidos</th>
                    <th>Correo</th>
                    <th>Usuario</th>
                    <th>Fecha de Registro</th>
                    <!--<th>Estado</th>-->
                     <?php   
                 if ($_SESSION['vsValidacion'] == 1){
                        echo "<th>Opciones</th>";
                      }
                    ?>
                  </tr>
                  </thead>
                  <tbody>
                  <?php
                  	$activos=0;
                  	$inactivos=0;
                    while($rows=$verUsuarios->fetch_array())
        				    {
        				      echo "<tr>";
                      echo ' <form action="index.php?c=user" method="post"> ';
                      echo ' <input type="hidden" name="id" value="' . $rows['ID_usuario'] . ' "> ';
        				      echo "  <td>".$rows['nombre']."</td>";
                      echo "  <td>".$rows['apellido']."</td>";
                      echo "  <td>".$rows['correo']."</td>";
                      echo "  <td>".$rows['usuario']."</td>";
                      echo "  <td>".$rows['fecha_creacion']."</td>";
        				      /*
                      if ($rows['estado'] == 1) {
        				        echo "<td><span class='label label-success'>Activo</span></td>";
        				        $activos=$activos+1;
        				      }else{
        				        echo "<td><span class='label label-danger'>Inactivo</span></td>";
        				        $inactivos=$inactivos+1;
        				      }                 
                      */
                      //OPCIONES
                            if ($_SESSION['vsValidacion'] == 1) {
                        
                        echo '  <td class="icon">'.' <button  name="ev" type="submit" value="formEditar"><span class="glyphicon glyphicon-pencil text-primary"
></button>&nbsp;&nbsp;&nbsp;&nbsp;<button name="ev" type="submit" value="eliminar"><span class="glyphicon glyphicon-remove text-danger"
></button> '."</td>";
                        
                        echo "</form>";                
                      }
        				    }
                  ?>                 
                  </tbody>
                </table>


              </div>
              <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
          
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->



        

        </div>


           <script src="jquery.min.js"></script>
<script type="text/javascript">
 // Write on keyup event of keyword input element
 $(document).ready(function(){
 $("#search").keyup(function(){
 _this = this;
 // Show only matching TR, hide rest of them
 $.each($("#mytable tbody tr"), function() {
 if($(this).text().toLowerCase().indexOf($(_this).val().toLowerCase()) === -1)
 $(this).hide();
 else
 $(this).show();
 });
 });
});
</script>