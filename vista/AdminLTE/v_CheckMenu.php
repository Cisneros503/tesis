<br>
<div class="row">  
  <div class="col-md-3"> <CENTER><h1> &nbsp ASIGNAR MENÚ</h1> </CENTER> </div>
<div class="container-fluid col-md-6" >
    <!-- TO DO List -->
          <div  class="box box-primary">
            <div class="box-header">
              <i class="ion ion-clipboard"></i>

              <h3 class="box-title">Seleccione las opciones de menu del nuevo usuario...</h3>
              <!--
              <div class="box-tools pull-right">
                <ul class="pagination pagination-sm inline">
                  <li><a href="#">&laquo;</a></li>
                  <li><a href="#">1</a></li>
                  <li><a href="#">2</a></li>
                  <li><a href="#">3</a></li>
                  <li><a href="#">&raquo;</a></li>
                </ul>
              </div>
            -->
            </div>
            <!-- /.box-header -->
            <form action="index.php?c=user" method="post">
            <div class="box-body">

              <!-- See dist/js/pages/dashboard.js to activate the todoList plugin -->
              <ul class="todo-list">
                
<?php

  while($row=$verOpcionesMenu->fetch_array())
    {
      opMenu($row['id_opcion'],$row['opcion']);
    }
  
  
  function opMenu($id, $nomOpc)
  {
    echo 
        '
          <li>
            <!-- drag handle -->
            <span class="handle">
                  <i class="fa fa-ellipsis-v"></i>
                  <i class="fa fa-ellipsis-v"></i>
                </span>
            <!-- checkbox -->
            <input type="checkbox" name="' . $id .'" value="1">
            <!-- todo text -->
            <span class="text"> ' . $nomOpc . ' </span>
            <!-- Emphasis label -->
             

            <div class="tools">
              <small class="label label-success"><i class="fa fa-clock-o"></i> Disponible</small>  
              <i class="fa fa-edit"></i>
            </div>

          </li>
        ';
  }
?>

                



              </ul>
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix no-border">
              <button type="submit"name="ev" value="insertarMenu" class="btn btn-default pull-right"><i class="fa fa-plus"></i> Asignar</button>

            </div>
          </form>
          </div>
          <!-- /.box -->

</div>
<div class="col-md-3"></div>
</div>