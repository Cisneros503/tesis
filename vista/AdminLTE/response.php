
<?php
	//include connection file 
	$conn = mysqli_connect("localhost", "root", "", "bd_utec");
	 
	// initilize all variable
	$params = $columns = $totalRecords = $data = array();

	$params = $_REQUEST;

	//define index of column
	$columns = array( 
		0 =>'IDTRAMITE',
		1 =>'CARRERA', 
		2 => 'ALUMNO',
		3 => 'MATERIA',
		4 => 'ESCUELA',
		5 => 'SECCION',
		6 => 'DOCENTE',
		7 => 'EVALUACION',
		8 => 'PERIODO',
		9 => 'FECHAREG',
		10=> 'FECHASOL',
		11=> 'CICLO',
		12=> 'ESTADO'
	);

	$where = $sqlTot = $sqlRec = "";

	// check search value exist
	if( !empty($params['search']['value']) ) {   
		$where .=" WHERE ";
		$where .=" ( ALUMNO LIKE '".$params['search']['value']."%' ";    
		$where .=" OR IDTRAMITE LIKE '".$params['search']['value']."%' ";
		$where .=" OR CICLO LIKE '".$params['search']['value']."%' ";
		$where .=" OR ESTADO LIKE '".$params['search']['value']."%' )";
	}

	// getting total number records without any search
	$sql = "SELECT * FROM vw_diferidostodos";
	$sqlTot .= $sql;
	$sqlRec .= $sql;
	//concatenate search sql if value exist
	if(isset($where) && $where != '') {

		$sqlTot .= $where;
		$sqlRec .= $where;
	}


 	$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";

	$queryTot = mysqli_query($conn, $sqlTot) or die("database error:". mysqli_error($conn));


	$totalRecords = mysqli_num_rows($queryTot);

	$queryRecords = mysqli_query($conn, $sqlRec) or die("error to fetch employees data");

	//iterate on results row and create new index array of data
	while( $row = mysqli_fetch_array($queryRecords) ) { 

		$nestedData = array();
		$nestedData[] = $row['IDTRAMITE'];
		$nestedData[] = utf8_decode($row['CARRERA']);
		$nestedData[] = utf8_decode($row['ALUMNO']);
		$nestedData[] = utf8_decode($row['MATERIA']);
		$nestedData[] = utf8_decode($row['ESCUELA']);
		$nestedData[] = utf8_decode($row['SECCION']);
		$nestedData[] = utf8_decode($row['DOCENTE']);
		$nestedData[] = utf8_decode($row['EVALUACION']);
		$nestedData[] = $row['PERIODO'];
		$nestedData[] = $row['FECHAREG'];
		$nestedData[] = $row['FECHASOL'];
		$nestedData[] = $row['CICLO'];
		$nestedData[] = ($row['ESTADO'] == 'Activo' ? '<p  class="bg-success">'.$row['ESTADO'].'</p>':'<p  class="bg-danger">'.$row['ESTADO'].'</p>');
		$nestedData[] = '<td><form action="index.php?c=diferido" method="post"><button name="ev" type="submit" value="formEditar"><span class="glyphicon glyphicon-edit text-primary"></span></button><button name="ev" type="submit" value="eliminar"><span class="glyphicon glyphicon-remove text-danger"></span></button><input type="hidden" name="id" value="'.$row['IDTRAMITE'].'"></form></td>';
		$data[] = $nestedData;
	}	

	$json_data = array(
			"draw"            => intval( $params['draw'] ),   
			"recordsTotal"    => intval( $totalRecords ),  
			"recordsFiltered" => intval($totalRecords),
			"data"            => $data   // total data array
			);

	echo json_encode($json_data);  // send data as json format
?>
	