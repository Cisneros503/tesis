<?php 
//$rr2 = "../";

if(!isset($_SESSION['vsIdUsuario']))
  {
    //header('Location: ../index.php');
   }else{
    
   }
?>


<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Sistema UTEC</title>

  <!-- ############ PENDIENTE EL ICONO DE LA PESTAÑA DEL NAVEGADOR ##########  -->




  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo $GLOBALS['r'];?>bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo $GLOBALS['r'];?>bower_components/font-awesome/css/font-awesome.min.css">
  <!-- DATATABLES-->
  <link rel="stylesheet" type="text/css" href="<?php echo $GLOBALS['r'];?>bower_components/datatables.net-bs/css/datatables.bootstrap.css">

  <link rel="stylesheet" type="text/css" href="<?php echo $GLOBALS['r'];?>bower_components/datatables.net-bs/css/datatables.bootstrap.min.css">

  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo $GLOBALS['r'];?>bower_components/Ionicons/css/ionicons.min.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="<?php echo $GLOBALS['r'];?>bower_components/jvectormap/jquery-jvectormap.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo $GLOBALS['r'];?>dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo $GLOBALS['r'];?>dist/css/skins/_all-skins.min.css">


<!--ONLINE LIBRARY DATATABLES-->

<link rel="stylesheet" type="text/css" href="<?php echo $GLOBALS['r'];?>bower_components/DataTables/datatables.css">

<link rel="stylesheet" type="text/css" href="<?php echo $GLOBALS['r'];?>bower_components/DataTables/datatables.min.css">

<link rel="stylesheet" type="text/css" href="<?php echo $GLOBALS['r'];?>bower_components/DataTables/DataTables-1.10.18/css/dataTables.bootstrap.css">

<link rel="stylesheet" type="text/css" href="<?php echo $GLOBALS['r'];?>bower_components/DataTables/DataTables-1.10.18/css/dataTables.bootstrap.min.css">

<link rel="stylesheet" type="text/css" href="<?php echo $GLOBALS['r'];?>bower_components/DataTables/DataTables-1.10.18/css/dataTables.bootstrap4.css">

<link rel="stylesheet" type="text/css" href="<?php echo $GLOBALS['r'];?>bower_components/DataTables/DataTables-1.10.18/css/dataTables.bootstrap4.min.css">

<link rel="stylesheet" type="text/css" href="<?php echo $GLOBALS['r'];?>bower_components/DataTables/DataTables-1.10.18/css/dataTables.foundation.css">

<link rel="stylesheet" type="text/css" href="<?php echo $GLOBALS['r'];?>bower_components/DataTables/DataTables-1.10.18/css/dataTables.foundation.min.css">

<link rel="stylesheet" type="text/css" href="<?php echo $GLOBALS['r'];?>bower_components/DataTables/DataTables-1.10.18/css/dataTables.jqueryui.css">

<link rel="stylesheet" type="text/css" href="<?php echo $GLOBALS['r'];?>bower_components/DataTables/dataTables.responsive.css">


<link rel="stylesheet" type="text/css" href="<?php echo $GLOBALS['r'];?>bower_components/DataTables/datatablesReports.min.css"/>




  <script src="menu.js"></script>

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>

<!--    INABILITAR BOTON REGRESAR DEL NAVEGADOR    -->
  <script type="text/javascript">
  function nobackbutton(){ 
     window.location.hash="no-back-button";
     window.location.hash="Again-No-back-button" //chrome
     window.onhashchange=function(){window.location.hash="no-back-button";}
  }
  </script>


<body class="hold-transition skin-green sidebar-mini" onload="nobackbutton();">
<div class="wrapper">

  <header class="main-header">

    <!-- Logo -->
    <a href="index.php" class="logo" style="background-color:  #800000">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>UTEC</b>!</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>U</b>-<b>Tec</b>nologica</span>
    </a>

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" style="background-color:  #800000 ">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button" style="hover{background-color: #800000;}">
        <span class="sr-only">Toggle navigation</span>
      </a>


      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu" >
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          


          <!-- Notifications: style can be found in dropdown.less -->
          <li class="dropdown notifications-menu" >
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-bell-o"></i>
              <span class="label label-warning"></span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">Bienvenido a la nueva plataforma!!</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                  <li>
                    <a href="#">
                      <i class="fa fa-warning text-yellow"></i> 
                      Sistema creado por:
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <center><i class="fa fa-users text-aqua"></i></center>
                      <center>
                         <br>
                      Jessica Interiano<br>
                      Jonathan Garcia <br>
                      German Cisneros
                      </center>
                    </a>
                  </li>
                  
                </ul>
              </li>
              <li class="footer"><a href="#">Proyecto de graduacion Ciclo 02-2018</a></li>
            </ul>
          </li>
          <!-- Tasks: style can be found in dropdown.less -->
          




          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu" >
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" >
              <img src="<?php echo $GLOBALS['r'];?>dist/img/logo.png" class="user-image" alt="User Image">
              <span class="hidden-xs"><?php echo $_SESSION['vsNombre']." ".$_SESSION['vsApellido'];?></span>
            </a>
            <ul class="dropdown-menu"  >
              <!-- User image -->
              <li class="user-header" style="background-color:  #800000">
<!--+++++++++++ IMG USR -->
                <img src="<?php echo $GLOBALS['r'];?>dist/img/logo.png" class="img-circle" alt="User Image">

                <p>
                  <?php echo $_SESSION['vsEmail'] ?>
                  <small>Perfil: <?php echo $_SESSION['vsNombre']." ".$_SESSION['vsApellido'] ?></small>
                </p>
              </li>
              <!-- Menu Body -->
              
              <!-- Menu Footer-->
              <li class="user-footer" style="background-color:  #353549">
                <div class="pull-left">
                
                </div>
                <div class="pull-right">
                  <a href="<?php echo $GLOBALS['ppal'];?>controlador/salir.php" class="btn btn-default btn-flat">Salir</a>
                </div>
              </li>
            </ul>
          </li>

          <!-- Control Sidebar Toggle Button -->
          
        </ul>
      </div>





    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar control-sidebar-dark"  style="/*background-color:  #800000*/">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo $GLOBALS['r'];?>dist/img/logo.png" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo $_SESSION['vsNombre']." ".$_SESSION['vsApellido'] ?></p>
          <?php if ($_SESSION['vsEstado']==1){
          echo "<a href='#'><i class='fa fa-circle text-success'></i> Activo</a>";
          }else{echo "<a href='#'><i class='fa fa-circle text-red'></i> Inactivo</a>";}
          ?>        
        </div>
      </div>
      
      <!-- search form -->
  <form action="index.php?c=procesos" method="post" class="sidebar-form">
        <div class="input-group">


    <!-- or set via JS -->


<script src="jquery.js"></script>
<script src="vista/AdminLTE/Inputmask/dist/jquery.inputmask.bundle.js"></script>



          <input type="text" name="id" id="busqueda" minlength="10" maxlength="12" class="form-control" placeholder="Ingrese el Carnet..." onkeypress="return valida(event)" >
          <!-- En esta funcion valido entrada numerica para el carné solo numeros sin guiones -->
            <script>
                function valida(e){
                    tecla = (document.all) ? e.keyCode : e.which;

                    //Tecla de retroceso para borrar, siempre la permite
                    if (tecla==8){
                        return true;
                    }
                        
                    // Patron de entrada, en este caso solo acepta numeros
                    patron =/[0-9]/;
                    tecla_final = String.fromCharCode(tecla);
                    return patron.test(tecla_final);
                }
            </script>

            <script type="text/javascript">
$("#busqueda").inputmask({"mask": "99-9999-9999"});
</script>
          <span class="input-group-btn">
                <button type="submit" name="ev" value="inicio" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>

      
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li id="menuPpal" class="header">MENU PRINCIPAL</li>
        <!--  MENU  
        <li class=" treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> 
            <span>Correccione de notas</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
          </a>
        -->
          <!--  SUBMENU  
          <ul class="treeview-menu">
            <li class="active"><a href="index.html"><i class="fa fa-circle-o"></i> Nuevo proceso</a></li>
            <li><a href="index2.html"><i class="fa fa-circle-o"></i> Ver diferidos</a></li>
          </ul>
        </li>
        -->

        

        <script type="text/javascript">
          //MENU CON JS
 /*         createElement()

          var scr = document.createElement(script);
          scr.setAttribute("src", "menu.js");
          document.head.appendchild(src);
*/
          //el atrib onload es para que no cargue el archivo hasta que ya esté listo
            //caso conteraio entregaría datos erroneos*/
          //src.onload = function(){

          //}

        </script>


        <?php
         //MENU CON PHP 
          require('menu.php');
          $me = new Menu();
          $me->mostrar_Menu();
          
        ?>

  
       </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <div class="container">
        
      </div>
    
    



