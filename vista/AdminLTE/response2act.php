
<?php
	//include connection file 
	$conn = mysqli_connect("localhost", "root", "", "bd_utec");
	 
	// initilize all variable
	$params = $columns = $totalRecords = $data = array();

	$params = $_REQUEST;



	//define index of column
	$columns = array( 
		0 =>'IDTRAMITE',
		1 =>'EVALUACION', 
		2 => 'CARRERA',
		3 => 'MATERIA',
		4 => 'SECCION',
		5 => 'CICLO',
		6 => 'DOCENTE',
		7 => 'CASOS',
		8 => 'ESTADO'
	);

	$where = $sqlTot = $sqlRec = "";

	// check search value exist
	if( !empty($params['search']['value']) ) {   
		$where .=" WHERE ";
		$where .=" ( DOCENTE LIKE '".$params['search']['value']."%' ";    
		$where .=" OR IDTRAMITE LIKE '".$params['search']['value']."%' ";
		$where .=" OR CICLO LIKE '".$params['search']['value']."%' ";
		$where .=" OR ESTADO LIKE '".$params['search']['value']."%' )";
	}

	// getting total number records without any search
	$sql = "SELECT * FROM vw_actividadestodos";
	$sqlTot .= $sql;
	$sqlRec .= $sql;
	//concatenate search sql if value exist
	if(isset($where) && $where != '') {

		$sqlTot .= $where;
		$sqlRec .= $where;
	}


 	$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";

	$queryTot = mysqli_query($conn, $sqlTot) or die("database error:". mysqli_error($conn));


	$totalRecords = mysqli_num_rows($queryTot);

	$queryRecords = mysqli_query($conn, $sqlRec) or die("error to fetch employees data");

	//iterate on results row and create new index array of data
	while( $row = mysqli_fetch_array($queryRecords) ) { 


		$nestedData = array();
		$nestedData[] = $row['IDTRAMITE'];
		$nestedData[] = utf8_decode($row['EVALUACION']);
		$nestedData[] = utf8_decode($row['CARRERA']);
		$nestedData[] = utf8_decode($row['MATERIA']);
		$nestedData[] = utf8_decode($row['SECCION']);
		$nestedData[] = $row['CICLO'];
		$nestedData[] = utf8_decode($row['DOCENTE']);
		$nestedData[] = $row['CASOS'];
		
		$nestedData[] = ($row['ESTADO'] == 'Activo' ? '<p  class="bg-success">'.$row['ESTADO'].'</p>':'<p  class="bg-danger">'.$row['ESTADO'].'</p>');

		$nestedData[] = '<td><form action="index.php?c=actividad2" method="post"><button name="ev" type="submit" value="formEditar"><span class="glyphicon glyphicon-edit text-primary"></span></button><button name="ev" type="submit" value="eliminar"><span class="glyphicon glyphicon-remove text-danger"></span></button><button name="ev" type="submit" value="verDetalle"><span class="glyphicon glyphicon-th-list"></br></button><input type="hidden" name="id" value="'.$row['IDTRAMITE'].'"></form></td>';
		$data[] = $nestedData;
	}	

	$json_data = array(
			"draw"            => intval( $params['draw'] ),   
			"recordsTotal"    => intval( $totalRecords ),  
			"recordsFiltered" => intval($totalRecords),
			"data"            => $data   // total data array
			);

	echo json_encode($json_data);  // send data as json format
?>
	