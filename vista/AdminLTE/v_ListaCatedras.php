

<br>
<!-- TABLE: LATEST ORDERS -->
		<div class="container">
          <div class="box box-info">
            <div class="box-header with-border">
              <center><b><h3 class="">Catedras Registradas</h3></b></center>
 

             
                   <?php   
                  if ($_SESSION['vsValidacion'] == 1){
                echo '<form action="index.php?c=catedra" method="post"> 
                 <center> <button name="ev" type="submit" value="insertar">Ingresar Catedra</button> </center>
                </form>';
            }
            ?>    

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget=""><i class="fa fa-times"></i></button>
              </div>
            </div>
         
            
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive table-striped">
                <div class="form-group">

                 <input type="text" class="form-control pull-right" style="width:20%" id="search" placeholder="Digite lo que desea buscar...">
                </div>

                <table  class="table no-margin" id="mytable">
                  <thead>
                  <tr>
                    <th>Codigo</th>             
                    <th>Catedra</th>  
                    <th>Escuela a la que pertenece</th>                    
                    <th>Estado</th>
                        <?php   
                    if ($_SESSION['vsValidacion'] == 1){
                        echo "<th colspan='2'>Opciones</th>";
                      }
                    ?>
                  </tr>
                  </thead>
                  <tbody>
                  <?php
                  	$activos=0;
                  	$inactivos=0;
                    while($rows=$verCatedras->fetch_array())
        				    {
        				      echo "<tr>";
                      echo ' <form action="index.php?c=catedra" method="post"> ';


        				      echo "  <td>".$rows['ID_catedra']."</td>";

                      echo ' <input type="hidden" name="id" value="' . $rows['ID_catedra'] . ' "> ';
        				      echo "  <td>".utf8_decode($rows['nombre_catedra'])."</td>";

                       echo "  <td>".utf8_decode($rows['nombre_escuela'])."</td>";
        				    
        				      if ($rows['estado'] == 1) {
        				        echo "<td><span class='label label-success'>Activo</span></td>";
        				        $activos=$activos+1;
        				      }else{
        				        echo "<td><span class='label label-danger'>Inactivo</span></td>";
        				        $inactivos=$inactivos+1;
        				      }

        				      //echo "  <td>".."</td>";
        	
                      

                      //OPCIONES
                      
                    if ($_SESSION['vsValidacion'] == 1) {
                        
                        echo '  <td class="icon">'.' <button  name="ev" type="submit" value="formEditar"><span class="glyphicon glyphicon-pencil text-primary"
></button>&nbsp;&nbsp;&nbsp;&nbsp;<button name="ev" type="submit" value="eliminar"><span class="glyphicon glyphicon-remove text-danger"
></button> '."</td>";


                      }



                        echo "</form>";

        				      echo "</tr>"; 


        				    }
                                 
                 echo "</tbody>";
               echo " </table>";

               
             
                ?> 
               
          



              </div>
              <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
          
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->

<div class="">
	<div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-green"><i class="ion ion-ios-people-outline"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">Activos</span>
          <span class="info-box-number">
          <?php echo $activos;?> 
          <small> Catedras</small>
          </span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>

    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-red"><i class="ion ion-ios-people-outline"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">Inactivos</span>
          <span class="info-box-number">
          <?php echo $inactivos;?> 
          <small> Catedras</small>
          </span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
</div>


        

        </div>


<script src="jquery.min.js"></script>
<script type="text/javascript">
 // Write on keyup event of keyword input element
 $(document).ready(function(){
 $("#search").keyup(function(){
 _this = this;
 // Show only matching TR, hide rest of them
 $.each($("#mytable tbody tr"), function() {
 if($(this).text().toLowerCase().indexOf($(_this).val().toLowerCase()) === -1)
 $(this).hide();
 else
 $(this).show();
 });
 });
});
</script>