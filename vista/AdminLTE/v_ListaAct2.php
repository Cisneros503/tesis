<div class="container">
   <div class="">
      <h1>Conteo de Actividades por Materias</h1>
      <!-- /.box-header -->
      <div class="box-body">
         <div class="table-responsive table-striped" style="width:70%">
            <table id="tblAct2" class="display compact"  class="stripe" style="width:100%" cellspacing="0" class="display">
               <thead>
                  <tr>
                     <th>Materia|Carrera</th>
                     <th>Conteo</th>
                  </tr>
               </thead>

            </table>
               <a href="index.php?c=reportesActMat"><span class="glyphicon glyphicon-arrow-left
">&nbsp;Regresar</span></a>
         </div>
      </div>
   </div>
</div>
