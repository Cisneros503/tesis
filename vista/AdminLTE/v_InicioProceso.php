<?php
if (isset($_SESSION['vsMsj'])) {

      //echo mysqli_errno($_SESSION['vsMsj']);
      echo ' <br>
      <div class="container-fluid">
        <div class="alert alert-success alert-dismissible">
          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
          <strong>AVISO!</strong> '. $_SESSION['vsMsj'] .'.
        </div>
      </div>' ;
    }
    $_SESSION['vsMsj'] = null;

?>

<br>
<!-- TABLE: LATEST ORDERS -->
		<div class="container">
      <div class="box box-success"
      <?php 
        if ($verAlumno==null)
          echo "hidden";
       ?>
      >

        <div class="box-header with-border">
              <center><b><h3 class="">Alumno:</h3></b></center>
               
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget=""><i class="fa fa-times"></i></button>
              </div>
            </div>

            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive table-striped">
                <table class="table no-margin">
                  <thead>
                  <tr>
                    <th>Carnet</th>             
                    <th>Nombres</th>
                    <th>Estado</th>
                    <th>Correo</th>
                  </tr>
                  </thead>
                  <tbody>
<?php  
  if ($verAlumno!=null) {
                  	$activos=0;
                  	$inactivos=0;
                    if($rows=$verAlumno->fetch_array())
        				    {
                          echo "<tr>";
                          
                      echo ' <form action="index.php?c=alumno" method="post"> ';
        				      
        				      echo "  <td>".$rows['carnet']."</td>";

                        echo ' <input type="hidden" name="id" value="' . $rows['carnet'] . ' "> ';

        				      echo "  <td>".$rows['alumno']."</td>";
  
        				      if ($rows['estado'] == 1) {
        				        echo "<td><span class='label label-success'>Activo</span></td>";
        				        $activos=$activos+1;
        				      }else{
        				        echo "<td><span class='label label-danger'>Inactivo</span></td>";
        				        $inactivos=$inactivos+1;
        				      }

        				      //echo "  <td>".."</td>";
        				      echo "  <td>".$rows['correo_alumno']."</td>";
        				      
                      echo "</form>";

        				      echo "</tr>"; 

        				    }
                }
                $verAlumno = null;

                echo '
                
                ';


                  ?>                 
                  
                  </tbody>
                </table>
              </div>
              <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
          
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
         


         
         <center><b><h3 class="">¿Que proceso desea realizar?</h3></b></center>
         <br>

         <form action="index.php?c=alumno" method="post">
          <div class="row" >  
        <div class="col-lg-2 ">
        </div>
        
        <!-- ./col -->
        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3>Nuevo Alumno</h3>

              <p>Crear Nuevo Alumno</p>
            </div>
            <div class="icon">
              <i class="fa fa-user-plus"></i>
            </div>
            
             
            <button type="submit"name="ev" value="insertar" class="small-box-footer">&nbsp Registrar Nuevo Alumno  &nbsp &nbsp <i class="fa fa-arrow-circle-right"></i></button>
          </div>
        </div>
      </form>
        <!-- ./col -->


        <form action="index.php?c=procesos" method="post">
        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-blue">
            <div class="inner">
              <h3>Correcciones</h3>

              <p>Correccion de notas</p>
            </div>
            <div class="icon">
              <i class="fa fa-check-square-o"></i>
            </div>
            <!--
            <a href="#" class="small-box-footer">
              Menu administrador <i class="fa fa-arrow-circle-right"></i>
            </a>
        -->
            <button type="submit"name="ev" value="frmInserTramite" class="small-box-footer">&nbsp Corrección de notas  &nbsp &nbsp <i class="fa fa-arrow-circle-right"></i></button>

          </div>
        </div>
      </form>
        <!-- ./col -->
        <div class="col-lg-2 ">
        </div>
      </div>




       <div class="row">
          
          <div class="col-lg-2 ">
        </div>


  <form action="index.php?c=actividad2" method="post">
        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>Actividades<sup style="font-size: 20px"></sup></h3>

              <p>Reporte de actividades</p>
            </div>
            <div class="icon">
              <i class="fa fa-bar-chart"></i>
            </div>
            
            <button type="submit"name="ev" value="insertar" class="small-box-footer">&nbsp Reporte de actividades  &nbsp &nbsp <i class="fa fa-arrow-circle-right"></i></button>

          </div>
        </div>
    </form>

        <!-- ./col -->
       <form action="index.php?c=diferido" method="post">
        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-orange">
            <div class="inner">
              <h3>Diferidos</h3>

              <p>Examenes Diferidos</p>
            </div>
            <div class="icon">
              <i class="fa fa-copy "></i>
            </div>

            <button type="submit"name="ev" value="insertar" class="small-box-footer">&nbsp Examenes Diferidos &nbsp &nbsp<i class="fa fa-arrow-circle-right"></i></button>

          </div>


        </div>
      </form>
        <!-- ./col -->

        <div class="col-lg-2 ">
        </div>

      </div>

    


</div>