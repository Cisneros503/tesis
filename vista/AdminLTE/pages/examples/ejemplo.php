
<div class="row" align="center">
<!-- ## ACÁ PORDREMOS UBICAR EL CONTENIDO CON CSS y BOOTSTRAP ## -->

<br>

<!-- /.login-logo -->
  <div class="login-box-body "  align="center">
    <h1 class="login-box-msg">Esta es una vista de ejemplo</h1>
    <p>
    Acá van a poder colocar el contenido (y modificarlo con css y bootstrap) de cada pagina simplemente llamandolo desde el controlador principal
    el cual por defecto mostrará el menú segun su rol, para poder acceder a cada controlador le mandamos un parametro para definir que vista mostraremos el codigo queda con sus comentarios.
    </p>

    <form action="../../../../index.php?acc=login" method="post"> 
      <div class="form-group has-feedback">
        <input type="text" class="form-control" placeholder="Email" name="txtUsr">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" placeholder="Password" name="txtPass">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-8">
          <div class="checkbox icheck">
            <label>
              <input type="checkbox"> Recordar Contraseña
            </label>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Ingresar</button>
        </div>
        <!-- /.col -->
      </div>
    </form>
    <!--
    <div class="social-auth-links text-center">
      <p>- OR -</p>
      <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign in using
        Facebook</a>
      <a href="#" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> Sign in using
        Google+</a>
    </div>
    -->
    <!-- /.social-auth-links -->

    <a href="#">I forgot my password</a><br>
    <a href="register.html" class="text-center">Register a new membership</a>

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->



