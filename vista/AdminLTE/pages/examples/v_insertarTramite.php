<?php
//$connect = mysqli_connect("localhost", "root", "", "bd_u");
$connect=$GLOBALS['m_Conexion']->Conectar();
  
  $query = "SELECT * FROM carrera_materia cm left join carreras c on cm.ID_carrera = c.ID_carrera WHERE cm.estado = 1 group by cm.ID_carrera";

  $resultado = mysqli_query($connect, $query);
?>


<?php
if (isset($_SESSION['vsMsj'])) {

      //echo mysqli_errno($_SESSION['vsMsj']);
      echo ' <br>
      <div class="container-fluid">
        <div class="alert alert-success alert-dismissible">
          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
          <strong>Aviso!</strong> '. @$_SESSION['vsMsj'] .'.
        </div>
      </div>' ;
      $_SESSION['vsMsj'] = null;
    }

?>




<div class="container">
<div class="container-fluid" width="80%">
<div class="row" align="left">
<!-- ## ACÁ PORDREMOS UBICAR EL CONTENIDO CON CSS y BOOTSTRAP ## -->

<br>

<!-- /.login-logo -->
<form action="index.php?c=procesos" method="post" >
  <div class="login-box-body row">
    <div><h1 class="login-box-msg">INSERTAR PROCESO DE CORRECCION</h1></div>

    <div class=" col-md-6"  align="center">   
     

      <style type="text/css">
        table {
          border-collapse: separate;
          border-spacing:  1px 11px;         
        }
      </style>

    <table width="80%" cellpadding="4" align="center">
      <tr >
        <td><label>Carnet </br> del alumno</label></td>
        <td>
          <div class="form-group has-feedback">
         <!-- <input minlength="10" maxlength="10" required="true" type="text" class="form-control" placeholder="Carné" name="txtCarne" onkeypress="return valida(event)" style="vertical-align: baseline;">
          <button type="button" name="add" id="add" data-toggle="modal" data-target="#add_data_Modal" class="btn btn-warning" style="vertical-align: baseline;">Agregar</button>-->

           <select id="txtCarne"  name="txtCarne" class="selectpicker1 form-control"  data-size="5" data-live-search="true"  >
                           <option value="">Seleccione un Alumno
                           </option>
                           <?php
                              $cnn=$GLOBALS['m_Conexion']->Conectar();
                              if (!$cnn->set_charset("utf8")) {//asignamos la codificación comprobando que no falle
                              die("Error cargando el conjunto de caracteres utf8");
                              }
                              $contador=0;
                              while($misdatos = $verDifAlu->fetch_array()){ $contador++;?>
                           <option value="<?php echo $misdatos["carnet"]; ?>" data-subtext="<?php echo $misdatos["carnet"]; ?>">
                              <?php echo utf8_decode($misdatos["alumno"]); ?>
                           </option>
                           <?php }?> 
                        </select>
          <span class="glyphicon  form-control-feedback"></span>
        </div>

     

      </td>
      </tr>

       <tr>
      <div class="form-group has-feedback">
        <td align="left"><label>Docente</label></td>
        <td><select required="true"  data-size="5"  name="listCarnDocen" class="selectpicker2 form-control" data-show-subtext="true" data-live-search="true">
         <option value="*">Seleccióne una opción</option>
             <?php        
                while ($row=$verDocentes->fetch_array()) {                
                    if (@$_POST['listCarnDocen']==$row['carnet_docente']) {
                        echo '<option value="'.$row['carnet_docente'].'" selected>'. utf8_decode($row['docente_nombres']). " * " .$row['carnet_docente'].'</option>';
                    }else{
                        echo '<option value="'.$row['carnet_docente'].'">'. utf8_decode($row['docente_nombres']). " * " .$row['carnet_docente'].'</option>';
                    }
              }
          ?>      
        </select></td>
      </div>

    </tr>
      <tr>
        <div class="form-group has-feedback">
        <div>
          <td><label>Carrera</label></td> 
          <td><select  name="cbx_carrera" id="cbx_carrera" class="form-control">
          <option value="0" required>Seleccionar Carrera</option>
          <?php while($row = $resultado->fetch_assoc()) { 
              if (@$_POST['cbx_carrera']!=null AND $_POST['cbx_carrera'] == $row['ID_carrera']) {

                echo '
                    <option value="'.$row['ID_carrera'].'" selected>'.utf8_decode($row['nombre_carrera']).'</option>
                  ';
                $_POST['cbx_carrera']=null;
              }else{
                  echo '
                    <option value="'.$row['ID_carrera'].'" selected>'.utf8_decode($row['nombre_carrera']).'</option>
                  ';
                }
            ?>

            
          <?php } ?>
        </select></td>
        </div>
        </div>
      </tr>
      
      <tr>
        <div class="form-group has-feedback">
        <div> 
          <td align="left"><label>Materia</label> </td>
          <td><select name="cbx_materia" id="cbx_materia" class="form-control" required>
            <?php
                if (@$_POST['cbx_materia']!=null) {
                    while ($row1 = $verMaterias->fetch_assoc()AND$_POST['cbx_materia']!=null) {
                          if ($_POST['cbx_materia']==$row1['ID_materia']) {
                              echo '<option value="'.$row1['ID_materia'].'" SELECTED>'.utf8_decode($row1["nombre_materia"]).'</option>';
                          }
                     }
                }else {
                  echo '
                    <option value="x">Seleccionar materia</option>
                  ';
                }
              
            ?>
            </select>
          </td></div>
        </div>
      </tr>
    <!--
      <div class="form-group has-feedback">
      <div>Selecciona Materia : <select name="cbx_materia" id="cbx_materia" class="form-control"></select></div>
      </div>
    -->
      
    <tr>
      <div class="form-group has-feedback">
        <td align="left"><label>Sección</label></td>
        <td><select required="true" name="listSecciones" class="form-control">
         <option value="x">Seleccionar una opción</option>
             <?php        
                while ($row=$verSecciones->fetch_array()) {                       
                  
             if (@$_POST['listSecciones']!=null AND $_POST['listSecciones'] == $row['ID_seccion']) {
                echo '
                    <option value="'.$row['ID_seccion'].'" selected>'.utf8_decode($row['seccion']).'</option>
                  ';
                $_POST['listSecciones']=null;
              }else{
                  echo '
                    <option value="'.$row['ID_seccion'].'">'.utf8_decode($row['seccion']).'</option>
                  ';
              }
          }

          ?>      
        </select></td>
      </div>
    </tr>
    <tr>
      <div class="form-group has-feedback">
        <td align="left"><label>Evaluación</label></td>
        <td><select required="true" name="listEvaluaciones" class="form-control">
         <option value="x">Seleccióne una opción</option>
             <?php        
                while ($row=$verEvaluaciones->fetch_array()) {

                  if (@$_POST['listEvaluaciones']!=null AND $_POST['listEvaluaciones'] == $row['ID_eval']) {
                     //@$_SESSION['ev'] = 0; 
                echo '<option value="'.$row[ID_eval].'" selected>'.utf8_decode($row[descripcion]).'</option>';
                $_POST['listEvaluaciones']=null;
              }else{
                  echo '<option value="'.$row[ID_eval].'">'.utf8_decode($row[descripcion]).'</option>';
                }


                }
             ?>      
        </select></td>
      </div>
    </tr>
   

    
    <tr>
      <div class="form-group has-feedback">
        <td align="left"><label>Ciclo</label></td>
        <td><select required="true" name="listIdCiclos" class="form-control">
         <option value="x">Seleccióne una opción</option>
             <?php        
                while ($row=$verCiclos->fetch_array()) {                       
               
                  if (@$_POST['listIdCiclos']==$row['ID_ciclo']) {
                        echo '<option value="'.$row['ID_ciclo'].'" selected >'.$row['nombre_ciclo'].'</option>';
                    }else{
                        echo '<option value="'.$row['ID_ciclo'].'">'.$row['nombre_ciclo'].'</option>';   
                    }
              }
          ?>      
        </select></td>
      </div>
    </tr>
    <tr>
      <div class="form-group">
        <td align="left"><label>Recepción </br>en la Escuela</label></td>
        <td><div class="input-group date">
          <div class="input-group-addon">
            
          </div>
          <input required="true" type="date" name="fRec" id="fRec" class="form-control pull-right" value="<?php  echo @$_POST['fRec']; ?>" >
        </div></td>
        <!-- /.input group -->
      </div>
    </tr>
   
    <tr>
      <div class="form-group">
        <td align="left"><label>Recepción </br> en Administración</label></td>

        <td><div class="input-group date">
          <div class="input-group-addon">
            
          </div>
          <input required="true" type="date" name="fSolicitud" id="fSolicitud" class="form-control pull-right" value="<?php
          if(@$_POST['fSolicitud'] != null){
            echo @$_POST['fSolicitud'];
          }   ?>" onblur="validarFec();">
        </div></td>
        <!-- /.input group -->
      </div>
    </tr>
    
       


      </table>
      <br><br>

    

    <br>
    <br>

</div>

<div class="col-md-6"  align="center"> 


    <table width="80%" cellpadding="4" align="center">
      <tr >
        <td><label>Nota actual</label></td>
        <td><div class="form-group has-feedback">
          <input required="true" min="0" max="10" type="number" step="0.01"  class="form-control" placeholder="Nota actual" name="txtNactual" value="<?php  echo @$_POST['txtNactual']; ?>">
          <span class="glyphicon form-control-feedback"></span>
        </div></td>
      </tr>

      <tr >
        <td><label>Nota corregida</label></td>
        <td><div class="form-group has-feedback">
          <input required="true" min="0" max="10" type="number" step="0.01" minlength="1" maxlength="4" class="form-control" placeholder="Nota corregida" name="txtNcorregida" value="<?php  echo @$_POST['txtNcorregida']; ?>">
          <span class="glyphicon form-control-feedback"></span>
        </div></td>
      </tr>

      <tr >
        <td><label>Motivo </br> Corrección</label></td>
        <td><div class="form-group has-feedback">
          <textarea required="true" type="text" rows="3" class="form-control" placeholder="Motivo Corrección" name="txtMotCorrec" value="<?php  echo @$_POST['txtMotCorrec']; ?>"></textarea>
          <span class="glyphicon form-control-feedback"></span>
        </div></td>
      </tr>

      
     
      

      <tr >
        <td><label>Observaciones</label></td>
        <td><div class="form-group has-feedback">
          <textarea required="true" type="text" rows="3" class="form-control" placeholder="Observaciones" name="txtObcerv" value="<?php  echo @$_POST['txtObcerv']; ?>"></textarea>
          <span class="glyphicon  form-control-feedback"></span>
        </div></td>
      </tr>
      <tr>
        <div class="form-group has-feedback">
                <td align="left"><label>Periodo</label></td>
                <td><select required="true" name="listIdPeriodos" class="form-control">
                 <option >Seleccióne una opción</option>
                    <?php
                      if ( @$_POST['listIdPeriodos']!=null AND @$_POST['listIdPeriodos']==0) {
                          echo '
                            <option value="0" selected >Periodo con Autorización</option>
                          ';
                      }elseif ( @$_POST['listIdPeriodos']!=null AND @$_POST['listIdPeriodos']==1) {
                        echo '
                            <option value="1" selected >Periodo Extraordinario</option>
                          ';
                      }
                    ?>
                    <option value="0">Periodo con Autorización</option>;   
                     <option value="1">Periodo Extraordinario</option>;   
                </select></td>
          </div>
        </tr>
      <tr >
        <td><label>Usuario</label></td>
        <td><div class="form-group has-feedback">
          <input readonly="true" type="text" class="form-control" value="
          <?php echo $_SESSION['vsNombre']." ".$_SESSION['vsApellido'] ?>
          " name="txtUsrRecibe">
          <span class="glyphicon  form-control-feedback"></span>
        </div></td>
      </tr>

      <tr >
        <td><label>Estado </br> Corrección</label></td>
        <td><div class="form-group has-feedback">
          <input readonly="true" type="text" class="form-control" value="En proceso" name="txtEstCorrec">
          <span class="glyphicon  form-control-feedback"></span>
        </div></td>
      </tr>

      <tr>
        <div class="form-group">
          
          <!-- /.input group -->
        </div>
      </tr>
    
    

      </table>
      <br>
      <div class="col-md-4"></div>
      <div class="col-md-8" >
          
          <button align="center" type="submit" name="ev" value="insertarTr" class="btn btn-primary btn-block btn-flat">Siguiente</button>
        </div>

    

    <br>

</div>

<div class="col-md-12 row">   
    <div class="col-md-12" align="center"><a href="index.php?c=procesos&ev=inicioCorrecciones">Ir a Correcciones</a></a></div>
  </div>
</form>
<!--<div class="col-md-6"></div>-->

</div>


<div class="col-md=4"><br></div>


</div>
</div>
</div>


<!-- ############################################################################## -->
<!-- ############################################################################## -->




<script type="text/javascript" src="./jquery/jquery-1.8.3.min.js" charset="UTF-8"></script>
<script type="text/javascript" src="./bootstrap/js/bootstrap.min.js"></script>

  

<!-- ############################################################################## -->
<!-- ############################################################################## -->


<!-- jQuery 3 -->
<script src="../../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="../../plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' /* optional */
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    });
  });
</script>
<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })

    })

    })
  })
</script>

<script type="text/javascript">
  var password = document.getElementById("psw")
  , confirm_password = document.getElementById("psw2");

function validatePassword(){
  if(password.value != confirm_password.value) {
    confirm_password.setCustomValidity("Contraseñas no coinciden");
  } else {
    confirm_password.setCustomValidity('');
  }
}

password.onchange = validatePassword;
confirm_password.onkeyup = validatePassword;

if(history.forward(1)){
history.replace(history.forward(1));
}
</script>

<script src="jquery.js"></script>

<!--Script para combo Box anidados CON ALUMNO, CARRERA Y MATERIA-->
<script>
    $(document).ready(function() {
        $("#txtCarne").change(function() {
            $('#cbx_materia').find('option').remove().end().append('<option value="whatever"></option>').val('whatever');
            $("#txtCarne option:selected").each(function() {
                carnet = $(this).val();
                $.post("includes/getMunicipio.php", {
                    carnet: carnet
                }, function(data) {
                    $("#cbx_carrera").html(data);
                });
            });
        })
    });
    $(document).ready(function() {
        $("#cbx_carrera").change(function() {
            $("#cbx_carrera option:selected").each(function() {
                ID_carrera = $(this).val();
                $.post("includes/getLocalidad.php", {
                    ID_carrera: ID_carrera
                }, function(data) {
                    $("#cbx_materia").html(data);
                });
            });
        })
    });
</script>
<!--Script para combo Box anidados-->   


<!--<script language="javascript">
      $(document).ready(function(){
        $("#cbx_carrera").change(function () {

          
          $("#cbx_carrera option:selected").each(function () {
            ID_carrera = $(this).val();
            $.post("includes/getMaterias.php", { ID_carrera: ID_carrera }, function(data){
              $("#cbx_materia").html(data);
            });            
          });
        })
      });
      
      $(document).ready(function(){
        $("#cbx_materia").change(function () {
          $("#cbx_materia option:selected").each(function () {
            id_materia = $(this).val();
                        
          });
        })
      });
</script>  --> 

<!-- *************************************AGREGARALUMNO**********************-->
<div id="add_data_Modal" class="modal fade">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;
            </button>
            <h4 class="modal-title">
               <strong>Agregar Alumno
               </strong>
            </h4>
         </div>
         <div class="modal-body">
            <form method="post" id="insert_form">
               <label>Carnet Alumno
               </label>
               <input required minlength="10" maxlength="10" type="text" name="carnetAlu" id="carnetAlu" class="form-control" onkeypress="return valida(event)" />
               <br />
               <label>Nombre del Alumno
               </label>
               <input required type="text" name="nombreAlu" id="nombreAlu" class="form-control"/>
               <br />
               <label>Correo del Alumno
               </label>
               <input required type="text" name="CorAlu" id="CorAlu" class="form-control"/>
               <br />
               <input type="submit" name="insert" id="insert" value="Insertar" class="btn btn-success" />
            </form>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar
            </button>
         </div>
      </div>
   </div>
</div>



<!--LIBRERIAS SELECT DE DOCENTES -->
<link rel='stylesheet prefetch' href='vista/AdminLTE/bootstrap-select-1.13.2/dist/css/bootstrap-select.min.css'>
<script src="vista/AdminLTE/bootstrap-select-1.13.2/dist/js/bootstrap-select.min.js"></script>
<script src="vista/AdminLTE/bootstrap-select-1.13.2/js/bootstrap-select.js"></script> 



<script type="text/javascript">
$('.selectpicker1').selectpicker({
    noneResultsText: '<strong>No existe el alumno {0}, Agregar un Alumno </strong><button type="button" name="add" id="add" data-toggle="modal" data-target="#add_data_Modal" class="btn btn-warning">Agregar</button>',
    liveSearchPlaceholder: 'Carnet o Nombre',
});
</script>




<script type="text/javascript">
$('.selectpicker2').selectpicker({
    noneResultsText: '<strong>No existe el Docente {0}, Agregar un Docente </strong><button type="button" name="add" id="add" data-toggle="modal" data-target="#add_data_Modal2" class="btn btn-warning">Agregar</button>',
    liveSearchPlaceholder: 'Codigo o Nombre',
});


</script>



<script type="text/javascript">
//VALIDACION PARA QUE FECHA SEA MAYOR
function validarFec(){

    var fini=document.getElementById("fRec").value;
    var ffin=document.getElementById("fSolicitud").value;
    //  console.log("fini:"+fini+"|");
    //console.log(ffin);
        if(fini === "" || fini > ffin){
          alert("Cambie la fecha, tiene que ser mayor a la de recepcion en la Escuela ");
          document.getElementById("fSolicitud").value = " ";
        }
        /*else {
           alert("fecha buena"+fini+"**"+ffin);
        }*/
}

</script>
<!--//FUNCION PARA VALIDAR FECHAS

  //https://www.lawebdelprogramador.com/codigo/JavaScript/2528-Determinar-si-una-fecha-es-superior-a-otra-fecha.html
-->
<!--
<script>
        /**
         * Funcion que dadas dos fechas, valida que la fecha final sea
         * superior a la fecha inicial.
         * Tiene que recibir las fechas en formato español dd/mm/yyyy
         * No valida que las fechas sean correctas
         * Devuelve 1 si es mayor
         *
         * Para validar si una fecha es correcta, utilizar la función:
         * http://www.lawebdelprogramador.com/codigo/JavaScript/1757-Validar_una_fecha.html
         */ 
        function validate_fechaMayorQue(fechaInicial,fechaFinal)
        {
            valuesStart=fechaInicial.split("/");
            valuesEnd=fechaFinal.split("/");
 
            // Verificamos que la fecha no sea posterior a la actual
            var dateStart=new Date(valuesStart[2],(valuesStart[1]-1),valuesStart[0]);
            var dateEnd=new Date(valuesEnd[2],(valuesEnd[1]-1),valuesEnd[0]);
            if(dateStart>=dateEnd)
            {
                return 0;
            }
            return 1;
        }
 
    /*    var fechaInicial=document.getElementById("fRec");
        var fechaFinal=document.getElementById("fSolicitud");
        if(validate_fechaMayorQue(fechaInicial,fechaFinal))
        {
            document.write("La fecha "+fechaFinal+" es superior a la fecha "+fechaInicial);
        }else{
            document.write("La fecha "+fechaFinal+" NO es superior a la fecha "+fechaInicial);
        }*/
    </script>
-->

<!-- *************************************AGREGARALUMNO**********************-->
<script>  
$(document).ready(function() {
    $('#insert_form').on("submit", function(event) {
        event.preventDefault();
        if ($('#carnetAlu').val() == "") {
            alert("Carnet es requerido");
        } else if ($('#nombreAlu').val() == '') {
            alert("Nombre es requerido");
        } else {
         // var x = "Mi nombre es X";
         // console.log(x);
         // alert("Hooola "+ x);

          var cAlum = document.getElementById("carnetAlu").value;
          var nAlum = document.getElementById("nombreAlu").value;
          var valorSelect = cAlum+"********"+nAlum;
         // document.getElementById("txtCarne").value = cAlum;
         // document.getElementById("txtCarne").placeholder = valorSelect;

          //vari = new Option("text","value","defaultSelected","selected");
          vari = new Option(valorSelect,cAlum, true, true);

          document.getElementById("txtCarne").options = vari;

         //document.getElementById("txtCarne").options[document.getElementById("txtCarne").selected].value =  nAlum;

          //nombre_formulario.nombre_select.options[index] = vari
          console.log(valorSelect);
          alert(valorSelect);

            $.ajax({
                url: "includes/insert.php",
                method: "POST",
                data: $('#insert_form').serialize(),
                success: function(data) {
                    $('#insert_form')[0].reset();
                    $('#add_data_Modal').modal('hide');
                }
            });
        }
    });
    $(document).on('click', '.view_data', function() {
        //$('#dataModal').modal();
        var employee_id = $(this).attr("id");
        $.ajax({
            url: "select.php",
            method: "POST",
            data: {
                employee_id: employee_id
            },
            success: function(data) {
                $('#employee_detail').html(data);
                $('#dataModal').modal('show');
            }
        });
    });
});
</script>

<script type="text/javascript">
document.getElementById("insert_form").onsubmit = function() {
    location.reload(true);
}
</script>






<!-- ***********************************AGREGARDOCENTE**********************-->
<div id="add_data_Modal2" class="modal fade">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;
            </button>
            <h4 class="modal-title">
               <strong>Agregar Docente
               </strong>
            </h4>
         </div>
         <div class="modal-body">
            <form method="post" id="insert_form2">
               <label>Codigo Docente
               </label>
               <input type="text" maxlength="10" minlength="1" name="carnetDoc" id="carnetDoc" class="form-control" onkeypress="return valida(event)"/>
               <br />
               <label>Nombre del Docente
               </label>
               <input type="text" name="nombreDoc" id="nombreDoc" class="form-control"/>
               <br />
               <label>Correo del Docente
               </label>
               <input type="text" name="CorDoc" id="CorDoc" class="form-control"/>
               <br />
               <input type="submit" name="insert" id="insert" vDoce="Insertar" class="btn btn-success" />
            </form>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar
            </button>
         </div>
      </div>
   </div>
</div>
<!-- ***********************************AGREGARDOCENTE**********************-->
<script>  
$(document).ready(function() {
    $('#insert_form2').on("submit", function(event) {
        event.preventDefault();
        if ($('#carnetDoc').val() == "") {
            alert("Carnet es requerido");
        } else if ($('#nombreDoc').val() == '') {
            alert("Nombre es requerido");
        } else {
            $.ajax({
                url: "includes/insert2.php",
                method: "POST",
                data: $('#insert_form2').serialize(),
                success: function(data) {
                    $('#insert_form2')[0].reset();
                    $('#add_data_Modal2').modal('hide');
                }
            });
        }
    });
    $(document).on('click', '.view_data', function() {
        //$('#dataModal').modal();
        var employee_id = $(this).attr("id");
        $.ajax({
            url: "select.php",
            method: "POST",
            data: {
                employee_id: employee_id
            },
            success: function(data) {
                $('#employee_detail').html(data);
                $('#dataModal').modal('show');
            }
        });
    });
});
</script>

<script type="text/javascript">
document.getElementById("insert_form2").onsubmit = function() {
    location.reload(true);
}
</script>







