<?php
   //$connect = mysqli_connect("localhost", "root", "", "bd_utec_test");
     
     $connect=$GLOBALS['m_Conexion']->Conectar();
     $query = "SELECT * FROM carrera_materia cm left join carreras c on cm.ID_carrera = c.ID_carrera WHERE cm.estado = 1  group by cm.ID_carrera";
   
     $resultado = mysqli_query($connect, $query);
?>
<div class="container">
   <div class="container-fluid" width="80%">
      <div class="row" align="center">
         <!-- ## ACÁ PORDREMOS UBICAR EL CONTENIDO CON CSS y BOOTSTRAP ## -->
         <br>
         <!-- /.login-logo -->
         <div class="login-box-body "  align="center">
            <h1 class="login-box-msg">MODIFICAR DIFERIDO</h1>
            <form action="index.php?c=diferido" method="post" class="container-fluid">
               <div class="form-group has-feedback">
                  <div><strong>ID de tramite : </strong>
                     <?php
                        if($row=$verDiferidos->fetch_array()){
                          $_SESSION['va'] = $row['DOCENTE'];
                        echo ' <input type="text" class="form-control" placeholder="IDTramite" id= "txtId" name="txtId" value="' . @$row['IDTRAMITE'] . '" readonly> ';
                        ?> 
                  </div>
               </div>
               <div class="form-group has-feedback">
                  <div><strong>Selecciona Carrera :</strong>
                     <?php
                        //FUNCIONAL 
                        echo"<select name='cbx_carrera' id='cbx_carrera' class='form-control'>";
                        //  echo " <option value=0>Selección:</option>";
                        while ($row2=$verDifCarre->fetch_array()) {
                        if ($row2["nombre_carrera"]==$row["CARRERA"]){
                        echo '<option value="'.$row2["ID_carrera"].'" SELECTED>'.utf8_decode($row2["nombre_carrera"]).'</option>';
                        }   
                        else   {
                        echo '<option value="'.$row2["ID_carrera"].'">'.utf8_decode($row2["nombre_carrera"]).'</option>';
                        }
                        }
                        echo  " </select>";  
                        ?>  
                     <span class=""></span>
                  </div>
               </div>
               <div>
                  <strong>Selecciona Materia :</strong>
                  <div class="form-group has-feedback">
                     <?php
                        $carrera = $row["CARRERA"];
                         $queryMat = "SELECT distinct m.id_materia AS IDMATERIA,m.nombre_materia AS MATERIAS, nombre_carrera FROM `materias` m join carrera_materia cm on m.ID_materia = cm.ID_materia join carreras c on c.ID_carrera = cm.ID_carrera WHERE c.nombre_carrera ='".$carrera."'";
                        
                        $resultado2 = mysqli_query($connect, $queryMat);
                        
                        echo "<select name='cbx_materia' id='cbx_materia' class='form-control'>";
                        while($rowMat=$resultado2->fetch_assoc())
                        {
                          if($rowMat['MATERIAS'] == $row['MATERIA'])
                          {
                            echo '<option value="'.$rowMat["IDMATERIA"].'" SELECTED>'.utf8_decode($rowMat["MATERIAS"]).'</option>';
                          }else{
                             echo '<option value="'.$rowMat["IDMATERIA"].'">'.utf8_decode($rowMat["MATERIAS"]).'</option>';
                          }
                        }
                        echo  " </select>"; 
                        ?>
                     <span class=""></span>
                  </div>
               </div>
               <div>
                  <strong>Selecciona Seccion :</strong>
                  <div class="form-group has-feedback">



                     <?php  
                        echo "<select name=listSeccion class='form-control'>";
                          
                          $connect=$GLOBALS['m_Conexion']->Conectar();
                          $query = "SELECT ID_seccion FROM secciones  WHERE seccion = ". $row['SECCION'];
                         

                           $resultado = mysqli_query($connect, $query);
                           echo 'alert('.$resultado.')';
                          echo '<option value="'.$resultado.'" SELECTED>'.utf8_decode($row["SECCION"]).'</option>';


                        while ($row3=$verDifSecci->fetch_array()) {
                        if ($row3["seccion"]==$row["SECCION"]){
                        echo '<option value="'.$row3["ID_seccion"].'" SELECTED>'.utf8_decode($row3["seccion"]).'</option>';
                        }   
                        else   {
                        echo '<option value="'.$row3["ID_seccion"].'">'.utf8_decode($row3["seccion"]).'</option>';
                             }
                        }
                        echo  " </select>";  
                        ?>
                     <span class=""></span>
                  </div>
               </div>


 


               <div>
                  <strong>Selecciona Evaluacion :</strong>
                  <div class="form-group has-feedback">
                     <?php  
                        echo "<select name=listEvaluacion class='form-control'>";

                         $connect=$GLOBALS['m_Conexion']->Conectar();
                         $query = "SELECT ID_eval FROM evaluaciones  WHERE descripcion = ". $row['EVALUACION'];
                         

                          $resultado = mysqli_query($connect, $query);
                          echo 'alert('.$resultado.')';
                          echo '<option value="'.$resultado.'" SELECTED>'.utf8_decode($row["EVALUACION"]).'</option>';


                        while ($row5=$verDifEva->fetch_array()) {
                        if ($row5["descripcion"]==$row["EVALUACION"]){
                        echo '<option value="'.$row5["ID_eval"].'" SELECTED>'.utf8_decode($row5["descripcion"]).'</option>';
                        }   
                        else   {
                        echo '<option value="'.$row5["ID_eval"].'">'.utf8_decode($row5["descripcion"]).'</option>';
                             }
                        }
                        echo  " </select>";  
                        
                        ?>
                     <span class=""></span>
                  </div>
               </div>
               <div>
                  <strong>Selecciona periodo :</strong>
                  <div class="form-group has-feedback">
                     <?php
                        echo  '<select name="listTipo" class="form-control">';
                        
                                       if ($row["PERIODO"]== 'Periodo Extraordinario'){
                                       echo '<option value="1" SELECTED>Periodo Extraordinario</option>';
                                       echo '<option value="2" >Periodo Ordinario</option>';
                                       }   
                                       else if ($row["PERIODO"]== 'Periodo Ordinario') {
                                        echo '<option value="1" SELECTED>Periodo Extraordinario</option>';
                                       echo '<option value="2" SELECTED>Periodo Ordinario</option>';
                                            }else{
                                             echo '<option value="0" SELECTED>Selecciona un Tipo:</option>';
                                  
                                    echo '<option value="1">Periodo Extraordinario</option>';  
                                    echo '<option value="2">Periodo Ordinario</option>'; 
                                            }
                                       
                                       echo  " </select>";  
                        
                                      ?>
                     <span class=""></span>
                  </div>
               </div>
               <div>
                  <strong>Selecciona estado :</strong>
                  <div class="form-group has-feedback" class="form-control">
                     <?php  
                        echo "<select name=listEstado class= 'form-control'>";
                              //echo "<option value='".$row['estado']."'>".$row['estado']."</option>"; 
                         //     echo "<option value='1'>1</option>";                    
                        
                                   if ($row["ESTADO"]== 'inactivo'){
                                   echo '<option value="0" SELECTED>inactivo</option>';
                                   echo '<option value="1" >activo</option>';
                                   }   
                                   else  {
                                   echo '<option value="0">inactivo</option>';
                                   echo '<option value="1" SELECTED>activo</option>';
                        }
                        
                        echo "</select>"
                        ?>
                     <span class=""></span>
                  </div>
               </div>


  
         

            <div>
                <strong>Selecciona Fecha Solicitud</strong>
                <div class="form-group has-feedback">
        <label class="col-lg-2 control-label">Fecha Solicitud</label>
       <input type="date" name="fechTramite" class="form-control" value="<?php echo date("Y-m-d", strtotime($row['FECHASOL']));  ?>" >
     
      <span class=""></span>
                  </div>
               </div>



               <div>
                  <strong>Selecciona Ciclo :</strong>
                  <div class="form-group has-feedback">

                    <select name=listCiclo class='form-control'>




                        <?php
   //$connect = mysqli_connect("localhost", "root", "", "bd_utec_test");
     
                         $connect=$GLOBALS['m_Conexion']->Conectar();
                         $query = "SELECT ID_ciclo FROM docentes  WHERE nombre_ciclo = ". $row['CICLO'];
                       

                         $resultado = mysqli_query($connect, $query);
                         echo 'alert('.$resultado.')';
                    ?>


                <option value="<?php echo $resultado;?>"><?php echo utf8_decode($row['CICLO'])?></option>
                     <?php  
                        
                     while ($row6=$verDifCic->fetch_array()) {
                        if ($row6["nombre_ciclo"]==$row["CICLO"]){
                        echo '<option value="'.$row6["ID_ciclo"].'" SELECTED>'.utf8_decode($row6["nombre_ciclo"]).'</option>';
                        }                                
                        else {
                             while($misdatosCic = $verDifCicIn->fetch_array()){ $contador++;
                             echo '<option value="'.$misdatosCic['ID_ciclo'].'" data-subtext="'. $misdatosCic['ID_ciclo'].'">'.utf8_decode($misdatosCic['nombre_ciclo']).'</option>';}
                          }
                        }
                              
                        ?>


                      </select>
                     <span class=""></span>
                  </div>
               </div>



             


           <!-- <div>
                  <strong>Selecciona Docente :</strong>
                  <div class="form-group has-feedback">
                   //  <?php  
                      //$carnet = null;
                      //  echo "<select name=listAlumno class='form-control'>";
                      //  while ($row7=$verDifDoc->fetch_array()) {
                        //if ($row7["docente_nombres"]==$row["DOCENTE"]){
                        //echo '<option value="'.$row8["carnet"].'" SELECTED>'.$row8["carAlum"].'</option>';
                          
                        // echo '<input type="text" name="listDocente" id="country" class="form-control" value ="'.$row7["carnet_docente"].'"/>'; 
                            //$carnet = 
                           //   echo '<div id="countryList"></div>';  
                      //  }   
                      
                      //  }
                       //?>
                     <span class=""></span>
                  </div>
               </div> -->


<div>
                 <strong>Selecciona Docente :</strong>
    <div class="form-group has-feedback">
           <select name="listDocente" id="listDocente" class="selectpicker form-control" data-show-subtext="true" data-size="5"   data-live-search="true" style="height: 100%; width: 100%" required>

               <option value="">Seleccione un docente</option>
               <?php
                  
                   $cnn=$GLOBALS['m_Conexion']->Conectar();
                   if (!$cnn->set_charset("utf8")) {//asignamos la codificación comprobando que no falle
                          die("Error cargando el conjunto de caracteres utf8");
                   }

                   $contador=0;
                   

                               
                   while($misdatos = $verDifDoc->fetch_array()){ $contador++;
                      if ($row['DOCENTE']==$misdatos['docente_nombres']){

                                       echo '<option value="'.$misdatos['carnet_docente'].'" SELECTED>'.utf8_decode($misdatos['docente_nombres']).'</option>';
                                       } else {
                                            while($misdatos10 = $verDifDocIn->fetch_array()){ $contador++;
                                          echo '<option value="'.$misdatos10['carnet_docente'].'" data-subtext="'. $misdatos10['carnet_docente'].'">'.utf8_decode($misdatos10['docente_nombres']).'</option>';}
                   }

                 }
               ?>
                  
 </select>  
   
       <span class=""></span>
                 </div>
              </div>




<div>
                  <strong>Selecciona Alumno :</strong>
     <div class="form-group has-feedback">
            <select name="listAlumno" id="listAlumno" class="selectpicker form-control" data-show-subtext="true" data-live-search="true" style="height: 100%; width: 100%" data-size="5" required>

                <?php
                   
                    $cnn=$GLOBALS['m_Conexion']->Conectar();
                    if (!$cnn->set_charset("utf8")) {//asignamos la codificación comprobando que no falle
                           die("Error cargando el conjunto de caracteres utf8");
                    }

                    $contador=0;
                    

                                
                    while($misdatos = $verDifAlu->fetch_array()){ $contador++;
                       if ($misdatos['alumno']==$row['ALUMNO']){

                                        echo '<option value="'.$misdatos['carnet'].'" SELECTED>'.utf8_decode($misdatos['alumno']).'</option>';
                                        } else {
                                          while($misdatosAlu = $verDifAluIn->fetch_array()){ 
                                            $contador++;
                                             echo '<option value="'.$misdatos['carnet'].'" data-subtext="'. $misdatosAlu['carnet'].'">'.utf8_decode($misdatosAlu['alumno']).'</option>';
                                             }
                                          }

                  }
                ?>
                   
  </select>  
    
        <span class=""></span>
                  </div>
</div>


   <?php
                     }//FINAL IF
                     ?>
          
        <!-- /.col -->
               <div class="row">
                  <div class="col-md-2 col-md-offset-5">
                     <button type="submit" name="ev" value="editar" class="btn btn-primary btn-block btn-flat">Modificar</button>
                  </div>
               </div>
               <!-- /.col -->
            </form>
            <a href="index.php?c=diferido" class="text-center">Regresar al listado
            </a>
            <!-- /.login-box-body -->
         </div>
         <!-- /.login-box -->
      </div>
   </div>
</div>
<link href="vista/AdminLTE/DateTimePicker/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">

<script src="jquery.js"></script>
<script type="text/javascript">
   $('.form_datetime').datetimepicker({
       language:  'es',
       weekStart: 1,
       todayBtn:  1,
   autoclose: 1,
   todayHighlight: 1,
   startView: 2,
   forceParse: 0,
       showMeridian: 1
   }); 
</script>
<script language="javascript">
   $(document).ready(function(){
     $("#cbx_carrera").change(function () {
       $("#cbx_carrera option:selected").each(function () {
         ID_carrera = $(this).val();
         $.post("includes/getMaterias.php", { ID_carrera: ID_carrera }, function(data){
           $("#cbx_materia").html(data);
         });            
       });
     })
   });
   
   $(document).ready(function(){
     $("#cbx_materia").change(function () {
       $("#cbx_materia option:selected").each(function () {
         id_materia = $(this).val();
                     
       });
     })
   });
</script>

<script>  
 $(document).ready(function(){  
      $('#country').keyup(function(){  
           var query = $(this).val();  
           if(query != '')  
           {  
                $.ajax({  
                     url:"includes/BuscarDocentes.php",  
                     method:"POST",  
                     data:{query:query},  
                     success:function(data)  
                     {  
                          $('#countryList').fadeIn();  
                          $('#countryList').html(data);  
                     }  
                });  
           }  
      });  
      $(document).on('click', 'li', function(){  
           $('#country').val($(this).text());  
           $('#countryList').fadeOut();  
      });  

 });  
 </script>  


<script>  
 $(document).ready(function(){  
      $('#alumno').keyup(function(){  
           var query = $(this).val();  
           if(query != '')  
           {  
                $.ajax({  
                     url:"includes/BuscarAlumno.php",  
                     method:"POST",  
                     data:{query:query},  
                     success:function(data)  
                     {  
                          $('#alumnoList').fadeIn();  
                          $('#alumnoList').html(data);  
                     }  
                });  
           }  
      });  
      $(document).on('click', 'dt', function(){  
           $('#alumno').val($(this).text());  
           $('#alumnoList').fadeOut();  
      });  

 });  
 </script>  


<link rel='stylesheet prefetch' href='vista/AdminLTE/bootstrap-select-1.13.2/dist/css/bootstrap-select.min.css'>
 
<script src="vista/AdminLTE/bootstrap-select-1.13.2/dist/js/bootstrap-select.min.js"></script>
<script src="vista/AdminLTE/bootstrap-select-1.13.2/js/bootstrap-select.js"></script>
