<script type="text/javascript" >
   function preventBack(){
     window.history.forward();
   }
   setTimeout("preventBack()", 0);
   window.onunload=function(){
     null};
</script>


<div class="container">
   <div class="container-fluid" width="80%">
      <div class="row" align="center">
         <!-- /.login-logo -->
         <div class="login-box-body "  align="center">
            <h1 class="login-box-msg">INSERTAR NUEVO DIFERIDO
            </h1>
            <form action="index.php?c=diferido" method="post" class="container-fluid">
               <!-- ***************************************Alumno***************************-->
               <div id="employee_table">
                  <div class="form-group has-feedback">
                     <div>
                        <strong>Ingrese carnet de Alumno : 
                        </strong>
                        <select id="listCarnet"  name="listCarnet" class="selectpicker1 form-control"  data-size="5" data-live-search="true"  >
                           <option value="">Seleccione un Alumno
                           </option>
                           <?php
                              $cnn=$GLOBALS['m_Conexion']->Conectar();
                              if (!$cnn->set_charset("utf8")) {//asignamos la codificación comprobando que no falle
                              die("Error cargando el conjunto de caracteres utf8");
                              }
                              $contador=0;
                              while($misdatos = $verDifAlu->fetch_array()){ $contador++;?>
                           <option value="<?php echo $misdatos["carnet"]; ?>" data-subtext="<?php echo $misdatos["carnet"]; ?>">
                              <?php echo utf8_decode($misdatos["alumno"]); ?>
                           </option>
                           <?php }?> 
                        </select>
                     </div>
                  </div>
                  <!-- ***************************************Alumno***************************-->
                  <!-- *************************************Docente***************************-->
                  <div class="form-group has-feedback">
                     <div>
                        <strong>Ingrese nombre de docente : 
                        </strong>
                        <select name="listDocente" class="selectpicker2 form-control" data-show-subtext="true" data-size="5"  data-live-search="true" style="height: 100%; width: 100%" required>
                           <option value="">Seleccione un Docente
                           </option>
                           <?php
                              $cnn=$GLOBALS['m_Conexion']->Conectar();
                              if (!$cnn->set_charset("utf8")) {//asignamos la codificación comprobando que no falle
                              die("Error cargando el conjunto de caracteres utf8");
                              }
                              $contador=0;
                              while($misdatos = $verDifDoc->fetch_array()){ $contador++;?>
                           <option value="<?php echo $misdatos["carnet_docente"]; ?>" data-subtext="<?php echo $misdatos["carnet_docente"]; ?>">
                              <?php echo utf8_decode($misdatos["docente_nombres"]); ?>
                           </option>
                           <?php }?> 
                        </select>
                        <!-- *************************************Docente***************************-->
                        <!-- ************************************Carrera***************************-->
                        <div>
                           <strong>Selecciona Carrera : 
                           </strong>
                           <select name="cbx_carrera" id="cbx_carrera" class="form-control">
                              <option value="0" selected>Selecciona una Carrera:
                              </option>
                           </select>
                        </div>
                        <!-- ************************************Carrera***************************-->
                        <!-- ************************************Materias***************************-->
                        <div>
                           <strong>Selecciona Materias : 
                           </strong> 
                           <select name="cbx_materia" id="cbx_materia" class="form-control">
                              <option value="0" selected>Selecciona una Materia:
                              </option>
                           </select>
                        </div>
                        <!-- ************************************Materias***************************-->
                        <!-- ***********************************Seccion***************************-->
                        <div class="form-group has-feedback">
                           <div>
                              <strong>Selecciona Seccion : 
                              </strong>
                              <select name="listSeccion" class="form-control" required>
                                 <div>
                                    Selecciona Seccion : 
                                    <option value="0">Selecciona una Seccion:
                                    </option>
                                    <?php        
                                       while ($row=$verDifSecci->fetch_array()) {
                                       echo '<option value="'.$row[ID_seccion].'">'.utf8_decode($row[seccion]).'</option>';           
                                       }
                                       ?>    
                                 </div>
                              </select>
                           </div>
                        </div>
                        <!-- ***********************************Seccion***************************-->
                        <!-- *********************************Evaluacion***************************-->
                        <div class="form-group has-feedback">
                           <div>
                              <strong>Selecciona Evaluacion : 
                              </strong>
                              <select name="listEvaluacion" class="form-control">
                                 <div>
                                    Selecciona Evaluacion : 
                                    <option value="0">Selecciona una Evaluacion:
                                    </option>
                                    <?php        
                                       while ($row=$verDifEva->fetch_array()) {
                                       echo '<option value="'.$row[ID_eval].'">'.utf8_decode($row[descripcion]).'</option>';           
                                       }
                                       ?>    
                                 </div>
                              </select>
                           </div>
                        </div>
                        <!-- *********************************Evaluacion***************************-->
                        <!-- *********************************Tipo***************************-->
                        <div class="form-group has-feedback">
                           <div>
                              <strong> Selecciona Tipo : 
                              </strong>
                              <select name="listTipo" class="form-control">
                                 <div>
                                    Selecciona Tipo : 
                                    <option value="0">Selecciona un Tipo:
                                    </option>
                                    <option value="1">Periodo Extraordinario
                                    </option>
                                    <option value="2">Periodo Ordinario
                                    </option>
                                 </div>
                              </select>
                           </div>
                        </div>
                        <!-- *********************************Tipo***************************-->
                        <!-- *********************************Fecha***************************-->
                        <div class="form-group has-feedback">
                           <strong>Selecciona Fecha de Solicitud:
                           </strong>
                           <input name = "fechTramite" type="date"  class="form-control"  id="fechTramite" value="<?php  date_default_timezone_set('America/El_Salvador'); echo date("Y-m-d");?>" required/>
                           <span class="">
                           </span>
                        </div>
                        <!-- *********************************Fecha***************************-->
                        <!-- *********************************Ciclo***************************--> 
                        <div class="form-group has-feedback">
                           <div>
                              <strong> Selecciona un Ciclo : 
                              </strong>
                              <select name="listCiclo" class="form-control">
                                 <div>
                                    Selecciona un Ciclo : 
                                    <option value="0">Selecciona un Ciclo:
                                    </option>
                                 <?php
while ($row = $verDifCic->fetch_array())
{
    echo '<option value="' . $row[ID_ciclo] . '">' . $row[nombre_ciclo] . '</option>';
}
?>   
                                 </div>
                              </select>
                           </div>
                        </div>
                        <!-- *********************************Ciclo***************************-->
                     </div>
                  </div>
               </div>
               <!-- </form> -->
               <!-- /.col -->
               <div class="row">
                  <div class="col-md-2 col-md-offset-5">
                     <button type="submit" name="ev" value="insertar" class="btn btn-primary btn-block btn-flat">Registrar
                     </button>
                  </div>
               </div>
               <!-- /.col -->
         </div>
         </form>
         <a href="index.php?c=diferido" class="text-center">Regresar al listado
         </a>
      </div>
      <!-- /.login-box-body -->
   </div>
   <!-- /.login-box -->
</div>
<script src="jquery.js"></script>
<!--Script para combo Box anidados-->
<script>
    $(document).ready(function() {
        $("#listCarnet").change(function() {
            $('#cbx_materia').find('option').remove().end().append('<option value="whatever"></option>').val('whatever');
            $("#listCarnet option:selected").each(function() {
                carnet = $(this).val();
                $.post("includes/getMunicipio.php", {
                    carnet: carnet
                }, function(data) {
                    $("#cbx_carrera").html(data);
                });
            });
        })
    });
    $(document).ready(function() {
        $("#cbx_carrera").change(function() {
            $("#cbx_carrera option:selected").each(function() {
                ID_carrera = $(this).val();
                $.post("includes/getLocalidad.php", {
                    ID_carrera: ID_carrera
                }, function(data) {
                    $("#cbx_materia").html(data);
                });
            });
        })
    });
</script>
<!--Script para combo Box anidados-->   


<!-- *************************************AGREGARALUMNO**********************-->
<div id="add_data_Modal" class="modal fade">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;
            </button>
            <h4 class="modal-title">
               <strong>Agregar Alumno
               </strong>
            </h4>
         </div>
         <div class="modal-body">
            <form method="post" id="insert_form">
               <label>Carnet Alumno
               </label>
               <input required type="text" name="carnetAlu" id="carnetAlu" class="form-control" minlength="10" maxlength="10" onkeypress="return valida(event)"/>
               <br />
               <label>Nombre del Alumno
               </label>
               <input required type="text" name="nombreAlu" id="nombreAlu" class="form-control"/>
               <br />
               <label>Correo del Alumno
               </label>
               <input required type="text" name="CorAlu" id="CorAlu" class="form-control"/>
               <br />
               <input required type="submit" name="insert" id="insert" value="Insertar" class="btn btn-success" />
            </form>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar
            </button>
         </div>
      </div>
   </div>
</div>

<!-- *************************************AGREGARALUMNO**********************-->
<script>  
$(document).ready(function() {
    $('#insert_form').on("submit", function(event) {
        event.preventDefault();
        if ($('#carnetAlu').val() == "") {
            alert("Carnet es requerido");
        } else if ($('#nombreAlu').val() == '') {
            alert("Nombre es requerido");
        } else {
            $.ajax({
                url: "includes/insert.php",
                method: "POST",
                data: $('#insert_form').serialize(),
                success: function(data) {
                    $('#insert_form')[0].reset();
                    $('#add_data_Modal').modal('hide');
                }
            });
        }
    });
    $(document).on('click', '.view_data', function() {
        //$('#dataModal').modal();
        var employee_id = $(this).attr("id");
        $.ajax({
            url: "select.php",
            method: "POST",
            data: {
                employee_id: employee_id
            },
            success: function(data) {
                $('#employee_detail').html(data);
                $('#dataModal').modal('show');
            }
        });
    });
});
</script>

<script type="text/javascript">
document.getElementById("insert_form").onsubmit = function() {
    location.reload(true);
}
</script>






<!-- ***********************************AGREGARDOCENTE**********************-->
<div id="add_data_Modal2" class="modal fade">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;
            </button>
            <h4 class="modal-title">
               <strong>Agregar Docente
               </strong>
            </h4>
         </div>
         <div class="modal-body">
            <form method="post" id="insert_form2">
               <label>Codigo Docente
               </label>
               <input required type="text" name="carnetDoc" id="carnetDoc" class="form-control" maxlength="10" minlength="1" onkeypress="return valida(event)"/>
               <br />
               <label>Nombre del Docente
               </label>
               <input required type="text" name="nombreDoc" id="nombreDoc" class="form-control"/>
               <br />
               <label>Correo del Docente
               </label>
               <input required type="text" name="CorDoc" id="CorDoc" class="form-control"/>
               <br />
               <input type="submit" name="insert" id="insert" vDoce="Insertar" class="btn btn-success" />
            </form>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar
            </button>
         </div>
      </div>
   </div>
</div>
<!-- ***********************************AGREGARDOCENTE**********************-->
<script>  
$(document).ready(function() {
    $('#insert_form2').on("submit", function(event) {
        event.preventDefault();
        if ($('#carnetDoc').val() == "") {
            alert("Carnet es requerido");
        } else if ($('#nombreDoc').val() == '') {
            alert("Nombre es requerido");
        } else {
            $.ajax({
                url: "includes/insert2.php",
                method: "POST",
                data: $('#insert_form2').serialize(),
                success: function(data) {
                    $('#insert_form2')[0].reset();
                    $('#add_data_Modal2').modal('hide');
                }
            });
        }
    });
    $(document).on('click', '.view_data', function() {
        //$('#dataModal').modal();
        var employee_id = $(this).attr("id");
        $.ajax({
            url: "select.php",
            method: "POST",
            data: {
                employee_id: employee_id
            },
            success: function(data) {
                $('#employee_detail').html(data);
                $('#dataModal').modal('show');
            }
        });
    });
});
</script>

<script type="text/javascript">
document.getElementById("insert_form2").onsubmit = function() {
    location.reload(true);
}
</script>



<!--LIBRERIAS SELECT DE DOCENTES -->
<link rel='stylesheet prefetch' href='vista/AdminLTE/bootstrap-select-1.13.2/dist/css/bootstrap-select.min.css'>
<script src="vista/AdminLTE/bootstrap-select-1.13.2/dist/js/bootstrap-select.min.js"></script>
<script src="vista/AdminLTE/bootstrap-select-1.13.2/js/bootstrap-select.js"></script> 



<script type="text/javascript">
$('.selectpicker1').selectpicker({
    noneResultsText: '<strong>No existe el alumno {0}, Agregar un Alumno </strong><button type="button" name="add" id="add" data-toggle="modal" data-target="#add_data_Modal" class="btn btn-warning">Agregar</button>',
    liveSearchPlaceholder: 'Carnet o Nombre',
});
</script>




<script type="text/javascript">
$('.selectpicker2').selectpicker({
    noneResultsText: '<strong>No existe el Docente {0}, Agregar un Docente </strong><button type="button" name="add" id="add" data-toggle="modal" data-target="#add_data_Modal2" class="btn btn-warning">Agregar</button>',
    liveSearchPlaceholder: 'Codigo o Nombre',
});
</script>