<div class="container-fluid">
	<!-- Small boxes (Stat box) -->
	<form action="index.php?c=user" method="post">
	<div>
		<center><b><h1>¿Que opciones de menú tendrá el nuevo usuario?</h1></b> </center>
		<br>
	</div>
      <div class="row">
        
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3>Report<sup style="font-size: 20px"></sup></h3>

              <p>Solo Reportes</p>
            </div>
            <div class="icon">
              <i class="fa fa-edit"></i>
            </div>
            
            <button type="submit"name="ev" value="finsertAuto3" class="small-box-footer">&nbsp Menu reportes  &nbsp &nbsp <i class="fa fa-arrow-circle-right"></i></button>

          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3>Asistente</h3>

              <p>Recepcion</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>

            <button type="submit"name="ev" value="finsertAuto2" class="small-box-footer">&nbsp Menu Asistente  &nbsp &nbsp <i class="fa fa-arrow-circle-right"></i></button>

          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3>Admin</h3>

              <p>SysAdmin</p>
            </div>
            <div class="icon">
              <i class="fa fa-cogs"></i>
            </div>
            <!--
            <a href="#" class="small-box-footer">
              Menu administrador <i class="fa fa-arrow-circle-right"></i>
            </a>
        -->
            <button type="submit"name="ev" value="finsertAuto1" class="small-box-footer">&nbsp Menu administrador  &nbsp &nbsp <i class="fa fa-arrow-circle-right"></i></button>

          </div>
        </div>
        <!-- ./col -->

        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>Nuevo</h3>

              <p>Personalizar</p>
            </div>
            <div class="icon">
              <i class="fa fa-spinner fa-spin"></i>
            </div>

            <button type="submit"name="ev" value="finsert2" class="small-box-footer">&nbsp Crear nuevo menu &nbsp &nbsp<i class="fa fa-arrow-circle-right"></i></button>

          </div>


        </div>
        <!-- ./col -->

      </div>
      <!-- /.row -->
 </form>

</div>