<div class="container">
   <div class="container-fluid" width="80%">
      <div class="row" align="center">
         <!-- ## ACÁ PORDREMOS UBICAR EL CONTENIDO CON CSS y BOOTSTRAP ## -->
         <br>
         <!-- /.login-logo -->
         <div class="login-box-body "  align="center">
            <h1 class="login-box-msg">MODIFICAR ESCUELA</h1>
            <form action="index.php?c=escuela" method="post" class="container-fluid">
              


               <div>
                  <strong>Codigo de la Facultad:</strong>
               <div class="form-group has-feedback">
                  <?php
                     if($row=$verEscuela->fetch_array()){
                     echo ' <input type="text" class="form-control" placeholder="Codigo" name="txtIdEscuela" value="' . @$row['ID_escuela'] . '" readonly> ';
                     ?> 
                  <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
               </div>


               <div>
                  <strong>Nombre de la escuela:</strong>
               <div class="form-group has-feedback">
                  <?php  
                     echo ' <input type="text" class="form-control" value="' . utf8_decode($row['nombre_escuela']) .'" placeholder="Escuela" name="txtEscuela"> ';
                     ?>
                  <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
               </div>
             </div>
</div>
               <div>
                  <strong>Nombre de la Facultad:</strong>
               <div class="form-group has-feedback">
                  <?php
                     echo"<select name=listFacultad class='form-control'>";
                     //  echo " <option value=0>Selección:</option>";
                     
                      while ($row2=$verFacultades->fetch_array()) {
                      if ($row["ID_facultad"]==$row2["id_facultad"]){
                        
                        echo '<option value="'.$row["ID_facultad"].'" SELECTED>'.utf8_decode($row2["nombre_facultad"]).'</option>';
                      }   
                     
                     else   {              
                     
                         echo '<option value="'.$row2["id_facultad"].'">'.utf8_decode($row2["nombre_facultad"]).'</option>';
                      
                       
                          }
                                 
                     }
                     
                     echo  " </select>";  
                     ?>     
                  <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
               </div>
             </div>


               <div>
                  <strong>Estado:</strong>
               <div class="form-group has-feedback">
                  <?php  
                     echo "<select name=listEstado class='form-control'>";
           
                      if ($row["estado"]== 0){
                     echo '<option value="0" SELECTED>Inactivo</option>';
                     echo '<option value="1" >Activo</option>';
                     }   
                     else  {
                     echo '<option value="0">Inactivo</option>';
                     echo '<option value="1" SELECTED>Activo</option>';
     }

                     
                     echo "</select>";
                     
                     }//cierro el if
                     
                     
                     
                     
                     ?>
                  <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
               </div>
               </div>
           <!-- /.col -->
       <div class="row">
                  <div class="col-md-2 col-md-offset-5">
                     <button type="submit" name="ev" value="editar" class="btn btn-primary btn-block btn-flat">Modificar</button>
                  </div>
               </div>
               <!-- /.col -->
               </div>
            </form>
            <a href="index.php?c=escuela&ev=listar" class="text-center">Regresar al listado</a>
         </div>
         <!-- /.login-box-body -->
      </div>
      <!-- /.login-box -->
   </div>
