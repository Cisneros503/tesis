 <?php
//$connect = mysqli_connect("localhost", "root", "", "bd_u");
$connect=$GLOBALS['m_Conexion']->Conectar();
  $query = "SELECT * FROM carrera_materia cm left join carreras c on cm.ID_carrera = c.ID_carrera group by cm.ID_carrera";
  $resultado = mysqli_query($connect, $query);


?>


<?php
if (isset($_SESSION['vsMsj'])) {

      //echo mysqli_errno($_SESSION['vsMsj']);
      echo ' 
      <div class="container-fluid">
        <div class="alert alert-success alert-dismissible">
          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
          <strong>Aviso!</strong> '. @$_SESSION['vsMsj'] .'.
        </div>
      </div>' ;
      $_SESSION['vsMsj'] = null;
    }

?>




<div class="container">
<div class="container-fluid" width="80%">
<div class="row" align="left">
<!-- ## ACÁ PORDREMOS UBICAR EL CONTENIDO CON CSS y BOOTSTRAP ## -->

<br>

<!-- /.login-logo -->
<form action="index.php?c=procesos" method="post" >
  <div class="login-box-body row">
    <div><h1 class="login-box-msg">DETALLE DE CORRECCION  </h1>
      <center>
        <h3>
      <?php 
      $verAlumno=$GLOBALS['m_Procesos']->mostrarUno($tr['carnet']);
      if ($r = $verAlumno->fetch_assoc()) {
        echo "<u>". utf8_decode($r['alumno'])."</u>";

      } ?>
      </h3>
      </center> </div>

    <div class=" col-md-6"  align="center">   
     

      <style type="text/css">
        table {
          border-collapse: separate;
          border-spacing:  1px 11px;         
        }
      </style>
<input hidden="true" value="<?php echo $tr['ID_tramite'];?>" type="number" name="idTramite">
    <table width="80%" cellpadding="4" align="center">

      <tr>
        <div class="form-group has-feedback">
        <div>
          <td><label>Carrera</label></td> 
          <td>
            <!--<select required="true" name="cbx_carrera" id="cbx_carrera" class="form-control">-->
          <?php while($row = $resultado->fetch_assoc()) { 
            if ($row['ID_carrera']==$tr['ID_carrera']){
          ?>
                <input readonly="true" type="text" class="form-control" 
                value="<?php echo utf8_decode($row['nombre_carrera']) ?>
                " >               
          <?php    
              }              
            }    
          ?>
      </td>
        </div>
        </div>
      </tr>
      
      <tr>
        <div class="form-group has-feedback">
        <div> 
          <td align="left"><label>Materia</label> </td>
          <td>

              <?php 
              $x=0;
                while ($rowMaterias = $verMaterias->fetch_assoc()) {
                    if ($tr['ID_materia']==$rowMaterias['ID_materia'] AND $x==0) {  
              ?> <input readonly="true" type="text" class="form-control" 
                value="<?php echo $rowMaterias['ID_materia'] ?>
                " >  

              <?php                                               
                    $x=1;}
                  }
              ?>
              </select>
          </td></div>
        </div>
      </tr>
    
      
    <tr>
      <div class="form-group has-feedback">
        <td align="left"><label>Sección</label></td>
        <td>
          
          <?php $seccion=$GLOBALS['m_Correcciones']->verSeccion($tr['ID_seccion']);     
                while ($row=$verSecciones->fetch_array()) { 
                  if ($tr['ID_seccion']==$row['ID_seccion']) {
                    $seccion=$GLOBALS['m_Correcciones']->verSeccion($tr['ID_seccion']);

                    echo '<input readonly value="'.$seccion.'" class="form-control" '.$seccion.'>'; 
                  }
                }

          ?>      
        </select>
      </td>
      </div>
    </tr>
    <tr>
      <div class="form-group has-feedback">
        <td align="left"><label>Evaluación</label></td>
        <td>
          
          <?php $nEvaluacion=$GLOBALS['m_Correcciones']->verEvaluacion($tr['ID_eval']);?>
         <input readonly="true" type="text" class="form-control" 
                value="<?php echo utf8_decode($nEvaluacion); ?>
                " >     
              
      </td>
      </div>
    </tr>
    <tr>
      <div class="form-group has-feedback">
        <td align="left"><label>Carnet Docente</label></td>
        <?php $nDocente=$GLOBALS['m_Correcciones']->verDocente($tr['carnet_docente']); ?>

        <td>
          <input readonly="true" type="text" class="form-control" 
                value="<?php echo utf8_decode($nDocente); ?>
                " >
        </td>
      </div>

    </tr>

    
    <tr>
      <div class="form-group has-feedback">
        <td align="left"><label>Ciclo</label></td>
        <td>

          <?php $nCiclo=$GLOBALS['m_Correcciones']->verCiclo($tr['ID_ciclo']); ?>

         <input readonly="true" type="text" class="form-control" 
                value="<?php echo $nCiclo; ?>
                " >
        </td>
      </div>
    </tr>
    <tr>
      <div class="form-group">
        <td align="left"><label>Recepción en la Escuela</label></td>
        <td><div class="form-group has-feedback">
          <div class="form-group has-feedback">
            
          </div>
          <input readonly="true" value="<?php echo substr($tr['fechaRecibe'], 0, 10) ; ?>" type="date" name="fRec" class="form-control pull-right" >
        </div></td>
        <!-- /.input group -->
      </div>
    </tr>
    <tr>
    <div class="form-group has-feedback">
            <td align="left"><label>Periodo</label></td>
            <td>
              <input readonly="true" type="text" class="form-control" 
                value="<?php 
                  if ($tr['periodo'] == 1) {
                    echo "Periodo con Autorización";
                  }else if ($tr['periodo'] == 0){
                    echo "Periodo Extraordinario";
                  }
                 ?>
                " >
            </td>
      </div>
    </tr>
    <tr>
      <div class="form-group">
        <td align="left"><label>Recepción en Administración</label></td>

        <td><div class="form-group has-feedback">
          <div class="form-group has-feedback">
             <i></i>
          </div>
          <input readonly="true" value="<?php echo substr($tr['fechaSolicitud'], 0, 10) ; ?>" type="date" name="fSolicitud" class="form-control pull-right" >
        </div></td>
        <!-- /.input group -->
      </div>
    </tr>
    
       


      </table>
      <br><br>

    

    <br>
    <br>

</div>

<div class="col-md-6"  align="center"> 


    <table width="80%" cellpadding="4" align="center">
      <tr >
        <td><label>Nota actual</label></td>
        <td><div class="form-group has-feedback">
          <input readonly="true" value="<?php echo $tr['notaActual'];?>" type="number" min="0" max="10" step="0.1" minlength="1" maxlength="4" class="form-control" placeholder="Nota actual" name="txtNactual">
          <span class="glyphicon form-control-feedback"></span>
        </div></td>
      </tr>

      <tr >
        <td><label>Nota corregida</label></td>
        <td><div class="form-group has-feedback">
          <input readonly="true" min="0" max="10" value="<?php echo $tr['notaCorregida'];?>" type="number" step="0.01" minlength="1" maxlength="4" class="form-control" placeholder="Nota corregida" name="txtNcorregida" >
          <span class="glyphicon form-control-feedback"></span>
        </div></td>
      </tr>

      <tr >
        <td><label>Motivo Corrección</label></td>
        <td><div class="form-group has-feedback">
          <input readonly="true" value="<?php echo $tr['motivoCorreccion'];?>" type="text" class="form-control" placeholder="Motivo Corrección" name="txtMotCorrec">
          <span class="glyphicon form-control-feedback"></span>
        </div></td>
      </tr>

      <tr>
    <div class="form-group has-feedback">
            <td align="left"><label>Estado Corrección</label></td>
            <td>
              <input readonly="true" type="text" class="form-control" 
                value="<?php echo $tr['estadoCorreccion']; ?>
                " >
            </td>
      </div>

      <tr >
        <td><label>Carné del alumno</label></td>
        <td><div class="form-group has-feedback">
          <input readonly="true" value="<?php echo $tr['carnet'];?>" type="text" class="form-control" placeholder="Carné" name="txtCarne" onkeypress="return valida(event)">
          <span class="glyphicon  form-control-feedback"></span>
        </div></td>
      </tr>

      <tr >
        <td><label>Observaciones</label></td>
        <td><div class="form-group has-feedback">
          <input readonly="true"  value="<?php echo $tr['observaciones'];?>" type="text" class="form-control" placeholder="Observaciones" name="txtObcerv">
          <span class="glyphicon  form-control-feedback"></span>
        </div></td>
      </tr>

      <tr >
        <td><label>Usuario que atendió</label></td>
        <td><div class="form-group has-feedback">
          <input readonly="true" type="text" class="form-control"
          <?php $prec = $tr['personaRecibe'];
          $prec = str_replace( " ", "", $tr['personaRecibe']) ?> 
          value="<?php echo $prec ?>
          " name="txtUsrRecibe">
          <span class="glyphicon  form-control-feedback"></span>
        </div></td>
      </tr>

      


      <tr>
        <div class="form-group">
          
          <!-- /.input group -->
        </div>
      </tr>
    
    

      </table>
      <br>
      <div class="col-md-4"></div>
      <div class="col-md-8" >
          
          
        </div>

    

    <br>

</div>

<div class="col-md-12 row">   
    <div class="col-md-12" align="center"><a href="index.php?c=procesos&ev=inicioCorrecciones">Ir a Correcciones</a></a></div>
  </div>
</form>
<!--<div class="col-md-6"></div>-->

</div>


<div class="col-md=4"><br></div>


</div>
</div>
</div>


<!-- ############################################################################## -->
<!-- ############################################################################## -->




<script type="text/javascript" src="./jquery/jquery-1.8.3.min.js" charset="UTF-8"></script>
<script type="text/javascript" src="./bootstrap/js/bootstrap.min.js"></script>

  

<!-- ############################################################################## -->
<!-- ############################################################################## -->


<!-- jQuery 3 -->
<script src="../../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="../../plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' /* optional */
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    });
  });
</script>
<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })

    })

    })
  })
</script>

<script type="text/javascript">
  var password = document.getElementById("psw")
  , confirm_password = document.getElementById("psw2");

function validatePassword(){
  if(password.value != confirm_password.value) {
    confirm_password.setCustomValidity("Contraseñas no coinciden");
  } else {
    confirm_password.setCustomValidity('');
  }
}

password.onchange = validatePassword;
confirm_password.onkeyup = validatePassword;

if(history.forward(1)){
history.replace(history.forward(1));
}
</script>

<script src="jquery.js"></script>


<script language="javascript">
      $(document).ready(function(){
        $("#cbx_carrera").change(function () {

          
          $("#cbx_carrera option:selected").each(function () {
            ID_carrera = $(this).val();
            $.post("includes/getMaterias.php", { ID_carrera: ID_carrera }, function(data){
              $("#cbx_materia").html(data);
            });            
          });
        })
      });
      
      $(document).ready(function(){
        $("#cbx_materia").change(function () {
          $("#cbx_materia option:selected").each(function () {
            id_materia = $(this).val();
                        
          });
        })
      });
</script>   














