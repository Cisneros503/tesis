<div class="container">
   <div class="container-fluid" width="80%">
      <div class="row" align="center">
         <!-- ## ACÁ PORDREMOS UBICAR EL CONTENIDO CON CSS y BOOTSTRAP ## -->
         <br>
         <!-- /.login-logo -->
         <div class="login-box-body "  align="center">
            <h1 class="login-box-msg">MODIFICAR ASIGNACION</h1>
            <form action="index.php?c=asigna" method="post" class="container-fluid">

              <div>
                  <strong>Codigo de asignacion:</strong>
               <div class="form-group has-feedback">
                  <?php
                     if($row=$verAsignacion->fetch_array()){
                     echo ' <input type="text" class="form-control" placeholder="Codigo" name="txtId" value="' . @$row['ID_carrera_materia'] . '" readonly> ';
                     ?> 
                  <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
               </div>

             </div>


             <div>
                  <strong>Nombre de la carrera:</strong>
               <div class="form-group has-feedback">
                  <?php
                     //FUNCIONAL 
                                  echo"<select name=listCarrera class='form-control'>";
                                 //  echo " <option value=0>Selección:</option>";
                             
                                   while ($row2=$verCarreras->fetch_array()) {
                                   if ($row["ID_carrera"]==$row2["ID_carrera"]){
                                     
                                     echo '<option value="'.$row["ID_carrera"].'" SELECTED>'.utf8_decode($row2["nombre_carrera"]).'</option>';
                                   }   
                     
                                  else   {
                     
                                   echo '<option value="'.$row2["ID_carrera"].'">'.utf8_decode($row2["nombre_carrera"]).'</option>';
                                       }
                                              
                               }
                     
                               echo  " </select>";  
                               ?>  
                  <span class=""></span>
               </div>
             </div>


             <div>
                  <strong>Nombre de la Catedra:</strong>
               <div class="form-group has-feedback" >
                  <?php
                     //FUNCIONAL 
                                  echo"<select name=listCatedra class='form-control'>";
                                 //  echo " <option value=0>Selección:</option>";
                             
                                   while ($row2=$verCatedras->fetch_array()) {
                                   if ($row["ID_catedra"]==$row2["ID_catedra"]){
                                     
                                     echo '<option value="'.$row["ID_catedra"].'" SELECTED>'.utf8_decode($row2["nombre_catedra"]).'</option>';
                                   }   
                     
                                  else   {
                     
                                   echo '<option value="'.utf8_decode($row2["ID_catedra"]).'">'.utf8_decode($row2["nombre_catedra"]).'</option>';
                                       }
                                              
                               }
                     
                               echo  " </select>";  
                               ?>  
                  <span class=""></span>
               </div>
             </div>

             <div>
                  <strong>Nombre de la Materia:</strong>
               <div class="form-group has-feedback">
                  <?php
                     //FUNCIONAL 
                                  echo"<select name=listMateria class='form-control'>";
                                 //  echo " <option value=0>Selección:</option>";
                             
                                   while ($row2=$verMaterias->fetch_array()) {
                                   if ($row["ID_materia"]==$row2["ID_materia"]){
                                     
                                     echo '<option value="'.$row["ID_materia"].'" SELECTED>'.utf8_decode($row2["nombre_materia"]).'</option>';
                                   }   
                     
                                  else   {
                     
                                   echo '<option value="'.$row2["ID_materia"].'">'.utf8_decode($row2["nombre_materia"]).'</option>';
                                       }
                                              
                               }
                     
                               echo  " </select>";  
                               ?>  
                  <span class=""></span>
               </div>
             </div>

             <div>
                  <strong>Estado:</strong>
               <div class="form-group has-feedback">
                  <?php  
                     echo "<select name=listEstado class='form-control'>";
                           //echo "<option value='".$row['estado']."'>".$row['estado']."</option>"; 
                      //     echo "<option value='1'>1</option>";                    
                    /* $datos = array("Inactivo","Activo", "Selecciona una Opcion");
                     for($i=0; $i<count($datos); $i++) {
                      if($row['estado']==$i) {
                            echo "<option value='".$row['estado']."'>".$datos[$i]."</option>";
                         }
                             else {
                            echo "<option value='".$i."'>".$datos[$i]."</option>";
                       }
                     }*/

                       if ($row["estado"]== 0){
                     echo '<option value="0" SELECTED>Inactivo</option>';
                     echo '<option value="1" >Activo</option>';
                     }   
                     else  {
                     echo '<option value="0">Inactivo</option>';
                     echo '<option value="1" SELECTED>Activo</option>';
              }

                     
                     echo "</select>";
                     
                     }//cierro el if
                     
                     
                     
                     
                     ?>
                   </div>
                  <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
               </div>
        
       <div class="row">
                  <div class="col-md-2 col-md-offset-5">
                     <button type="submit" name="ev" value="editar" class="btn btn-primary btn-block btn-flat">Modificar</button>
                  </div>
               </div>
               <!-- /.col -->
               </div>
            </form>
            <a href="index.php?c=asigna&ev=listar" class="text-center">Regresar al listado</a>
         </div>
         <!-- /.login-box-body -->
      </div>
      <!-- /.login-box -->
   </div>
