<div class="container">
   <div class="container-fluid" width="80%">
      <div class="row" align="center">
         <!-- ## ACÁ PORDREMOS UBICAR EL CONTENIDO CON CSS y BOOTSTRAP ## -->
         <br>
         <!-- /.login-logo -->
         <div class="login-box-body "  align="center">
            <h1 class="login-box-msg">MODIFICAR DOCENTE</h1>
            <form action="index.php?c=docente" method="post" class="container-fluid">
               <div>
                  <strong>Codigo del Docente:</strong>
                  <div class="form-group has-feedback">
                     <?php
                        if($row=$verDocentes->fetch_array()){
                        echo ' <input type="text" class="form-control" placeholder="Carné" name="txtCarne" value="' . @$row['carnet_docente'] . '" readonly> ';
                        ?> 
                     <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                  </div>
               </div>
               <div>
                  <strong>Nombre del Docente:</strong>
                  <div class="form-group has-feedback">
                     <?php  
                        echo ' <input type="text" class="form-control" value="' . $row['docente_nombres'] .'" placeholder="Nombres" name="txtNombre"> ';
                        ?>
                     <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                  </div>
               </div>
               <div>
                  <strong>Estado:</strong>
                  <div class="form-group has-feedback">
                     <?php  
                        echo "<select name=listEstado class='form-control'>";
                              //echo "<option value='".$row['estado']."'>".$row['estado']."</option>"; 
                         //     echo "<option value='1'>1</option>";                    
                        //$datos = array("Inactivo","Activo", "Selecciona una Opcion");
                        
                         if($row['estado']==1) {
                               echo "<option value='".$row['estado']."' selected> Activo </option>";
                               echo "<option value='0'>Inactivo</option>";
                        
                            }
                                else {
                               echo "<option value='".$row['estado']."' selected>Inactivo</option>";
                               echo "<option value='1'>Activo</option>";
                          }
                        
                        
                        echo "</select>"
                        ?>
                     <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                  </div>
               </div>
               <div>
                  <strong>Correo del Docente:</strong>
                  <div class="form-group has-feedback">
                     <?php
                        echo '<input type="text" class="form-control" placeholder="Email" name="txtEmail" value="' . $row['correo_docente'] . '">' ;

                        }//cierro el if
                        ?>
                  </div>
                  <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
               </div>
               <!-- /.col -->
               <div class="row">
                  <div class="col-md-2 col-md-offset-5">
                     <button type="submit" name="ev" value="editar" class="btn btn-primary btn-block btn-flat">Modificar</button>
                  </div>
               </div>
               <!-- /.col -->
         </div>
         </form>
         <a href="index.php?c=docente&ev=listar" class="text-center">Regresar al listado</a>
      </div>
      <!-- /.login-box-body -->
      <!-- /.login-box -->
   </div>
</div>