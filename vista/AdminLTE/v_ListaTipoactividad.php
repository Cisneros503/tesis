

<br>
<!-- TABLE: LATEST ORDERS -->
    <div class="container">
          <div class="box box-info">
            <div class="box-header with-border">
              <center><b><h3 class="">Tipos de Actividades Registradas</h3></b></center>
               

              
                 <?php   
                  if ($_SESSION['vsValidacion'] == 1){
                echo '<form action="index.php?c=tipoactividad" method="post"> 
                 <center> <button name="ev" type="submit" value="insertar">Ingresar Seccion</button> </center>
                </form>';
            }
            ?>  

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget=""><i class="fa fa-times"></i></button>
              </div>
            </div>


            
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive table-striped">
                <table class="table no-margin">
                  <thead>
                  <tr>
                    <th>ID</th>
                    <th>Nombre de Actividad</th>             
                        <th>Estado</th>

                     <?php   
                 if ($_SESSION['vsValidacion'] == 1){
                        echo "<th>Opciones</th>";
                      }
                    ?>


                  </tr>
                  </thead>
                  <tbody>
                  <?php
                    $activos=0;
                    $inactivos=0;
                    while($rows=$verTipoactividad->fetch_array())
                    {
                      echo "<tr>";
                      echo ' <form action="index.php?c=tipoactividad" method="post"> ';

                      echo "  <td>".$rows['ID_tipo_actividad']."</td>";

                      echo ' <input type="hidden" name="id" value="' . $rows['ID_tipo_actividad'] . ' "> ';

                      echo "  <td>".utf8_decode($rows['nombreActividad'])."</td>";
                      
                     

                      if ($rows['estado'] == 1) {
                        echo "<td><span class='label label-success'>Activo</span></td>";
                        $activos=$activos+1;
                      }else{
                        echo "<td><span class='label label-danger'>Inactivo</span></td>";
                        $inactivos=$inactivos+1;
                      }

                      //echo "  <td>".."</td>";

                     if ($_SESSION['vsValidacion'] == 1) {
                        
                        echo '  <td class="icon">'.' <button  name="ev" type="submit" value="formEditar"><span class="glyphicon glyphicon-pencil text-primary"
></button>&nbsp;&nbsp;&nbsp;&nbsp;<button name="ev" type="submit" value="eliminar"><span class="glyphicon glyphicon-remove text-danger"
></button> '."</td>";


                      }
                      echo "</form>";
                      echo "</tr>"; 

                    }
                  ?>                 
                  </tbody>
                </table>
              </div>
              <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
          
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->

<div class="">
  <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-green"><i class="ion ion-ios-people-outline"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">Activos</span>
          <span class="info-box-number">
          <?php echo $activos;?> 
          <small> Tipos de Actividades</small>
          </span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>

    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-red"><i class="ion ion-ios-people-outline"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">Inactivos</span>
          <span class="info-box-number">
          <?php echo $inactivos;?> 
          <small> Tipos de Actividades</small>
          </span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
</div>


        

        </div>