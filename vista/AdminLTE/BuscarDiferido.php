<?php
$connect = mysqli_connect("localhost", "root", "", "bd_utec");
$output = '';
if(isset($_POST["query"]))
{
  $search = mysqli_real_escape_string($connect, $_POST["query"]);
  $query = "SELECT t.ID_tramite, c.nombre_carrera, a.alumno, m.nombre_materia, e.nombre_escuela, s.seccion, dc.docente_nombres,ev.descripcion,t.periodo,t.fechaSolicitud
,t.autorizado
FROM tramites t  
left join diferidos d on t.id_tramite = d.ID_tramite 
left join carrera_materia cm on t.ID_carrera_materia = cm.ID_carrera_materia
left join alumnos a on a.carnet = d.carnet
left join materias m on m.ID_materia = cm.ID_materia 
left join carreras c on c.ID_carrera = cm.ID_carrera
left join catedras ct on ct.ID_catedra = cm.ID_catedra
left join escuela e on e.ID_escuela = ct.ID_escuela
left join secciones s on s.ID_seccion = t.ID_seccion
left join docentes dc on dc.carnet_docente = t.carnet_docente
left join evaluaciones ev on ev.ID_eval = t.ID_eval
left join tipotramite tt on tt.ID_tipo_tramite = t.ID_tipo_tramite
WHERE tt.ID_tipo_tramite = 3 and a.carnet LIKE '%".$search."%'
  ";
}
else
{
  $query = "SELECT t.ID_tramite, c.nombre_carrera, a.alumno, m.nombre_materia, e.nombre_escuela, s.seccion, dc.docente_nombres,ev.descripcion,t.periodo,t.fechaSolicitud
,t.autorizado
FROM tramites t  
left join diferidos d on t.id_tramite = d.ID_tramite 
left join carrera_materia cm on t.ID_carrera_materia = cm.ID_carrera_materia
left join alumnos a on a.carnet = d.carnet
left join materias m on m.ID_materia = cm.ID_materia 
left join carreras c on c.ID_carrera = cm.ID_carrera
left join catedras ct on ct.ID_catedra = cm.ID_catedra
left join escuela e on e.ID_escuela = ct.ID_escuela
left join secciones s on s.ID_seccion = t.ID_seccion
left join docentes dc on dc.carnet_docente = t.carnet_docente
left join evaluaciones ev on ev.ID_eval = t.ID_eval
left join tipotramite tt on tt.ID_tipo_tramite = t.ID_tipo_tramite
WHERE tt.ID_tipo_tramite = 3";
}

$result = mysqli_query($connect, $query);
if(mysqli_num_rows($result) > 0)
{
  $output .= '<div class="table-responsive">
          <table class="table table bordered">
            <tr>
              <th>Tramite</th>
              <th>Carrera</th>
              <th>Alumno</th>
              <th>Materia</th>
              <th>Escuela</th>
              <th>Seccion</th>
              <th>Docente</th>
              <th>Evaluacion</th>
              <th>Periodo</th>
              <th>Fecha</th>
              <th>Estado</th>


            </tr>';
  while($row = mysqli_fetch_array($result))
  {
    $activos=0;
    $inactivos=0;

    $output .= '
      <tr>
        <td>'.$row["ID_tramite"].'</td>
        <td>'.$row["nombre_carrera"].'</td>
        <td>'.$row["alumno"].'</td>
        <td>'.$row["nombre_materia"].'</td>
        <td>'.$row["nombre_escuela"].'</td>
        <td>'.$row["seccion"].'</td>
        <td>'.$row["docente_nombres"].'</td>
        <td>'.$row["descripcion"].'</td>
        <td>'.$row["periodo"].'</td>
        <td>'.$row["fechaSolicitud"].'</td>'.
        (($row['autorizado'] == 1) ? '<td><span class="label label-success">Autorizados</span></td>' : '<td><span class="label label-danger">No Autorizados</span></td>').'</tr>';
  }
  echo $output;
}
else
{
  echo 'No encontrado';
}
?>