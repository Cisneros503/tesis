<?php 
session_start();
//ruta general a la vista ustedes definen dependiendo de la ruta del controlador 
$r = "vista/AdminLTE/"; 
$ppal= " ";
require_once 'controlador/c_Archivos.php';

if(isset($_GET['c'])){
//*********VALIDO PARA NO REINSERTAR AL RECARGAR LA PAGINA*********	
/*	if (@$_REQUEST['ev'] == @$_SESSION['ev']) {
		@$_REQUEST['ev'] = 0;
	}else{
		@$_SESSION['ev'] = @$_REQUEST['ev'];
		$_SESSION['vsMsj'] = "Acción no realizada intentelo de nuevo...";
	}
*/
//*****************************************************************

	//OBJETOS QUE SE USARÁN EN TODO EL CODIGO
	$c=$_GET['c'];
	$m_Conexion = new conexion();
//	$m_Tramites2 = new m_Tramites2();
	$c_Usr = new Usuario();

	
	
	//para iniciar sesion datos vivenen POST
	if ($c == "login") {
		$c_Usr->login();
	}

	/*Si ya está logueado lo dejaremos interactuar con la info, 
	de lo contrario... Vaya al login antes de anaizar la url que busca.*/
	if(isset($_SESSION['vsPerfil']) and $_SESSION['vsNombre'])
	{	
		$c_Usr->menu();//LLena el menú antes de definir el contenido de la pagina
		//$c_Usr->menu2();//llena los datos para verificar las url
		$c_Usr->validarUrl();//valida la URL antes llamar vistas.

		//Mostrar la vista Base: Controlador/Evento
		switch ($c) {
		//CONTROLADORES			
			case 'docente':
				require_once 'modelo/m_Docente.php';
				require_once 'controlador/c_Docente.php';
				$c_Docente = new c_Docente();
				$m_Docente = new m_Docente();
				$c_Docente->swDocente();
				break;

			case 'alumno':
				require_once 'modelo/m_Alumnos.php';
				require_once 'controlador/c_Alumnos.php';
				$c_Alumnos = new c_Alumnos();
				$m_Alumnos = new m_Alumnos();
				$c_Alumnos->swAlumno();
				break;
						
			case 'carrera':
					require_once 'modelo/m_Carrera.php';
					require_once 'controlador/c_Carrera.php';
					$c_Carrera = new c_Carrera();
					$m_Carrera = new m_Carrera();
					$c_Carrera->swCarrera();
					break;

			case 'escuela':
					require_once 'modelo/m_Escuela.php';
					require_once 'controlador/c_Escuela.php';
					$c_Escuela = new c_Escuela();
					$m_Escuela = new m_Escuela();
					$c_Escuela->swEscuela();
				break;

			case 'materia':
					require_once 'modelo/m_Materia.php';
					require_once 'controlador/c_Materia.php';
					$c_Materia= new c_Materia();
					$m_Materia = new m_Materia();
					$c_Materia->swMateria();
				break;

			case 'asigna':
					require_once 'modelo/m_Asigna.php';
					require_once 'controlador/c_Asigna.php';
					$c_Asigna= new c_Asigna();
					$m_Asigna = new m_Asigna();
					$c_Asigna->swAsigna();
				break;

			case 'ciclo':
				require_once 'modelo/m_Ciclos.php';
				require_once 'controlador/c_Ciclos.php';
				$c_Ciclos = new c_Ciclos();
				$m_Ciclos = new m_Ciclos();
				$c_Ciclos->swCiclo();
				break;	


			case 'evaluacion':
				require_once 'modelo/m_Evaluaciones.php';
				require_once 'controlador/c_Evaluaciones.php';
				$c_Evaluacion = new c_Evaluacion();
				$m_Evaluacion = new m_Evaluacion();
				$c_Evaluacion->swEvaluacion();
				break;	

			case 'seccion':
				require_once 'modelo/m_Secciones.php';
				require_once 'controlador/c_Secciones.php';
				$c_Seccion = new c_Seccion();
				$m_Seccion = new m_Seccion();
				$c_Seccion->swSeccion();
				break;		

			case 'tipoactividad':
				require_once 'modelo/m_Tipoactividad.php';
				require_once 'controlador/c_Tipoactividad.php';
				$c_Tipoactividad = new c_Tipoactividad();
				$m_Tipoactividad = new m_Tipoactividad();
				$c_Tipoactividad->swTipoactividad();
				break;	

			case 'catedra':
				require_once 'modelo/m_Catedra.php';
				require_once 'controlador/c_Catedra.php';
				$c_Catedra = new c_Catedra();
				$m_Catedra = new m_Catedra();
				$c_Catedra->swCatedra();
				break;

			case 'facultad':
				require_once 'modelo/m_Facultad.php';
				require_once 'controlador/c_Facultad.php';
				$c_Facultad = new c_Facultad();
				$m_Facultad = new m_Facultad();
				$c_Facultad->swFacultad();
				break;


			case 'user':				
				require_once 'modelo/m_Usuario.php';				
				$m_Usuario = new Usr();
				$c_Usr->swUsuario();
				break;
				
			case 'error':
				$c_Usr->error();
				break;

			case 'procesos':
				require_once 'modelo/m_Procesos.php';
				require_once 'modelo/m_Actividades.php';
				require_once 'modelo/m_Correcciones.php';
				require_once 'controlador/c_Procesos.php';
				$c_Procesos = new c_Procesos();
				$m_Procesos = new m_Procesos();
				$m_Actividades = new m_Actividades();
				$m_Correcciones = new m_Correcciones();
				$c_Procesos->swProcesos();
				break;

			case 'reportesCor':
			
				require_once 'modelo/m_ReportesCor.php';
				require_once 'controlador/c_ReportesCor.php';
				$c_ReportesCor = new c_ReportesCor();
				$m_ReportesCor = new m_ReportesCor();
				$c_ReportesCor->swReporteCor();
				break;	

			case 'reporteDif2':
			
				require_once 'modelo/m_ReportesDifMat.php';
				require_once 'controlador/c_ReportesDifMat.php';
				$c_ReportesDifMat = new c_ReportesDifMat();
				$m_ReportesDifMat = new m_ReportesDifMat();
				$c_ReportesDifMat->swReporteDifMat();
				break;		
				
			case 'reportesActMat':
			
				require_once 'modelo/m_ReportesActMat.php';
				require_once 'controlador/c_ReportesActMat.php';
				$c_ReportesActMat = new c_ReportesActMat();
				$m_ReportesActMat = new m_ReportesActMat();
				$c_ReportesActMat->swReporteActMat();
				break;	

			case 'actividad2':
				require_once 'modelo/m_Actividades.php';
				require_once 'controlador/c_Actividades2.php';
				$c_Actividades2 = new c_Actividades2();
				$m_Actividades = new m_Actividades();
				$c_Actividades2->swActividades();
				break;

			case 'diferido':
				require_once 'modelo/m_Diferidos.php';
				require_once 'controlador/c_Diferidos.php';
				$c_Diferidos = new c_Diferidos();
				$m_Diferidos = new m_Diferidos();
				$c_Diferidos->swDiferido();
				break;

			

			case 'reporteDif':
				require_once 'modelo/m_ReportesDif.php';
				require_once 'controlador/c_ReportesDif.php';
				$c_ReportesDif = new c_ReportesDif();
				$m_ReportesDif = new m_ReportesDif();
				$c_ReportesDif->swReporteDif();
				break;	
					
			case 'reportesAct':
				require_once 'modelo/m_ReportesAct.php';
				require_once 'controlador/c_ReportesAct.php';
				$c_ReportesAct = new c_ReportesAct();
				$m_ReportesAct = new m_ReportesAct();
				$c_ReportesAct->swReporteAct();
				break;
				
			case 'reportesCorMat':
				require_once 'modelo/m_ReportesCorMat.php';
				require_once 'controlador/c_ReportesCorMat.php';
				$c_ReportesCorMat = new c_ReportesCorMat();
				$m_ReportesCorMat = new m_ReportesCorMat();
				$c_ReportesCorMat->swReporteCorMat();
				break;

			default:
				require_once 'modelo/m_Procesos.php';
				require_once 'modelo/m_Actividades.php';
				require_once 'modelo/m_Correcciones.php';
				require_once 'controlador/c_Procesos.php';
				$c_Procesos = new c_Procesos();
				$m_Procesos = new m_Procesos();
				$m_Actividades = new m_Actividades();
				$m_Correcciones = new m_Correcciones();
				$c_Procesos->inicio();
				break;	


		}



			
	}
	else{
			header('location: vista/AdminLTE/pages/examples/login.php');
	}


}else{
	header('location: vista/AdminLTE/pages/examples/login.php');
 }




			
?>




